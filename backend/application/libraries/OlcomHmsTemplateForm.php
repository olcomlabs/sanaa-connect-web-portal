<?php
if (!defined('BASEPATH'))
	exit('No direct access allowed');

/*
 *
 * OlcomTemplateForm
 * will generate dynamic form
 * sections
 * 	-form part (labels and fields)
 * 	-form specific scripts
 */

class OlcomHmsTemplateForm {

	var $title;
	var $header_info;
	//var $action;
	var $fields;
	var $div_id;
	var $div_class;
	//var $method;
	var $with_submit_reset;
    var $inflated_view;
	var $num_titles = 0;
    var $group_titles = array();
    var $group_title_index = 0;
    var $is_multipart = FALSE;
	/*
	 *
	 * constructor
	 *
	 */
	
	function __construct($preferences)
	{
		$this -> title = $preferences['title'];
		$this -> header_info = isset( $preferences['header_info'] ) ? $preferences[ 'header_info' ] : NULL;
		$this -> action = "";
		$this -> div_id =  isset( $preferences['id'] ) ? $preferences[ 'id' ] : 'olcomhms-template-form';
		$this -> div_class = isset( $preferences['class'] ) ? $preferences[ 'class' ] : 'form-horizontal';//$preferences['class'];
		$this -> fields = array('fields'  => array());
		$this -> method = "";
		$this -> with_submit_reset = isset( $preferences['with_submit_reset'] ) ? TRUE : FALSE;
        $this -> group_title_index = 0 ;
        if( isset($preferences['is_multipart']) AND $preferences['is_multipart']  == TRUE ){
        	$this->is_multipart = TRUE;
        }

        
	}
	
	/*
	 * function create_form
	 * returns form view corresponding preferences
	 */
	 function create_form()
	 {
	 	//get CI instance 
	 	$CI = &get_instance();
		return $CI -> load -> view('template_form',array('template'  => array('form_info'  => array(
						'action'  => $this -> action,'id'  => $this -> div_id,
						'class'  => $this -> div_class,
						'is_multipart'  => $this->is_multipart,
						'method'  => $this -> method
						),
						'with_submit_reset'  => $this -> with_submit_reset,
						'fields'  => $this -> fields['fields'],
						'title'  => $this -> title,
						'header_info'  => $this -> header_info,
						'inflated_view' => $this -> inflated_view,
						'num_titles' => $this -> num_titles,
						'group_titles' => $this -> group_titles
						)),TRUE);
	 }
	 /*
	  * 
	  * function add_field
	  * params-type,options
	  */
	  function add_field($type,$options)
	  {
	    $this -> group_title_index ++;
	  	$this -> fields['fields'][] = array('type'  => $type,'options'  => $options);
	  }
      /*
       * 
       * 
       */
       function add_group_title($title)
       {
           $this -> num_titles ++;
           $this -> group_titles[ $this -> group_title_index - 1 ] = $title;
           
       } 
      /*
       * 
       * 
       * 
       */
      function set_class( $class )
      {
          $this -> div_class = $class;
      }
      /*
       * 
       * 
       */
       function inflate_view( $view )
       {
            $this -> inflated_view = $view;   
       }
	   /*
	    * 
	    * 
	    */
	    function get_field( $field_name ){
	    	foreach( $this -> fields[ 'fields' ] as $index => $field  ){
	    		if( $field[ 'options' ][ 'name' ] == $field_name ){
	    			return array( 'f_index' => $index, 'field' => $this -> fields[ 'fields' ][ $index ]);
	    		}
	    	}
			return NULL;
	    }
      /*
	   * 
	   * 
	   */
	   function set_field_options( $field_name,$options ){
	   	
		$field = $this -> get_field( $field_name);
		if( $field != NULL ){
			$this -> fields[ 'fields' ][ $field[ 'f_index' ]][ 'options' ] = $options;
			return SUCCESS_CODE;
		}
		return -1; 
	   }
	  /*
	   * 
	   * 
	   */
	   function set_fields_values( $values ) {
	   		
			$errors = 0;
			foreach( $values  as $field_name => $value ){
				 $field = $this -> get_field($field_name);
				 if( $field != NULL ){
				 	if( $field[ 'field' ][ 'type' ] == 'select' ){
				 		$field[ 'field' ][ 'options' ][ 'selected' ] = $value;
					 }else{
					 	$field[ 'field' ][ 'options' ][ 'value' ] = $value;	
					 }
					 if( $this -> set_field_options($field_name, $field[ 'field' ][ 'options' ] ) == -1 ){
					 	$errors ++;
					 }	
				 }
			}
			if( $errors != 0 ){
				return -1;//finished with errors
			}
			return SUCCESS_CODE;
			
	   }
	   /*
	    * 
	    * 
	    */
	    function set_form_property( $property, $value ){
	    	switch ($property) {
				case 'with_submit_reset':
					$this -> with_submit_reset = $value;
					break;
				case 'title':
						$this -> title = $value;
					break;
				default:
					
					break;
			}
	    }
		/*
		 * 
		 * 
		 */
		 function remove_field( $field_name ){
		 	$field = $this -> get_field( $field_name );
			if( $field != NULL ){
				unset( $this -> fields[ 'fields' ][ $field[ 'f_index' ] ] );
				return SUCCESS_CODE;
			}
			return  -1;
		 }
		 /*
		  *
		  * 
		  */
		  
		  function set_field_value( $field_name,$value ){
		  			$field = $this -> get_field($field_name);
					if( $field != NULL ){
						if( $field[ 'field' ][ 'type' ] == 'select' ){
							$this -> fields[ 'fields' ][ $field[ 'f_index' ] ][ 'choices' ] = $value;
						}else{
							$this -> fields[ 'fields' ][ $field[ 'f_index' ] ][ 'value' ] = $value;
						}
					}
		  }
		  /*
		   * 
		   * 
		   */
}
