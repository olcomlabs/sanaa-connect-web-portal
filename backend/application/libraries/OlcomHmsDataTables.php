<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 
 * 
 * The implementation of jquery datatables server side script
 * @site https://datatables.net/release-datatables/examples/server_side/server_side.html
 */
 
class OlcomHmsDataTables{
	//parameters
	var  $columns;
	var $index_column;
	var $table_name;
	var $CI;//ci instance
	var $controller_fx;//controller function uri
	var $id_type   =  array('employee'  => FALSE,'patient'  =>FALSE);
	var $tds_style_format   =  NULL; 
	function OlcomHmsDataTables($config){
			//get ci instance;
			$this -> CI   = & get_instance();
			$this -> columns  = $config['columns'];
			$this -> index_column  = $config['index_column'];
			$this -> table_name  = $config['table_name'];
			$this -> controller_fx  = $config['controller_fx'];
			
             
			if(isset($config['tds_style_format'])){
				$this -> tds_style_format  = $config['tds_style_format'];
			}
			if(isset($config['id_type']))
			switch($config['id_type']){
				case 'employee':
					$this -> id_type['employee']  = TRUE;
				break;
				
				case 'patient':
					$this -> id_type['patient']  = TRUE;
				break;
			}
	}
	
	/*
	 * get_data fx
	 * parameters-columns,
	 */
	 
	 public function get_data(){
	 	
			/* 
			 * Paging
			 */
			$display_start  = $this -> CI -> input -> get('iDisplayStart');
			$display_length  = $this -> CI -> input -> get('iDisplayLength');
			/*$fp  = fopen('/var/www/log3.txt',"w+");
			fwrite($fp,'Pagin displayStart  = '.$display_start."   Paging displaylength  = ".$display_length);
			fclose($fp);
			*/
			if ( isset($display_start ) &&  $display_length !=  '-1')
			{
				$this -> CI -> db -> limit(intval( $display_length),
							intval($display_start)		
					);
			}
		
			/*
			 * Ordering
			 */
			 
			$sort_column  = $this -> CI -> input -> get('isSortCol_0');
			if ( isset($sort_column) )
			{
				
				for ( $i  = 0 ; $i<intval( $this -> CI -> input -> get('iSortingCols') ) ; $i++ )
				{
					if ( $this -> CI -> input -> get( 'bSortable_'.intval($this -> CI -> input -> get('iSortCol_'.$i)) )   ==  "true" )
					{
						$this -> CI -> db -> order_by($this -> columns[ intval( $this -> CI -> input -> get('iSortCol_'.$i) ) ],
							($this -> CI -> input -> get('sSortDir_'.$i)  === 'asc' ? 'desc' : 'asc') );
					}
				}
			}
			/* 
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			 
	 		$sWhere  = FALSE;//see uses below
	 		$search  = $this -> CI -> input -> get('sSearch');
			if ( isset($search) && $search  !=  "" )
			{
				
				for ( $i  = 0 ; $i<count($this -> columns) ; $i++ )
				{
					if($this -> columns[$i]  == $this -> index_column){
						$db_id  = olcomhms_db_id($this -> CI -> input -> get('sSearch'));
						if($db_id !== null){
							$this -> CI -> db -> or_where($this -> columns[$i],$db_id);
							$this -> CI -> db -> or_like($this -> columns[$i],$db_id);
						}else{
							$this -> CI -> db -> or_where($this -> columns[$i],$this -> CI -> input -> get('sSearch'));
							$this -> CI -> db -> or_like($this -> columns[$i],$this -> CI -> input -> get('sSearch'));
						}
							
						//$this -> CI -> db -> or_like($this -> columns[$i],olcomhms_db_id($this -> CI -> input -> get('sSearch')));
					}else{
						$this -> CI -> db -> or_like($this -> columns[$i],$this -> CI -> input -> get('sSearch'));
						//$this -> CI -> db -> or_like($this -> columns[$i],$this -> CI -> input -> get('sSearch'));//prevents the addition of 'AND LIKE'
					}
				}
				$sWhere  = TRUE;
			}
			
		
			/*
			 * Query data
			 */
			$this -> CI -> db -> select(implode(',', $this -> columns));
			$rResult   =  $this -> CI -> db -> get($this -> table_name);
			/*$fp  = fopen('/var/www/log.txt', "w+");
			fwrite($fp, $this -> CI -> db -> last_query().'\n DisplayStart  = '.$display_start."  DisplayLength  = ".$display_length);
			
			fclose($fp);
			*/
			//return json_encode(array('last_query'  => $this -> CI -> db -> last_query()));
			//Total rows by filter
			$iFilteredTotal   =  $rResult -> num_rows();
			/* Total data set length */
			$iTotal   =  $this -> CI -> db -> count_all($this -> table_name);
			
			
			/*
			 * Output
			 */
			$output   =  array(
				"sEcho"   =>  intval($this -> CI -> input -> get('sEcho')),
				"iTotalRecords"   =>  $iTotal,
				"iTotalDisplayRecords"   =>  $iFilteredTotal,
				"aaData"   =>  array()
			);
			//result as array
			$result_array  = $rResult -> result_array();
			foreach( $result_array as $aRow )
				{
					$row   =  array();
					for ( $i  = 0 ; $i<count($this -> columns) ; $i++ )
					{
						/*
						 * 
						 * last column for actions 
						 */
						 if($i  == count($this -> columns)-1){
						 	$data  = array(
								'view'  => site_url($this -> controller_fx.'/view/'.$aRow[$this -> index_column]),								'edit'  => site_url($this -> controller_fx.'/edit/'.$aRow[$this -> index_column]),
								'delete'  => site_url($this -> controller_fx.'/delete/'.$aRow[$this -> index_column])
							);
						 	$row[]  = $this -> CI -> load -> view('actions_view',array('actions_data'  => $data),TRUE);
						 }
						 else if($i  == 0){
						 		$row[]  = olcomhms_create_id($aRow[$this -> columns[$i]],$this -> id_type['employee'],$this -> id_type['patient']);
						 }else{
						     
						 	if($this -> tds_style_format != null)
						 	{
						 		if( isset( $this -> tds_style_format[ $this -> columns[ $i ] ] ) )
						 		{
						 			$row[]  = $this -> CI -> load -> view('data_style_format',
									array(
									'format' => array(
										'format'  => $this -> tds_style_format[ $this -> columns[ $i ] ],
										'data' => $aRow[ $this -> columns[ $i ] ] )
									),TRUE);
						 		
                                }
                                else {
                                    $row[] = $aRow[ $this -> columns[ $i ] ];    
                                }
                                
						 	}
						 	else
						 	{
						 			$row[]  = $aRow[$this -> columns[$i]];
						 	}
						 		
						 }
						
					}
					$output['aaData'][]   =  $row;
				}
				
				return    json_encode($output);	
		}
}
