<?php if(!defined('BASEPATH'))exit('Direct accces is restricted');

/*
 * Template datatable for loading dynamic datatables 
 * 
 * 
 */
 
 class OlcomHmsTemplateDatatable{
 	
	var $table_header;//table header
	var $columns = array();//table columns
	var $controller_fx;//the controller fx processing ajax request from datatable (server-side)
	var $with_actions = FALSE;
	var $ci ;
    var $create_controller_fx;
    var $id_suffix = NULL;
    var $extra_params = NULL;
	function OlcomHmsTemplateDatatable($settings){
		$this -> table_header = $settings['header'];
		$this -> columns = $settings['columns'];
		$this -> controller_fx = $settings['controller_fx'];
		$this -> with_actions = $settings['with_actions'];
        $this -> create_controller_fx = isset( $settings[ 'create_controller_fx' ]) ? $settings[ 'create_controller_fx' ] :  NULL;
        $this -> ci = & get_instance();
       
        if( isset( $settings[ 'id_suffix' ] ) )
          {
                $this -> id_suffix = $settings[ 'id_suffix' ];
          }
        
        if( isset( $settings[ 'extra_params' ] ) AND $settings[ 'extra_params' ]  != NULL )
        {
            $this -> extra_params = json_encode( $settings[ 'extra_params' ] );
        }
	}
	
	/*
	 * this should return the table and table specific scripts
	 * 
	 */
	 function create_view(){
	 
	 	$datatable = "";
		$scripts = "";
	 	//load the datatable view
	 	$datatable = $this -> ci -> load -> view('template_datatable',array('info_view' => array(
			'header' => $this -> table_header,
			'columns' => $this -> columns,
			'with_actions' => $this -> with_actions,
			'id_suffix' => $this -> id_suffix,
			'create_controller_fx' => $this -> create_controller_fx
		)),TRUE);
		
		//load specific scripts
		$scripts = $this -> ci -> load -> view('template_datatable_specific_scripts',array(
			'info_view' => array(
				'controller_fx' => $this -> controller_fx,
				'with_actions' => $this -> with_actions,
				'id_suffix' => $this -> id_suffix,
				'extra_params' => $this -> extra_params
			)
		),TRUE);
		return array(
		'datatable' => $datatable,
		'specific_scripts' => $scripts
		);
	 }
	/*
     * 
     * 
     * 
     */
     function create_inflate_view(){
        
        $datatable = "";
        $scripts = "";
        //load the datatable view
        $datatable = $this -> ci -> load -> view('template_inflated_dtt_view',array('info_view' => array(
            'header' => $this -> table_header,
            'columns' => $this -> columns,
            'with_actions' => $this -> with_actions
        )),TRUE);
        
        //load specific scripts
        $scripts = $this -> ci -> load -> view('template_inflated_dtt_view_specific_scripts',array(
            'info_view' => array(
                'controller_fx' => $this -> controller_fx,
                'with_actions' => $this -> with_actions,
                'create_controller_fx' => $this -> create_controller_fx
            )
        ),TRUE);
        return array(
        'datatable' => $datatable,
        'specific_scripts' => $scripts
        );
     }
 }
