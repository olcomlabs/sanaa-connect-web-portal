<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 
 * 
 * The implementation of jquery datatables server side script
 * @site https://datatables.net/release-datatables/examples/server_side/server_side.html
 */
 
class OlcomHmsDataTablesMultiDBTables{
	//parameters
	var $columns;
	var $index_column;
	var $db_tables;
	var $CI;//ci instance
	var $controller_fx;//controller function uri
	var $id_type = array('employee' => FALSE,'patient' => FALSE);
	var $dtt_columns = array();
	var $dtt_hidden_cols = array();
	var $with_actions = FALSE;
	var $where;
	var $data_style_format = NULL;
	var $order = NULL;
	var $group_by = NULL;
    var $cell_ops = NULL;
	function OlcomHmsDataTablesMultiDBTables($config){
			//get ci instance;
			$this -> CI  = & get_instance();
			$this -> index_column = $config['index_column'];
			$this -> controller_fx = $config['controller_fx'];
			$this -> dtt_hidden_cols = $config['hidden_cols'];
			$this -> with_actions = $config['with_actions'] == TRUE ? TRUE : FALSE;
			$this -> where = $config['where'];
            
                
			
            $this -> group_by = isset( $config[ 'group_by' ] ) ? $config[ 'group_by' ] : NULL;
			if(isset($config['order'])){
				$this -> order = $config['order'];
			}
			
			//data style formating
			if(isset( $config['data_style_format'] ) ){
				$this -> data_style_format = $config['data_style_format'];
			}
			//create  table_name.column_name 
			$tables = $config['tables'];
			foreach($tables as $table =>$columns)
			{
				$this -> db_tables[] = $table;
				
                if( is_array( $columns ) )
                {
                    foreach($columns as $column)
                    {
                    
                        $this -> columns[] = $table.'.'.$column;
                        $this -> dtt_columns[] = $column;    
                    }    
                }
                else {
                    $this -> columns[] = $columns ;
                    $this -> dtt_columns[] = 'patient';
                }
					
			}
			
            
            if( isset( $config[ 'cell_ops' ] ) )
            {
                $this -> cell_ops = $config[ 'cell_ops' ];
                if( $this -> cell_ops != NULL )
                    {
                        foreach( $this -> cell_ops as $as => $op )
                        {
                            $this -> columns[] = '('.$op.') AS '.$as;
                            $this -> dtt_columns[] = $as;
                        }
                    }
            }
			switch($config['id_type']){
				case 'employee':
					$this -> id_type['employee'] = TRUE;
				break;
				
				case 'patient':
					$this -> id_type['patient'] = TRUE;
				break;
                case 'appointment':
                    $this -> id_type[ 'appointment' ] = TRUE;
                break;
                case 'prescription' :
                    $this -> id_type[ 'prescription' ] = TRUE;
                 break;
                case 'diagnosis' :
                     $this ->id_type[ 'diagnosis' ] = TRUE;
                 break;
			}
	}
	
	/*
	 * get_data fx
	 * parameters-columns,
	 */
	 
	 public function get_data( $return_array = FALSE){
	 	
			/* 
			 * Paging
			 */
			$display_start = $this -> CI -> input -> get('iDisplayStart');
			$display_length = $this -> CI -> input -> get('iDisplayLength');
	
			
			if ( isset($display_start ) &&  $display_length!=  '-1')
			{
				$this -> CI -> db -> limit(intval( $display_length),
							intval($display_start)		
					);
			}
		
			/*
			 * Ordering
			 */
			 
			$sort_column = $this -> CI -> input -> get('isSortCol_0');
            
			if ( isset( $sort_column ) )
			{
			    
               
            	
				for ( $i = 0 ; $i < intval( $this -> CI -> input -> get('iSortingCols') ) ; $i++ )
				{
					if ( $this -> CI -> input -> get( 'bSortable_'.intval($this -> CI -> input -> get('iSortCol_'.$i)) )  ==  "true" )
					{
						$this -> CI -> db -> order_by( $this -> columns[ intval( $this -> CI -> input -> get('iSortCol_'.$i) ) ],
							( $this -> CI -> input -> get('sSortDir_'.$i) === 'asc' ? 'desc' : 'asc') );
					}
				}
			}
            
			/* 
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			 
	 		$sWhere = FALSE;//see uses below
	 		$search = $this -> CI -> input -> get('sSearch');
			if ( isset($search) && $search !=  "" )
			{
			
				
				for ( $i = 0 ; $i < count($this -> columns) ; $i++ )
				{
					/*
					 *An expensive process for many columns , large number of users at the same time  and many records  
					 *Runs matching of data for one column to another not painful if the match will just happen in the first column
					 */
					$this -> CI -> db -> where($this -> where,NULL,FALSE);
					$this -> CI -> db -> select(implode(',', $this -> columns) , FALSE);
                     
					$this -> CI -> db -> from($this -> db_tables);
					
					//strip formatted id to db id format
					if($this -> dtt_columns[$i] === $this -> index_column){
						$db_id = olcomhms_db_id($this -> CI -> input -> get('sSearch'));
						
						if($db_id!= null){
							$this -> CI -> db -> like($this -> columns[$i],$db_id);
						}else
							$this -> CI -> db -> like($this -> columns[$i],$this -> CI -> input -> get('sSearch'));//false alarm
						/*$fp = fopen('/var/www/check.txt','w+');
						fwrite($fp,$this -> dtt_columns[$i]."  ".$this -> columns[$i]."  ".$db_id.'  '.$i);
						fclose($fp);*/
					}	
					else {
							 
							$this -> CI -> db -> like($this -> columns[$i],$this -> CI -> input -> get('sSearch'));
					}
					
                    if( $this -> group_by != NULL )
                        $this -> CI -> db -> group_by( $this -> group_by );
                    
                    
					$rResult  =  $this -> CI -> db -> get();
					/*$fp = fopen('/var/www/last_query3.txt','w+');
					fwrite($fp,$this -> CI -> db -> last_query().'  i = '.$i);
					fclose($fp);*/
					
					if($rResult!= null && $rResult -> num_rows()!= 0){
						break;
					}else{
						;
					}
				}
				
				$sWhere = TRUE;
			}else{
					/*
					 * Query data
					 */
					$this -> CI -> db -> where($this -> where,NULL,FALSE); 
					$this -> CI -> db -> select(implode(',', $this -> columns) , FALSE);
					$this -> CI -> db -> from($this -> db_tables);
                    
                    if( $this -> group_by != NULL )
                        $this -> CI -> db -> group_by( $this -> group_by );
					$rResult  =  $this -> CI -> db -> get();
					/*$fp = fopen('/var/www/last_query3.txt','w+');
					fwrite($fp,$this -> CI -> db -> last_query());
					fclose($fp);*/
						
			}
			#echo $this -> CI -> db -> last_query();
			//Total rows by filter
			$iFilteredTotal  =  $rResult -> num_rows();
			/* Total data set length */
			$iTotal  =  $this -> CI -> db -> count_all($this -> db_tables[0]);
			
			
			/*
			 * Output
			 */
			$output  =  array(
				"sEcho"  => intval($this -> CI -> input -> get('sEcho')),
				"iTotalRecords"  => $iTotal,
				"iTotalDisplayRecords"  => $iFilteredTotal,
				"aaData"  => array()
			);
			
			//print_r($this -> dtt_columns);
			//result as array
			$result_array = $rResult -> result_array();
			foreach( $result_array as $aRow )
				{
					$row  =  array();
					//remove unwanted columns from db result
					$aRow_keys = array_keys($aRow);
					foreach($this -> dtt_hidden_cols as $key =>$value){
						unset($this -> dtt_columns[$value]);
					}
					//remove unwanted columns to match the datatable cols
					foreach($this -> dtt_columns as $key =>$value){
						if(in_array($this -> dtt_columns[$key], $this -> dtt_hidden_cols)){
							unset($this -> dtt_columns[$key]);
						}
						
					}
			
					for ( $i = 0 ; $i < count( $this -> columns ) ; $i++ )
					{
						//id formatting
						 if($i == 0){
						 
						      $appointment = isset( $this -> id_type[ 'appointment' ] ) ? $this -> id_type[ 'appointment' ] : FALSE ;
						      $prescription = isset( $this -> id_type[ 'prescription' ] ) ? $this -> id_type[ 'prescription' ] : FALSE ;
                              $diagnosis = isset( $this -> id_type[ 'diagnosis' ] ) ? $this -> id_type[ 'diagnosis' ] : FALSE;
						      $row[] = $this -> CI -> load -> view( 'template_checkbox' , '', TRUE);//olcomhms_create_id($aRow[$this -> index_column],$this -> id_type['employee'],$this -> id_type['patient'], $appointment , $prescription , $diagnosis );
						 }else{
						 		//some columns are not suppossed to be seen only datatable columns
						 		if(isset($this -> dtt_columns[$i])){
						 			if($this -> data_style_format!= null){
						 				
						 				foreach ($this -> data_style_format as $column  => $format) {
						 					
							 				
										 	if($column == $this -> dtt_columns[ $i ]){
										 			//add data
										 			$this -> data_style_format[ $column ]['data'] = $aRow[ $this -> dtt_columns[ $i ] ];
													//load styled data view
										 			$row[$this -> order[ $column  ] ] = $this -> CI -> load -> view('data_style_format',
													array(
														'format' => $this -> data_style_format[ $column ]
													),TRUE);
													break;//get out of the loop to avoid overwriting earliest columns with the last 
										 		}else{
										 		    // Cell oPs
										 		    
                                                    
										 			$row[ $this -> order[ $this -> dtt_columns[ $i ] ] ] = $aRow[ $this -> dtt_columns[ $i ] ];
										 		}
											
										}
                                        
								 	}else{
								 	    
								 			$row[$this -> order[$this -> dtt_columns[$i]]] = $aRow[$this -> dtt_columns[$i]];
								 	}
						 		 
						 		}	
						 			
						 }
						 if($this -> with_actions == TRUE){
							 if($i == count($this -> columns)-1){
							 	$data = array(
									'view' => site_url($this -> controller_fx.'/view/'.$aRow[$this -> index_column]),
									'edit' => site_url($this -> controller_fx.'/edit/'.$aRow[$this -> index_column]),
									'delete' => site_url($this -> controller_fx.'/delete/'.$aRow[$this -> index_column])
								);
							 		$row[] = $this -> CI -> load -> view('actions_view',array('actions_data' =>$data),TRUE);
							 	}
						 }
						
					}
					$output['aaData'][]  =  $row;
				}
				if( $return_array === TRUE ){
					return $output;
				}
				return    json_encode($output);	
		}
}
