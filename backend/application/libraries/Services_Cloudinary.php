<?php

require 'Cloudinary/Cloudinary.php';
require 'Cloudinary/Uploader.php';
require 'Cloudinary/Api.php';

class Services_Cloudinary{

	public $ci ;

	function __construct(){
	 	$this->ci = &get_instance();
	 	log_message('info',"********** Setting Cloudinary configurations ************ ");
	 	$this->ci -> load->config('cloudinary_config');
		\Cloudinary::config(array( 
		  "cloud_name" => $this->ci->config->item('cloud_name'), 
		  "api_key" => $this->ci->config->item('api_key'), 
		  "api_secret" => $this->ci->config->item('api_secret') 
		));
		log_message('info','*********** Done setting Cloudinary Configs ***********' );
	}

	function upload($image_path,$options){
		log_message('info',"*************** Invoked ".__FILE__. ' : '.__FUNCTION__."************");
		$results = \Cloudinary\Uploader::upload($image_path,$options);
		log_message('info',"**************** FInished uploading image to Cloudinary results : ".print_r( $results,true));
		return $results;
	}
}
