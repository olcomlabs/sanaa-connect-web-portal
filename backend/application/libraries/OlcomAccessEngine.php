<?php if(!defined('BASEPATH')) exit('Direct access is restricted');
/*
 * 
 * 
 */
class OlcomAccessEngine{
	 private $group_id;
	 private $employee_id;
	 private $admin_group_id = NULL;
	 private $super_admin_group_id = NULL;
	 private $ci;
	 private $access_engine_config;
	 
	 function OlcomAccessEngine(){
	 	$this -> ci = & get_instance();
		
		//load engine configs
		
		$this -> ci -> load -> config('olcom_access_engine');
		$this -> access_engine_config = $this -> ci -> config -> item('access_engine');
        
        $this -> admin_group_id = 81;//intval( $this -> ci -> encrypt -> decode( $this -> access_engine_config['admin_gid'])) ;
        $this -> super_admin_group_id = 66 ;//intval( $this -> ci -> encrypt -> decode( $this -> access_engine_config['super_admin_gid'])) ;
        
     }
	 /*
      * 
      * 
      * 
      */
      function get_super_admin_gid(){
          return $this -> super_admin_group_id;
      } 
      /*
       * 
       * 
       * 
       */
       function get_admin_gid(){
           return $this -> admin_group_id;
       } 
	 /*
	  * 
	  * checks if users(employees) or admins logged
	  * 
	  */
	  function is_logged(){
	  	
	 	$employee_id = $this -> ci -> session -> userdata( $this -> access_engine_config['employee_id']);
		$group_id = $this -> ci -> session -> userdata( $this -> access_engine_config['group_id']);
		
		if(((isset($employee_id) AND $employee_id != NULL) AND (isset($group_id) AND $group_id != NULL))  OR
			 (isset($group_id) AND $group_id != NULL))
			{
				return TRUE;
			}else{
				return FALSE;
			}
			
			
	  }
	   /*
        * 
        * 
        * 
        */
	  function login($username,$password){
	  	    
            
            //get group_id
            $group_id = NULL;
            $password = md5($password);
            
            $user_info = $this -> ci -> accessengine_model -> get_user_info($username,$password);
           
            if(isset($user_info) AND $user_info !== NULL){
                    
                $group_id = $user_info['group_id'];
                //is super administrator
                if($group_id !== NULL AND isset($group_id)){
          
                    $session_data = $this -> ci -> session -> all_userdata();
                    $group_id = intval($group_id);
                    //unset session_id 
                    unset($session_data['session_id']);
               
                
                   //log activity
                   $this -> ci -> accessengine_model -> log_activity((
                    $group_id === intval( $this -> ci -> encrypt -> decode( $this -> access_engine_config['admin_group_id'])) OR 
                    $group_id === intval( $this -> ci -> encrypt -> decode( $this -> access_engine_config['super_admin_group_id'])) 
                    ) === TRUE ? 'admin' : 'users',$session_data,$this -> super_admin_group_id);
                       
                    //clear session userdata 
                    $this -> ci -> session -> unset_userdata( $this -> access_engine_config['group_id']);
                    $this -> ci -> session -> unset_userdata( $this -> access_engine_config['employee_id']);
                
                
                    //set  group id 
                    $this -> ci -> session -> set_userdata( $this -> access_engine_config['group_id'],
                    $this -> ci -> encrypt -> encode($group_id));
                
                     
                    //set employee_id for employees
                    if(isset($user_info['employee_id']) AND $user_info['employee_id'] !== NULL)
                    {
                        
                        //check if user is active
                        if(intval($user_info['active']) === 0){
                            //clear session  
                            $this -> ci -> session -> sess_destroy();
                            return 0;
                        } 
                         
                        $this -> ci -> session -> set_userdata( $this -> access_engine_config['employee_id'],
                        $this -> ci -> encrypt -> encode($user_info['employee_id']));

                          $userProfile = $this->ci->accessengine_model->get_profile($user_info['employee_id']);
                          $this->ci->session->set_userdata('userProfile',$userProfile);
                    }
                
                   
                   
                    return 1;
                }else 
                    return NULL; 
            }else{
                return NULL;
            }
	  }
       /*
        * 
        * returns logged in user group id ,employee id if any
        * 
        */
        function get_logged_in_user(){
            $group_id = NULL;
            $employee_id = NULL;
            $logged_info = NULL;
            
            if( $this -> is_logged() === TRUE){
                
                $group_id = $this -> ci -> session -> userdata( $this -> access_engine_config['group_id']);
                
                if(isset($group_id) AND $group_id !== FALSE){
                    $logged_info = array();
                    
                    $logged_info['group_id'] = intval( $this -> ci -> encrypt -> decode( $this -> ci -> session
                         -> userdata( $this -> access_engine_config['group_id'])));
                        
                    //if is an employee
                    $employee_id = $this -> ci -> session -> userdata( $this -> access_engine_config['employee_id']);
                   
                    if(isset($employee_id) AND  $employee_id !== FALSE)
                    {
                        $logged_info['employee_id'] = intval( $this -> ci -> encrypt -> decode( $employee_id ));
                    }
                    
                    return $logged_info;
                }
                
                return NULL;
            }
            return NULL;
        }
        /*
         * 
         * 
         * 
         * 
         */
         function load_modules( $return_array = FALSE ){
             $modules = NULL;
             $logged_info = NULL;
             
             //modules lib
             $this -> ci -> load -> library('OlcomHmsModules');
            
             //modules config 
             $this -> ci -> load -> config('olcomhms_modules_configs');
             $modules_configs = $this -> ci -> config -> item('modules_configs');
             
             $logged_info = $this -> get_logged_in_user();
            
             if($logged_info !== NULL){
                 
                 $modules = $this -> ci -> accessengine_model -> get_modules();
                 
                 if($modules !== NULL){
                     
                     // all modules for super admin
                     if($logged_info['group_id'] === $this -> super_admin_group_id){
                         
                         foreach($modules as $module => $sub_modules){
                             
                             $this -> ci -> olcomhmsmodules -> add_main_module($modules_configs[$module],$module);
                             
                             if(isset( $sub_modules ) AND $sub_modules != NULL)
                             {
                                foreach( $sub_modules as $sub_module)
                                {
                                 $this -> ci -> olcomhmsmodules -> add_sub_module($module,$modules_configs[$sub_module]);
                                }    
                             }
                             
                         }

						if( $return_array == TRUE ) return $modules;
						
                         return $this -> ci -> olcomhmsmodules -> create_modules_html();
                     }
                     // load administrator modules
                    if($logged_info['group_id'] === $this -> admin_group_id){
                        $restricted_modules_sub_modules = $this -> ci -> config -> item('restricted_modules_sub_modules');
                        
                         
                        //disable restricted modules
                        foreach($modules as $module => $sub_modules){
                            //modules if any
                            if(in_array($module, $restricted_modules_sub_modules) === TRUE){
                                 
                                unset($modules[$module]);
                            }
                            foreach($sub_modules as $key => $sub_module){
                                if(in_array($sub_module, $restricted_modules_sub_modules) === TRUE)
                                {
                                    unset($sub_modules[$key]);
                                    $modules[$module] = $sub_modules;
                                }
                                if( $modules_configs[ $sub_module ][ 'permission' ] == 'create' )
                                {
                                    unset( $modules[ $module ][ $key ]);
                                }    
                            }
                            
                        } 
						
						if( $return_array == TRUE ) return $modules;
                        foreach($modules as $module => $sub_modules){
                             
                             $this -> ci -> olcomhmsmodules -> add_main_module($modules_configs[ $module ],$module);
                             foreach( $sub_modules as $sub_module){
                                 
                                 $this -> ci -> olcomhmsmodules -> add_sub_module($module,$modules_configs[ $sub_module ]);
                             }
                         }

                         return $this -> ci -> olcomhmsmodules -> create_modules_html();
                    }
                 // employees
                if($logged_info['group_id'] !== $this -> admin_group_id AND $logged_info['group_id'] !== $this -> super_admin_group_id){
                        
                        $result_permissions  = $this -> ci -> accessengine_model -> get_group_permissions($logged_info['group_id']);
                        if($result_permissions  === NULL){
                            return NULL;
                        }
                        
                       $modules = array();
                       foreach($result_permissions as $module => $module_permission){
                                $module_read = 0;
                                $module_create = 0;
                                
                              
                                if($modules_configs[$module]['permission'] === 'read'){
                                    
                                    $module_read = $module_permission['read'] & ($modules_configs[$module]['permission'] === 'read' ? 1 : 0 );
                                    
                                    if(isset($result_permissions[$module]['sub_modules']) AND $result_permissions[$module]['sub_modules'] != NULL)
                                    {
                                       foreach($result_permissions[$module]['sub_modules'] as $sub_module => $sub_module_permission){
                                          
                                            if($modules_configs[$sub_module]['permission'] === 'read')
                                            {
                                                   $sub_module_read = $sub_module_permission['read'] & $module_read & $modules_configs[$sub_module]['permission'] === 'read' ? 1 : 0;
                                                   
                                                   if($sub_module_read === 1){
                                                      
                                                      if(isset( $modules[ $module ]) AND $modules[ $module ] != NULL ){
                                                          
                                                        if(!in_array($sub_module,$modules[$module]))
                                                            $modules[$module][] = $sub_module;
                                                          
                                                      }else{
                                                          $modules[$module][] = $sub_module;
                                                      }
                                                         
                                                   }
                                            }
                                            
                                     /*if($modules_configs[$sub_module]['permission'] === 'create')
                                         {
                                           
                                                $sub_module_create = $sub_module_permission['create']   & $modules_configs[$sub_module]['permission'] === 'create' ? 1 : 0;
                                                   
                                                  if($sub_module_create === 1){
                                                      
                                                       if(isset($modules[ $module ] ) AND $modules[ $module ] != NULL){
                                                           
                                                        if(!in_array($sub_module,$modules[$module]))
                                                            $modules[$module][] = $sub_module;
                                                          
                                                      }else{
                                                          $modules[$module][] = $sub_module;
                                                      }
                                                   }
                                          }*/
                                       }
                                    }
                                }
                                    
                            }
                        if( $return_array == TRUE )
                        {
                            return $modules;
                        }
                        foreach($modules as $module => $sub_modules){
                             
                             $this -> ci -> olcomhmsmodules -> add_main_module($modules_configs[$module],$module);
                           //  $sub_modules = $sub_modules['sub_modules'];
                             foreach( $sub_modules as $sub_module){
                                 
                                 $this -> ci -> olcomhmsmodules -> add_sub_module($module,$modules_configs[$sub_module]);
                             }
                         }

                         return $this -> ci -> olcomhmsmodules -> create_modules_html();
                    }
                 }
             }
            return NULL;
         }
        /*
         * 
         * returns 0 - no logged in user, false - not allowed, true - allowed
         * 
         */
         function is_allowed_module( $path ){
             
             if($this -> is_logged() === TRUE){
                 
                 $logged_info = NULL;
                 
                 $logged_info = $this -> get_logged_in_user();
                 
                 
                 //target admin and users
                 
                 if($logged_info !== NULL){
                     
                     $path = explode('/',$path);
                     $module = $path[0];
                      
                     
                     if( intval( $logged_info[ 'group_id' ] ) === $this -> admin_group_id){
                         //check if accessing restricted modules sub_modules
                         $restricted_modules_sub_modules = $this -> ci -> config -> item('restricted_modules_sub_modules');
                         
                         if(in_array($module, $restricted_modules_sub_modules) === TRUE)
                         {    
                             return FALSE;
                         }
                         if(isset( $path[ 1 ]) AND $path[ 1 ] != NULL)
                         {    
                             if(in_array($path[1], $restricted_modules_sub_modules) === TRUE)
                            {    
                                 return FALSE;
                            }
                         
                         }
                         
                         return TRUE;   
                     }
                   
                      // users 
                     if(intval($logged_info['group_id']) !== $this -> admin_group_id AND intval($logged_info['group_id']) !== $this -> super_admin_group_id)
                     {
                         
                              
                          //check if accessing restricted modules sub_modules
                         $restricted_modules_sub_modules = $this -> ci -> config -> item('restricted_modules_sub_modules');
                         
                         if(in_array($module, $restricted_modules_sub_modules) === TRUE)
                         {    
                             return FALSE;
                         }
                         
						 if( isset( $path[1] ) && $path[ 1 ] == 'index' )
						 	return TRUE;
						 
                         if(isset($path[1]) AND $path[1] != NULL)
                         {    
                             if(in_array($path[1], $restricted_modules_sub_modules) === TRUE)
                            {    
                                 return FALSE;
                            }
                         
                         }
                        
                         //load module configs
                         $this -> ci -> load -> config( 'olcomhms_modules_configs' );
                         $modules_configs = $this -> ci -> config -> item( 'modules_configs' );
                         
                         $result = $this -> ci -> accessengine_model -> get_group_module_permissions($logged_info['group_id'],$module,'module');
                         
                         if( $result === NULL)
                         {
                             return FALSE;
                         }
                         else if( intval ( $result[ $modules_configs[ $module ][ 'permission' ]] ) !== 1)
                                {
                                    return FALSE;
                                }
                         
                         if(isset($path[1]) AND $path[1] != NULL)
                         {
                             $result = $this -> ci -> accessengine_model -> get_group_module_permissions($logged_info['group_id'],$path[1],'sub_module');
                             
                             if($result === NULL)
                             {
                                 return FALSE;
                                 
                             }else if( intval ( $result[ $modules_configs[ $path[1] ][ 'permission' ]] ) !== 1)
                                        {
                                            return FALSE;
                                        }
                         }
                         
                         return TRUE;
                     }
                     
                     
                     
                     
                 }
              
             }
             else{
                     return 0;//not logged in 
              }   
         }
        /*
         * 
         * 
         * 
         */
         function is_allowed_module_action($path){
             
             if( $this -> is_logged() === TRUE){
             // match personnel/actions/delete/0 or humanresources/actions/departments/view/20
                 if(preg_match('/\/actions\//', $path) === 1)
                 {
                     $logged_info = $this -> get_logged_in_user();
                     
                     if( intval($logged_info['group_id']) === $this -> admin_group_id OR 
                     intval($logged_info['group_id']) === $this -> super_admin_group_id )
                     {
                         return TRUE;
                     } 
                        
                     $path = explode( '/' , $path );
                     
                     $requested_permission = $path[ count($path) - 2 ];// gives delete , edit or view
                     
                     if( $requested_permission === 'view')
                     {
                         return TRUE;
                     }
                  
                     $module_permission = $this -> ci -> accessengine_model -> get_group_module_permissions( $logged_info['group_id'],$path[ 0 ],'module');
                    
                     if( $module_permission === NULL )
                     {
                         return FALSE;
                     }
                     
                     $requested_permission = $requested_permission == 'edit' ? 'write' : $requested_permission; //change from edit to write
                    
                     
                    if( intval( $module_permission[ $requested_permission ] ) !== 1)
                     {
                          return FALSE;                        
                     }
                     // if set with sub_module 
                     if( count( $path ) == 5)
                     {
                         $sub_module_permission = $this -> ci -> accessengine_model -> get_group_module_permissions( $logged_info['group_id'],
                         $path[ count($path) - 3 ],'sub_module');
                         
                         if( $sub_module_permission === NULL)
                         {
                             return FALSE;
                         }
                         
                         // AND module and sub_module permissions
                         return 1 & intval( $sub_module_permission[ $requested_permission ] ) === 1 ? TRUE : FALSE;
                     }
                     
                    return TRUE;
                     
                  }else
                     {
                         return FALSE;
                     }
            }else
                {
                    return FALSE;
                }
         }
        
}
