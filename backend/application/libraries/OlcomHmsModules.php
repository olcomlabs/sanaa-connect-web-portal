<?php if(!defined('BASEPATH'))exit('No direct script access allowed');
/*
 * class define of olcom hms menus module
 * loads menu ,links,text,icon submen etc
 * 
 */

class  OlcomHmsModules{
	/*
	 * main_module
	 * items-link,icon,menu_text
	 */
	var $main_module;
	
	/*
	 * sub_module
	 * menu_text as key,sub_module text,link,icon
	 * 
	 */
	var $sub_module;
	
    var $current_module;
    
    var $ci;
	function  OlcomHmsModules(){
		$this -> main_module = array();
		$this -> sub_module = array();
        
        $ci = & get_instance( );
        
        $this -> current_module = $ci -> uri -> segment( 1 );
        
	}
	
	/*
	 * 
	 * function add_main_module
	 * parameters-link,icon,menu text,alias
	 */
	 
	 function add_main_module($main_module_item,$alias){
	 		
	 		$this->main_module[$alias]=$main_module_item;	//repeating the key will override the menu contents
	 }

	/*
	 * function add_sub_module
	 * params-main_module_alias as key,sub_module link,sub_module icon,sub_module text
	 */
	 
	 function add_sub_module($main_module_alias,$sub_module_item){
	 	
	 	if(!array_key_exists($main_module_alias,$this->sub_module)){
	 		$this->sub_module[$main_module_alias]=array();
	 	}
		array_push($this->sub_module[$main_module_alias],$sub_module_item);
	 }
	 
	 /*
	  * function get_menu
	  * loads main menu and sub menus
	  */
	  function create_modules_html(){
	  	
			$modules_html = "";//menu html
	  		$menu_data = array('main_module'=>$this->main_module,'sub_module'=>$this->sub_module);
           // print_r($menu_data);
			if(isset($menu_data) && $menu_data != ''){
				while(list($key,$value) = each($menu_data['main_module'])){
					
						$modules_html.="<li class ='". ( $key == $this -> current_module ? 'active open' : NULL ) ."'>
								<a href='".site_url($menu_data['main_module'][$key]['link'])."'>
									<span class='".$menu_data['main_module'][$key]['icon']."'>&nbsp;</span>
									<span class='menu-text'> ".$menu_data['main_module'][$key]['menu_text']." </span>
								</a>
							";
							
							//add sub menus for particular main menu alias
							/*$modules_html.="<ul class='submenu'>";
							
                            if(isset( $menu_data['sub_module'][$key] ) AND $menu_data['sub_module'][$key] != NULL )
                            {
                                    foreach ($menu_data['sub_module'][$key] as $sub_module)
                                     {
                                        $modules_html.= "
                                            <li>
                                                <a href='".site_url($sub_module['link'])."'>
                                                    <i class='".$sub_module['icon']."'></i>
                                                    ".$sub_module['menu_text']."
                                                </a>
                                            </li>";
                                    
                                     }    
                            }*/
							
						//$modules_html.="</ul></li>";
				}
			}
		
			return $modules_html;
	  }
}
