<?php

$config = array( 

		'help_text' => array(
			'get_patient_id' => array( 'title' => 'How to obtain a Patient ID ' , 'content' => 'Type the Patient  ID and hit Enter or click Search icon '),
			'assoc_diagnosis' => array( 'title' => 'Associate Diagnosis' , 'content' => 'This is will auto-load patient\'s diagnosis after filling the Patient ID field. To write a new patient diagnosis goto Prescriptions -> Patient Diagnosis -> Create'),
			'medicament_id' => array( 'title' => 'Choosing a Medicament' , 'content' => 'Type a few characters of a medicament and select among the auto-filled options'),
			'get_indication' => array( 'title' => 'Choosing Disease Indication' , 'content' => 'Type a few characters of a disease name and select among the auto-filled options'),
			'id_or_name' => array( 'title' => 'How to obtain Personnel ID','content' => 'Enter patient/employee ID or name and select from listed options to load billing records')			
		)
);
