<?php if(!defined('BASEPATH')) exit('Direct access is restricted');
/*
 * 
 * 
 * 
 * stores encrypted cookie name for employee id ,group id ,admin id and super admin id
 */

$config = array(
	'access_engine' => array(
		'employee_id' => 'Wz8EaAV3B2IMblIvAmtUNQFVAzoCZg==',
		'group_id'  => 'AWcJegRpBnpYJQVeDGlVNQ==',
		'admin_group_id' => 'B2dTNlQ7B2cBYlIJUDsHcQ5qUndUJA4FUzhaZA==',
		'super_admin_group_id' => 'UCIDd1MhA28McwNYBGkCYgttBj8GaFRfUjdSegQ5XSxXIAcKAGpUbA==',
		'admin_gid' => 'AThSYg==',
		'super_admin_gid' => 'VmEEMw=='
	),
	'restricted_modules_sub_modules' => array('modules','create_sub_module','sub_modules','create_module')
);
