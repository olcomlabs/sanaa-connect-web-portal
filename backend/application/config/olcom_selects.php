<?php if(!defined('BASEPATH')) exit('Direct access restricted');
/*
 * olcom selects choices for <select>
 */

 $config = array(
 		'selects' => array(
 				'yes_no' => array(
					1 => 'Yes',
					0 => 'No'
				), 
				'gender_select' => array(
				
				'Female' => 'Female',
				'Male' => 'Male'
				),
				'region_select' => array(
						"Arusha" => "Arusha",
						"Dar es Salaam" => "Dar es Salaam",
						"Dodoma" => "Dodoma",
						"Iringa" => "Iringa",
						"Kagera" => "Kagera",
						"Kigoma" => "Kigoma",
						"Kilimanjaro" => "Kilimanjaro",
						"Lindi" => "Lindi",
						"Manyara" => "Manyara",
						"Mara" => "Mara",
						"Mbeya" => "Mbeya",
						"Morogoro" => "Morogoro",
						"Mtwara" => "Mtwara",
						"Mwanza" => "Mwanza",
						"Njombe" => "Njombe",
						"Pwani" => "Pwani",
						"Rukwa" => "Rukwa",
						"Ruvuma" => "Ruvuma",
						"Shinyanga" => "Shinyanga",
						"Singida" => "Singida",
						"Tabora" => "Tabora",
						"Tanga" => "Tanga",
						"Zanzibar" => "Zanzibar (Unguja/Pemba)"
						),
					'marital_status_select' => array(
								'Single' => 'Single',
								'Married' => 'Married',
								'Divorced' => 'Divorced'
							
						),
					'blood_type' => array(
                        ' ' => ' ',
                        'A' => 'A',
                        'B' => 'B',
                        'AB' => 'AB',
                        'O' => 'O'
                        ),
                     'blood_type_sign' => array(
                        ' ' => ' ',
                        '+' => '+',
                        '-' => '-'
                     ),
                     'update_register_select' => array(
                       'update' => 'Update',
                       'register' => 'Register'
                     ),
                     'period' => array( 
                        'minutes' => 'Minutes',
                        'hours' => 'Hours',
                        'days' => 'Days',
                        'weeks' => 'Weeks',
                        'months' => 'Months',
                        'years' => 'Years',
                        'indefinite' => 'Indefinite'
                        
                     ),
                     'maintenance_periods' => array(
                        'Day(s)' => 'Day(s)',
                        'Week(s)' => 'Week(s)',
                        'Month(s)' => 'Months(s)',
                        'Year(s)' => 'Year(s)'
                     ), 
                     'ward_genders' => array(
                        '1' => 'Men Ward' ,
                        '2' => 'Women Ward',
                        '3' => 'Unisex'
                     ),
                     'bed_status' => array(
                        1 => 'Free' , 
                        2 => 'Occupied'
                     ),
                     'admission_types' => array(
                        1 => 'Elective',
                        2 => 'Emergency',
                        3 => 'Maternity',
                        4 => 'Routine' , 
                        5 => 'Urgent' 
                     ),
                     'hospitalizations_status'  => array(
                        1 => 'Hospitalized',
                        2 => 'Discharged'
                     ),
                     'bill_types' => array(
                        1 => 'CASH SALE',
                        2 => 'PROFOMA INVOICE'
                     ),
					 'order_status' => array(
					 	0 => 'Draft',
					 	1 => 'Processed',
					 	2 => 'Pending',
					 	3 => 'Accepted',
					 	4 => 'Received',
					 	5 => 'Cancelled',
					 	6 => 'Incompleted',
					 	-1 => 'Low Quantity' 
					 ),


                      'supporter_status' => array(
                        1 => 'Active',
                        0 => 'Inactive'
                     ),
                     
			)  
 );
