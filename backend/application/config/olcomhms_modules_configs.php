<?php if(! defined('BASEPATH')) exit('Direct acccess is restricted');
/*
 * 
 * 
 * 
 * 
 */
 $config = array( 
        'modules_configs'  => array(
        
            /*
             * main modules
             * 
             */
            
            //Personnel
            'personnel' => array(
                   'menu_text' => 'Personnel',
                   'link' => 'personnel/index',
                   'icon' => 'olcom-icon-personnel',
                   'permission' =>'read'
            ),
            // Human resources
            'humanresources' => array(
                'menu_text' => 'General HR',
                'link' => 'humanresources/index',
                'icon' => 'olcom-icon-hr',
                'permission' =>'read'
            ),
            // User Management
            'usermanagement' => array(
                'menu_text' => 'User Management',
                'link' => 'usermanagement/index',
                'icon' => 'olcom-icon-um',
                'permission' =>'read'
            ),
            
            // news
            'news' => array(
                    'menu_text' => 'News',
                    'link' => 'news/index',
                    'icon' => 'olcom-icon-blood',
                    'permission' => 'read'
            ),
                        // news
            'events' => array(
                    'menu_text' => 'Events',
                    'link' => 'events/index',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),
             'organizations' => array(
                    'menu_text' => 'Organizations',
                    'link' => 'organizations/index',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),
              'artists' => array(
                    'menu_text' => 'Artists',
                    'link' => 'artists/index',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),
              'unpublished' => array(
                    'menu_text' => 'Unpublished Articles',
                    'link' => 'news/unpublished',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),
            /*
             * sub_modules
             * 
             */
             
             //Personnel 
            'view_personnel' => array(
                'menu_text' => 'View Personnel',
                'link'   => 'personnel/view_personnel',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'add_personnel' => array(
                   'menu_text' => 'Add Personnel',
                   'link' => 'personnel/add_personnel',
                   'icon' => 'icon-plus',
                   'permission' => 'create'
            ),
            
            // Human Resources
            'departments' => array(
                    'menu_text' => 'Departments',
                    'link' => 'humanresources/departments',
                    'icon' => 'icon-eye-open',
                    'permission' => 'read'
            ),
            'create_department' => array(
                    'menu_text' => 'Create Departments',
                    'link' => 'humanresources/create_department',
                    'icon' => 'icon-plus',
                    'permission' => 'create'        
            ),
            'job_positions' => array(
                    'menu_text' => 'Job Positions',
                    'link' => 'humanresources/job_positions',
                    'icon' => 'icon-eye-open',
                    'permission' => 'read'
            ),
            'create_position' => array(
                    'menu_text' => 'Create Position',
                    'link' => 'humanresources/create_position',
                    'icon' => 'icon-plus',
                    'permission' => 'create'
            ),
            'employees' => array(
                'menu_text' => 'Employees',
                'link' => 'humanresources/employees',
                'icon' =>' icon-eye-open',
                'permission' => 'employees'
            ),
            'hire_employee' => array(
                'menu_text' => 'Hire Employee',
                'link' => 'humanresources/hire_employee',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            
            // User Management
            'groups' => array(
                'menu_text' => 'Groups',
                'link' => 'usermanagement/groups',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'users' => array(
                'menu_text' => 'Users',
                'link' => 'usermanagement/users',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'create_group' => array(
                'menu_text' => 'Create Group',
                'link' => 'usermanagement/create_group',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            'create_user' => array(
                'menu_text' => 'Create User',
                'link' => 'usermanagement/create_user',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            'modules' => array(
                'menu_text' =>'Modules',
                'link' => 'usermanagement/modules',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'create_module' => array(
                'menu_text' => 'Create Module',
                'link' => 'usermanagement/create_module',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            'sub_modules' => array(
                'menu_text' => 'Sub-modules',
                'link' => 'usermanagement/sub_modules',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'module_roles' => array(
                'menu_text' => 'Module Roles',
                'link' => 'usermanagement/module_roles',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'create_module_role' => array(
                'menu_text' => 'Create Module Role',
                'link' => 'usermanagement/create_module_role',
                'icon'=> 'icon-plus',
                'permission' => 'create'            
            ),
            'create_sub_module_role' => array(
                'menu_text' => 'Create Sub-module Role',
                'link' => 'usermanagement/create_sub_module_role',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            'sub_module_roles' => array(
                'menu_text' => 'Sub-module roles',
                'link' => 'usermanagement/sub_module_roles',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'create_sub_module' => array(
                'menu_text' => 'Create Sub-module',
                'link' => 'usermanagement/create_sub_module',
                'icon' => 'icon-plus',
                'permission' => 'create'
            ),
            // Patients
            'published' => array(
                'menu_text' => 'Published Articles',
                'link' => 'news/published',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
            'create_article' => array(
                'menu_text' => 'Create Article',
                'link' => 'news/create_article',
                'permission' => 'create',
                'icon' => 'icon-plus'

            ),
            //Organizations
            'registered_organizations' => array(
                'menu_text' => 'Registered Organizations',
                'link'   => 'organizations/registered_organizations',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),

              //Events
            'published_events' => array(
                'menu_text' => 'Published Events',
                'link'   => 'events/published_events',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
              'registered_artists' => array(
                'menu_text' => 'Registered Artists',
                'link'   => 'artists/registered_artists',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),

                 'jobs' => array(
                    'menu_text' => 'Jobs',
                    'link' => 'jobs/index',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),

                'posted_jobs' => array(
                'menu_text' => 'Posted Jobs',
                'link'   => 'jobs/posted_jobs',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),

                 'pages' => array(
                    'menu_text' => 'Pages',
                    'link' => 'pages/index',
                    'icon' => 'olcom-icon-blood',
                    'permission' => 'read'
            ),

                  'published_slide' => array(
                    'menu_text' => 'Slide Show',
                    'link' => 'pages/published_slide',
                    'icon' => 'olcom-icon-assets',
                    'permission' => 'read'
            ),

            //         'supporters' => array(
            //         'menu_text' => 'Supporters',
            //         'link' => 'supporters/index',
            //         'icon' => 'olcom-icon-assets',
            //         'permission' => 'read'
            // ),

            'published_supporters' => array(
                'menu_text' => 'Supporters',
                'link'   => 'supporters/published_supporters',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),

             'published_welcome' => array(
                'menu_text' => 'Welcome Message',
                'link'   => 'welcome/published_welcome',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),

              'published_about' => array(
                'menu_text' => 'About Us',
                'link'   => 'about/published_about',
                'icon' => 'icon-eye-open',
                'permission' => 'read'
            ),
           
        )  
        
        
       
 );
