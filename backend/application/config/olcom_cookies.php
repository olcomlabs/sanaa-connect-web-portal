<?php if(!defined('BASEPATH')) exit('Direct access restricted');

$config = array(
	'action_cookies'=>array(
		'action_save_click'=>'olcom-action-edit-save-click',
		'action_edit'=>'olcom-action-edit',
		'action_delete_confirm'=>'olcom-action-delete-confirm-status',
		'action_delete'=>'olcom-action-delete'
	)
);
