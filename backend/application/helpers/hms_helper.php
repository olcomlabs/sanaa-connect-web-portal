<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * 
 * OLCOMLABS HMS Helper functions
 */
 
 if(!function_exists('olcomhms_create_id')){
 	
	function olcomhms_create_id($intID,$employee = FALSE,$patient = FALSE , $appointment = FALSE , $prescription = FALSE , $diagnosis = FALSE ){
		$id_length = strlen($intID);
		$olcomhms_id = "";
		//using prefix 'E' for employees and 'P' for patients
		//target is to obtain id of length more 5 eg. E0001
		if($employee == TRUE){
			$olcomhms_id .= 'E';
		}
		if($patient == TRUE){
			$olcomhms_id .= 'P';
		}
		if( $appointment == TRUE )
        {
            $olcomhms_id.='AP';
        }
        if( $prescription == TRUE )
        {
            $olcomhms_id.='PX';
        }
        if( $diagnosis == TRUE )
        {
            $olcomhms_id.='D';
        }
		//for shorter than 4 ids
		if( $id_length < 4 ){
			$max_zeroes = 4-$id_length;
			
			//add the first zeroes
			for($i = 0;$i<$max_zeroes;$i++)
				$olcomhms_id .= '0';
			
			//concat the id number
			$olcomhms_id .= $intID;
			return $olcomhms_id;
		}
		
		//for longer than 4 ids
		$olcomhms_id .= $intID;
		
		return $olcomhms_id;
	}
 }
 
if(!function_exists('olcomhms_modules_html')){
	/*
	 * create required menu using the menu library
	 * 
	 */
	 function olcomhms_modules_html(){
	 	//get the ci instance
	 	$CI = & get_instance();
		if($CI !=  NULL){
			$CI -> load -> library('OlcomHmsModules');
            $CI -> load -> library('OlcomAccessEngine');
            
            if( $CI -> olcomaccessengine -> is_logged()  ===  FALSE)
               redirect('authentication/login','refresh');
            else {
                
                return  $CI -> olcomaccessengine -> load_modules();
		 	}
		}
	 }
	} 
	 /*
	  * 
	  * returns database id from given formatted id
	  */
	  if(!function_exists('olcomhms_db_id')){
	  		function olcomhms_db_id($formatted_id, $both=FALSE){
	  			preg_match('/^([a-zA-Z]+[0-9]+|E[0-9]+)$/',strtoupper($formatted_id),$match);//id must start with 'e' or 'p'
				if(count($match) !=  0){
					
					if( $both == TRUE ){
						$matches = array();
						preg_match( '/[a-zA-Z]+/',strtoupper( $formatted_id),$inital);
						preg_match('/[0-9]+/',strtoupper($formatted_id),$int_id);//
						$matches[ ] = $inital[0];
						$matches[ ] = $int_id[0];
						return $matches;
					}
					preg_match('/[0-9]+/',strtoupper($formatted_id),$match);//
					return (int)$match[0];//return the int part only
				}else{
					return NULL;
				}
	  		}
	  }
	   
	   /*
	   * 
	   * 
	   * 
	    * 
	   */
	   if(!function_exists('olcom_actions_cookies')){
	   		
			
			function olcom_actions_cookies($action,$operation){
				$CI = &get_instance();
				$CI -> load -> config('olcom_cookies');
				$olcom_cookies = $CI -> config -> item('action_cookies');
			
				$cookie_save = $olcom_cookies['action_save_click'];
				$cookie_action_edit = $olcom_cookies['action_edit'];
				$cookie_status = $olcom_cookies['action_delete_confirm'];
				$cookie_action_delete = $olcom_cookies['action_delete'];
				
				$CI = &get_instance();
				
				switch($operation){
					case 'create':
								if($action == 'edit'){
									
									
								set_cookie(
										array(
											'name'  =>$cookie_save,
											'value'  =>'no',
											'path'  =>'/',
											'expire'  =>300
										)
									);
								
									set_cookie(array(
									'name'  =>$cookie_action_edit,
									'expire'  =>300,
									'value'  =>site_url($CI -> uri -> uri_string()),
									'path'  =>'/'
									));
									
								}
								
								if($action == 'delete'){
									
									set_cookie(array(
									'name'  =>$cookie_status,
									'value'  =>'no',
									'expire'  =>300
									));
									set_cookie(array(
										'name'  =>$cookie_action_delete,
										'value'  =>site_url($CI -> uri -> uri_string()),
										'expire'  =>300
									));
								}
						break;
					case 'delete':
							if($action == 'edit'){
							//	
								
								delete_cookie(
									array(
									'name'  =>$cookie_action_edit,
									'path'  =>'/'
										)
									);
								
								 delete_cookie(array(
									'name'  =>$cookie_save,
									'path'  =>'/'
								));
							}
							if($action == 'delete'){
								delete_cookie(
									array(
										'name'  =>$cookie_action_delete,
										'path'  =>'/'
									)
								);
								delete_cookie(
									array(
											'name'  =>$cookie_status,
											'path'  =>'/'
										)
								);
							}
							
							delete_cookie(array('name' => $action ,'path'=>site_url($CI -> uri -> uri_string())) );
						}
				}
			}//end fn
			
	/*
	 * 
	 * 
	 * olcom_show_dialog
	 */
	 
	 if(!function_exists('olcom_show_dialog')){
	 	
		function olcom_show_dialog($type,$data,$dialog_type){
			$CI = &get_instance();
			
			if($type == 'table'){
					$CI -> load -> config('olcom_labels');
					$CI -> load -> config('olcom_messages');
					$labels = $CI -> config -> item('labels');
					$messages = $CI -> config -> item('messages');
					
					$dialog = $CI -> load -> view('template_modal_view',array(
								'modal_view'  =>array(
									'header'  =>$labels['record_details'],
									'content'  =>$data
									)
								),TRUE);
					echo json_encode(array(
									'type'  =>'table',
									'dialog'  =>$dialog
								));				
				}
			//confirm modal dialogs
			if($type == 'confirm'){
				$dialog = $CI -> load -> view('template_confirm_modal_dialog',
							array('modal_view'  =>array('message'  =>$data,
							'header'  =>'Confirm',
							'dialog_type'  =>'info')),					
							TRUE);
					echo  json_encode(array(
							'type'  =>'message','dialog'  =>$dialog
					));
				}
			//message dialogs
			if($type == 'message'){
				$dialog = $CI -> load -> view('template_message_dialog',
										array('modal_view'  =>array('message'  =>$data['message'],
										'header'  => $data['header'],
										'dialog_type'  => $dialog_type,
										'print_link' => isset( $data[ 'print_link' ] ) ? $data[ 'print_link' ] : NULL 
                                         )),					
										TRUE);
					echo  json_encode(array(
										'type'  =>'message',
										'dialog'  =>$dialog
						));
										
				}

			//update dialogs
			if($type == 'update' OR $type == 'form' ){
					
					 
					$dialog = $CI -> load -> view('template_modal_form', array( 'modal_view'  => $data ), TRUE );
									
					echo json_encode(
								array(
										'type'  => $type,
										'dialog'  =>$dialog,
										'specific_scripts'  =>$CI -> load -> view( $data['specific_scripts'],
										"",TRUE)
									)
								);
				}
               // tabbed dialogs
               if( $type == 'tabbed')
               {
                   $dialog = $CI -> load -> view( 'template_tabbed_view', $data ,TRUE);
                   echo json_encode(array(
                        'type' => 'message','dialog' => $dialog
                   ));
               }
                // static_table
                if( $type == 'static_table' )
                {
                    $dialog = $CI -> load -> view( 'template_static_table_view' , $data , TRUE );
                    echo json_encode( array( 
                        'type' => 'info' , 'dialog' => $dialog
                    )); 
                }
                if( $type == 'static_update_table' )
                {
                    $dialog = $CI -> load -> view( 'template_static_update_table_view' , $data , TRUE );
                    echo json_encode( array( 
                        'type' => 'update' , 'dialog' => $dialog
                    )); 
                }
               // dynamic table
                
                if( $type == 'dynamic_table' )
                {
                    $dialog = $CI -> load -> view( 'template_dynamic_table_view' , $data , TRUE );
                    echo json_encode( array( 
                        'type' => 'info' , 'dialog' => $dialog
                    )); 
                }
			}
		 }
	/*
	 * 
	 * 
	 * converts db array([0] = array('key1'  =>'value1','key2'  =>'value2' )) to array('value1'  =>'value2',..)
	 * @params $result obj
	 */
	if(!function_exists('olcom_db_to_drop')){
		
		function olcom_db_to_dropdown($result_obj){
			$dropdown  =  NULL;
			
			if($result_obj  !=   NULL){
				//$dropdown = array('##'  =>'');
				$result_array  =  $result_obj -> result_array();
				foreach($result_array as $row){
					$keys  =  array_keys($row);
					foreach($row as $key   => $value)
						{
							$dropdown[ $row[ $keys[ 0 ] ] ]  =  $row[ $keys[ 1 ] ];//order is much more important for this helper fx
						}
				}	
				return $dropdown;	
			}
			return array(NULL   => NULL);
		}
		
		/*
		 * 
		 * 
		 */
		if(!function_exists('olcom_server_form_message')){
			
			function olcom_server_form_message($message,$success_status){
				echo json_encode(array('message'   => $message,'success'   => $success_status));
			}
		}
		/*
		 * 
		 * 
		 * 
		 */function olcom_server_chosen_ajax($data){
		 	
			if($data !=  NULL){
				unset($data['']);
				echo json_encode($data);
			}
			else{
				echo json_encode(array('0'  =>'no_records')); 
			}
		 }
         /*
          * 
          * 
          * 
          */
          if(! function_exists('olcom_is_allowed_module_helper'))
          {
              function olcom_is_allowed_module_helper( $uri ){
                 
                if( preg_match('/\_data/',$uri)  ===  1){
                     return TRUE;
                 }
                if(preg_match('/actions/',$uri)  ===  1){
                     return TRUE;
                 }
                 $ci  =  & get_instance();
                 $is_allowed  =  $ci  ->  olcomaccessengine  ->  is_allowed_module( $uri ); 
                 if($is_allowed  ===  FALSE)
                 {
                    redirect('accessengine/access_denied','refresh');
                 }
                 if($is_allowed  ===  0)
                 {
                     redirect('authentication/login','refresh');
                 }
                 
              }
          }
          /*
           * 
           * 
           * 
           */
           if(! function_exists( 'olcom_extract_numbers' ) )
           {
               function olcom_extract_numbers( $str )
               {
                   if( preg_match('/[0-9]+/', $str, $match ) == 1 )
                       {
                           return $match[ 0 ] * 1 ;
                       }
                       return NULL;
               }
           }
          /*
           * 
           * 
           */
           if( ! function_exists( 'olcom_empty_dtt' ) )
           {
               function olcom_empty_dtt()
               {
                   echo "{\"sEcho\":4,\"iTotalRecords\":19,\"iTotalDisplayRecords\":0,\"aaData\":[]}";
               }
           }
           /*
            * 
            * 
            * 
            */
            if( ! function_exists( 'olcom_money_format' ) )
            {
                function olcom_money_format( $amount )
                {
                    if( $amount !== NULL )
                        return number_format( $amount, 2 );
                    return NULL;
                }
            }
            /*
             * 
             * 
             * 
             */
             if( ! function_exists( 'olcom_get_logged_username' ) ) 
             {
                 function olcom_get_logged_username()
                 {
                     $ci = & get_instance();
                     if( $ci -> olcomaccessengine -> is_logged() == FALSE )
                     {
                         redirect( 'authentication/login' ,'refresh' );
                         return;
                     }
                     $logged_user = $ci -> olcomaccessengine -> get_logged_in_user( );
                     
                     $username = '';
                     if( $logged_user[ 'group_id'] == $ci -> olcomaccessengine -> get_admin_gid()   )
                     {
                         
                         $username = 'Administrator';
                     }
                     else if( $logged_user[ 'group_id' ] == $ci -> olcomaccessengine -> get_super_admin_gid() ) 
                     {
                         
                         $username = 'Super Admin';
                     }
                     else
                         {
                            $ci -> load -> model( 'usermanagement_model');
                            $user = $ci -> usermanagement_model -> get_users( $logged_user[ 'employee_id' ] );
                            $username = $user[ 'username' ];        
                         } 
                     
                     return $username;
                 }
             }
            /*
             * 
             * 
             * 
             */
             function olcom_pretty_date( $date )
             {
                 if( $date == NULL )
                    return NULL;
                 if( $date[ 0 ] == '0000' ){
                 	return '--';
                 }
                return  date('D,M d, Y', mktime( NULL,NULL, NULL ,$date[ 1 ], $date[ 2 ] , $date[ 0 ] ) );
             }
			 
			 function get_sub_modules()
			 {
			 	$ci = &get_instance();
			 	$modules = $ci -> olcomaccessengine -> load_modules( TRUE );
				//print_r( $modules[  $this -> uri -> segment( 1)] );return;
				
				if( ! isset(  $modules[  $ci -> uri -> segment( 1)] ) )
				{
					return NULL;
				}
				$sub_modules = $modules[  $ci -> uri -> segment( 1)];
				$ci -> load -> config( 'olcomhms_modules_configs' );
				$modules_configs = $ci -> config -> item('modules_configs'  );
				//print_r( $modules_configs[ $sub_modules[ 44 ]] );
				
			 
				$launchers = array();
				foreach ($sub_modules as $key => $sub_module) {
					$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
				}
				
				$data[ 'header' ] = $modules_configs[  $ci -> uri -> segment( 1 ) ][ 'menu_text' ];
				$data[ 'launchers' ] = $launchers;
				
				return $data;
			 }
			 
			 /*
			  * 
			  * 
			  * 
			  */
			  
	}
