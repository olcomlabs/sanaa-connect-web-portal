<?php
if (!defined('BASEPATH'))
    exit(' Direct access is restricted');
/* 
  * 
  * 
  * 
  */
class Assets_mngt extends CI_Controller {

    var $logged = FALSE;
    var $return_codes;
    function Assets_mngt() {
        parent::__construct();

        if ($this -> olcomaccessengine -> is_logged() === TRUE) {
            
            olcom_is_allowed_module_helper($this -> uri -> uri_string());

            $this -> is_logged = TRUE;
        } else {

            if ($this -> input -> is_ajax_request()) {
                if (preg_match('/data/', $this -> uri -> uri_string()) === 1) {
                    echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                }
            } else {
                redirect('authentication/login', 'refresh');
            }

            $this -> is_logged = FALSE;

            $this -> load -> config('olcomhms_return_codes');
            
            if (!defined('SUCCESS_CODE'))
                define('SUCCESS_CODE', $this -> config -> item('success_code'));
           
            
        }
        $this  -> return_codes = $this -> config -> item( 'return_codes');
        // load models
        $this -> load -> model( 'assets_model' );
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');
        
    }
	/*
	 * 
	 */
	 function index()
	{
			
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	}  
    /* 
      * 
      * 
      * 
      */
    function asset_categories() {
        
        $labels = $this -> config -> item('labels');

        $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['asset_categories'], 'with_actions' => TRUE, 
        'create_controller_fx' => 'assets_mngt/create_asset_category',
         'columns' => array($labels['id'], $labels['category'] ), 'controller_fx' => 'assets_mngt/asset_categories_data'));

        $datatable = $this -> olcomhmstemplatedatatable -> create_view();

        $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
    }
    /* 
      * 
      * 
      * 
      */
    function assets_status() {
        
        $labels = $this -> config -> item('labels');

        $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['assets_status'], 'with_actions' => TRUE, 
        'create_controller_fx' => 'assets_mngt/create_asset_status',
         'columns' => array($labels['id'], $labels['status'] ), 'controller_fx' => 'assets_mngt/assets_status_data'));

        $datatable = $this -> olcomhmstemplatedatatable -> create_view();

        $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
    }
    /* 
      * 
      * 
      * 
      */
    function asset_categories_data() {
        $this -> load -> library('OlcomHmsDataTables', 
        array('columns' => array('category_id', 'category', 'category_id'), 'index_column' => 'category_id',
         'table_name' => 'asset_categories', 'controller_fx' => 'assets_mngt/actions/asset_categories',
          'id_type' => ''));

        //get the data
        echo $this -> olcomhmsdatatables -> get_data();
    }
    /* 
      * 
      * 
      * 
      */
    function assets_status_data() {
        $this -> load -> library('OlcomHmsDataTables', 
        array('columns' => array('status_id', 'status', 'status_id'), 'index_column' => 'status_id',
         'table_name' => 'assets_status', 'controller_fx' => 'assets_mngt/actions/assets_status',
          'id_type' => ''));

        //get the data
        echo $this -> olcomhmsdatatables -> get_data();
    }
    /* 
      *  
      *  
      */
     function create_asset_category()
     {
        $form_data = $this -> input -> post(NULL);
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){
          
            $result = $this  ->  assets_model  ->  create_asset_category( $form_data );
            
            if( $result === SUCCESS_CODE ){
                olcom_server_form_message($messages[ 'category_created' ], 1);
            }else{
                    olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                }
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array(
                 
                'with_submit_reset'  => TRUE,
                'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                'title'  => $labels[ 'create_asset_category'],
                'header_info'  => $messages['fill_details']
            )); 
            $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'category',
                'label'  => $labels['category_name']
            ));
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('create_asset_category_specific_scripts','',TRUE);
            
            $this -> load -> view('main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
     }
    /* 
      *  
      *  
      */
     function create_asset_status()
     {
        $form_data = $this -> input -> post(NULL);
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){
          
            $result = $this  ->  assets_model  ->  create_asset_status( $form_data );
            
            if( $result === SUCCESS_CODE ){
                olcom_server_form_message($messages[ 'status_created' ], 1);
            }else{
                    olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                }
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array(
                'with_submit_reset'  => TRUE,
                'title'  => $labels[ 'create_asset_status']
            )); 
            $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'status',
                'label'  => $labels['status']
            ));
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('create_asset_status_specific_scripts','',TRUE);
            
            $this -> load -> view('main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
     }
    /* 
      *  
      *  
      *  
      */
    function actions( ) 
        {
           
            $messages = $this -> config -> item('messages');
                   
            if( $this -> olcomaccessengine -> is_allowed_module_action($this -> uri -> uri_string() )  ===  FALSE){
                
                
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                          'message'  => $messages[ 'permission_denied_'.$this -> uri  -> segment( 4 ) ]
                                        ), 'error');
                return ;                        
            }
          
                $section = $this -> uri -> segment(3);
                $this -> load -> config('olcom_cookies');
                
                
                $labels = $this -> config -> item('labels');
                
                $olcom_cookies = $this -> config -> item('action_cookies');
                $cookie_save = $olcom_cookies['action_save_click'];
                $cookie_status = $olcom_cookies['action_delete_confirm'];
            /* 
              *  
              *  
              *  
              */
                if( $section === 'asset_categories' )
                {
                    switch( $this -> uri -> segment( 4 ) )
                    {
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){//clicked edit
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $result = $this -> assets_model -> get_asset_category( $this -> uri -> segment( 5 ) );
                                //load update form
                                $this -> load -> library('OlcomHmsTemplateForm',array(
                                    'title'  => $labels['edit_category']
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'category',
                                    'label'  => $labels['category_name'],
                                    'value'  => $result[ 'category' ]
                                ));
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_asset_category_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_asset_category( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['dialog_delete_party'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_asset_category( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                }
            /* 
              *  
              *  
              *  
              */ 
            if( $section === 'assets_status' )
                {
                    switch( $this -> uri -> segment( 4 ) )
                    {
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){//clicked edit
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $result = $this -> assets_model -> get_asset_status( $this -> uri -> segment( 5 ) );
                                //load update form
                                $this -> load -> library('OlcomHmsTemplateForm',array(
                                    'title'  => $labels['edit_status']
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'status',
                                    'label'  => $labels['status'],
                                    'value'  => $result[ 'status' ]
                                ));
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_asset_status_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_asset_status( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_record'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_asset_status( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                }
              /* 
                *  
                *  
                *  
                *  
                */ 
            if( $section === 'maintenance_parties' )
                {
                    switch( $this -> uri -> segment( 4 ) )
                    {
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){//clicked edit
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $result = $this -> assets_model -> get_party_info( $this -> uri -> segment( 5 ) );
                                //load update form
                                $this -> load -> library('OlcomHmsTemplateForm',array( 
                                     
                                    'title'  => $labels[ 'edit_maintenance_party']
                                )); 
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'name',
                                    'label'  => $labels['name'],
                                    'value' => $result[ 'name' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'phone',
                                    'label'  => $labels['phone'],
                                    'id' => 'phone',
                                    'value' => $result[ 'phone' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'email',
                                    'label'  => $labels['email'],
                                    'value' => $result[ 'email']
                                ));
                                $this -> olcomhmstemplateform -> add_field('textarea',array(
                                    'name'  => 'other_contacts',
                                    'label'  => $labels['other_contacts'],
                                    'value' => $result[ 'other_contacts']
                                ));
                                $form = $this -> olcomhmstemplateform -> create_form();
                                
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_party_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_maintenance_party( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['dialog_delete_party'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_party( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                }
                /* 
                  * 
                  *  
                  *   
                  */
                 if( $section === 'maintenance_schedule' )
                {
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    switch( $this -> uri -> segment( 4 ) )
                    {
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){//clicked edit
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $result = $this -> assets_model -> get_maintenance_schedule( $this -> uri -> segment( 5 ) );
                                $asset_categories = $this -> assets_model -> get_asset_category( );
                                //load update form
                                $this -> load -> library('OlcomHmsTemplateForm',array( 
                                     
                                    'title'  => $labels[ 'edit_maintenance_schedule']
                                )); 
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'duration',
                                    'label'  => $labels['duration'],
                                    'value' => $result[ 'duration' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'period',
                                    'label'  => $labels['period'],
                                    'choices' => $selects[ 'maintenance_periods'],
                                    'selected' => $result[ 'period' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'asset_category_id',
                                    'label'  => $labels['asset_category'],
                                    'choices' => $asset_categories,
                                    'selected' => $result[ 'asset_category_id' ]
                                ));
                                $form = $this -> olcomhmstemplateform -> create_form();
                                
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_party_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_maintenance_schedule( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['dialog_delete_party'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_schedule( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                } 
                /* 
                  *  
                  *  
                  *  
                  */
                 if( $section === 'assets' )
                {
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    $labels = $this -> config -> item('labels');
                    switch( $this -> uri -> segment( 4 ) )
                    {
                        
                        case 'view' : 
                                   $result = $this -> assets_model -> get_asset_info( $this -> uri -> segment( 5 ) );
                                   
                                   $result[ 'purchase_price' ] = 'Tzs. '. number_format( $result[ 'purchase_price' ] , 2 );
                                   $result[ 'total_maintenance_cost' ] = 'Tzs. '.number_format( $this -> assets_model -> get_asset_total_maintenance( $result[ 'serial_number' ] ) ,2 );
                                   
                                   // next scheduled maintenance
                                   $maintenance_schedule = $this -> assets_model -> get_maintenance_schedule( NULL, $result[ 'category_id' ] );
                                   unset( $result[ 'status_id' ] );
                                   unset( $result[ 'category_id' ] );
                                   unset( $result[ 'assets_serial_number' ] );
                                   unset( $result[ 'department_id' ] ) ;
                                   // schedule days 
                                   $schedule_days = 0;
                                   $next_maintenance_date = NULL;
                                   $time_left = NULL;
                                   if( $maintenance_schedule != NULL )
                                   {
                                       $duration = $maintenance_schedule[ 'duration' ];
                                       $period = $maintenance_schedule[ 'period' ];
                                       
                                       // last maintenance      
                                       $last_maintenance = $this -> assets_model -> get_last_asset_maintenance( $result[ 'serial_number' ] );
                                      
                                       if( $last_maintenance !== NULL )
                                       {
                                            $last_maintenance = strtotime( $last_maintenance[ 'maintenance_date'] );    
                                       
                                       
                                       
                                           $schedule = '';
                                           switch( $period )
                                           {
                                               case 'Days(s)' :
                                                    $period = 'days';
                                                break;
                                               case 'Month(s)' :
                                                   $period = 'months';
                                                break;
                                               case 'Year(s)' :
                                                   $period = 'years';
                                                break;
                                               case 'Week(s)' : 
                                                   $period = 'weeks';
                                                break;
                                           }
                                           
                                           $next_maintenance_date = strtotime("+".$duration."".$period , $last_maintenance );
                                           $next_maintenance_date = date('M d, Y', $next_maintenance_date );
                                            
                                           $diff = abs(strtotime(date('Y-m-d')) - strtotime( $next_maintenance_date ) );

                                            $years = floor($diff / (365 * 60 * 60 * 24));
                                            $months = floor(($diff - $years  *  365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($diff - $years  *  365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24)/ (60 * 60 * 24));
                                            
                                            $time_left = $years. ' Years '.$months.' Months '.$days. ' Days';
                                            
                                        }else{
                                            
                                            $next_maintenance_date = '-';
                                        }
                                   }
                                   $result[ 'next_maintenance_schedule' ] = $next_maintenance_date;
                                   
                                   if( $time_left != NULL )
                                   {
                                       $result[ 'time_left'] = $time_left;
                                   }
                                   $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['maintenance_history'], 'with_actions' => FALSE,
                                    'id_suffix' =>'maintenance',
                                    'extra_params' => array( 'name' => 'serial_number' , 'value' => $this -> uri -> segment( 5 )  ),
                                      'columns' => array( $labels[ 'id' ] , $labels[ 'maintenance_date' ] , $labels[ 'maintenance_description' ],
                                      $labels[ 'maintenance_party'] , $labels[ 'maintenance_cost' ]),
                                      'controller_fx' => 'assets_mngt/asset_maintenance_history_data'));
                            
                                    $datatable = $this -> olcomhmstemplatedatatable -> create_view();
                                   olcom_show_dialog('dynamic_table',
                                        array(
                                            'table_content' => $result,
                                            'dynamic_content' => array(
                                                'maintenance' => $datatable
                                            )
                                        )
                                    , NULL );
                                   
                            break; 
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){ 
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $asset_info = $this -> assets_model -> get_asset_info( $this -> uri -> segment( 5 ) );
                                
                                
                                $asset_categories = $this -> assets_model -> get_asset_category( );
                                $assets_status = $this -> assets_model -> get_asset_status();
                                $this -> load -> model( 'humanresources_model' );
                                $departments = $this -> humanresources_model -> get_departments();
                                $this -> load -> library('OlcomHmsTemplateForm',array(
                                    'title'  => $labels[ 'edit_asset']
                                )); 
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'category_id',
                                    'label'  => $labels['asset_category'],
                                    'choices' => $asset_categories,
                                    'selected' => $asset_info[ 'category_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'status_id',
                                    'label'  => $labels['status'],
                                    'choices' => $assets_status,
                                    'selected' => $asset_info[ 'status_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'department_id',
                                    'label'  => $labels['department_location'],
                                    'choices' => $departments,
                                    'selected' => $asset_info[ 'department_id' ] 
                                ));
                               $this -> olcomhmstemplateform -> add_field('text',array(
                                        'name'  => 'serial_number',
                                        'label'  => $labels['serial_number'],
                                        'value' => $asset_info[ 'serial_number' ]
                                    ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'date_located',
                                    'label'  => $labels['date_located'],
                                    'class' => 'datepicker',
                                    'readonly' => '' ,
                                    'value' => isset( $asset_info[ 'date_located' ] ) ? $asset_info[ 'date_located' ] : NULL  
                                ));
                                 $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'purchase_price',
                                    'label'  => $labels['purchase_price'],
                                    'value' => $asset_info[ 'purchase_price' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'model',
                                    'label'  => $labels['model'],
                                    'value' => $asset_info[ 'model' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'date_acquired',
                                    'label'  => $labels['date_acquired'],
                                    'class' => 'datepicker',
                                    'readonly' => '' ,
                                    'value' => $asset_info[ 'date_acquired']
                                ));
                                $this -> olcomhmstemplateform -> add_field('textarea',array(
                                    'name'  => 'description',
                                    'label'  => $labels['description'],
                                    'value' => $asset_info[ 'description' ]
                                ));
                                $form = $this -> olcomhmstemplateform -> create_form();
                                
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_asset_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_asset_info( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['dialog_delete_party'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_asset( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                }
                /* 
                  * 
                  *   
                  */
                 if( $section === 'assets_maintenance' )
                {
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    switch( $this -> uri -> segment( 4 ) )
                    {
                        
                        case 'view' : 
                                   
                                   $maintenance_record = $this -> assets_model -> get_maintenance_record( $this -> uri -> segment( 5 ) );
                                   
                                   $maintenance_record[ 'maintenance_cost' ] = 'Tzs. '. number_format( $maintenance_record[ 'maintenance_cost' ] , 2 );
                                   unset( $maintenance_record[ 'serial_number' ] );
                                   unset( $maintenance_record[ 'party_id' ] );
                                   unset( $maintenance_record[ 'maintenance_id' ] );
                                   
                                   olcom_show_dialog('table', array( $maintenance_record ), '');
                            break; 
                       case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){//clicked edit
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $maintenance_record = $this -> assets_model -> get_maintenance_record( $this -> uri -> segment( 5 ) );
                                $assets = $this -> assets_model -> get_assets( );
                                $parties  = $this -> assets_model -> get_party_info( );
                               // print_r( $parties );
                                if( $assets != NULL )
                                {
                                    $assets_choices = array();
                                    foreach( $assets  as $asset )
                                    {
                                       $assets_choices[ $asset[ 'serial_number' ] ]=  $asset[ 'serial_number' ] .' / '. $asset[ 'model' ] .' / 
                                       '.$asset[ 'description'];
                                    }
                                    
                                    $assets = $assets_choices ;
                                }
                                
                                if( $parties != NULL )
                                {
                                    $parties_choices = array();
                                    foreach( $parties as $party )
                                    {
                                        $parties_choices[ $party[ 'party_id' ] ] = $party[ 'name' ] ;
                                    }     
                                    $parties = $parties_choices;
                                }
                                $this -> load -> library('OlcomHmsTemplateForm',array(
                                    'title'  => $labels[ 'edit_maintenance_record' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'serial_number',
                                    'label'  => $labels['asset'],
                                    'choices' => $assets,
                                    'selected' => $maintenance_record[ 'serial_number']
                                ));
                                $this -> olcomhmstemplateform -> add_field('select',array(
                                    'name'  => 'party_id',
                                    'label'  => $labels['maintenance_party'],
                                    'choices' => $parties,
                                    'selected' => $maintenance_record[ 'party_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'maintenance_cost',
                                    'label'  => $labels['maintenance_cost'],
                                    'value' => $maintenance_record[ 'maintenance_cost']
                                ));
                               $this -> olcomhmstemplateform -> add_field('text',array(
                                    'name'  => 'maintenance_date',
                                    'label'  => $labels['maintenance_date'],
                                    'class' => 'datepicker',
                                    'readonly' => '',
                                    'value' => $maintenance_record[ 'maintenance_date']
                                ));
                               $this -> olcomhmstemplateform -> add_field('textarea',array(
                                    'name'  => 'maintenance_description',
                                    'label'  => $labels['maintenance_description'],
                                    'value' => $maintenance_record[ 'maintenance_description' ]
                                ));
                                $form = $this -> olcomhmstemplateform -> create_form();
                                
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_maintenance_record_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                
                                
                                $result = $this  ->  assets_model  ->  update_maintenance_record( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_record'],'');
                            }else{
                                
                                $result = $this  ->  assets_model  ->  delete_maintenance_record( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
                }

            }

        /* 
          *  
          *  
          *  
          */
         function maintenance_parties()
         {
            $labels = $this -> config -> item('labels');
    
            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['maintenance_parties'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'assets_mngt/create_maintenance_party',
             'columns' => array($labels['id'], $labels['name'], $labels[ 'phone' ],$labels[ 'email' ],$labels[ 'other_contacts'] ), 'controller_fx' => 'assets_mngt/maintenance_parties_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
         }
         /* 
           *  
           *  
           */
          function maintenance_parties_data( )
          {
               $this -> load -> library('OlcomHmsDataTables', 
                array('columns' => array('party_id', 'name', 'phone' , 'email' , 'other_contacts', 'party_id'),
                 'index_column' => 'party_id',
                 'table_name' => 'third_parties', 'controller_fx' => 'assets_mngt/actions/maintenance_parties',
                  'id_type' => ''));
        
                //get the data
                echo $this -> olcomhmsdatatables -> get_data();
          }
          /* 
            *  
            *  
            */
           function create_maintenance_party()
           {
               $form_data = $this -> input -> post(NULL);
                $messages = $this -> config -> item('messages');
                $labels = $this -> config -> item('labels');
                
                if(isset($form_data) && $form_data!= NULL){
                  
                    $result = $this  ->  assets_model  ->  create_maintenance_party( $form_data );
                    
                    if( $result === SUCCESS_CODE ){
                        olcom_server_form_message($messages[ 'party_created' ], 1);
                    }else{
                            olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                        }
                    
                }else{
                    $this -> load -> library('OlcomHmsTemplateForm',array(
                         
                        'with_submit_reset'  => TRUE,
                        'title'  => $labels[ 'create_maintenance_party']
                    )); 
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'name',
                        'label'  => $labels['name']
                    ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'phone',
                        'label'  => $labels['phone'],
                        'id' => 'phone'
                    ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'email',
                        'label'  => $labels['email']
                    ));
                    $this -> olcomhmstemplateform -> add_field('textarea',array(
                        'name'  => 'other_contacts',
                        'label'  => $labels['other_contacts']
                    ));
                    $form = $this -> olcomhmstemplateform -> create_form();
                    
                    $form_specific_scripts = $this -> load -> view('create_party_specific_scripts','',TRUE);
                    
                    $this -> load -> view('main_template',array(
                        'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                        
                    ));
                }
           }
        /* 
          *  
          *  
          *  
          */
         function maintenance_schedules()
         {
             $labels = $this -> config -> item('labels');
    
            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['maintenance_schedules'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'assets_mngt/create_maintenance_schedule',
             'columns' => array($labels['id'], $labels['duration'], $labels[ 'period' ], $labels[ 'category' ] ),
              'controller_fx' => 'assets_mngt/maintenance_schedules_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
             
         }
         /* 
           *  
           *  
           */
          function maintenance_schedules_data()
          {
            $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  => array(
                    'maintenance_schedule'  => array('schedule_id','duration','period'),
                    'asset_categories'  => array('category')
                    ),
                    'with_actions'  => TRUE,
                    'hidden_cols'  => array(),
                    'index_column'  => 'schedule_id',
                    'controller_fx'  => 'assets_mngt/actions/maintenance_schedule',//for actions
                    'id_type'  => '',
                    'where'  => array('maintenance_schedule.asset_category_id'  => 'asset_categories.category_id'),
                    'data_style_format'  => NULL,
                    'order'  => array('duration'  => 1,'period'  => 2 , 'category' => 3)
                 )
             );
            echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
          }
          
         /* 
           *  
           *  
           *  
           */
          function create_maintenance_schedule( )
          {
               $form_data = $this -> input -> post(NULL);
                $messages = $this -> config -> item('messages');
                $labels = $this -> config -> item('labels');
                
                if(isset($form_data) && $form_data!= NULL){
                  
                    $result = $this  ->  assets_model  ->  create_maintenance_schedule( $form_data );
                    
                    if( $result === SUCCESS_CODE ){
                        olcom_server_form_message($messages[ 'schedule_created' ], 1);
                    }else{
                            olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                        }
                    
                }else{
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    $asset_categories = $this -> assets_model -> get_asset_category( );
                   
                    $this -> load -> library('OlcomHmsTemplateForm',array(
                         
                        'with_submit_reset'  => TRUE,
                        'title'  => $labels[ 'create_maintenance_schedule']
                    )); 
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'duration',
                        'label'  => $labels['duration']
                    ));
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'period',
                        'label'  => $labels['period'],
                        'choices' => $selects[ 'maintenance_periods']
                    ));
                    
                    if( count( $asset_categories ) == 0 )
                        $asset_categories = array();
                    
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'asset_category_id',
                        'label'  => $labels['asset_category'],
                        'choices' => $asset_categories
                    ));
                   
                    $form = $this -> olcomhmstemplateform -> create_form();
                    
                    $form_specific_scripts = $this -> load -> view('create_schedule_specific_scripts','',TRUE);
                    
                    $this -> load -> view('main_template',array(
                        'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                        
                    ));
                }        
          }
      /* 
        *  
        *  
        *  
        */
       function assets()
       {
           $labels = $this -> config -> item('labels');
    
            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['assets'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'assets_mngt/create_asset',
             'columns' => array(  $labels[ 'serial_number' ],$labels['description'], $labels[ 'model' ],
             $labels[ 'purchase_price'] ,$labels[ 'date_acquired' ] ,$labels[ 'status'], $labels[ 'category'] ),
              'controller_fx' => 'assets_mngt/assets_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
       }
       /* 
         *  
         *  
         */
        function assets_data()
        {
            $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  => array(
                    'assets'  => array('serial_number','description','model','purchase_price','date_acquired'),
                    'assets_status'  => array('status'),
                    'asset_categories' => array( 'category' )
                    ),
                    'with_actions'  => TRUE,
                    'hidden_cols'  => array(),
                    'index_column'  => 'serial_number',
                    'controller_fx'  => 'assets_mngt/actions/assets',//for actions
                    'id_type'  => '',
                    'where'  => array('assets.status_id'  => 'assets_status.status_id',
                        'assets.category_id' => 'asset_categories.category_id'
                    ),
                    'data_style_format'  => array(
                        'purchase_price' =>array(
                            'money' => TRUE
                        )
                    ),
                   'order'  => array('serial_number' => 0 ,'description'  => 1,'model'  => 2 , 'purchase_price' => 3,'date_acquired' => 4 ,'status' => 5, 'category' => 6 )
                 )
             );
            echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
        }
        /* 
          *  
          *  
          *  
          */
         function create_asset( )
         {
                $form_data = $this -> input -> post(NULL);
                $messages = $this -> config -> item('messages');
                $labels = $this -> config -> item('labels');
                
                if(isset($form_data) && $form_data!= NULL){
                  
                    $result = $this  ->  assets_model  ->  create_asset( $form_data );
                    
                    if( $result === SUCCESS_CODE ){
                        olcom_server_form_message($messages[ 'asset_created' ], 1);
                    }else{
                            olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                        }
                    
                }else{
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    $asset_categories = $this -> assets_model -> get_asset_category( );
                    $assets_status = $this -> assets_model -> get_asset_status();
                    if( count( $assets_status ) == 0 )
                    {
                        $assets_status = array();
                    }
                    $this -> load -> model( 'humanresources_model' );
                    $departments = $this -> humanresources_model -> get_departments();
                    $this -> load -> library('OlcomHmsTemplateForm',array(
                         
                        'with_submit_reset'  => TRUE,
                        'title'  => $labels[ 'create_asset']
                    )); 
                    
                    if( count( $asset_categories ) == 0 )
                        $asset_categories = array();
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'category_id',
                        'label'  => $labels['asset_category'],
                        'choices' => $asset_categories
                    ));
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'status_id',
                        'label'  => $labels['status'],
                        'choices' => $assets_status
                    ));
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'department_id',
                        'label'  => $labels['department_location'],
                        'choices' => $departments
                    ));
                   $this -> olcomhmstemplateform -> add_field('text',array(
                            'name'  => 'serial_number',
                            'label'  => $labels['serial_number']
                        ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'date_located',
                        'label'  => $labels['date_located'],
                        'class' => 'datepicker',
                        'readonly' => '' 
                    ));
                     $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'purchase_price',
                        'label'  => $labels['purchase_price']
                    ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'model',
                        'label'  => $labels['model']
                    ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'date_acquired',
                        'label'  => $labels['date_acquired'],
                        'class' => 'datepicker',
                        'readonly' => '' 
                    ));
                    $this -> olcomhmstemplateform -> add_field('textarea',array(
                        'name'  => 'description',
                        'label'  => $labels['description']
                    ));
                    
                    $form = $this -> olcomhmstemplateform -> create_form();
                    
                    $form_specific_scripts = $this -> load -> view('create_asset_specific_scripts','',TRUE);
                    
                    $this -> load -> view('main_template',array(
                        'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                        
                    ));
                }   
         }  
        /* 
          *  
          *  
          */
         function assets_maintenance() 
         {
            $labels = $this -> config -> item('labels');
    
            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['assets_maintenance'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'assets_mngt/create_maintenance_record',
             'columns' => array( $labels[ 'id' ] , $labels[ 'serial_number' ],$labels['description'], $labels[ 'maintenance_cost' ],
             $labels[ 'maintenance_party'] ,$labels[ 'maintenance_date' ] ),
              'controller_fx' => 'assets_mngt/assets_maintenance_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
         }
         
         /* 
           *  
           *  
           *  
           */
          function assets_maintenance_data()
          {
              $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'assets_maintenance' => array( 'maintenance_id' , 'maintenance_cost','maintenance_date' ),
                        'assets'  => array('serial_number','description' ),
                        'third_parties'  => array('name')
                        ),
                        'with_actions'  => TRUE,
                        'hidden_cols'  => array(),
                        'index_column'  => 'maintenance_id',
                        'controller_fx'  => 'assets_mngt/actions/assets_maintenance',//for actions
                        'id_type'  => '',
                        'where'  => array('assets_maintenance.serial_number'  => 'assets.serial_number',
                            'assets_maintenance.party_id' => 'third_parties.party_id'
                        ),
                        'data_style_format'  => array(
                            'maintenance_cost' =>array(
                                'money' => TRUE
                            )
                        ),
                       'order'  => array('serial_number' => 1 ,'description'  => 2,'maintenance_cost'  => 3 , 'name' => 4,'maintenance_date' => 5 )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
          }
        /* 
          *  
          *  
          *  
          */
         function create_maintenance_record()
         {
             $form_data = $this -> input -> post(NULL);
                $messages = $this -> config -> item('messages');
                $labels = $this -> config -> item('labels');
                
                if(isset($form_data) && $form_data!= NULL){
                  
                    $result = $this  ->  assets_model  ->  create_maintenance_record( $form_data );
                    
                    if( $result === SUCCESS_CODE ){
                        olcom_server_form_message($messages[ 'maintenance_record_created' ], 1);
                    }else{
                            olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                        }
                    
                }else{
                    
                    $this -> load -> config( 'olcom_selects' );
                    $selects = $this -> config -> item( 'selects' );
                    $assets = $this -> assets_model -> get_assets( );
                    $parties  = $this -> assets_model -> get_party_info( );
                   // print_r( $parties );
                    if( $assets != NULL )
                    {
                        $assets_choices = array();
                        foreach( $assets  as $asset )
                        {
                           $assets_choices[ $asset[ 'serial_number' ] ]=  $asset[ 'serial_number' ] .' / '. $asset[ 'model' ] .' / 
                           '.$asset[ 'description'];
                        }
                        
                        $assets = $assets_choices ;
                    }
                    
                    if( $parties != NULL )
                    {
                        $parties_choices = array();
                        foreach( $parties as $party )
                        {
                            $parties_choices[ $party[ 'party_id' ] ] = $party[ 'name' ] ;
                        }     
                        $parties = $parties_choices;
                    }
         
                    $this -> load -> library('OlcomHmsTemplateForm',array(
                         
                        'with_submit_reset'  => TRUE,
                        'title'  => $labels[ 'create_maintenance_record']
                    )); 
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'serial_number',
                        'label'  => $labels['asset'],
                        'choices' => $assets
                    ));
                    $this -> olcomhmstemplateform -> add_field('select',array(
                        'name'  => 'party_id',
                        'label'  => $labels['maintenance_party'],
                        'choices' => $parties
                    ));
                    $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'maintenance_cost',
                        'label'  => $labels['maintenance_cost']
                    ));
                   $this -> olcomhmstemplateform -> add_field('text',array(
                        'name'  => 'maintenance_date',
                        'label'  => $labels['maintenance_date'],
                        'class' => 'datepicker',
                        'readonly' => ''
                    ));
                   $this -> olcomhmstemplateform -> add_field('textarea',array(
                        'name'  => 'maintenance_description',
                        'label'  => $labels['maintenance_description']
                    ));
                   
                    
                    $form = $this -> olcomhmstemplateform -> create_form();
                    
                    $form_specific_scripts = $this -> load -> view('create_maintenance_record_specific_scripts','',TRUE);
                    
                    $this -> load -> view('main_template',array(
                        'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                        
                    ));
                }
         }
        /* 
          *  
          *  
          *  
          */
         function asset_maintenance_history_data( )
         {
             
             $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'assets_maintenance' => array( 'maintenance_id' , 'maintenance_date','maintenance_description','maintenance_cost' ),
                        'third_parties'  => array('name')
                        ),
                        'with_actions'  => FALSE,
                        'hidden_cols'  => array(),
                        'index_column'  => 'maintenance_id',
                        'controller_fx'  => NULL,
                        'id_type'  => '',
                        'where'  => array('assets_maintenance.serial_number'  => $this -> input -> get( 'serial_number' ),
                            'assets_maintenance.party_id' => 'third_parties.party_id'
                        ),
                        'data_style_format'  => array(
                            'maintenance_cost' =>array(
                                'money' => TRUE
                            )
                        ),
                       'order'  => array('maintenance_date' => 1 ,'maintenance_description'  => 2,'name'  => 3 , 'maintenance_cost' => 4 )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
         }
}
