<?php if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
/*
 *@class - Prescriptions 
 * 
 * 26-09-2013
 */
 class Prescriptions_mngt extends CI_Controller
 {
     
     var $logged = FALSE;
     var $return_codes;
	 var $help;
     function Prescriptions_mngt()
     {
          
         parent:: __construct();
         
         $this -> load -> model( 'medicaments_model' );
         $this -> load -> config ('olcomhms_return_codes');
         $this -> load -> model( 'prescriptions_model' );
         $this -> return_codes = $this -> config -> item( 'return_codes' );
         
          if($this  ->  olcomaccessengine  ->  is_logged()  === TRUE)
          {
              
                olcom_is_allowed_module_helper( $this -> uri -> uri_string() ) ;
                
                $this -> is_logged = TRUE;
           }
           else
           {
            
            if( $this ->input -> is_ajax_request())
            {
                if(preg_match('/data/', $this -> uri -> uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
            
            $this->is_logged = FALSE;
            
            
        }
            if( ! defined( 'SUCCESS_CODE' ))
                define( 'SUCCESS_CODE',$this -> config -> item( 'success_code'));
            
         $this -> load -> config('olcom_messages'); 
         $this -> load -> config( 'olcom_labels' );
		 $this -> load -> config( 'olcom_help_text' );
		 $this -> help = $this -> config -> item( 'help_text' );
      }

	function index()
	{
			
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
    /*
     * 
     * 
     * 
     * 
     */
     function prescription_writting()
     {
          
        $form_data = $this -> input -> post( NULL );
       
        
        $messages = $this -> config ->item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL )
        {
          
          
            
        }else{
            $this->load->library('OlcomHmsTemplateForm',array(
                 
                'with_submit_reset'  => TRUE,
                'title'  => $labels[ 'prescription_writting'],
                'header_info'  => ''
            )); 
           
            // Prescribing doctor
            
            
            $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
            
            if( $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_admin_gid() OR 
                $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_super_admin_gid() )
                {
                    $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                        'name' => 'prescribing_doctor',
                        'label' => $labels[ 'prescribing_doctor' ],
                        'value' => NULL,
                        'readonly' => ''
                    ));
                }
             else{
                    $this -> load -> model( 'employees' );
                   
                    $employee_info = $this -> employees -> info( $logged_user[ 'employee_id' ],'basic',FALSE);
                      
                    $this -> olcomhmstemplateform -> add_field( 'text', array(
                        'name' => 'prescribing_doctor',
                        'label' => $labels[ 'prescribing_doctor'],
                        'value' => strtoupper($employee_info[ 'lastname' ] ).','.$employee_info[ 'firstname' ],
                        'readonly' => '' ,
                        
                    ));           
                }
                
            $this -> olcomhmstemplateform -> add_field('search',array(
                'name'  => 'patient',
                'label'  => $labels['patient'],
                'id' => 'patient_auto_complete',
                'placeholder' => 'Patient ID eg. P013' ,
                 'help' => $this -> help[ 'get_patient_id' ]
                 
            ));
            $this -> olcomhmstemplateform -> add_field( 'select' , array(
                'name' => 'diagnosis_id',
                'label' => $labels[ 'assoc_diagnosis' ],
                'choices' => array(),
                'js'  => 'id  = \'diagnosis_id\'' ,
                'help' => $this -> help[ 'assoc_diagnosis' ]
               
            ));
            $this -> olcomhmstemplateform -> add_field('checkbox' ,array(
                'name' => 'verified',
                'label' => $labels[ 'verified' ],
                'class' => 'ace-switch ace-switch-6',
                'value' => 1
            ));
            $this -> olcomhmstemplateform -> add_field( 'hidden' ,array(
                'name' => 'write_start_timestamp', 
                'value' => mktime()
            ));
            
            $this -> load -> library( 'OlcomHmsTemplateDatatable',array(
                    'header'  => $labels[ 'prescription_lines'],
                    'columns'  => array(
                        $labels[ 'medicament' ],$labels[ 'dose' ]
                        ,$labels[ 'unit'],$labels[ 'quantity' ],$labels['form']//,$labels[ 'start_date' ],$labels[ 'end_date' ]
                    ),
                    'with_actions'  => TRUE,
                    'controller_fx'  => 'prescriptions_mngt/px_line_data',
                    'create_controller_fx' =>'prescriptions_mngt/px_modal_form_data'
            ));
            $datatable_view = $this -> olcomhmstemplatedatatable -> create_inflate_view();
            
            $this -> olcomhmstemplateform -> inflate_view( $datatable_view[ 'datatable' ] );
            
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('prescriptions_specific_scripts','',TRUE);
            $form_specific_scripts.= $datatable_view[ 'specific_scripts' ];
            
            $this -> load -> view( 'main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
     }
    /*
     * 
     * 
     * 
     */
     function patient_autocomplete_data( $query = NULL )
     {
     	$patient_id = $this -> uri -> segment( 3 );
        
        $patients_data = array();
        if( isset( $patient_id ) AND $patient_id != NULL)
        {
            $patient_general = $this -> patients_model -> get_patient_general_info( olcom_extract_numbers( $patient_id ) );
            if( $patient_general[ 'deceased' ]  == 1 )
            {
                echo json_encode( array( 'result' => 0 ) );
                return ;
            }
            $patients_data = $this -> patients_model -> get_patients_name_id( $patient_id );
            
            if( is_array( $patients_data )  )
            {
                $patients_data = each( $patients_data );
                echo  json_encode( array( 'result' => 1,'key' => $patients_data[ 'key'],'value' => $patients_data[ 'value']  ));
                return ; 
            }
			echo json_encode( array( 'result' => 0));
            return ;
        }
     }
     /*
      * 
      * 
      * 
      */
      function px_line_data()
      {
            $logged_user = $this -> olcomaccessengine -> get_logged_in_user(); 
            $is_admin = FALSE;
            if( $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_admin_gid() OR 
                $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_super_admin_gid() )
                {
                    $is_admin = TRUE;
                }
                
            $where = array('medicaments.medicament_id'  => 'prescription_lines_tempo.medicament_id',
                                'prescription_lines_tempo.unit_id' => 'drug_dose_units.unit_id',
                                'prescription_lines_tempo.form_id' => 'drug_forms.form_id'
                                );
            if( $is_admin == FALSE )
            {
                $where['prescription_lines_tempo.prescribing_doctor' ] = $logged_user[ 'employee_id' ];
            }
            
            $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  =>  array(
                    'prescription_lines_tempo'  =>  array('line','dose','quantity'),
                    'medicaments'  =>  array('active_component'),
                    'drug_dose_units' => array( 'unit' ),
                    'drug_forms' => array('form')
                    
                    ),
                'with_actions'  => TRUE,
                'hidden_cols'  => array(),
                'index_column'  => 'line',
                'controller_fx'  => 'prescriptions_mngt/actions/prescription_writting',//for actions
                'id_type'  => '',
                'where'  => $where,
                'order'  => array('active_component' => 0,'dose'  => 1,'unit' => 2,
                                  'quantity'  => 3,'form' => 4 )
            )
        );
        echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
          
      }
      /*
       * 
       * 
       * 
       */
       function px_modal_form_data()
       {
       
            $this -> load -> config( 'olcom_selects' );
            $messages = $this -> config -> item('messages' );
            $labels = $this-> config-> item('labels');
            $selects = $this -> config -> item( 'selects' );
            
            $this->load->library('OlcomHmsTemplateForm',array( 
                'title'  => $labels[ 'new_px_line'],
                'id' => 'olcom-px-line-form' 
            ));
            $this -> olcomhmstemplateform -> add_field( 'search', array(
                'name' => 'medicament_id',
                'label' => $labels[ 'medicament' ],
                'id' => 'medicament',
                'help' => $this -> help[ 'medicament_id' ]
            ));
            
            $result = $this -> olcomhms -> read( 'drug_forms',array( 'form_id','form'),NULL);
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'form_id',
                'label' => $labels[ 'form' ],
                'choices' => olcom_db_to_dropdown( $result ),
                'js' => 'id=drug_forms'
            ));
            $result = $this -> olcomhms -> read( 'administration_routes' ,array( 'route_id','route' ),NULL );
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'route_id',
                'label' => $labels[ 'admin_route' ],
                'choices' => olcom_db_to_dropdown( $result )
            ));
            /*$this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'start_end_date',
                'label' => $labels[ 'start_end_date' ],
                'class' => 'date-range-picker',
                'readonly' => ''
            ));*/ 
            $this -> olcomhmstemplateform -> add_field( 'checkbox', array(
                'name' => 'allow_substitution',
                'label' => $labels[ 'allow_substitution' ],
                'value' => 1,
                'class' => 'ace-switch ace-switch-5'
            ));
            $this -> olcomhmstemplateform -> add_group_title( $labels[ 'dosage' ] );
            
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'dose',
                'label' => $labels[ 'medication_amount' ],
                'choices' => array(),
                'js' => "id='amounts' "
            ));
            $result = $this -> olcomhms -> read( 'drug_dose_units' , array( 'unit_id' , 'unit' ),NULL );
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'unit_id',
                'label' => $labels[ 'medication_unit' ],
                'choices' => olcom_db_to_dropdown( $result ),
                'js' => "id='dose_units'"
            ));
            $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'quantity',
                'label' => $labels[ 'quantity' ],
                'placeholder' => 'Number of tablets,capsules etc'
            ));
            $this -> olcomhmstemplateform -> add_group_title( $labels[ 'common_dosage' ] );
            $result = $this -> olcomhms -> read( 'medication_dosage', array( 'dosage_id' , 'dosage' ),NULL );
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'frequency_id',
                'label' => $labels[ 'dosage_frequency' ],
                'choices' => olcom_db_to_dropdown( $result )
            ));
            /*$this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'admin_hours',
                'label' => $labels[ 'admin_hours' ],
                'placeholder' => ' eg. 10,14 for 10:00,14:00'
            ));
            $this -> olcomhmstemplateform -> add_group_title( $labels[ 'specific_dosage' ] );
			 
			*/
            $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'treatment_duration',
                'label' => $labels[ 'treatment_duration' ]
            ));
			
            $this -> olcomhmstemplateform -> add_field( 'select', array(
                'name' => 'treatment_period',
                'label' => $labels[ 'treatment_period' ],
                'choices' => $selects[ 'period' ]
            ));
			/*
            $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'refills',
                'label' => $labels[ 'refills' ]
            ));
            $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'units',
                'label' => $labels[ 'num_units' ],
                'placeholder' => '10 capsules of morphine'
            ));
             $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'review_date',
                'label' => $labels[ 'review_date' ],
                'id' => 'review_date',
                'readonly' => ''
            ));
			 */ 
			
            $this -> olcomhmstemplateform -> add_field( 'text', array(
                'name' => 'comments',
                'label' => $labels[ 'comments' ]
            ));
            olcom_show_dialog('form',array( 'content' => $this -> olcomhmstemplateform -> create_form(),
            'specific_scripts' =>'prescription_line_specific_scripts' ,'save_edit' => 'save' ) , NULL); 
       }
       /*
        */
       	function medicament_px_data()
        {
            $query = $this -> input -> get('query');
            
            $result = $this -> medicaments_model -> get_medicament_info_by_name ( $query, 'autocomplete', FALSE );
            if( $result !== NULL )
            {
                $medicaments = array( );
               // print_r($result);
                foreach( $result as $medicament )
                {
                	if( !isset( $medicament[ 'expiry_date' ] ) ){
                		$medicaments[ ] = array( 'data' => $medicament[ 'medicament_id' ], 'value' =>  '( '.$medicament[ 'medicament_id' ] .' ) '.$medicament[ 'active_component' ].'/ NA' );
                	}
					else
						 $medicaments[ ] = array( 'data' => $medicament[ 'medicament_id' ], 'value' => '( '.$medicament[ 'medicament_id' ] .' ) '.$medicament[ 'active_component' ].'/'. $medicament[ 'expiry_date' ]);
                }
                
                echo  json_encode( array( 'query' => 'Unit', 'suggestions' => $medicaments ) );
            }
            else
             echo json_encode( array( 'query' => 'Unit' , 'suggestions' => array()));
        }  
       /* 
        * 
        */
        function medicament_data()
        {
            $query = $this -> input -> get('query');
            
            $result = $this -> medicaments_model -> search_medicaments ( $query);
            if( $result !== NULL )
            {
                $medicaments = array( );
                foreach( $result as $medicament )
                {
                    $medicaments[ ] = array( 'data' => $medicament[ 'medicament_id' ], 'value' => "(".$medicament[ 'medicament_id' ]. ")  ".$medicament[ 'active_component' ]);
                }
                
                echo  json_encode( array( 'query' => 'Unit', 'suggestions' => $medicaments ) );
            }
            else
             echo json_encode( array( 'query' => 'Unit' , 'suggestions' => array()));
        }
       /*
        * 
        * 
        * 
        */
        function indication_data( )
        {
            $query = $this -> input -> get('query');
            $this -> load -> model( 'prescriptions_model' );
            $result = $this -> prescriptions_model -> get_indication_by_name ( $query );
            if( $result !== NULL )
            {
                $indications = array( );
                
                foreach( $result as $indication )
                {
                    $indications[ ] = array( 'data' => $indication[ 'disease_id' ], 
                    'value' => '( '.$indication[ 'disease_id' ].' ) '.
                    $indication[ 'disease_name' ] );
                }
                
                echo  json_encode( array( 'query' => 'Unit', 'suggestions' => $indications ) );
            }
            else
             echo json_encode( array( 'query' => 'Unit' , 'suggestions' => array()));
        }
        /*
         * 
         * 
         * 
         */
         function write_px_line_data( )
         {
            
            $form_data = $this -> input -> post( NULL );
            // retrieve prescribing doctor 
            $logged_info = $this -> olcomaccessengine -> get_logged_in_user();
            $messages = $this -> config -> item( 'messages' );
            $form_data[ 'prescribing_doctor' ] = $logged_info[ 'employee_id' ];
            // format admin hours
            if( $form_data[ 'admin_hours' ] != NULL )
            {
                $admin_hours_data = explode(',' ,$form_data[ 'admin_hours'] );
                $admin_hours = '';
                
                if( $admin_hours_data != NULL )
                {
                    foreach( $admin_hours_data  as $index => $admin_hour )
                    {
                        if( intval( $admin_hour ) >= 00 AND intval( $admin_hour ) <= 23 )
                        {
                             $admin_hours .= $admin_hour.':00';
                             
                             if( $index != count( $admin_hours_data ) - 1 )
                             {
                                 $admin_hours.=',';
                             }
                        }   
                        
                    } 
                    $form_data[ 'admin_hours' ] = $admin_hours;
                }
                else
                    {
                        $form_data[ 'admin_hours' ] = NULL;
                    }   
                  
            }
            else
                {
                    $form_data[ 'admin_hours' ] = NULL;
                }
            
            // treatment duration not set remove treatmet period 
            if( $form_data[ 'treatment_duration' ] == NULL )
            {
                $form_data[ 'treatment_period' ] = NULL;
            }
            
            $write_start_timestamp = $form_data[ 'write_start_timestamp' ];
            $start_end_date = $form_data[ 'start_end_date' ];
            
            $start_end_date = str_split( $start_end_date ,13 );
            
           
            $start_date = str_split( $start_end_date[ 0 ] , 10 );
            $form_data[ 'start_date' ] = $start_date[ 0 ];
            $form_data[ 'end_date' ] = $start_end_date[ 1 ];
         
            $form_data[ 'treatment_duration' ] .= ' '.$form_data[ 'treatment_period' ];
            $form_data[ 'allow_substitution' ] = isset( $form_data[ 'allow_substitution' ] )  ? 1 : 0;
            
            $form_data[ 'medicament_id' ] = olcom_extract_numbers( $form_data[ 'medicament_id' ] );
            //$form_data[ 'disease_id' ] = olcom_extract_numbers( $form_data[ 'disease_id' ] );
            //unset CSRF value
            unset( $form_data[ 'olcomhms_csrf' ] );
            unset( $form_data[ 'patient' ] );
            unset( $form_data[ 'start_end_date' ] );
            unset( $form_data[ 'treatment_period' ] );
            unset( $form_data[ 'verified' ] );
            unset( $form_data[ 'diagnosis_id' ] );
            
            
            $result = $this -> prescriptions_model -> add_tempo_prescription_line( $write_start_timestamp,$form_data );
            
            if( $result === SUCCESS_CODE )
            {
                
               // olcom_show_dialog('message', $messages[ 'operation_successful'], 'info' );
                
            }
            else {
                $this -> load -> config( 'olcom_messages' );
                $messages = $this -> config -> item( 'messages' );
                //olcom_show_dialog('message', array( 'message' =>   $this -> return_codes[ $result ],
                //'header' => $messages[ 'error']), 'error' );
            }
         }
       /*
        * 
        * 
        * 
        */
        function actions( ) 
        {
           
            $messages = $this -> config -> item('messages');
                   
            if( $this -> olcomaccessengine -> is_allowed_module_action($this -> uri -> uri_string() )  ===  FALSE){
                
                
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                          'message'  => $messages[ 'permission_denied_'.$this -> uri ->segment( 4 ) ]
                                        ), 'error');
                return ;                        
            }
          
            $section = $this->uri->segment(3);
            $this->load->config('olcom_cookies');
            
            
            $labels = $this->config->item('labels');
            
            $olcom_cookies = $this->config->item('action_cookies');
            $cookie_save = $olcom_cookies['action_save_click'];
            $cookie_status = $olcom_cookies['action_delete_confirm'];
        
            if( $section === 'prescription_writting' )
            {
                switch( $this -> uri -> segment( 4 ) )
                {
                    case 'view':
                            $result = $this -> prescriptions_model -> get_temp_px_line_info( $this -> uri ->
                            segment( 5 ) );
                            olcom_show_dialog('table', array( $result ), NULL );
                        break;
                    case 'edit':
                                
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE)
                            {
                                 
                                olcom_actions_cookies('edit', 'create');
                                $result = $this -> prescriptions_model -> get_temp_px_line_info( $this -> uri
                                 -> segment( 5 ) ,TRUE);
                                 
                                
                                $this -> load -> config( 'olcom_selects' );
                                $messages = $this -> config -> item('messages' );
                                $labels = $this -> config -> item('labels');
                                $selects = $this -> config -> item( 'selects' );
                                
                                $this->load->library('OlcomHmsTemplateForm',array( 
                                    'title'  => $labels[ 'edit_px_line'],
                                    'id' => 'olcom-px-line-form' 
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'search', array(
                                    'name' => 'medicament_id',
                                    'label' => $labels[ 'medicament' ],
                                    'id' => 'medicament',
                                    'value' => $result[ 'medicament_id' ]
                                ));
                                $result_forms = $this -> olcomhms -> read( 'drug_forms',array( 'form_id','form'),NULL);
                                $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'form_id',
                                    'label' => $labels[ 'form' ],
                                    'choices' => olcom_db_to_dropdown( $result_forms ),
                                    'selected' => $result[ 'form_id' ]
                                )); 
                                $result_routes = $this -> olcomhms -> read( 'administration_routes' ,array( 'route_id','route' ),NULL );
                                $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'route_id',
                                    'label' => $labels[ 'admin_route' ],
                                    'choices' => olcom_db_to_dropdown( $result_routes ),
                                    'selected' => $result[ 'route_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'start_end_date',
                                    'label' => $labels[ 'start_end_date' ],
                                    'class' => 'date-range-picker',
                                    'readonly' => '',
                                    'value' => $result[ 'start_date' ].' - '.$result[ 'end_date' ]
                                )); 
                                $this -> olcomhmstemplateform -> add_field( 'checkbox', array(
                                    'name' => 'allow_substitution',
                                    'label' => $labels[ 'allow_substitution' ],
                                    'value' => 1,
                                    'class' => 'ace-switch ace-switch-5',
                                    'checked' => $result[ 'allow_substitution' ] == 1 ? TRUE : FALSE
                                    
                                ));
                                $this -> olcomhmstemplateform -> add_group_title( $labels[ 'dosage' ] );
                                
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'dose',
                                    'label' => $labels[ 'medication_amount' ],
                                    'placeholder' => 'eg. 100 ',
                                    'value' => $result[ 'dose' ]
                                ));
                                $result_units = $this -> olcomhms -> read( 'drug_dose_units' , array( 'unit_id' , 'unit' ),NULL );
                                $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'unit_id',
                                    'label' => $labels[ 'medication_unit' ],
                                    'choices' => olcom_db_to_dropdown( $result_units ),
                                    'selected' => $result[ 'unit_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'quantity',
                                    'label' => $labels[ 'medicament_quantity' ],
                                    'value' => $result[ 'quantity' ]
                                ));
                                $this -> olcomhmstemplateform -> add_group_title( $labels[ 'common_dosage' ] );
                                $result_dosage = $this -> olcomhms -> read( 'medication_dosage', array( 'dosage_id' , 'dosage' ),NULL );
                                $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'frequency_id',
                                    'label' => $labels[ 'dosage_frequency' ],
                                    'choices' => olcom_db_to_dropdown( $result_dosage ),
                                    'selected' => $result[ 'frequency_id' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'admin_hours',
                                    'label' => $labels[ 'admin_hours' ],
                                    'placeholder' => ' eg. 10,14 for 10:00,14:00',
                                    'value' => str_replace( ':00' ,NULL,$result[ 'admin_hours'] )
                                ));
                                
                                $treatment_duration = explode( ' ',$result[ 'treatment_duration' ] );
                                
                                $this -> olcomhmstemplateform -> add_group_title( $labels[ 'specific_dosage' ] );
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'treatment_duration',
                                    'label' => $labels[ 'treatment_duration' ],
                                    'value' => $treatment_duration[ 0 ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'treatment_period',
                                    'label' => $labels[ 'treatment_period' ],
                                    'choices' => $selects[ 'period' ],
                                    'selected' => strtolower( $treatment_duration[ 1 ] )
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'refills',
                                    'label' => $labels[ 'refills' ],
                                    'value' => $result[ 'refills' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'units',
                                    'label' => $labels[ 'num_units' ],
                                    'placeholder' => '10 capsules of morphine',
                                    'value' => $result[ 'units' ]
                                ));
                                 $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'review_date',
                                    'label' => $labels[ 'review_date' ],
                                    'id' => 'review_date',
                                    'readonly' => '',
                                    'value' => $result[ 'review_date' ]
                                ));
                                $this -> olcomhmstemplateform -> add_field( 'text', array(
                                    'name' => 'comments',
                                    'label' => $labels[ 'comments' ],
                                    'value' => $result[ 'comments' ]
                                ));
                                olcom_show_dialog('form',array('content' => $this -> olcomhmstemplateform -> create_form(),
                                                    'specific_scripts' => 'prescription_line_specific_scripts'), NULL );  
                                }
                            else {
                                
                                $form_data = $this -> input -> post( NULL );
                              
                            	olcom_actions_cookies('edit', 'delete');
                                // retrieve prescribing doctor 
                                $logged_info = $this -> olcomaccessengine -> get_logged_in_user();
                                
                                $form_data[ 'prescribing_doctor' ] = $logged_info[ 'employee_id' ];
                                // format admin hours
                                if( $form_data[ 'admin_hours' ] != NULL )
                                {
                                    $admin_hours_data = explode(',' ,$form_data[ 'admin_hours'] );
                                    $admin_hours = '';
                                    
                                     if( $admin_hours_data != NULL )
                                        {
                                            foreach( $admin_hours_data  as $index => $admin_hour )
                                            {
                                                if( intval( $admin_hour ) >= 00 AND intval( $admin_hour ) <= 23 )
                                                {
                                                     $admin_hours .= $admin_hour.':00';
                                                     
                                                     if( $index != count( $admin_hours_data ) - 1 )
                                                     {
                                                         $admin_hours.=',';
                                                     }
                                                }   
                                                
                                            } 
                                            $form_data[ 'admin_hours' ] = $admin_hours;
                                        }
                                        else
                                            {
                                                $form_data[ 'admin_hours' ] = NULL;
                                            }
                                    $form_data[ 'admin_hours' ] = $admin_hours;
                                      
                                }
                                
                                // treatment duration not set remove treatmet period 
                                if( ! isset( $form_data[ 'treatment_duration' ] ) )
                                {
                                    $form_data[ 'treatment_period' ] = NULL;
                                }
                                
                                $write_start_timestamp = $form_data[ 'write_start_timestamp' ];
                                $start_end_date = $form_data[ 'start_end_date' ];
                                
                                $start_end_date = str_split( $start_end_date ,13 );
                                
                               
                                $start_date = str_split( $start_end_date[ 0 ] , 10 );
                                $form_data[ 'start_date' ] = $start_date[ 0 ];
                                $form_data[ 'end_date' ] = $start_end_date[ 1 ];
                             
                                $form_data[ 'treatment_duration' ] .= ' '.$form_data[ 'treatment_period' ];
                                $form_data[ 'allow_substitution' ] = isset( $form_data[ 'allow_substitution' ] )  ? 1 : 0;
                                
                                
                                //unset CSRF value
                                unset( $form_data[ 'olcomhms_csrf' ] );
                                unset( $form_data[ 'patient' ] );
                                unset( $form_data[ 'start_end_date' ] );
                                unset( $form_data[ 'treatment_period' ] );
                                
                                
                                // medicament info 
                                $form_data[ 'medicament_id' ] =  olcom_extract_numbers( $form_data[ 'medicament_id' ] );
                                
                                // indication info
                                $form_data[ 'disease_id' ] = olcom_extract_numbers( $form_data[ 'disease_id' ] );
                                
                                $result = $this -> prescriptions_model -> update_tempo_prescription_line( $this -> uri -> segment( 5 ) ,$form_data );
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', $messages[ 'record_updated' ], 'success' );
                                }
                                else {
                                    $this -> load -> config( 'olcom_messages' );
                                    $messages = $this -> config -> item( 'messages' );
                                    olcom_show_dialog('message', array( 'message' =>   $this -> return_codes[ $result ],
                                    'header' => $messages[ 'error']), 'error' );
                                }
                            }
                        break;
                        case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_record'],'');
                            }else{
                                
                                $result = $this  ->  prescriptions_model  ->  delete_px_line( $this  ->  uri  
                                ->  segment( 5 ) );
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages
                                    [ 'record_deleted' ],'header' => $messages['operation_successful']), 
                                    'success');
                                    
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                   
                }
            }
           /*
            * 
            * 
            */
            if( $section === 'prescriptions' )
            {
                switch( $this -> uri -> segment( 4 ) )
                {
                    case 'view':
                            $result = $this -> prescriptions_model -> get_prescription_info( $this -> uri -> segment( 5 ) );
                        
                            $lines = $result[ 'lines' ];
                            unset( $result[ 'lines' ] );
                            
                            // prescribing doctor 
                            $this -> load -> model( 'employees' );
                            $result_employee = $this -> employees -> info( $result[ 'employee_id' ] , 'basic', FALSE );
                            $result[ 'prescribing_doctor' ] = strtoupper( $result_employee[ 'lastname' ] ) . ','.$result_employee[ 'firstname' ];
                            unset( $result[ 'employee_id' ] );
                            
                            // patient id 
                            $result[ 'patient_id' ] = olcomhms_create_id( $result[ 'patient_id' ] , FALSE , TRUE );
                            
                            
                            $this -> load -> model( 'medicaments_model');
                            $line_num = 1;
                                                      
                            foreach( $lines as $key => $line )
                            {
                                // form
                                $result_form = $this -> medicaments_model -> get_forms( $line[ 'form_id' ] );
                                $lines[ $key ][ 'form' ] = $result_form[ 'form' ];
                                unset( $lines[ $key ][ 'form_id' ] );
                                
                                // dose 
                                $result_dose = $this -> medicaments_model -> get_dosage( $line[ 'frequency_id' ] );
                                $lines[ $key ][ 'dosage' ] = $result_dose[ 'dosage' ];
                                unset( $lines[ $key ][ 'dosage_id' ] );
                                unset( $lines[ $key ][ 'frequency_id' ] );
                                
                                //dose units 
                                $result_units = $this -> medicaments_model -> get_units( $line[ 'unit_id' ] );
                                $lines[ $key ][ 'unit' ] = $result_units[ 'unit' ];
                                unset( $lines[ $key ][ 'unit_id' ] );
                                
                                // disease 
                               // $result_disease = $this -> prescriptions_model -> get_indication_by_id( $line[ 'disease_id' ] );
                                //$lines[ $key ][ 'indication' ] = $result_disease[ 'disease_name'];
                               // unset( $lines[ $key ][ 'disease_id' ] );
                                
                                // medicament 
                                $result_medicament = $this -> medicaments_model -> get_medicament_info( $line[ 'medicament_id' ] );
                                $lines[ $key ][ 'medicament' ] = $result_medicament[ 'active_component' ];
                                unset( $lines[ $key ][ 'medicament_id' ] );
                                
                                // admin routes
                                $result_route = $this -> medicaments_model -> get_routes( $line[ 'route_id' ] ) ;
                                $lines[ $key ][ 'admin_route' ] = $result_route[ 'route' ];
                                unset( $lines[ $key ][ 'route_id' ] );
                                
                                $lines[ $key ][ 'line' ] = $line_num;
                                $line_num ++; 
                                unset( $lines[ $key ][ 'prescription_id' ] );
                            }
                            
                            $line_num = 0;
                            // px info grouping
                            $general_info = NULL;
                            $common_dosage_info = NULL;
                            $specific_dosage_info = NULL;
                            $dosage_info = NULL;
                            
                            foreach( $lines as $line )
                            {
                             // print_r( $line );
                                if( $general_info === NULL )
                                {
                                    $general_info = array();
                                }
                                 
                                    $general_info[ $line_num ][ 'medicament' ] = $line[ 'medicament' ];
                                    $general_info[ $line_num ][ 'start_date' ] = $line[ 'start_date' ];
                                    $general_info[ $line_num ][ 'end_date' ] = $line[ 'end_date' ];
                                    $general_info[ $line_num ][ 'allow_substitution' ] = $line[ 'allow_substitution' ] == 1 ? 'Yes' : 'No';
                                    $general_info[ $line_num ][ 'admin_route' ] = $line[ 'admin_route' ];
                                          
                              
                                if( $dosage_info === NULL )
                                {
                                    $dosage_info = array();
                                }
                               
                                    $dosage_info[ $line_num ][ 'dose' ] = $line[ 'dose' ];
                                    $dosage_info[ $line_num ][ 'unit' ] = $line[ 'unit' ];  
                                    $dosage_info[ $line_num ][ 'quantity'] = $line[ 'quantity' ];  
                                    $dosage_info[ $line_num ][ 'form'] = $line[ 'form' ];   
                             
                                if( $common_dosage_info === NULL )
                                {
                                    $common_dosage_info = array();
                                }
                                  
                                    $common_dosage_info[ $line_num ][ 'dosage' ] = $line[ 'dosage' ];
                                    $common_dosage_info[ $line_num ][ 'admin_hours'] = $line[ 'admin_hours' ];       
                                 
                                if( $specific_dosage_info === NULL )
                                {
                                    $specific_dosage_info = array();
                                }
                                
                                    $specific_dosage_info[ $line_num ][ 'treatment_duration' ] = $line[ 'treatment_duration' ];
                                    $specific_dosage_info[ $line_num ][ 'units'] = $line[ 'units' ];
                                    $specific_dosage_info[ $line_num ][ 'refills'] = $line[ 'refills' ]; 
                                    $specific_dosage_info[ $line_num ][ 'review_date'] = $line[ 'review_date' ];
                                    $specific_dosage_info[ $line_num ][ 'comments'] = $line[ 'comments' ];         
                              
                                $line_num ++;
                            }       

                            
                            olcom_show_dialog( 'static_table',
                                array( 'table_content' => $result,
                                'static_content' => array( 
                                    'general_info' => $general_info,
                                    'dosage' => $dosage_info,
                                    'common_dosage' => $common_dosage_info,
                                    'specific_dosage' => $specific_dosage_info 
                                ) ,'print_link' => 'prescriptions_mngt/print_prescription_data/'. $this -> uri -> segment( 5 ) )          
                             , NULL);
                            
                        break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_px'],'');
                            }else{
                                
                               $result = $this  ->  prescriptions_model  ->  delete_px( $this  ->  uri  ->  segment( 5 ) );
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],
                                    'header' => $messages['operation_successful']), 'success');
                                    
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],
                                    'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;   
                }
            } 
           /*
            * 
            * 
            */
            if( $section === 'disease_categories' )
            {
                switch( $this -> uri -> segment( 4 ) )
                {
                    case 'view' : 
                            $category = $this -> prescriptions_model -> get_disease_categories( $this -> uri -> segment( 5 ) );
                            
                            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['category_diseases'], 'with_actions' => FALSE,
                                    'id_suffix' =>'diseases',
                                    'extra_params' => array( 'name' => 'category_id' , 'value' => $this -> uri -> segment( 5 )  ),
                                      'columns' => array( $labels[ 'id' ] , $labels[ 'disease' ] ),
                                      'controller_fx' => 'prescriptions_mngt/category_diseases_data'));
                            
                                    $datatable = $this -> olcomhmstemplatedatatable -> create_view();
                                   olcom_show_dialog('dynamic_table',
                                        array(
                                            'table_content' => $category,
                                            'dynamic_content' => array(
                                                'diseases' => $datatable
                                            )
                                        )
                                    , NULL );
                        break;
                       
                case 'edit' :
                            
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            
                            if($save_click    ===  FALSE){ 
                                //edit action track 
                                olcom_actions_cookies('edit', 'create');
                                
                                $category = $this -> prescriptions_model -> get_disease_categories( $this -> uri -> segment( 5 ) ) ;
                                $this -> load -> library('OlcomHmsTemplateForm',array( 
                                    'title'  => $labels[ 'edit_disease_category']
                                )); 
                                $this -> olcomhmstemplateform -> add_field('textarea',array(
                                    'name'  => 'category_name',
                                    'label'  => $labels['category'],
                                    'value' => $category[ 'category_name' ]
                                ));
                               
                                olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'create_disease_category_specific_scripts'), '');
                            }else{
                                //form submitted
                                
                                $form_data = $this -> input -> post(NULL);
                                 
                                $result = $this  ->  prescriptions_model  ->  update_disease_category( $this  ->  uri  ->  segment( 5 ), $form_data );
                                
                                if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                }
                                olcom_actions_cookies('edit', 'delete');
                            }
                         break;
                      case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_px'],'');
                            }else{
                                
                               $result = $this  ->  prescriptions_model  ->  delete_disease_category( $this  ->  uri  ->  segment( 5 ) );
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],
                                    'header' => $messages['operation_successful']), 'success');
                                    
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],
                                    'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;   
                }
            }
           /*
            * 
            * 
            */
            if( $section === 'diseases' )
            {
                switch( $this -> uri -> segment( 4 ) )
                {
                 
                    case 'edit' :
                                
                                $save_click = 0;
                                $save_click = get_cookie($cookie_save);
                                
                                if($save_click    ===  FALSE){ 
                                    //edit action track 
                                    olcom_actions_cookies('edit', 'create');
                                    
                                    $disease = $this -> prescriptions_model -> get_diseases( $this -> uri -> segment( 5 ) );
                                    $category = $this -> prescriptions_model -> get_disease_categories( ) ;
                                     
                                    $this -> load -> library('OlcomHmsTemplateForm',array(
                                        
                                        'title'  => $labels[ 'edit_disease']
                                    )); 
                                    
                                    $result_categories = $this -> prescriptions_model -> get_disease_categories( );
                                    
                                    $this -> olcomhmstemplateform -> add_field('textarea',array(
                                        'name'  => 'disease_name',
                                        'label'  => $labels['disease'],
                                        'value' => $disease[ 'disease_name' ]
                                    ));
                                    $this -> olcomhmstemplateform -> add_field( 'select' ,array(
                                        'name' => 'category_id', 
                                        'label' =>  $labels[ 'category'],
                                        'choices' => $result_categories ,
                                        'selected' => $disease[ 'category_id' ]
                                    ));
                                                       
                                    olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                            'specific_scripts'  => 'create_disease_specific_scripts'), '');
                                }else{
                                    //form submitted
                                    
                                    $form_data = $this -> input -> post(NULL);
                                     
                                    $result = $this  ->  prescriptions_model  ->  update_disease( $this  ->  uri  ->  segment( 5 ), $form_data );
                                    
                                    if( $result === SUCCESS_CODE)
                                    {
                                         olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                            'header'  => $messages['operation_successful']), 'success');
                                    }
                                    if( $result !== SUCCESS_CODE)
                                    {
                                         olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                            'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                    }
                                    olcom_actions_cookies('edit', 'delete');
                                }
                             break;
                          case 'delete' :
                                $confirmed = 0;
                                $cookie_status = $olcom_cookies['action_delete_confirm'];
                                $confirmed = get_cookie($cookie_status);
                                if($confirmed    ===  FALSE){
                                    olcom_actions_cookies('delete', 'create');
                                    olcom_show_dialog('confirm',$messages['delete_px'],'');
                                }else{
                                    
                                   $result = $this  ->  prescriptions_model  ->  delete_disease( $this  ->  uri  ->  segment( 5 ) );
                                    
                                    if( $result === SUCCESS_CODE )
                                    {
                                        olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],
                                        'header' => $messages['operation_successful']), 'success');
                                        
                                    }else{
                                        olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],
                                        'header' => $messages['operation_failed']), 'warning'); 
                                    }
                                    olcom_actions_cookies('delete', 'delete');
                                }
                            
                            break;   
                }
            }
            /*
             * 
             * 
             */
             if( $section === 'patients_diagnosis' )
             {
                 switch( $this -> uri -> segment( 4 ) )
                 {
                     case 'view' :
                            $result_diagnosis = $this -> prescriptions_model -> get_diagnosis( $this -> uri -> segment( 5 ) );
                             
                            $this -> load -> model( 'employees' );
                            $doctor = $this -> employees -> info( $result_diagnosis[ 'employee_id' ] , 'basic' );
                            $table_content = array(
                                
                                'doctor' => strtoupper( $doctor[ 'lastname' ] ).",".$doctor[ 'firstname' ],
                                'diagnosis_date' => $result_diagnosis[ 'diagnosis_date' ]
                            );
                            
                            $result_disease = $this -> prescriptions_model -> get_diagnosis_disease( $this -> uri -> segment( 5 ) );
                            if( $result_disease != NULL )
                            {
                                
                                $table_content[ 'indication' ]  = $result_disease[ 'disease_name' ];
                            }
                            unset( $result_diagnosis[ 'patient_id' ] );
                            unset( $result_diagnosis[ 'employee_id' ] );
                            unset( $result_diagnosis[ 'diagnosis_id' ] );
                            unset( $result_diagnosis[ 'diagnosis_date' ] );
                            
                            olcom_show_dialog('tabbed', array( 'table_content' => $table_content,
                            'tab_content' =>  $result_diagnosis ), NULL );
                            
                         break;
                    case 'edit':
                                $save_click = 0;
                                $save_click = get_cookie($cookie_save);
                                $diagnosis = $this -> prescriptions_model -> get_diagnosis( $this -> uri -> segment( 5 ) );
								$disease = $this -> prescriptions_model -> get_diagnosis_disease( $diagnosis[ 'diagnosis_id' ] );
								
                                if($save_click    ===  FALSE){ 
                                    //edit action track 
                                    olcom_actions_cookies('edit', 'create');
                                                                          
                                    $this -> load -> library('OlcomHmsTemplateForm',array(
                                        'title'  => $labels[ 'update_diagnosis']
                                    )); 
                                    
                                    $this -> olcomhmstemplateform -> add_field('textarea' ,array(
                                        'name' => 'complaints',
                                        'label' => $labels[ 'complaints' ],
                                        'value' => $diagnosis[ 'complaints' ]
                                    ));
                                    $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                                        'name' => 'provisional_diagnosis', 
                                        'label' => $labels[ 'provisional_diagnosis'],
                                        'value' => $diagnosis[ 'provisional_diagnosis' ]
                                    ));
                                    $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                                        'name' => 'actual_diagnosis', 
                                        'label' => $labels[ 'actual_diagnosis'],
                                        'value' => $diagnosis[ 'actual_diagnosis' ]
                                    ));
                                    $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                                        'name' => 'remarks', 
                                        'label' => $labels[ 'remarks'],
                                        'value' => $diagnosis[ 'remarks' ]
                                    ));
                                    
                                    
                                    $this -> olcomhmstemplateform -> add_group_title( $labels[ 'indications' ] ); 
                                     
                                    $this -> olcomhmstemplateform -> add_field( 'search', array(
                                        'name' => 'disease_id',
                                        'label' => $labels[ 'disease' ],
                                        'id' => 'indication' ,
                                        'placeholder' => $disease == NULL ? 'Type for hints...' : NULl,
                                        'value' => $disease == NULL ? NULL : '( '.$disease[ 'disease_id' ].' ) '.$disease[ 'disease_name' ] 
                                    ));
                                                                                      
                                    olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                            'specific_scripts'  => 'diagnosis_specific_scripts'), '');
                                }else{
                                    //form submitted
                                    olcom_actions_cookies('edit', 'delete');
                                    $form_data = $this -> input -> post(NULL);
                                     
                                    if( isset( $form_data[ 'disease_id' ]) ){
                                    	$form_data[ 'disease_id' ] = olcom_extract_numbers( $form_data[ 'disease_id' ] );
                                    }
                                    $result = $this  ->  prescriptions_model  ->  update_diagnosis( $this  ->  uri  ->  segment( 5 ), $form_data );
                                    
                                    if( $result === SUCCESS_CODE)
                                    {
                                         olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                            'header'  => $messages['operation_successful']), 'success');
                                    }
                                    if( $result !== SUCCESS_CODE)
                                    {
                                         olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                            'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                    }
                                    
                                }
                        break;
                       case 'delete' :
                                $confirmed = 0;
                                $cookie_status = $olcom_cookies['action_delete_confirm'];
                                $confirmed = get_cookie($cookie_status);
                                if($confirmed    ===  FALSE){
                                    olcom_actions_cookies('delete', 'create');
                                    olcom_show_dialog('confirm',$messages['delete_px'],'');
                                }else{
                                    
                                   $result = $this  ->  prescriptions_model  ->  delete_diagnosis( $this  ->  uri  ->  segment( 5 ) );
                                    
                                    if( $result === SUCCESS_CODE )
                                    {
                                        olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],
                                        'header' => $messages['operation_successful']), 'success');
                                        
                                    }else{
                                        olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],
                                        'header' => $messages['operation_failed']), 'warning'); 
                                    }
                                    olcom_actions_cookies('delete', 'delete');
                                }
                            
                            break;   
                 }
             } 
        }
       /*
        * 
        * 
        * 
        */
        function save_prescription_lines_data()
        {
           $form_data = $this -> input -> post( NULL );
           $messages = $this -> config -> item( 'messages' );
           if( isset( $form_data ) AND $form_data != NULL )
           {
               $logged_info = $this -> olcomaccessengine -> get_logged_in_user();
               
               $px_data = array();
               $px_data[ 'employee_id' ] = $logged_info[ 'employee_id' ];
               $px_data[ 'patient_id' ] = olcom_extract_numbers( $form_data[ 'patient' ] );
               
               if( isset( $form_data[ 'diagnosis_id'] ) AND $form_data[ 'diagnosis_id'] != NULL )
               {
                   $px_data[ 'diagnosis_id'] = $form_data[ 'diagnosis_id'];
               }else
                   {
                       $px_data[ 'diagnosis_id' ] = NULL;
                   }
                    
               
               // add date
              $px_data[ 'date' ] = date( 'Y-m-d H:i:s' );
              $result = $this -> prescriptions_model -> write_prescription_lines( $logged_info[ 'employee_id' ] , $px_data );
              
              if( preg_match('/PX[0-9]+/', $result) != 1 )
              {
                  olcom_server_form_message( $this -> return_codes[ $result ], 0 );
              }
              else {
                  $this -> create_notification(  array( 'note_id' => olcom_extract_numbers( $result ) ) );
                  olcom_server_form_message( $messages[ 'created_px_success'], 1 );
              }
           }
           else {
                   olcom_show_dialog('message', array( 'message' => $this -> return_codes[ 104 ] , 'header' =>
                   $messages[ 'operation_failed']  ), 'error' );
           }
        }
       /*
        * 
        * 
        * 
        */
        function prescriptions() 
        {
           
           $labels = $this  ->  config  ->  item( 'labels' );
           $this  ->  load  ->  library(
            'OlcomHmsTemplateDatatable',
            array(
                    'header'  => $labels[ 'prescriptions' ],
                    'columns'  => array(
                        $labels[ 'prescription_id' ],$labels[ 'patient_id'] ,$labels[ 'firstname'],$labels['lastname'],$labels [ 'prescription_date' ]
                    ),
                    'with_actions'  => TRUE,
                    'create_controller_fx' => 'prescriptions_mngt/prescription_writting',
                    'controller_fx'  => 'prescriptions_mngt/prescriptions_data'
            )
        );
        //load the main template 
        $datatable_view = $this  ->  olcomhmstemplatedatatable  ->  create_view();
         
        $this  ->  load  ->  view('main_template',array(
            'template'  => array(
                'content'  => $datatable_view['datatable'],
                'page_specific_scripts'  => $datatable_view['specific_scripts']
            )
        ));
      }
      /*
       * 
       * 
       * 
       */
       function prescriptions_data()
       {
           
           if( $this -> olcomaccessengine -> is_logged() == FALSE )
           {
               olcom_empty_dtt();
               return;
           }
           $logged_info = $this -> olcomaccessengine -> get_logged_in_user();
           
           if( $logged_info[ 'group_id' ] != $this -> olcomaccessengine -> get_admin_gid( ) AND 
               $logged_info[ 'group_id' ] != $this -> olcomaccessengine -> get_super_admin_gid( ) 
           )
           {
                
                 
               if( $this -> olcomaccessengine -> 
               is_allowed_module( 'prescriptions_mngt/prescription_writting' ) )
               {
                    $where  = array(
                    'prescriptions.employee_id'  => $logged_info[ 'employee_id'],
                    'prescriptions.patient_id' => 'personnel.personnel_id',
                    'patients_general.patient_id' => 'personnel.personnel_id'
                    );   
                     
                    
               }
        
               else
                   {
                       $where = array( 
                             'prescriptions.patient_id' => 'personnel.personnel_id',
                             'patients_general.patient_id' => 'personnel.personnel_id'
                        );
                   }    
           }
           else {
               $where = array( 
                    'prescriptions.patient_id' => 'personnel.personnel_id',
                    'patients_general.patient_id' => 'personnel.personnel_id'
               );
           }
          
           $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  => array(
                    'prescriptions' => array( 'prescription_id','date'),
                    'personnel'  => array( 'personnel_id','firstname', 'lastname'),
					'patients_general' => array( 'patient_id')
                    ),
                    'with_actions'  => TRUE,
                    'hidden_cols'  => array( 'patient_id' ),
                    'index_column'  => 'prescription_id',
                    'controller_fx'  => 'prescriptions_mngt/actions/prescriptions',//for actions
                    'id_type'  => 'prescription',
                    'where'  => $where,
                    'data_style_format'  => array(
                       
                        'prescription_id' => array(
                            'id_format' => 'prescription'
                        )
                    ),
                    'cell_ops' => array( 'personnel_id' => "concat(initial,'',personnel_id)"),
                    'order'  => array(  'personnel_id' => 1,'firstname' => 2,'lastname' => 3 ,'date'  => 4)
                 )
             );
            echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
       }

     /*
     * 
     * 
     * 
     */
     function disease_categories() 
     {
         $labels = $this -> config -> item('labels');

        $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['disease_categories'], 'with_actions' => TRUE, 
        'create_controller_fx' => 'prescriptions_mngt/create_disease_category',
         'columns' => array($labels['id'], $labels['category']  ), 'controller_fx' => 'prescriptions_mngt/disease_categories_data'));

        $datatable = $this -> olcomhmstemplatedatatable -> create_view();

        $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
     }
     /*
      * 
      * 
      */
      function disease_categories_data()
      {
            $this -> load -> library('OlcomHmsDataTables', 
            array('columns' => array('category_id', 'category_name','category_id'), 'index_column' => 'category_id',
             'table_name' => 'disease_categories', 'controller_fx' => 'prescriptions_mngt/actions/disease_categories',
              'id_type' => ''));

        //get the data
        echo $this -> olcomhmsdatatables -> get_data();    
      }
      /*
       * 
       * 
       */
       function create_disease_category()
       {
           $form_data = $this -> input -> post(NULL);
            $messages = $this -> config -> item('messages');
            $labels = $this -> config -> item('labels');
            
            if(isset($form_data) && $form_data!= NULL){
              
                $result = $this  ->  prescriptions_model   ->  create_disease_category( $form_data );
                
                if( $result === SUCCESS_CODE ){
                    olcom_server_form_message($messages[ 'category_created' ], 1);
                }else{
                        olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                    }
                
            }else{
                $this -> load -> library('OlcomHmsTemplateForm',array(
                    'with_submit_reset'  => TRUE,
                    'title'  => $labels[ 'create_disease_category']
                )); 
                $this -> olcomhmstemplateform -> add_field('textarea',array(
                    'name'  => 'category_name',
                    'label'  => $labels['category']
                ));
                 
                $form = $this -> olcomhmstemplateform -> create_form();
                
                $form_specific_scripts = $this -> load -> view('create_disease_category_specific_scripts','',TRUE);
                
                $this -> load -> view('main_template',array(
                    'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                    
                ));
            }
       }
      /*
       * 
       * 
       */
       function category_diseases_data()
       {
           $category_id = $this -> input -> get( 'category_id');
           
           if( $category_id != NULL )
           {
               $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'diseases' => array( 'disease_id' , 'disease_name' )
                        ),
                        'with_actions'  => FALSE,
                        'hidden_cols'  => array(),
                        'index_column'  => 'disease_id',
                        'controller_fx'  => NULL,
                        'id_type'  => '',
                        'where'  => array('diseases.category_id'  => $category_id 
                        ),
                        'data_style_format'  => NULL ,
                       'order'  => array('disease_name' => 1  )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
           }
           else
           {
                echo olcom_empty_dtt();    
           }
       } 
       /*
       * 
       * 
       */
       function create_disease()
       {
           $form_data = $this -> input -> post(NULL);
            $messages = $this -> config -> item('messages');
            $labels = $this -> config -> item('labels');
            
            if(isset($form_data) && $form_data!= NULL){
              
                $result = $this  ->  prescriptions_model   ->  create_disease( $form_data );
                
                if( $result === SUCCESS_CODE ){
                    olcom_server_form_message($messages[ 'disease_created' ], 1);
                }else{
                        olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                    }
                
            }else{
                $this -> load -> library('OlcomHmsTemplateForm',array(
                    'with_submit_reset'  => TRUE,
                    'title'  => $labels[ 'create_disease']
                )); 
                
                $result_categories = $this -> prescriptions_model -> get_disease_categories( );
                
                $this -> olcomhmstemplateform -> add_field('textarea',array(
                    'name'  => 'disease_name',
                    'label'  => $labels['disease']
                ));
                $this -> olcomhmstemplateform -> add_field( 'select' ,array(
                    'name' => 'category_id', 
                    'label' =>  $labels[ 'category'],
                    'choices' => $result_categories 
                ));
                
                 
                $form = $this -> olcomhmstemplateform -> create_form();
                
                $form_specific_scripts = $this -> load -> view('create_disease_specific_scripts','',TRUE);
                
                $this -> load -> view('main_template',array(
                    'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                    
                ));
            }
       }
        /*
         * 
         * 
         * 
         */
         function diseases()
         {
             $labels = $this -> config -> item('labels');

            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['diseases'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'prescriptions_mngt/create_disease',
             'columns' => array($labels['id'], $labels['disease']  ,$labels[ 'category' ]), 'controller_fx' => 'prescriptions_mngt/diseases_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
         }
        
      /*
       * 
       * 
       */
       function diseases_data()
       { 
               $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'diseases' => array( 'disease_id' , 'disease_name' ),
                        'disease_categories' => array(
                                'category_name'
                            )
                        ),
                        'with_actions'  => TRUE,
                        'hidden_cols'  => array(),
                        'index_column'  => 'disease_id',
                        'controller_fx'  => 'prescriptions_mngt/actions/diseases',
                        'id_type'  => '',
                        'where'  => array('diseases.category_id'  => 'disease_categories.category_id'
                        ),
                        'data_style_format'  => NULL ,
                       'order'  => array('disease_name' => 1 , 'category_name' => 2 )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
           
       } 
     /*
      * 
      * 
      * 
      */
      function print_prescription_data()
      {
          if( is_nan( $this -> uri -> segment( 3 ) ) )
            {
                redirect( 'accessengine/not_found' , 'refresh' );
                return;     
            }
            $this -> load -> model( 'employees' );
            $this -> load -> model( 'patients_model' );
            $labels = $this -> config -> item( 'labels');
            $prescription = $this -> prescriptions_model -> get_prescription_info( $this -> uri -> segment( 3 ) );
            
            if( $prescription == NULL )
            {
                return;
            }
            
            
            $this -> load -> helper( array( 'dompdf' , 'file' ) );
            
            $this -> load -> model( 'medicaments_model' );
            $prescribing_doctor = $this -> employees -> info ( $prescription [ 'employee_id' ] , 'basic' );
            $patient = $this -> patients_model -> get_personnel_info( $prescription[ 'patient_id' ] , 'full' );
            
            $table_content = array();
            $headers = array();
            
           
            $count = 0 ;
            
             foreach( $prescription[ 'lines' ] as $key => $line )
            {
                $medicament = $this -> medicaments_model -> get_medicament_info( $line[ 'medicament_id' ] );
             
                $data = array();
                
                $data[ 'active_component' ] = $medicament[ 'active_component' ];
                
                $admin_route = $this -> medicaments_model -> get_routes( $line[ 'route_id' ] );
                $data[ 'route' ] = $admin_route[ 'route' ];
                $data[ 'allow_substitution' ] = $line[ 'allow_substitution' ] == 1 ? 'Yes' : 'No' ;
                $unit = $this -> medicaments_model -> get_units( $line[ 'unit_id' ] );
                $form = $this -> medicaments_model -> get_forms( $line[ 'form_id' ] );
                $dosage = $this -> medicaments_model -> get_dosage( $line[ 'frequency_id' ] );
                
                $data[ 'start_date' ] = $line[ 'start_date' ];
                $data[ 'end_date' ] = $line[ 'end_date' ];
                $data[ 'hours' ] = $line[ 'admin_hours' ];
                $data[ 'dose'   ] = $line[ 'dose' ] .'  '. $unit[ 'unit' ];
                $data[ 'quantity' ] = $line[ 'quantity' ]. '  '.$form[ 'form' ];
                $data[ 'treatment_duration' ] = $line[ 'treatment_duration' ];
                $data[ 'dosage' ] = $dosage[ 'dosage' ]; 
                
                $table_content[] = $data ;     
            }
           
             $patient_id = olcomhms_create_id( $patient[ 'personnel_id' ] , FALSE , TRUE );
             $prescription_data = array(
                'prescription_date' => $prescription[ 'date' ],
                'prescribing_doctor' => strtoupper( $prescribing_doctor[ 'lastname' ] ).','.$prescribing_doctor[ 'firstname'],
                'patient_id'   => $patient_id,
                'full_name' => $patient[ 'firstname' ].'  '.$patient[ 'middlename' ].' ' . $patient[ 'lastname' ],
                'address' => $patient[ 'address' ],
                'phone' => $patient[ 'phone' ],
                'lines' => $table_content
             );
              
             $prescription_template = $this -> load -> view( 'prescription_template' , $prescription_data , FALSE );
            
			//print_r( $prescription_template );
			//pdf_create( $prescription_template , $patient_id.'-Prescription' );
      }
      /*
       * 
       * 
       * 
       */
       function create_notification( $data )
       {
           
           $this -> load -> model( 'usermanagement_model' );
           
           // get users
           $users = $this -> usermanagement_model -> get_users( );
           $sub_module = 'prescriptions';
           $this -> load -> model( 'notifications_model' );
           $sub_module_id = $this -> notifications_model -> get_submodule_id_byname( $sub_module );
           
           $group_ids = NULL;
           if( $sub_module_id != NULL )
           {
              $group_ids = $this -> notifications_model -> get_submodule_readgroups( $sub_module_id );
              if( $group_ids == NULL )
              {
                  return NULL;
              }
              
              $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
              
              foreach( $users as $key => $user )
              {
                  if( ! in_array( $user[ 'group_id' ] , $group_ids ) OR $user[ 'group_id' ] == $logged_user[ 'group_id' ]  )
                  {
                      unset( $users[ $key ] );
                  }
                  else {
                      $users[ $key ] = $user[ 'employee_id' ];
                  }
              }
              
              $notification = array();
              
              if( isset( $logged_user[ 'employee_id' ] ) AND $logged_user[ 'employee_id' ] != NULL  )
              {
                  $notification[ 'employee_id' ] = $logged_user[ 'employee_id' ];
                  $notification[ 'note_id' ] = $data[ 'note_id' ];
                  $notification[ 'is_prescription' ] = 1;
                  $notification[ 'note_date' ] = date( 'Y-m-d H:i');
                  
                  if( $this -> notifications_model -> create_notification( $notification ) == SUCCESS_CODE )
                  {
                      if( $this -> notifications_model -> create_users_notifications ( $data[ 'note_id' ] , $users ) == SUCCESS_CODE )
                      {
                          return SUCCESS_CODE;
                      }
                  }
              }
              return NULL;
           }
       }
      /*
       * 
       * 
       * 
       */
       function  notifications_data()
       {
           
           $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
           
           if( isset( $logged_user[ 'employee_id' ] ) AND $logged_user[ 'employee_id' ] != NULL )
           {
               $this -> load -> model( 'notifications_model' );
               $notifications = $this -> notifications_model -> get_user_notifications( $logged_user[ 'employee_id' ] );
               
               $notes_html = '';
               foreach( $notifications as $key => $notification )
               {
                  
                  $icon = '';
                  $message = '';
                 
                      $icon = 'icon-medkit';
                      $message = 'New Prescription Order';
                   
                  
                  $notifications[ $key ][ 'icon' ] = $icon;
                  $notifications[ $key ][ 'message' ] = $message;
               }
               
               $notes_html = $this -> load -> view( 'notification_item' , array( 'notifications' => $notifications ) , TRUE );
               
               echo json_encode( array( 
                  'notifications' => $notes_html ,
                  'num_notifications' => count( $notifications ) 
               ));
           }
       }
       /*
        * 
        * 
        * 
        */
        function write_diagnosis()
        {
            $form_data = $this -> input -> post( NULL );
       
        
            $messages = $this -> config ->item('messages');
            $labels = $this -> config -> item('labels');
            $return_codes = $this -> config -> item( 'return_codes' );
            $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
            
            if(isset($form_data) && $form_data!= NULL )
            {
              
              $form_data[ 'patient_id' ] = olcom_extract_numbers( $form_data[ 'patient' ] );
              unset( $form_data[ 'patient' ] );
              
              $form_data[ 'disease_id' ] = olcom_extract_numbers( $form_data[ 'disease_id' ] );
              
              $form_data[ 'employee_id' ] = $logged_user[ 'employee_id' ] ;
              $form_data[ 'diagnosis_date' ] = date( 'Y-m-d H:m:i' );
              
              $result = $this -> prescriptions_model -> write_diagnosis( $form_data ) ;
              
              if( $result === SUCCESS_CODE )
              {
                  olcom_server_form_message( $messages[ 'success_write_diagnosis'], 1 );
              }
              else
                  {
                      olcom_server_form_message( $return_codes[ $result ]  , 0 );
                  }
                
            }else{
                $this->load->library('OlcomHmsTemplateForm',array(
                     
                    'with_submit_reset'  => TRUE,
                    'title'  => $labels[ 'write_diagnosis'],
                    'header_info'  => ''
                )); 
               
                
                
                if( $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_admin_gid() OR 
                    $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_super_admin_gid() )
                    {
                        $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                            'name' => 'employee_id',
                            'label' => $labels[ 'prescribing_doctor' ],
                            'value' => NULL,
                            'readonly' => ''
                        ));
                    }
                 else{
                        $this -> load -> model( 'employees' );
                       
                        $employee_info = $this -> employees -> info( $logged_user[ 'employee_id' ],'basic',FALSE);
                          
                        $this -> olcomhmstemplateform -> add_field( 'text', array(
                            'name' => 'employee_id',
                            'label' => $labels[ 'doctor'],
                            'value' => strtoupper($employee_info[ 'lastname' ] ).','.$employee_info[ 'firstname' ],
                            'readonly' => '' 
                        ));           
                    }
                    
                $this -> olcomhmstemplateform -> add_field('search',array(
                    'name'  => 'patient',
                    'label'  => $labels['patient'],
                    'id' => 'patient_auto_complete',
                    'placeholder' => 'Patient ID'  ,
                    'help' => $this -> help[ 'get_patient_id']
                ));
                $this -> olcomhmstemplateform -> add_field('textarea' ,array(
                    'name' => 'complaints',
                    'label' => $labels[ 'complaints' ]
                ));
                $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                    'name' => 'provisional_diagnosis', 
                    'label' => $labels[ 'provisional_diagnosis']
                ));
                $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                    'name' => 'actual_diagnosis', 
                    'label' => $labels[ 'actual_diagnosis']
                ));
                $this -> olcomhmstemplateform -> add_field( 'textarea' ,array(
                    'name' => 'remarks', 
                    'label' => $labels[ 'remarks']
                ));
                $this -> olcomhmstemplateform -> add_field( 'search', array(
                    'name' => 'disease_id',
                    'label' => $labels[ 'indication' ],
                    'id' => 'indication',
                    'help' => $this -> help[ 'get_indication'],
                    'placeholder' => 'Type few characters for hints...'
                 ));

                $form = $this -> olcomhmstemplateform -> create_form();
                
                $form_specific_scripts = $this -> load -> view('diagnosis_specific_scripts','',TRUE);
               
                $this -> load -> view( 'main_template',array(
                    'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                    
                ));
            }
        }
       /*
        * 
        * 
        * 
        */
        function patients_diagnosis()
        {
             $labels = $this -> config -> item('labels');

            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['patients_diagnosis'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'prescriptions_mngt/write_diagnosis',
             'columns' => array($labels['id'], $labels['diagnosis_date']  ,$labels[ 'patient_id' ],
              $labels[ 'firstname'] , $labels[ 'lastname' ] ), 
              'controller_fx' => 'prescriptions_mngt/patients_diagnosis_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
        }
        /*
         * 
         * 
         * 
         */
         function patients_diagnosis_data()
         {
             
             $logged_in = $this -> olcomaccessengine -> get_logged_in_user();
           
             $where =  array(
                        'patients_general.patient_id'  => 'personnel.personnel_id',
                        'patients_diagnosis.patient_id' => 'patients_general.patient_id'
                        
                    );
           
               if( isset( $logged_in[ 'employee_id' ]) )
               {
                   $where[ 'patients_diagnosis.employee_id' ] = $logged_in[ 'employee_id' ];
               }
             $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'patients_diagnosis' => array( 'diagnosis_id' , 'diagnosis_date' ),
                        'personnel' => array('personnel_id' , 'firstname', 'lastname' ),
                         'patients_general' => array( 'patient_id' ),
                         'employees' => array( 'employee_id' )
                        ),
                        'with_actions'  => TRUE,
                        'hidden_cols'  => array( 'personnel_id','employee_id' ),
                        'index_column'  => 'diagnosis_id',
                        'controller_fx'  => 'prescriptions_mngt/actions/patients_diagnosis',
                        'id_type'  => 'diagnosis',
                        'group_by' => 'diagnosis_id',
                        'where'  => $where,
                        'data_style_format'  => array(
                        'patient_id' => array(
                            'id_format' => 'patient' 
                            )
                         ),
                       'order'  => array( 'diagnosis_date' => 1 , 'patient_id' => 2 , 'firstname' => 3 , 'lastname' => 4 )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data(); 
         }

    function assoc_diagnosis_data()
    {
            $diagnosis  = array();
            $patient_id  = $this -> uri -> segment(3);
            if(isset( $patient_id )){
                
                $query2= $this -> db -> select( 'diagnosis_id' )
                   // -> where( 'patient_id' , $this -> uri -> segment( 3 ))
                    -> get( 'prescriptions_has_patients_diagnosis' );
                $query2 = $this -> db -> last_query();
                
                $result  = $this -> db -> where( array('patients_diagnosis.patient_id' => $patient_id) , NULL , FALSE)
                    -> where( 'diagnosis_id NOT IN('.$query2.')' ,null, false )
                    -> select( array( 'diagnosis_id' , 'diagnosis_date' ))
                    -> from( array( 'patients_diagnosis' ))
                    -> get();
               //echo $this -> db -> last_query();
                if($result -> num_rows() > 0 ){
                   
                        $result = olcom_db_to_dropdown($result);
                       // print_r( $result );
                        olcom_server_chosen_ajax( $result);
                   
                }else{
                        olcom_server_chosen_ajax( array( 'result' => 0 ) );
                }
            }
    }
	/*
	 * 
	 * 
	 */
	function drug_forms_data(){
		$drug_forms = array();
		$medicament_id = $this -> uri -> segment( 3 );
		
		$this -> load -> model( 'finance_model' );
		$this -> load -> model ( 'medicaments_model' );
		$product = $this -> finance_model -> get_products( NULL, $medicament_id );
		 
		if( empty( $product ) ){
			
			$forms = $this -> medicaments_model -> get_forms( );
			foreach( $forms as $form )
				$drug_forms[ $form[ 'form_id' ] ] = $form[ 'form' ];
		
			echo json_encode( $drug_forms );
			return;
		}
		foreach( $product as $p ){
			$form = $this -> medicaments_model -> get_forms( $p[ 'form_id' ] );
			$drug_forms[ $form[ 'form_id' ] ] = $form[ 'form' ];
		}

		echo json_encode( $drug_forms );
	}
	/*
	 * 
	 * 
	 */
	 function dose_units_data(){
	 	$dose_units = array();
		$medicament_id = $this -> uri -> segment( 3 );
		
		$this -> load -> model( 'finance_model' );
		$this -> load -> model ( 'medicaments_model' );
		$product = $this -> finance_model -> get_products( NULL, $medicament_id );
		
		if( empty( $product ) ){
			
			$units = $this -> medicaments_model -> get_units( );
			 
			foreach( $units as $unit )
				$dose_units[ $unit[ 'unit_id' ] ] = $unit[ 'unit' ];
		
			echo json_encode( $dose_units );
			return;
		}
		foreach( $product as $p ){
			$unit = $this -> medicaments_model -> get_units( $p[ 'unit_id' ] );
			$dose_units[ $unit[ 'unit_id' ] ] = $unit[ 'unit' ];
		}

		echo json_encode( $dose_units );
	 }
	 /*
	  * 
	  * 
	  */
	  function medicament_amounts_data(){
	  	$amounts = array();
		$medicament_id = $this -> uri -> segment( 3 );
		
		$this -> load -> model( 'finance_model' );
		$this -> load -> model ( 'medicaments_model' );
		$product = $this -> finance_model -> get_products( NULL, $medicament_id );
		if( empty( $product ) ){
			echo json_encode( $product );
			return;
		}
		foreach( $product as $p ){
			
			$amounts[  $p[ 'medicament_amount' ] ] = $p[ 'medicament_amount' ];
		}

		echo json_encode( $amounts );
	  }
 }

