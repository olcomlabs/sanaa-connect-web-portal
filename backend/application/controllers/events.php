<?php
if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
class Events extends CI_Controller{
	var $logged = FALSE;
     var $return_codes;
	 var $help;

	function __construct(){
		parent::__construct();
		if($this   ->   olcomaccessengine   ->   is_logged()  === TRUE)
          {
                olcom_is_allowed_module_helper($this  ->  uri  ->  uri_string()) ;
                
                $this  ->  is_logged = TRUE;
           }
           else
           {
            
            if( $this  -> input  ->  is_ajax_request())
            {
                if(preg_match('/data/', $this  ->  uri  ->  uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
		   }
	         $this -> is_logged = FALSE;
	         $this -> load -> config( 'olcom_messages' );
	         $this -> load -> config( 'olcom_labels' );
			     $this -> load -> config( 'olcom_help_text' );
	         $this -> load -> config( 'olcomhms_return_codes' );
	         $this -> return_codes = $this -> config -> item( 'return_codes' );
	         $this -> messages = $this -> config -> item( 'messages' );
	         $this -> labels = $this -> config -> item( 'labels' );
	        $this -> help = $this -> config -> item( 'help_text' );        
	        $this -> load -> library( 'OlcomHmsFx' );
	        $this->load->model('events_model');
		

	}

	function index(){

				 
		$modules = $this -> olcomaccessengine -> load_modules( TRUE );
		//print_r( $modules[  $this -> uri -> segment( 1)] );return;
		$modules_configs = $this -> config -> item('modules_configs'  );
		$sub_modules = $modules[  $this -> uri -> segment( 1)];
		$this -> load -> config( 'olcomhms_modules_configs' );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
		
	 
		$launchers = array();
		foreach ($sub_modules as $key => $sub_module) {
			$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
		}
		
		$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
		$data[ 'launchers' ] = $launchers;
		
		$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );	
		
		 $this -> load -> view('main_template',array(
	              'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
	                
	            ));
	}

	function  published_events()
	{
		$this -> load -> library('OlcomHmsTemplateDatatable', 
	             array('header' => 'Published Events' , 'with_actions' => TRUE, 
	            'create_controller_fx' => 'events/create_events', //for create button on view
	             'columns' => array( 'ID#' , 'Event Name', 'Event Location','Event Owner','Event Date'), 
	             'controller_fx' => 'events/publishedEvents_data'));
	    
	            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
	    
	            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
	}


		function publishedEvents_data()
	{
                                     if( $this -> olcomaccessengine -> is_logged() === TRUE)
                          {
    			//load datatable lib
    			$this -> load -> library('OlcomHmsDataTables',array('columns'  => array(
    				'eventId','eventTitle','eventLocation','eventPostedBy','eventDate','EventId'
    			),
    			'index_column'  => 'eventId',
    			'table_name'  => 'events',
    			'controller_fx'  => 'events/actions',
    			'id_type'  => ''
    			));
    			
    			//get the data 
    			echo $this -> olcomhmsdatatables -> get_data();
            }else{
                redirect('authentication/login','refresh');
            }
	}


    function create_events(){

        $form_data = $this -> input -> post(NULL);
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){

           // var_dump($_FILES);

                $this->load->library('Services_Cloudinary');
                

                 try{
                $result = $this->services_cloudinary -> upload($_FILES['eventPhotoURL']['tmp_name'],array());
                //var_dump($result);

                $events = $form_data;
                $events['eventPhotoURL'] = $result['url'];
                //var_dump($event);

                //$form_data['o_dateCreated'] = date('Y-m-d H:i:s');
                $result = $this->events_model->create_events($events);
                if($result === -1 ){
                 olcom_server_form_message('Event Already Exists!', 0);
                }else{
                 olcom_server_form_message('Event Created!',1);
                }

            }catch(Exception $e){

                    olcom_server_form_message($e->getMessage(), 0);
                }
           
                
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array(
                 
                'with_submit_reset'  => TRUE,
                'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                'title'  => 'Create Event',
                'header_info'  => $messages['fill_details'],
                'is_multipart'=>TRUE
            )); 
            $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventTitle',
                'label'  => 'Event Title',
            ));
             $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventLocation',
                'label'  => 'Event Location'
            ));
              $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventDate',
                'label'  => 'Event Date',
                'class'=> 'daterangepicker2'
            ));
            $this -> olcomhmstemplateform -> add_field('file',array(
                'name'  => 'eventPhotoURL',
                'label'  => 'Event Photo'
            ));
               $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventPostedBy',
                'label'  => 'Event Posted By'
            ));
                $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventCategory',
                'label'  => 'Event Category'
            ));
            $this -> olcomhmstemplateform -> add_field('textarea',array(
                'name'  => 'eventDescriptions',
                'label'  => 'Event Description'
            ));

           
  
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('create_event_specific_scripts','',TRUE);
            
            $this -> load -> view('main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
	}




 /*
     * action function for processing tabledata actions
     */
     function actions(){
      
      if($this -> uri -> total_segments()  === 4){
      
            $this -> load -> config('olcom_messages');
            $dialog_messages = $this -> config -> item('messages');  
      /*
       * 
       * do some permission checking before continuing
       * 
       */
       
      if( $this  ->  olcomaccessengine  ->  is_allowed_module_action($this  ->  uri  ->  uri_string() )  === FALSE){
          
                
          olcom_show_dialog('message',array(
                                            'header'  => $dialog_messages[ 'permission_denied' ],
                                            'message'  => $dialog_messages[ 'permission_denied_'.$this  ->  uri  -> segment(3) ]
                                        ), 'error');
                return ;                        
      }
            
      
      $this -> load -> config('olcom_labels');
      $this -> load -> config('olcom_cookies');
      
      $olcom_cookies = $this -> config -> item('action_cookies');
      $labels = $this -> config -> item('labels');
      switch($this -> uri -> segment(3)){
        case 'view':
            
              $result = $this -> olcomhms -> read('events',
              array('*'),
              array('eventId'  => $this -> uri -> segment(4))
              );
                            
              if($result !=  NULL){
              $result = $result -> result_array();  
              //load dialog with data
              olcom_show_dialog('table', $result,'');
            } 
          break;
          
        case 'delete':
              /*
               *client-server confirmation 
               * 
               * cookies one for action info storage other confirm status
               */
               
              //check for confirm status
              $confirmed = 0;
              $cookie_status = $olcom_cookies['action_delete_confirm'];
              $cookie_action = $olcom_cookies['action_delete'];
              
              $confirmed = get_cookie($cookie_status);//returns FALSE where cookie not set
              
              if($confirmed  === FALSE){//cookie not set and means just clicked delete ico
              
              olcom_actions_cookies('delete', 'create');
              //load the confirmation dialog
              olcom_show_dialog('confirm',$dialog_messages['dialog_delete_party'],'');
            }
            else{//cookie set 
                  olcom_actions_cookies('delete', 'delete');
                  
                  if($this -> olcomhms -> delete('events',array(
                  'eventId'  => $this -> uri -> segment(4)
                  )) > 0){
                    
                    olcom_show_dialog('message', array(
                      'header'  => $dialog_messages['operation_successful'],
                      'message'  => $dialog_messages['record_deleted']
                    ), 'success');
                  }else{
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['attention'],
                      'message'  => $dialog_messages['error_deleting_record'],
                    ),'error');
                    
                  }
              }
              
          break;
        case 'edit':
          
              //client server coms
              /*
                * cookies 
                *   olcom-action-edit-save-click [yes ,no]
                *   olcom-action-edit [action link]
                */
             
              $cookie_save = $olcom_cookies['action_save_click'];
              $cookie_action = $olcom_cookies['action_edit'];
              
              $save_click = 0;
              $save_click = get_cookie($cookie_save);
              if($save_click  === FALSE){//just clicked update button
                //create cookies 
                olcom_actions_cookies('edit', 'create');
                // load data
                $result = $this -> olcomhms -> read(
                'events','*',array('eventId'  => $this -> uri -> segment(4))
                );//requested everything
                
                  $result = $result -> result_array();
                  $result = $result[0];
                  //create update form 
                  $this -> load -> config('olcom_labels');
                  $this -> load -> config('olcom_selects');
                  $labels = $this -> config -> item('labels');
                  $selects = $this -> config -> item('selects');
                  $this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Update Event',
                            'header_info'  => 'Fill the following Details',
                            'action'  => '#','id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                            'method'  => 'POST'
                  ));
                  //type,options
                  $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'eventTitle',
                    'id'  => 'supporterName',
                    'label'  => 'Event Title',
                    'value'  => $result['eventTitle']
                  ));
                  $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'eventCategory',
                    'id'  => 'eventCategory',
                    'label'  => 'Event Category',
                    'value'  => $result['eventCategory']
                  ));
                   $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'eventLocation',
                    'id'  => 'eventLocation',
                    'label'  => 'Event Location',
                     'value' => $result['eventLocation']
                  ));

                    $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'eventPostedBy',
                    'id'  => 'eventPostedBy',
                    'label'  => 'Event Posted By',
                     'value' => $result['eventPostedBy']
                  ));

                $this -> olcomhmstemplateform -> add_field('file',array(
                'name'  => 'eventPhotoURL',
                'label'  => 'Event Photo',
                 'value' => $result['eventPhotoURL']
               ));

                $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'eventDate',
                'label'  => 'Event Date',
                'value' => $result['eventDate'],
                'class'=> 'daterangepicker2'
            ));

                 $this -> olcomhmstemplateform -> add_field('textarea',array(
                'name'  => 'eventDescriptions',
                'label'  => 'Event Description',
                'value' => $result['eventDescriptions'],
            ));
                  
                  olcom_show_dialog('update', array(
                  'content'  => $this -> olcomhmstemplateform -> create_form(),
                  'specific_scripts'  => 'add_personnel_form_scripts'
                  ), '');
          
                  
                }else{
                
                   olcom_actions_cookies('edit', 'delete');
                  //update record
            
                  $data = $this -> input -> post(NULL);
                  if($this -> olcomhms -> edit('events',$data,
                    array('eventId'  => $this -> uri -> segment(4))) !=  0){
                    
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['operation_successful'],
                      'message'  => $dialog_messages['record_updated']
                    ), 'success');
                    
                  }else{
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['info'],
                      'message'  => $dialog_messages['no_changes']
                    ), 'info');
                    
                  }
                }//end else
          break;
        }//end switch
    }
  }





}