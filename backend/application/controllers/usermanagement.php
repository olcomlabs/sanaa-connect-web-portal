<?php if(!defined('BASEPATH'))exit('Direct access is restricted');

class Usermanagement extends CI_Controller{
	
    var $return_codes ;
	var $form_data;
	function __construct(){
		parent::__construct();
		
        if ($this -> olcomaccessengine -> is_logged() === TRUE) {
            
            olcom_is_allowed_module_helper($this -> uri -> uri_string());

            $this -> is_logged = TRUE;
        } else {

            if ($this -> input -> is_ajax_request()) {
                if (preg_match('/data/', $this -> uri -> uri_string()) === 1) {
                    echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                }
            } else {
                redirect('authentication/login', 'refresh');
            }

            $this -> is_logged = FALSE;

            $this -> load -> config('olcomhms_return_codes');
            
            if (!defined('SUCCESS_CODE'))
                define('SUCCESS_CODE', $this -> config -> item('success_code'));
           
            
        }
        $this  -> return_codes = $this -> config -> item( 'return_codes');
        // load models
        $this -> load -> model( 'usermanagement_model' );
        $this -> load -> model( 'employees' ) ;
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');
        
        
            
	}
	
	function index()
	{
			
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
	
	/*
	 * 
	 * 
	 * 
	 */
	function create_group(){
		
		$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
		$this -> load -> config('olcom_messages');
		$this -> load -> config('olcom_labels');
		$messages = $this -> config -> item('messages');
		$labels = $this -> config -> item('labels');
		
		if(isset($form_data) && $form_data!= NULL){
			if($this -> olcomhms -> record_exists('groups',$form_data,FALSE,TRUE,'and')    ===  FALSE){
			if($this -> olcomhms -> create('groups',$form_data)!= NULL){
				olcom_server_form_message($messages['group_created'], 1);
			}else{
				olcom_server_form_message($messages['failed_group'], 0);
				}
			}else{
				olcom_server_form_message($messages['group_exists'], 0);
			}
		}else{
		
			
			$this -> load -> library('OlcomHmsTemplateForm',array(
				 
				'with_submit_reset'  => TRUE,
				'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
				'title'  => 'Create Group',
				'header_info'  => $messages['fill_details']
			));	
			
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'group_name',
				'label'  => $labels['group_name']
			));
			$form = $this -> olcomhmstemplateform -> create_form();
			
			$form_specific_scripts = $this -> load -> view('create_group_specific_scripts','',TRUE);
			
			$this -> load -> view('main_template',array(
				'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
				
			));
		}
	}
	/*
	 * 
	 * 
	 * 
	 */ 
	function groups(){
		$this -> load -> config('olcom_labels');
		$this -> load -> config('olcom_messages');
		
		$labels = $this -> config -> item('labels');
		$messages = $this -> config -> item('messages');
		
		$this -> load -> library('OlcomHmsTemplateDatatable',array(
			'header'  => $labels['groups'],'controller_fx'  => 'usermanagement/groups_data',
			'columns'  => array($labels['id'],$labels['group_name']),
			'with_actions'  => TRUE,
			'create_controller_fx' => 'usermanagement/create_group'
		));
		
		$datatable = $this -> olcomhmstemplatedatatable -> create_view();
		
		$this -> load -> view('main_template',array(
			'template'  => array('content'  => $datatable['datatable'],
			'page_specific_scripts'  => $datatable['specific_scripts'])
		));
	}/*
	 * 
	 * 
	 * 
	 * 
	 */
	 function groups_data(){
	 	$this -> load -> library('OlcomHmsDataTables',
			array('table_name'  => 'groups','index_column'  => 'group_id',
			'columns'  => array('group_id','group_name','group_id'),'controller_fx'  => 'usermanagement/actions/groups')
		);
		
		echo $this -> olcomhmsdatatables -> get_data();
	 } 
	  
	/*
	 * 
	 * 
	 * controller db ops wrapper
	 */
	function actions(){
		
        $this -> load -> config('olcom_messages');
        $messages = $this -> config -> item('messages');
        
        if( $this  ->  olcomaccessengine  ->  is_allowed_module_action($this  ->  uri  ->  uri_string() )  ===  FALSE){
                
                
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                            'message'  => $messages[ 'permission_denied_'.$this  ->  uri  -> segment(3) ]
                                        ), 'error');
                return ;                        
            }
            
		$section = $this -> uri -> segment(3);
		$this -> load -> config('olcom_cookies');
		$this -> load -> config('olcom_labels');
		
		$labels = $this -> config -> item('labels');
		
		$olcom_cookies = $this -> config -> item('action_cookies');
		$cookie_save = $olcom_cookies['action_save_click'];
		$cookie_status = $olcom_cookies['action_delete_confirm'];
		
		if($section     ===  'groups'){
			switch($this -> uri -> segment(4)){
				case 'view':
						$result_num_users = $this -> olcomhms -> read('users',array('group_id'),array('group_id'  => $this -> uri -> segment(5)),NULL);
						
						$result_num_users = $result_num_users    ===  NULL? 0 : $result_num_users -> num_rows();
						
						$result_group = $this -> usermanagement_model -> get_groups( $this -> uri -> segment(5) );
						
						$result_group['num_users'] = $result_num_users;
						
						olcom_show_dialog('table', array( $result_group ), '');
					break;
					
				case 'edit':
							
							$save_click = 0;
							$save_click = get_cookie($cookie_save);
							
							if($save_click    ===  FALSE){//clicked edit
								//edit action track	
								olcom_actions_cookies('edit', 'create');
								
								//load data
								$result_group = $this -> usermanagement_model -> get_groups( $this -> uri -> segment(5) );
								
								//load update form
								$this -> load -> library('OlcomHmsTemplateForm',array(
									'title'  => $labels['edit_group' ]
								));
								$this -> olcomhmstemplateform -> add_field('text',array(
									'name'  => 'group_name',
									'label'  => $labels['group_name'],
									'value'  => $result_group['group_name']
								));
								olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
										'specific_scripts'  => 'create_group_specific_scripts'), '');
							}else{
								//form submitted
								
								$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
								$result = $this -> usermanagement_model -> update_group( $this -> uri -> segment( 5 ) , $form_data ) ;
                                 
                                if( $result === SUCCESS_CODE)
                                   {
                                      olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                                'header'  => $messages['operation_successful']), 'success');
                                    }
                                 if( $result !== SUCCESS_CODE)
                                    {
                                       olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                                'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                    }
								olcom_actions_cookies('edit', 'delete');
							}
					break;
					
			case 'delete':
							$confirmed = 0;
							$cookie_status = $olcom_cookies['action_delete_confirm'];
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								if($this -> olcomhms -> delete('groups',array('group_id'  => $this -> uri -> segment(5)))!= NULL){
									olcom_show_dialog('message', array('message'  => $messages['record_deleted'],'header'  => $messages['operation_successful']), 'success');
								}else{
									olcom_show_dialog('message', array('message'  => $messages['error_deleting_record'],
									'header'  => $messages['operation_failed']), 'error');
								}
								olcom_actions_cookies('delete', 'delete');
							}
				break;
			}
		}
		if($section     ===  'users'){
			switch($this -> uri -> segment(4)){
				case 'view':
						$result = $this -> olcomhms -> read('users',array('password'),array('employee_id'  => $this -> uri -> segment(5)),NULL);
						olcom_show_dialog('table', $result -> result_array(), '');
						break;
				case 'edit':
							$save_click = 0;
							$save_click = get_cookie($cookie_save);
							if($save_click    ===  FALSE){
								$result_user = $this -> olcomhms -> read('users',array('username','active','group_id','password'),array('employee_id'  => $this -> uri -> segment(5)));
								$result_user = $result_user -> row_array();
								//prepare update form 
								$this -> load -> library('OlcomHmsTemplateForm',array(
									'title'  => $labels['edit_user']
								));
								
								$result_user = $result_user;
								$this -> olcomhmstemplateform -> add_field('text',
								array(
									'name'  => 'username',
									'label'  => $labels['username'],
									'value'  => $result_user['username']
								));
                                
                                $oldpwd = $this -> encrypt -> encode( '0!dpWd' );
								$this -> olcomhmstemplateform -> add_field('password',array(
									'name'  => 'password',
									'label'  => $labels['password'],
									'value'  =>$oldpwd,
									'id'  => 'password'
								));
								
								$this -> olcomhmstemplateform -> add_field('password',array(
									'name'  => 'confirm_password',
									'label'  => $labels['confirm_password'],
									'value'  => $oldpwd
								));
								
								$this -> load -> config('olcom_selects');
								$selects = $this -> config -> item('selects');
								
								$this -> olcomhmstemplateform -> add_field('checkbox',array(
										'name'  => 'active',
										'label'  => $labels['active'],
										'value'  => 1,
										'class'  => 'ace-switch ace-switch-2',
										'checked'  => $result_user['active']
										));
									 
								$result_groups = $this -> olcomhms -> read('groups',array('group_id','group_name'),
									FALSE,FALSE,NULL);		
								$this -> olcomhmstemplateform -> add_field('select',array(
									'name'  => 'group_id',
									'label'  => $labels['group'],
									'choices'  => olcom_db_to_dropdown($result_groups),
									'selected'  => $result_user['group_id'],
									'js'  => 'class = \'olcom-chosen\''
								));
								
								olcom_actions_cookies('edit', 'create');
								olcom_show_dialog('update', array('content'  => $this -> olcomhmstemplateform -> create_form(),
								'specific_scripts'  => 'users_specific_scripts'), '');
							}else{
								
									$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
									
                                    if( $this -> encrypt -> decode( $form_data[ 'password' ] ) === '0!dpWd' )
                                    {
                                        unset( $form_data[ 'password' ] );
                                        
                                    }
                                    else {
                                         $form_data[ 'password' ] = md5( $form_data[ 'password' ] );
                                    }
                                    unset( $form_data[ 'confirm_password'] );
                                    
                                   
                                    $employee_info = $this -> employees -> info( $this -> uri -> segment( 5 ) ,'full' );
                                    
                                    if( $employee_info[ 'active' ] == 0 )
                                    {
                                        $user_info = $this -> usermanagement_model -> get_users( $this -> uri -> segment( 5 ) );
                                        if( $form_data[ 'active' ] == 1 )
                                        {
                                            if( $user_info[ 'active' ] == 0 )
                                            {
                                                
                                            }
                                        }
                                    }
                                    
                                    $result = $this -> usermanagement_model -> update_user( $this -> uri -> segment( 5 ) , $form_data );
								    
                                     if( $result === SUCCESS_CODE)
                                        {
                                             olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                                'header'  => $messages['operation_successful']), 'success');
                                        }
                                     if( $result !== SUCCESS_CODE)
                                        {
                                             olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                                'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                        }
								olcom_actions_cookies('edit', 'delete');
							}
							
						
					break;
				case 'delete':
							$confirmed = 0;
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								
                                
                                    $result = $this -> usermanagement_model -> delete_user( $this -> uri -> segment( 5 ) );
                                    
                                     if( $result === SUCCESS_CODE)
                                        {
                                             olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                                'header'  => $messages['operation_successful']), 'success');
                                        }
                                     if( $result !== SUCCESS_CODE)
                                        {
                                             olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                                'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                        }
								olcom_actions_cookies('delete', 'delete');
							}
					break;
			}
				
		}
		if($section     ===  'modules'){
			
			switch($this -> uri -> segment(4)){
				case 'view':
							$num_sub_modules  =  $this -> olcomhms -> read('sub_modules',array('sub_module_id'),array('module_id'  => $this  ->  uri  ->  segment( 5 ) ) );
							$num_sub_modules  =  $num_sub_modules     ===   NULL ? 0 : $num_sub_modules -> num_rows();
							
							$result_module  =  $this -> olcomhms -> read('modules',array('*'),array('module_id'  => $this -> uri -> segment(5)));
							
							$result_module = $result_module -> result_array();
							$result_module[0]['num_sub_modules'] = $num_sub_modules;	
							olcom_show_dialog('table', $result_module, '');	
						break;
				case 'edit':
							$confirm = get_cookie($cookie_save);
							if($confirm    ===  FALSE){
								olcom_actions_cookies('edit', 'create');
								
								$result_module = $this -> olcomhms -> read('modules',array('*'),array('module_id'  => $this -> uri -> segment(5)));
								$result_module = $result_module -> result_array();
								
								///create update form 
								$this -> load -> library('OlcomHmsTemplateForm',array('title'  => $labels['edit_module'],'header_info'  => $messages['fill_details']));
								$this -> olcomhmstemplateform -> add_field('text',array(
									'name'  => 'module_name',
									'label'  => $labels['module_name'],
									'value'  => $result_module[0]['module_name']
								));
								
								$update_form = $this -> olcomhmstemplateform -> create_form();
								
								olcom_show_dialog('update', array(
										'content'  => $update_form,'specific_scripts'  => 'create_module_specific_scripts'
								), '');
							}else{
								
								$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
								if($this -> olcomhms -> edit('modules',$form_data,array('module_id'  => $this -> uri -> segment(5)))!= 0){
									olcom_show_dialog('message', array('message'  => $messages['record_updated'],'header'  => $messages['operation_successful']),'success');
								}else{
									olcom_show_dialog('message',array('message'  => $messages['no_changes'],'header'  => $messages['info']), 'info');
								}
								olcom_actions_cookies('edit', 'delete');
							}
					break;
				case 'delete':
							$confirmed = 0;
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								if($this -> olcomhms -> delete('modules',array('module_id'  => $this -> uri -> segment(5)))!= NULL){
									olcom_show_dialog('message', array('message'  => $messages['record_deleted'],'header'  => $messages['operation_successful']), 'success');
								}else{
									olcom_show_dialog('message', array('message'  => $messages['error_deleting_record'],
									'header'  => $messages['operation_failed']), 'error');
								}
								olcom_actions_cookies('delete', 'delete');
							}
						break;		
			}
		}
		if($section     ===  'module_roles'){
			switch( $this -> uri -> segment( 4 ) ){
				case  'edit':
						$confirm = get_cookie($cookie_save);
					if($confirm    ===  FALSE){
						olcom_actions_cookies('edit', 'create');
						$result_module_role = $this -> olcomhms -> read(
							'module_permissions',array('*'),array('permission_id'  => $this -> uri -> segment(5))
						);
						
						$result_module_role = $result_module_role -> result_array();
						$result_module_role = $result_module_role[0];
						
						$result_groups = $this -> olcomhms -> read('groups',array('*'),FALSE,FALSE,'');
						
						$result_modules = $this -> olcomhms -> read('modules',array('*'),FALSE,FALSE,'');
						
						$this -> load -> library('OlcomHmsTemplateForm',array(
							'title'  => $labels['edit_module_role']
						));
						
						$this -> olcomhmstemplateform -> add_field('select',array(
							'name'  => 'group_id',
							'label'  => $labels['group'],
							'choices'  => olcom_db_to_dropdown($result_groups),
							'selected'  => $result_module_role['group_id']
						));
						
						$this -> olcomhmstemplateform -> add_field('select',array(
							'name'  => 'module_id',
							'label'  => $labels['module'],
							'choices'  => olcom_db_to_dropdown($result_modules),
							'selected'  => $result_module_role['module_id']
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'read',
							'label'  => $labels['read'],
							'value'  => 1,
							'checked'  => $result_module_role['read'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'write',
							'label'  => $labels['write'],
							'value'  => 1,
							'checked'  => $result_module_role['write'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'create',
							'label'  => $labels['create'],
							'value'  => 1,
							'checked'  => $result_module_role['create'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'delete',
							'label'  => $labels['delete'],
							'value'  => 1,
							'checked'  => $result_module_role['delete'],
							'class'  => 'ace-switch ace-switch-4'
						));
						
						olcom_show_dialog('update', array('content'  => $this -> olcomhmstemplateform -> create_form(),
						'specific_scripts'  => 'roles_specific_scripts'), '');
					}else{
						
						$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
						//edit permissions only
						$group_id = $form_data[ 'group_id' ];
                        $module_id = $form_data[ 'module_id' ];
                        
                        
						unset($form_data['group_id']);
						unset($form_data['module_id']);
						$form_data['read'] = isset($form_data['read']) ? 1 : 0;
						$form_data['write'] = isset($form_data['write']) ? 1: 0;
						$form_data['create'] = isset($form_data['create']) ? 1 : 0;
						$form_data['delete'] = isset($form_data['delete']) ? 1 : 0;
                        
                        $status =  $this -> usermanagement_model -> update_group_module_permissions( $group_id , $form_data );
						if( $status === SUCCESS_CODE )
						{
						    $result_module_subs = $this -> usermanagement_model -> get_modules_sub_modules( $module_id );
                            
                            if( $result_module_subs != NULL )
                            {
                                foreach( $result_module_subs as $sub_module )
                                {
                                    $result_sub_module_permissions = $this -> usermanagement_model -> get_group_sub_module_permissions( $group_id , $sub_module[ 'sub_module_id' ] );
                                    
                                    if( is_array( $result_sub_module_permissions ) )
                                    {
                                        $permissions = array();
                                        $permissions[ 'read' ] = $form_data[ 'read' ]  & $result_sub_module_permissions[ 'read' ];
                                        $permissions[ 'write' ] = $form_data[ 'write' ] & $result_sub_module_permissions[ 'write' ];
                                        $permissions[ 'create' ] = $form_data[ 'create' ]  & $result_sub_module_permissions[ 'create' ];
                                        $permissions[ 'delete' ] = $form_data[ 'delete' ] & $result_sub_module_permissions[ 'delete' ];
                                        
                                       $this -> usermanagement_model -> sync_group_sub_module_permissions( $group_id , $sub_module[ 'sub_module_id' ] , $permissions );      
                                    }
                                   
                                }
                            }
							olcom_show_dialog('message', array('message'  => $messages['record_updated'],'header'  => $messages['operation_successful']),'success');
						
                        }else{
							olcom_show_dialog('message',array('message'  => $this -> return_codes[ $status ],'header'  => $messages['info']), 'info');
						}
						olcom_actions_cookies('edit', 'delete');
					}
				break;
			case 'delete':
							$confirmed = 0;
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								if($this -> olcomhms -> delete('module_permissions',array('permission_id'  => $this -> uri -> segment(5)))!= NULL){
									olcom_show_dialog('message', array('message'  => $messages['record_deleted'],'header'  => $messages['operation_successful']), 'success');
								}else{
									olcom_show_dialog('message', array('message'  => $messages['error_deleting_record'],
									'header'  => $messages['operation_failed']), 'error');
								}
								olcom_actions_cookies('delete', 'delete');
							}
				break;
			}
		}
		if($section     ===  'sub_modules'){
			switch($this -> uri -> segment(4)){
				case 'edit':
							$confirm = get_cookie($cookie_save);
							if($confirm    ===  FALSE){
								olcom_actions_cookies('edit', 'create');
								
								$result_sub_module = $this -> olcomhms -> read('sub_modules',array('*'),array('sub_module_id'  => $this -> uri -> segment(5)),NULL,NULL);
								$result_sub_module = $result_sub_module -> result_array();
								
								$result_modules = $this -> olcomhms -> read('modules',array('*'),NULL,NULL,NULL);
								
								///create update form 
								$this -> load -> library('OlcomHmsTemplateForm',array('title'  => $labels['edit_sub_module'],'header_info'  => $messages['fill_details']));
								$this -> olcomhmstemplateform -> add_field('text',array(
									'name'  => 'sub_module_name',
									'label'  => $labels['sub_module_name'],
									'value'  => $result_sub_module[0]['sub_module_name']
								));
								$this -> olcomhmstemplateform -> add_field('select',array(
									'name'  => 'module_id',
									'label'  => $labels['module_name'],
									'choices'  => olcom_db_to_dropdown($result_modules),
									'selected'  => $result_sub_module[0]['module_id']
								));
								$update_form = $this -> olcomhmstemplateform -> create_form();
								
								olcom_show_dialog('update', array(
										'content'  => $update_form,'specific_scripts'  => 'create_sub_module_specific_scripts'
								), '');
							}else{
								
								$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
                                
								if($this -> olcomhms -> edit('sub_modules',$form_data,array('sub_module_id'  => $this -> uri -> segment(5)))!= 0){
									olcom_show_dialog('message', array('message'  => $messages['record_updated'],'header'  => $messages['operation_successful']),'success');
								}else{
									olcom_show_dialog('message',array('message'  => $messages['no_changes'],'header'  => $messages['info']), 'info');
								}
								olcom_actions_cookies('edit', 'delete');
							}
					break;
				case 'delete':
							$confirmed = 0;
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								if($this -> olcomhms -> delete('sub_modules',array('sub_module_id'  => $this -> uri -> segment(5)))!= NULL){
									olcom_show_dialog('message', array('message'  => $messages['record_deleted'],'header'  => $messages['operation_successful']), 'success');
								}else{
									olcom_show_dialog('message', array('message'  => $messages['error_deleting_record'],
									'header'  => $messages['operation_failed']), 'error');
								}
								olcom_actions_cookies('delete', 'delete');
							}
						break;	
			}
		}
		if($section     ===  'sub_module_roles'){
			switch($this -> uri -> segment(4)){
				case  'edit':
					
					$confirm = get_cookie($cookie_save);
					if($confirm    ===  FALSE){
						olcom_actions_cookies('edit', 'create');
						$result_sub_module_role = $this -> olcomhms -> read(
							'sub_module_permissions',array('*'),array('sub_permission_id'  => $this -> uri -> segment(5))
						);
						
						$result_sub_module_role = $result_sub_module_role -> result_array();
						$result_sub_module_role = $result_sub_module_role[0];
						
						$result_groups = $this -> olcomhms -> read('groups',array('*'),FALSE,FALSE,'');
						
						$result_sub_modules = $this -> olcomhms -> read('sub_modules',array('sub_module_id','sub_module_name'),FALSE,FALSE,'');
						
						$this -> load -> library('OlcomHmsTemplateForm',array(
							'title'  => $labels['edit_sub_module_role'],
							'header_info'  => $messages['fill_details']
						));
						
						$this -> olcomhmstemplateform -> add_field('select',array(
							'name'  => 'group_id',
							'label'  => $labels['group'],
							'choices'  => olcom_db_to_dropdown($result_groups),
							'selected'  => $result_sub_module_role['group_id']
						));
						
						$this -> olcomhmstemplateform -> add_field('select',array(
							'name'  => 'sub_module_id',
							'label'  => $labels['sub_module'],
							'choices'  => olcom_db_to_dropdown($result_sub_modules),
							'selected'  => $result_sub_module_role['sub_module_id']
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'read',
							'label'  => $labels['read'],
							'value'  => 1,
							'checked'  => $result_sub_module_role['read'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'write',
							'label'  => $labels['write'],
							'value'  => 1,
							'checked'  => $result_sub_module_role['write'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'create',
							'label'  => $labels['create'],
							'value'  => 1,
							'checked'  => $result_sub_module_role['create'],
							'class'  => 'ace-switch ace-switch-4'
						));
						$this -> olcomhmstemplateform -> add_field('checkbox',array(
							'name'  => 'delete',
							'label'  => $labels['delete'],
							'value'  => 1,
							'checked'  => $result_sub_module_role['delete'],
							'class'  => 'ace-switch ace-switch-4'
						));
						
						olcom_show_dialog('update', array('content'  => $this -> olcomhmstemplateform -> create_form(),
						'specific_scripts'  => 'sub_module_roles_specific_scripts'), '');
					}else{
						olcom_actions_cookies('edit', 'delete');
						$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
						//edit permissions only
						unset($form_data['group_id']);
						unset($form_data['sub_module_id']);
						
						$result_sub_module_permissions = $this -> olcomhms -> read('sub_module_permissions',array('sub_module_id','group_id'),
							array('sub_permission_id'  => $this -> uri -> segment(5)),FALSE,TRUE,NULL);
						$result_sub_module_permissions = $result_sub_module_permissions -> result_array();
						$result_sub_module_permissions = $result_sub_module_permissions[0];
						
						$result_sub_module = $this -> olcomhms -> read('sub_modules',array('module_id'),
						array('sub_module_id'  => $result_sub_module_permissions['sub_module_id']),FALSE,TRUE,'and');
						
						$result_sub_module = $result_sub_module -> result_array();
						$result_sub_module = $result_sub_module[0];
						
						//retrieve main module permissions
						
						$result_module_permissions = $this -> olcomhms -> read('module_permissions',
						array('read','write','create','delete'),
						array('module_id'  => $result_sub_module['module_id'],
						'group_id'  => $result_sub_module_permissions['group_id']),
						FALSE,TRUE,'and');
						
						
						$result_module_permissions = $result_module_permissions -> result_array();
						$result_module_permissions = $result_module_permissions[0];
						
						//on / off sub_modules
						$form_data['read']  =    $result_module_permissions['read'] & isset($form_data['read'])?1:0;
						$form_data['write']  =   $result_module_permissions['write'] & isset($form_data['write'])?1:0 ;
						$form_data['create']  =  $result_module_permissions['create'] & isset($form_data['create'])?1:0;
						$form_data['delete']  =  $result_module_permissions['delete'] & isset($form_data['delete'])?1:0;
						if($this -> olcomhms -> edit('sub_module_permissions',$form_data,array('sub_permission_id'  => $this -> uri -> segment(5)))!= 0){
							olcom_show_dialog('message', array('message'  => $messages['record_updated'],'header'  => $messages['operation_successful']),'success');
						}else{
							olcom_show_dialog('message',array('message'  => $messages['no_changes'],'header'  => $messages['info']), 'info');
						}
						
					}
				break;
			case 'delete':
							$confirmed = 0;
							$confirmed = get_cookie($cookie_status);
							if($confirmed    ===  FALSE){
								olcom_actions_cookies('delete', 'create');
								olcom_show_dialog('confirm',$messages['confirm_delete'],'');
							}else{
								if($this -> olcomhms -> delete('sub_module_permissions',array('sub_permission_id'  => $this -> uri -> segment(5)))!= NULL){
									olcom_show_dialog('message', array('message'  => $messages['record_deleted'],'header'  => $messages['operation_successful']), 'success');
								}else{
									olcom_show_dialog('message', array('message'  => $messages['error_deleting_record'],
									'header'  => $messages['operation_failed']), 'error');
								}
								olcom_actions_cookies('delete', 'delete');
							}
				break;
			}
		}
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 */
	 function create_user(){
	 	$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
		$this -> load -> config('olcom_messages');
		$messages = $this -> config -> item('messages');
		
		if(isset($form_data) && $form_data!= NULL){
				
				if($this -> olcomhms -> record_exists('users',array('username'  => $form_data['username']),FALSE,TRUE,'and')    ===  FALSE){
					
					unset($form_data['confirm_password']);
					$form_data['active']  =  isset( $form_data['active'] ) ? 1 : 0;
                    $form_data['password']  =  md5($form_data['password']);
                    
					if($this -> olcomhms -> create('users',$form_data)!= NULL){
						olcom_server_form_message($messages['user_created'], 1);
					}else{
						olcom_server_form_message($messages['error_creating_record'], 0);
					}
					
				}else{
					olcom_server_form_message($messages['username_taken'],0);
				}
		}else{
			
			$this -> load -> config('olcom_labels');
			
			$this -> load -> config('olcom_selects');
			
			
			$labels = $this -> config -> item('labels');
			
			$selects = $this -> config -> item('selects');
			//load form
			
			$this -> load -> library('OlcomHmsTemplateForm',array(
				'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
				'with_submit_reset'  => TRUE,'title'  => $labels['create_user'],
				'header_info'  => $messages['fill_details']
			));
			$this -> load -> model('employees');
			$result = $this -> employees -> get_userless_employees();
			
			$this -> olcomhmstemplateform -> add_field('select',
				array(
					'name'  => 'employee_id',
					'label'  => $labels['employee_name'],
					'choices'  => olcom_db_to_dropdown($result),
					'js'  => 'class = \'olcom-chosen\''
				));
			
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'username',
				'label'  => $labels['username']
				)
			);
			$this -> olcomhmstemplateform -> add_field('password',array(
				'name'  => 'password',
				'label'  => $labels['password'],
				'id'  => 'password'
			));
			$this -> olcomhmstemplateform -> add_field('password',array(
				'name'  => 'confirm_password',
				'label'  => $labels['confirm_password']
			));
			$this -> olcomhmstemplateform -> add_field('checkbox',array(
				'name'  => 'active',
				'label'  => $labels['active'],
				'value'  => 1,
				'class'  => 'ace-switch ace-switch-2'
			));
			$result_group = $this -> olcomhms -> read('groups',array('group_id','group_name'),NULL,NULL,NULL);
			$this -> olcomhmstemplateform -> add_field('select',array(
				'name'  => 'group_id',
				'label'  => $labels['group'],
				'choices'  => olcom_db_to_dropdown($result_group),
				'js'  => 'class = \'olcom-chosen\''
				));
				
			$this -> load -> view('main_template',array('template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $this -> load -> view('users_specific_scripts','',TRUE))));	
		}
	 }
	/*
	 * 
	 * 
	 * 
	 */
	 function users(){
	 	$this -> load -> config('olcom_labels');
		$this -> load -> config('olcom_messages');
		
		$labels = $this -> config -> item('labels');
		$messages = $this -> config -> item('messages');
		
		$this -> load -> library('OlcomHmsTemplateDatatable',array(
			'header'  => $labels['users'],'controller_fx'  => 'usermanagement/user_data',
			'columns'  => array($labels['id'],$labels['firstname'],$labels['lastname'],$labels['username'],$labels['group'],$labels['active']),
			'with_actions'  => TRUE,
			'create_controller_fx' => 'usermanagement/create_user'
		));
		
		$datatable = $this -> olcomhmstemplatedatatable -> create_view();
		
		$this -> load -> view('main_template',array(
			'template'  => array('content'  => $datatable['datatable'],
			'page_specific_scripts'  => $datatable['specific_scripts'])));
	 }
	 /*
	  * 
	  * 
	  * 
	  */
	  function user_data(){
	  		  	$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
			array(
				'tables'  => array(
					'users'  => array('employee_id','username','group_id','active'),
					'groups'  => array('group_id','group_name'),
					'personnel'  => array('personnel_id','firstname','lastname')
					),
				'with_actions'  => TRUE,
				'hidden_cols'  => array('group_id','personnel_id'),
				'index_column'  => 'employee_id',
				'controller_fx'  => 'usermanagement/actions/users',//for actions
				'id_type'  => 'employee',
				'where'  => array('users.group_id'  => 'groups.group_id','users.employee_id'  => 'personnel.personnel_id'),
				'data_style_format'  => array(
					'active'  => array(
							'yes_no'  => TRUE
						)
				),
				'order'  => array('firstname'  => 1,'lastname'  => 2,'username'  => 3,'active'  => 5,'group_name'  => 4)
			)
		);
		echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
	  }
	  /*
	   * 
	   * 
	   * 
	   * 
	   */
	   function create_module(){
	   		$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_messages');
			
			$messages = $this -> config -> item('messages');
			$labels = $this -> config -> item('labels');
			
			$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
			if(isset($form_data) && $form_data!= NULL){
				$form_data['module_name'] = strtolower($form_data['module_name']);
				if($this -> olcomhms -> record_exists('modules',$form_data,FALSE,TRUE,'and')    ===  FALSE){
					if($this -> olcomhms -> create('modules',$form_data)!= NULL){
						olcom_server_form_message($messages['module_created'], 1);
					}else{
						olcom_server_form_message($messages['module_not_created'], 0);
					}
				}else{
					olcom_server_form_message($messages['module_exists'], 0);
				}
			
			}else{
				$this -> load -> library('OlcomHmsTemplateForm',array(
					'title'  => $labels['create_module'],
					'header_info'  => $messages['fill_details'],
					'with_submit_reset'  => TRUE
				));
				$this -> olcomhmstemplateform -> add_field('text',array(
					'name'  => 'module_name',
					'label'  => $labels['module_name']));
				$specific_scripts = $this -> load -> view('create_module_specific_scripts','',TRUE);
				$this -> load -> view('main_template',array('template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $specific_scripts)
				));
			}
	   }
		/*
		 * 
		 * 
		 * 
		 */
		 function modules(){
		 	$this -> load -> config('olcom_labels');
			$labels = $this -> config -> item('labels');
			
		 	$this -> load -> library('OlcomHmsTemplateDatatable',array(
			'header'  => $labels['modules'],
			'with_actions'  => TRUE,
			'create_controller_fx' => 'usermanagement/create_module',
			'columns'  => array($labels['id'],$labels['module_name']),
			'controller_fx'  => 'usermanagement/module_data'));
			
			$datatable = $this -> olcomhmstemplatedatatable -> create_view();
		
			$this -> load -> view('main_template',array(
			'template'  => array('content'  => $datatable['datatable'],
			'page_specific_scripts'  => $datatable['specific_scripts'])));
		 }
		 /*
		  * 
		  * 
		  * 
		  * 
		  */
		  function module_data(){
		  	$this -> load -> library('OlcomHmsDataTables',array(
				'index_column'  => 'module_id',
				'table_name'  => 'modules',
				'columns'  => array('module_id','module_name','module_id'),
				'with_actions'  => TRUE,
				'controller_fx'  => 'usermanagement/actions/modules'
			));
			
			echo $this -> olcomhmsdatatables -> get_data();
		  }
		  /*
		   * 
		   * 
		   */
		   function create_module_role(){
		   	
			$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
			$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_messages');
			
			
			if(isset($form_data) && $form_data!= ''){
				$messages = $this -> config -> item('messages');
				if($this -> olcomhms -> record_exists('module_permissions',array('group_id'  => $form_data['group_id'],
				'module_id'  => $form_data['module_id']),FALSE,TRUE,'and')    ===  FALSE){
				if($this -> olcomhms -> create('module_permissions',$form_data)!= NULL){
					olcom_server_form_message($messages['module_role_created'], 1);
				}else{
						olcom_server_form_message($messages['module_role_not_created'], 0);
					}
				}else{
					olcom_server_form_message($messages['group_role_exists'], 0);
				}
			}else{
				$labels = $this -> config -> item('labels');
				$messages = $this -> config -> item('messages');
				
				$this -> load -> library('OlcomHmsTemplateForm',array(
				 			'title'  => $labels['create_module_role'],
							'header_info'  => $messages['fill_details'],
							'with_submit_reset'  => TRUE));
				$result_groups = $this -> olcomhms -> read('groups',array('*'),NULL,NULL,NULL	);
				$this -> olcomhmstemplateform -> add_field('select',array(
					'name'  => 'group_id',
					'label'  => $labels['group'],
					'choices'  => olcom_db_to_dropdown($result_groups)
				));
				
				$result_modules = $this -> olcomhms -> read('modules',array('*'),NULL,NULL,NULL);
				
				$this -> olcomhmstemplateform -> add_field('select',array(
					'name'  => 'module_id',
					'label'  => $labels['module'],
					'choices'  => olcom_db_to_dropdown($result_modules)
					
				));
				
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'read',
					'label'  => $labels['read'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'write',
					'label'  => $labels['write'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'create',
					'label'  => $labels['create'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'delete',
					'label'  => $labels['delete'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> load -> view('main_template',array('template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $this -> load -> view('roles_specific_scripts','',TRUE)
				)));
			}
			
		}
		function module_roles(){
				$this -> load -> config('olcom_labels');
				$this -> load -> config('olcom_messages');
				
				$labels = $this -> config -> item('labels');
				$messages = $this -> config -> item('messages');
				
				$this -> load -> library('OlcomHmsTemplateDatatable',array(
					'header'  => $labels['module_roles'],'controller_fx'  => 'usermanagement/module_roles_data',
					'columns'  => array($labels['id'],$labels['group'],$labels['module'],$labels['read'],$labels['write'],$labels['create'],$labels['delete']),
					'with_actions'  => TRUE,
					'create_controller_fx' => 'usermanagement/create_module_role'
				));
				
				$datatable = $this -> olcomhmstemplatedatatable -> create_view();
				
				$this -> load -> view('main_template',array(
					'template'  => array('content'  => $datatable['datatable'],
					'page_specific_scripts'  => $datatable['specific_scripts'])));
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 */
		 function module_roles_data(){
		 	$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
				array(
					'tables'  => array(
						'module_permissions'  => array('permission_id','module_id','group_id','read','write',
						'create','delete'),
						'groups'  => array('group_id','group_name'),
						'modules'  => array('module_id','module_name')
						),
					'with_actions'  => TRUE,
					'hidden_cols'  => array('group_id','module_id'),
					'index_column'  => 'permission_id',
					'controller_fx'  => 'usermanagement/actions/module_roles',//for actions
					'id_type'  => '',
					'where'  => array('groups.group_id'  => 'module_permissions.group_id',
					'module_permissions.module_id'  => 'modules.module_id'),
					'data_style_format'  => array(
						'read'  => array(
								'on_off'  => TRUE
							),
						'write'  => array(
								'on_off'  => TRUE
							),
						'create'  => array(
								'on_off'  => TRUE
							),
						'delete'  => array(
								'on_off'  => TRUE
							)
					),
					'order'  => array('group_name'  => 1,'module_name'  => 2,'read'  => 3,'write'  => 4,'create'  => 5,'delete'  => 6)
				)
			);
			echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
		 }

		/*
		 * 
		 * 
		 * 
		 * 
		 */
		 function create_sub_module(){
		 	
		 	$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_messages');
			
			$messages = $this -> config -> item('messages');
			$labels = $this -> config -> item('labels');
			
			$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
			if(isset($form_data) && $form_data!= NULL){
				$form_data['sub_module_name'] = strtolower($form_data['sub_module_name']);
				if($this -> olcomhms -> record_exists('sub_modules',$form_data,FALSE,TRUE,'and')    ===  FALSE){
					if($this -> olcomhms -> create('sub_modules',$form_data)!= NULL){
						olcom_server_form_message($messages['sub_module_created'], 1);
					}else{
						olcom_server_form_message($messages['sub_module_not_created'], 0);
					}
				}else{
					olcom_server_form_message($messages['sub_module_exists'], 0);
				}
			
			}else{
				$this -> load -> library('OlcomHmsTemplateForm',array(
					'title'  => $labels['create_sub_module'],
					'header_info'  => $messages['fill_details'],
					'with_submit_reset'  => TRUE
				));
				//load modules
				$result_modules = $this -> olcomhms -> read('modules',array('*'),NULL,NULL,NULL);
				$this -> olcomhmstemplateform -> add_field('select',array(
					'name'  => 'module_id',
					'label'  => $labels['module'],
					'choices'  => olcom_db_to_dropdown($result_modules)
				));
				$this -> olcomhmstemplateform -> add_field('text',array(
					'name'  => 'sub_module_name',
					'label'  => $labels['sub_module_name']));
				$specific_scripts = $this -> load -> view('create_sub_module_specific_scripts','',TRUE);
				$this -> load -> view('main_template',array('template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $specific_scripts)
				));
			}
		 }
		/*
		 * 
		 * 
		 * 
		 */
		 function sub_modules(){
		 	$this -> load -> config('olcom_labels');
			$labels = $this -> config -> item('labels');
			
		 	$this -> load -> library('OlcomHmsTemplateDatatable',array(
			'header'  => $labels['sub_modules'],
			'with_actions'  => TRUE,
			'create_controller_fx' => 'usermanagement/create_sub_module',
			'columns'  => array($labels['id'],$labels['sub_module_name'],$labels['module_name']),
			'controller_fx'  => 'usermanagement/sub_module_data'));
			
			$datatable = $this -> olcomhmstemplatedatatable -> create_view();
		
			$this -> load -> view('main_template',array(
			'template'  => array('content'  => $datatable['datatable'],
			'page_specific_scripts'  => $datatable['specific_scripts'])));
		 }
		 /*
		  * 
		  * 
		  * 
		  */
		function sub_module_data(){
			$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
			array(
				'tables'  => array(
					'sub_modules'  => array('sub_module_id','sub_module_name','module_id'),
					'modules'  => array('module_id','module_name')
					),
				'with_actions'  => TRUE,
				'hidden_cols'  => array('module_id'),
				'index_column'  => 'sub_module_id',
				'controller_fx'  => 'usermanagement/actions/sub_modules',//for actions
				'id_type'  => '',
				'where'  => array('sub_modules.module_id'  => 'modules.module_id'),
				'data_style_format'  => NULL,
				'order'  => array('sub_module_name'  => 1,'module_name'  => 2)
			)
		);
		echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
		}
		/*
		 * 
		 * 
		 * 
		 */
		 function create_sub_module_role(){
		 	$form_data = $this -> input -> post( NULL );unset( $form_data[ 'hms-datatable_length']);
			$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_messages');
			
			
			if(isset($form_data) && $form_data!= ''){
				$messages = $this -> config -> item('messages');
				if($this -> olcomhms -> record_exists('sub_module_permissions',array('group_id'  => $form_data['group_id'],
				'sub_module_id'  => $form_data['sub_module_id']),FALSE,TRUE,'and')    ===  FALSE){
					
				//retrieve main module
				$result_sub_module = $this -> olcomhms -> read('sub_modules',array('module_id'),array('sub_module_id'  => $form_data['sub_module_id']),FALSE,TRUE,'and');
				$result_sub_module = $result_sub_module -> result_array();
				
				//retrieve main module permissions
				$result_module_permissions = $this -> olcomhms -> read('module_permissions',array('read','write','create','delete'),array('module_id'  => $result_sub_module[0]['module_id'],'group_id'  => $form_data['group_id']),FALSE,TRUE,'and');
				
				if( $result_module_permissions == NULL )
                {
                  olcom_server_form_message($messages[ 'no_group_module_role'], 0 );
                  return;
                }
				$result_module_permissions = $result_module_permissions -> result_array();
				$result_module_permissions = $result_module_permissions[0];
				
				//on / off sub_modules
				$form_data['read']  =    $result_module_permissions['read'] & isset($form_data['read'])?1:0;
				$form_data['write']  =   $result_module_permissions['write'] & isset($form_data['write'])?1:0 ;
				$form_data['create']  =  $result_module_permissions['create'] & isset($form_data['create'])?1:0;
				$form_data['delete']  =  $result_module_permissions['delete'] & isset($form_data['delete'])?1:0;
				
				if($this -> olcomhms -> create('sub_module_permissions',$form_data)!= NULL){
					olcom_server_form_message($messages['sub_module_role_created'], 1);
				}else{
						olcom_server_form_message($messages['module_role_not_created'], 0);
					}
				}else{
					olcom_server_form_message($messages['group_role_exists'], 0);
				}
			}else{
				$labels = $this -> config -> item('labels');
				$messages = $this -> config -> item('messages');
				
				$this -> load -> library('OlcomHmsTemplateForm',array(
				 			'title'  => $labels['create_sub_module_role'],
							'header_info'  => $messages['fill_details'],
							'with_submit_reset'  => TRUE));
				$result_groups = $this -> olcomhms -> read('groups',array('*'),NULL,NULL,NULL	);
				$this -> olcomhmstemplateform -> add_field('select',array(
					'name'  => 'group_id',
					'label'  => $labels['group'],
					'choices'  => olcom_db_to_dropdown($result_groups)
				));
				
				$result_sub_modules = $this -> olcomhms -> read('sub_modules',array('sub_module_id','sub_module_name'),NULL,NULL,NULL);
				
				$this -> olcomhmstemplateform -> add_field('select',array(
					'name'  => 'sub_module_id',
					'label'  => $labels['sub_module'],
					'choices'  => olcom_db_to_dropdown($result_sub_modules)
					
				));
				
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'read',
					'label'  => $labels['read'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'write',
					'label'  => $labels['write'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'create',
					'label'  => $labels['create'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',array(
					'name'  => 'delete',
					'label'  => $labels['delete'],
					'value'  => 1,
					'class'  => 'ace-switch ace-switch-4'
				));
				$this -> load -> view('main_template',array('template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $this -> load -> view('sub_module_roles_specific_scripts','',TRUE)
				)));
			}
		 }

		/*
		 * 
		 * 
		 * 
		 * 
		 */
		 function sub_module_roles(){
		 	$this -> load -> config('olcom_labels');
				$this -> load -> config('olcom_messages');
				
				$labels = $this -> config -> item('labels');
				$messages = $this -> config -> item('messages');
				
				$this -> load -> library('OlcomHmsTemplateDatatable',array(
					'header'  => $labels['sub_module_roles'],'controller_fx'  => 'usermanagement/sub_module_roles_data',
					'columns'  => array($labels['id'],$labels['group'],$labels['sub_module'],$labels['module'],$labels['read'],$labels['write'],$labels['create'],$labels['delete']),
					'with_actions'  => TRUE,
					'create_controller_fx' => 'usermanagement/create_sub_module_role'
				));
				
				$datatable = $this -> olcomhmstemplatedatatable -> create_view();
				
				$this -> load -> view('main_template',array(
					'template'  => array('content'  => $datatable['datatable'],
					'page_specific_scripts'  => $datatable['specific_scripts'])));
		 }
		 /*
		  * 
		  * 
		  * 
		  * 
		  */
		  function sub_module_roles_data(){
		  		$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
				array(
					'tables'  => array(
						'sub_module_permissions'  => array('sub_permission_id','sub_module_id','group_id','read','write',
						'create','delete'),
						'groups'  => array('group_id','group_name'),
						'sub_modules'  => array('sub_module_id','sub_module_name'),
						'modules'  => array('module_id','module_name')
						),
					'with_actions'  => TRUE,
					'hidden_cols'  => array('group_id','module_id','sub_module_id'),
					'index_column'  => 'sub_permission_id',
					'controller_fx'  => 'usermanagement/actions/sub_module_roles',//for actions
					'id_type'  => '',
					'where'  => array('groups.group_id'  => 'sub_module_permissions.group_id',
					'sub_module_permissions.sub_module_id'  => 'sub_modules.sub_module_id',
					'modules.module_id'  => 'sub_modules.module_id'),
					'data_style_format'  => array(
						'read'  => array(
								'on_off'  => TRUE
							),
						'write'  => array(
								'on_off'  => TRUE
							),
						'create'  => array(
								'on_off'  => TRUE
							),
						'delete'  => array(
								'on_off'  => TRUE
							)
					),
					'order'  => array('group_name'  => 1,'sub_module_name'  => 2,'module_name'  => 3,'read'  => 4,'write'  => 5,'create'  => 6,'delete'  => 7)
				)
			);
			echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
		  }
}

