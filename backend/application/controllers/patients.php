<?php if(! defined( 'BASEPATH')) exit( ' Direct access is restricted');
/*
 * 
 * 
 * 
 */
 class Patients extends CI_Controller{
     
     var $logged = FALSE;
     var $return_codes ;
     function Patients(){
          parent:: __construct();
          $this -> load -> config ('olcomhms_return_codes');
          $this -> return_codes = $this -> config -> item( 'return_codes' );
		  $this -> load -> model( 'finance_model' );
		  $this -> load -> model( 'hospitalizations_model' );
		  $this -> load -> model( 'medicaments_model' );
          
          if($this  ->  olcomaccessengine  ->  is_logged()  === TRUE)
          {
                olcom_is_allowed_module_helper($this -> uri -> uri_string()) ;
                
                $this -> is_logged = TRUE;
           }
           else
           {
            
            if( $this ->input -> is_ajax_request())
            {
                if(preg_match('/data/', $this -> uri -> uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
            
            $this->is_logged = FALSE;
            
            $this -> load -> config ('olcomhms_return_codes');
            if( ! defined ( 'SUCCESS_CODE' ))
                define ('SUCCESS_CODE',$this -> config -> item('success_code')); 
        }
     }
     
     function index()
	{
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
	
     function register_patient(){
        
       $form_data = $this -> input -> post(NULL);

        $this -> load -> config('olcom_messages');
        $messages = $this -> config -> item('messages');
        $this -> load -> config( 'olcom_labels' );
        $labels = $this -> config -> item('labels');
        
        
        $return_codes = $this ->config -> item( 'return_codes');
        if( isset( $form_data ) AND $form_data != NULL)
        {
            $idmatch =   olcomhms_db_id( $form_data[ 'patient_id' ], TRUE ) ;
			$form_data[ 'initial' ] = $idmatch[ 0 ];
			$form_data[ 'personnel_id' ] = $idmatch[ 1 ];
			unset( $form_data[ 'patient_id' ] );
            $return = $this -> patients_model -> register_patient($form_data);
			
           $return = SUCCESS_CODE;
           if( $return  === SUCCESS_CODE )
           {
                olcom_server_form_message($messages['patient_registered'],1);
           }
           else
           {
            olcom_server_form_message($return_codes[$return],0);
           } 
        }
        else
        {
            
            $this -> load -> library( 'OlcomHmsTemplateForm' ,array(
                'title' => $labels['register_patient'],
                'header_info' => $messages[ 'fill_details' ],
                'with_submit_reset' => TRUE  
            ));
            
            $this -> load -> config ('olcom_selects');
            $selects = $this -> config -> item( 'selects');
              
			$this -> olcomhmstemplateform -> add_field('search' ,array(
                'name' => 'patient_id',
                'label' => $labels['patient_id' ],
                'id' => 'patient_id'
            ));         
            $this -> olcomhmstemplateform -> add_field('text' ,array(
                'name' => 'firstname',
                'label' => $labels['firstname' ]
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                'name' => 'middlename',
                'label' => $labels['middlename']
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                'name' => 'lastname',
                'label' => $labels['lastname']
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                'name' => 'phone',
                'label' => $labels['phone'],
                'id' => 'phone'
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' ,array(
                'name' => 'address',
                'label' => $labels['address']
            ));
            $this -> olcomhmstemplateform -> add_field( 'select' ,array(
                'name' => 'region',
                'label' => $labels['region'],
                'choices' => $selects['region_select'],
                'selected' => 'Dar es Salaam'
            ));
            
            $this -> olcomhmstemplateform -> add_field('radio',array(
                'name'  => 'gender',
                'id'  => 'gender',
                'label'  => $labels['gender'],
                'choices'  => $selects['gender_select']
                
            ));
            $this -> olcomhmstemplateform -> add_field( 'text',array(
                'name' => 'age',
                'label' => $labels['age']
            ));
            
            $this -> olcomhmstemplateform -> add_field('select', array(
                'name' => 'blood_type',
                'label' => $labels['blood_type'],
                'choices' => $selects['blood_type'],
                'js'  => 'class = \'olcom-chosen\''
            ));
             $this -> olcomhmstemplateform -> add_field('select', array(
                'name' => 'blood_type_sign',
                'label' => '  ',
                'choices' => $selects['blood_type_sign'],
                'js'  => 'class = \'olcom-chosen\''
             ));
             $this -> olcomhmstemplateform -> add_field( 'text',array(
                'name' => 'temperature',
                'label' => $labels['temperature']
             ));
             $this -> olcomhmstemplateform -> add_field ('text' ,array(
                'name' => 'weight',
                'label' => $labels['weight']
             ));
             $this -> olcomhmstemplateform -> add_field ( 'textarea' ,array(
                'name' => 'allegies_critical_info',
                'label' => $labels['allegies_critical_info']
              
             ));
             
             $this -> load -> view ( 'main_template' ,array( 'template' => array(
                    'content' => $this -> olcomhmstemplateform -> create_form(),
                    'page_specific_scripts' => $this -> load -> view ('patients_specific_scripts','',TRUE)
             )));
        }
     }
    /*
     * 
     * 
     * 
     */
     function registered_patients()
     {
         $this -> load -> config('olcom_labels');
            $labels  = $this -> config -> item('labels');
            
            $this -> load -> library(
                'OlcomHmsTemplateDatatable',
                array(
                        'header'  => $labels['registered_patients'],
                        'columns'  => array(
                            $labels['id'],$labels[ 'patient_id'],$labels['firstname'],$labels['lastname'],$labels['age'],$labels['gender'],$labels['address']
                        ),
                        'with_actions'  => TRUE,
                        'create_controller_fx' => 'patients/register_patient',
                        'controller_fx'  => 'patients/patients_data'
                )
          );
        //load the main template 
        $datatable_view  = $this -> olcomhmstemplatedatatable -> create_view();
        $this -> load -> view('main_template',array(
            'template'  => array(
                'content'  => $datatable_view['datatable'],
                'page_specific_scripts'  => $datatable_view['specific_scripts']
            )
        ));
     }
    /*
     * 
     * 
     * 
     */
     function patients_data()
     {
         $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  =>  array(
                    'patients_general'  => array('patient_id'),
                    'personnel'  =>  array('firstname','lastname','age','gender','address','personnel_id')
                    ),
                'with_actions'  => TRUE,
                'hidden_cols'  => array(),
                'index_column'  => 'patient_id',
                'controller_fx'  => 'patients/actions/registered_patients',
                'id_type'  => 'patient',
                'cell_ops' => array(
					'personnel_id' => "concat( initial,'',patient_id )"
				),
                'where'  => array('personnel.personnel_id'  => 'patients_general.patient_id'),
                'order'  => array('personnel_id' => 1,'firstname'  => 2,'lastname'  => 3,'age'  => 4,'gender'=> 5,'address' => 6)
            )
        );
        echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
     }
     /*
      * 
      * 
      * 
      */
      function actions()
      {
             
        $this->load->config('olcom_messages');
        $messages = $this->config->item('messages');
        
        if( $this -> olcomaccessengine -> is_allowed_module_action($this -> uri -> uri_string() )  ===  FALSE)
        {   
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                            'message'  => $messages[ 'permission_denied_'.$this -> uri -> segment(4) ]
                                        ), 'error');
                return ;                        
        } 
        //load cookies configurations
       
        
        $section  = $this -> uri -> segment(3);
        $this -> load -> config('olcom_labels');
        $labels  = $this -> config -> item('labels');
        $this -> load -> config('olcom_cookies');
        
        $olcom_cookies  = $this -> config -> item('action_cookies'); 
        $cookie_save = $olcom_cookies['action_save_click'];
        $cookie_status = $olcom_cookies['action_delete_confirm'];
        
        $this -> load -> config( 'olcomhms_return_codes');
        $return_codes = $this -> config -> item( 'return_codes' );
        
        if( $section === 'registered_patients')
        {
            switch( $this -> uri -> segment(4))
            {
                case 'view':
                        //patient info 
                        $patient_info = array();
                        $patient_id = $this -> uri -> segment( 5 );
                        //personnel info 
                        $personnel_info = $this -> patients_model -> get_personnel_info( $patient_id );
                        if ( ! is_array( $personnel_info ))
                        {
                            $return_codes = $this ->config -> item( 'return_codes');
                            olcom_show_dialog('message', array(
                                                'header' => $messages['error'],
                                                'message' => $return_codes[$personnel_info]
                            ), 'error');
                            
                            return;
                        }
                        $patient_info = array_merge( $patient_info , $personnel_info);
                        
                        // temperature
                        $temperature = NULL;
                        $temperature = $this -> patients_model 
                        -> get_temp_weight_history( $this -> uri -> segment( 5 ),'temperature_history');
                        
                        if( $temperature !== NULL)
                        {
                            $temperature = json_decode( $temperature['temperature_history'] );
                            //get the latest entry
                            $temperature = $temperature[ count( $temperature ) -1];
                            if ( $temperature != NULL)
                            {
                                $patient_info['temperature'] = $temperature -> reading.' °C - '.$temperature -> taken  ;
                            }
                            
                        }
                        // weight
                        $weight = NULL;
                        $weight = $this -> patients_model 
                        -> get_temp_weight_history( $this -> uri -> segment( 5 ),'weight_history');
                        if( $weight !== NULL)
                        {
                            $weight = json_decode( $weight[ 'weight_history' ] );
                            //get the latest entry
                            $weight = $weight[ count( $weight ) -1];
                            if ( $weight != NULL)
                            {
                                $patient_info['weight'] = $weight -> reading.' Kg - '.$weight -> taken  ;
                            }
                            else {
                                $weight = NULL;
                            }
                        }
                        
                        // general info
                        
                        $general_info = $this -> patients_model -> get_patient_general_info( $this -> uri -> segment( 5 ));
                        
                        if ( is_array( $general_info ) )
                        {
                            $patient_info = array_merge( $patient_info, $general_info );
                        }
                        
                        if( $patient_info != NULL)
                        {
                            $patient_info[ 'deceased' ] =    $patient_info[ 'deceased' ] == 0 ? "No" : "Yes"; 
							$patient_info[ 'blood_type' ] = $patient_info[ 'blood_type'].''.$patient_info[ 'type_sign' ];
							
							unset( $patient_info[ 'initial' ] );
							unset( $patient_info[ 'type_sign' ] ); 
							
							$recent_payments = $this -> finance_model -> get_recent_service_payments( $patient_id, 5 );
							#echo $this -> db -> last_query();
							$recent = array();
							if( $recent_payments != NULL ){
								foreach( $recent_payments  as  $key => $payment ){
									$today = new DateTime( date( 'Y-m-d' ) );
									$payment_date = new DateTime( $payment[ 'date' ] );
									
									$interval = $today -> diff(  $payment_date );
									$years = $interval->format('%y');
									$months = $interval->format('%m');
									$days = $interval->format('%d');
									//$days = $days % 7;
									
									if( $payment['date']  == date( 'Y-m-d' ) )
									{
										$recent_payments[ $key ][ 'date' ] =  ' today ';
									}
									 
									
									
								}
							}
							if( $patient_info[ 'deceased' ] == 0 ){
								unset( $patient_info[ 'cause_of_death' ] );
								unset( $patient_info[ 'time_of_death' ] );
							}
							$allergies = $patient_info[ 'allegies_critical_info' ];
							unset( $patient_info[ 'allegies_critical_info' ] );
							$family_history = $patient_info[ 'family_history' ];
							unset( $patient_info[ 'family_history' ] );
							
                            olcom_show_dialog('static_table',
                            array( 'table_content' => $patient_info , 
                            	'static_content' => array( 
                            	'allegies_critical_info' => $allergies,
                            	'patient_history' => anchor(site_url( 'patients/patient_history/'.$patient_id  ), 'View Patient History' ),
                            	'recent_payments' => $recent_payments
								)), NULL );
                        }
                        else {
                            olcom_show_dialog('message', array('header' => $messages['error'],'message' => $messages['no_records']), 'error');
                        }
                    break;
            case 'edit':
                            $save_click = 0;
                            $save_click = get_cookie($cookie_save);
                            if($save_click    ===  FALSE){
                                $general_info = $this -> patients_model -> get_patient_general_info( $this -> uri -> segment( 5 ));
                                $temperature = $this -> patients_model -> get_temp_weight_history( $this -> uri -> segment( 5 ), 'temperature_history'); 
                                $weight = $this -> patients_model -> get_temp_weight_history( $this -> uri -> segment( 5 ),'weight_history');
                                
                                
                                
                                $this -> load -> config( 'olcom_selects' );
                                $selects = $this -> config -> item( 'selects'); 
                                //prepare update form 
                                $this->load->library('OlcomHmsTemplateForm',array(
                                    'title'  => $labels['edit_patient']
                                ));
                                 
                                
                                $this -> olcomhmstemplateform -> add_field('select', array(
                                    'name' => 'blood_type',
                                    'label' => $labels['blood_type'],
                                    'choices' => $selects['blood_type'],
                                    'selected'  => $general_info['blood_type'] == NULL ? '' : $general_info['blood_type' ],
                                    'js'  => 'class = \'olcom-chosen\''
                                ));
                                 $this -> olcomhmstemplateform -> add_field('select', array(
                                    'name' => 'blood_type_sign',
                                    'label' => '  ',
                                    'choices' => $selects['blood_type_sign'],
                                    'selected'  => $general_info[ 'type_sign' ],#$blood_type == NULL ? '' : empty( $blood_type[1] )  ? '' : $blood_type[ 1 ],
                                    'js'  => 'class = \'olcom-chosen\''
                                 ));
                                
                                 if( $temperature !== NULL)
                                 {
                                     if( $temperature[ 'temperature_history' ] !== NULL)
                                     {
                                        
                                        $temperature = json_decode( $temperature['temperature_history'] );
                                        if( count( $temperature ) > 0)
                                        {
                                            $temperature = $temperature[ count( $temperature ) -1];
                                            $temperature = $temperature -> reading;     
                                        }else {
                                            $temperature = NULL;
                                        }
                                     
                                        
                                           
                                     }else {
                                            $temperature = NULL;
                                        }
                                     
                                 }
                                 $this -> olcomhmstemplateform -> add_field( 'text',array(
                                    'name' => 'temperature',
                                    'label' => $labels['temperature'],
                                    'value' => $temperature
                                 ));
                                 $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'update_register_temperature',
                                    'label' => $labels[ 'update_register'],
                                    'choices' => $selects[ 'update_register_select']
                                 ));
                                 if( $weight !== NULL)
                                 {
                                     if( $weight[ 'weight_history'] !== NULL)
                                     {
                                        $weight = json_decode( $weight[ 'weight_history' ] );
                                        if( count( $weight ) > 0)
                                        {
                                            $weight = $weight[ count( $weight ) -1];
                                            $weight = $weight -> reading;        
                                        }else{
                                            $weight = NULL;
                                        }
                                     
                                        
                                        
                                     }else{
                                            $weight = NULL;
                                        }
                                     
                                 }
                                 $this -> olcomhmstemplateform -> add_field ('text' ,array(
                                    'name' => 'weight',
                                    'label' => $labels['weight'],
                                    'value' => $weight
                                 ));
                                 $this -> olcomhmstemplateform -> add_field( 'select', array(
                                    'name' => 'update_register_weight',
                                    'label' => $labels[ 'update_register'],
                                    'choices' => $selects[ 'update_register_select']
                                 ));
                                 $this -> olcomhmstemplateform -> add_field ( 'textarea' ,array(
                                    'name' => 'allegies_critical_info',
                                    'label' => $labels['allegies_critical_info'],
                                    'value' => $general_info['allegies_critical_info']
                                 ));
								  $this -> olcomhmstemplateform -> add_field ( 'textarea' ,array(
                                    'name' => 'family_history',
                                    'label' => $labels['family_history'],
                                    'value' => $general_info['family_history']
                                 ));
                                 $this -> olcomhmstemplateform -> add_field('checkbox' ,array(
                                    'name' => 'deceased',
                                    'label' => $labels[ 'deceased' ],
                                    'class' => 'ace-switch ace-switch-2',
                                    'value' => 1 ,
                                    'id' => 'deceased',
                                    'checked' => $general_info[ 'deceased' ] == 1  ? TRUE : FALSE 
                                ));
								 $this -> olcomhmstemplateform -> add_field ( 'textarea' ,array(
                                    'name' => 'cause_of_death',
                                    'label' => $labels['cause_of_death'],
                                    'value' => $general_info['cause_of_death'],
                                    'js' => "id = 'cod'"
                                 ));
								 $this -> olcomhmstemplateform -> add_field( 'text' , array(
							                'name' => 'date_of_death',
							                'label' => $labels[ 'time_of_death' ],
							                'id' => 'dod',
							                 'value' => date('Y-m-d'),
							                'readonly' => ''
							            ));
							     $this -> olcomhmstemplateform -> add_field( 'timepicker' , array(
							                'name' => 'time_of_death',
							                'label' => '',
							                'id' => 'tod',
							                'disabled' => 'disabled'
							               
					            )); 
                                olcom_actions_cookies('edit', 'create');
                                olcom_show_dialog('update', array('content'  => $this -> olcomhmstemplateform->create_form(),
                                'specific_scripts'  => 'patients_specific_scripts'), '');
                            }else{
                                
                                $form_data = $this -> input -> post( NULL );
                                $success = FALSE;
                                olcom_actions_cookies('edit', 'delete');
                                $patient_general_info = $this -> patients_model -> get_patient_general_info( $this -> uri -> segment( 5 ) );
                                
                                
                                if( $patient_general_info[ 'deceased' ] == 1 )
                                    {
                                        olcom_show_dialog('message', array(
                                            'message' => $messages[ 'deceased' ],
                                            'header' => $messages[ 'operation_failed' ]
                                        ), 'error' );
                                        return;
                                     }
                                
                                // general info 
                                $general_info = array();
                                $general_info['allegies_critical_info'] = $form_data[ 'allegies_critical_info'];
								$general_info['family_history'] = $form_data[ 'family_history'];
								 
								if( isset( $form_data[ 'deceased' ] ) AND $form_data[ 'deceased' ] == 1 ){
									$general_info[ 'cause_of_death' ] = $form_data[ 'cause_of_death' ];
									$general_info[ 'time_of_death' ] = $form_data[ 'date_of_death' ].' '.$form_data[ 'time_of_death' ];	
								}  
								
                                $general_info[ 'blood_type'] = $form_data['blood_type'];
                                $general_info[ 'type_sign' ] = $form_data['blood_type_sign'];
								
								
                                
                                $general_info[ 'deceased' ] = isset( $form_data[ 'deceased' ] ) ? ( $form_data[ 'deceased' ] == 1 ? 1 : 0 ) : 0 ;
                                if( $this -> patients_model -> update_patient_general_info( $this -> uri -> segment(5) ,$general_info) === SUCCESS_CODE)
                                {
                                    $success = TRUE;
                                }
                                else {
                                    /*olcom_show_dialog('message', array( 'message' => $messages[ 'general_info_not_updated' ],
                                    'header' => $messages[ 'warning' ] ), 'warning');
                                    $success = FALSE;*/
                                }
                                
                                
                                // temperature 
                                
                                if( isset( $form_data[ 'temperature']) AND $form_data[ 'temperature'] != NULL)
                                {
                                    if( $form_data[ 'update_register_temperature' ] === 'update')
                                    {
                                        if( $this -> patients_model -> update_patient_temp_weight( $this -> uri -> segment( 5 ) , 'temperature_history' , $form_data['temperature']) === TRUE)
                                        {
                                            $success = TRUE;
                                        } else {
                                            olcom_show_dialog('message', array( 'message' => $messages[ 'temperature_not_updated' ],
                                            'header' => $messages[ 'warning' ] ), 'warning');
                                            $success = FALSE;
                                        }
                                
                                    }
                                    
                                    // register
                                    if( $form_data[ 'update_register_temperature' ] === 'register')
                                    {
                                         if( $this -> patients_model -> record_temp_weight( $this -> uri -> segment( 5 ) , 'temperature' , $form_data['temperature']) === TRUE)
                                        {
                                            $success = TRUE;
                                        } else {
                                            olcom_show_dialog('message', array( 'message' => $messages[ 'temperature_not_registered' ],
                                            'header' => $messages[ 'warning' ] ), 'warning');
                                            $success = FALSE;
                                        }
                                    }
                                }
                                 // weight 
                                
                                if( isset( $form_data[ 'weight']) AND $form_data[ 'weight'] != NULL)
                                {
                                    if( $form_data[ 'update_register_temperature' ] === 'update')
                                    {
                                        if( $this -> patients_model -> update_patient_temp_weight( $this -> uri -> segment( 5 ) , 'weight_history' , $form_data['weight']) === TRUE)
                                        {
                                            $success = TRUE;
                                        } else {
                                            olcom_show_dialog('message', array( 'message' => $messages[ 'weight_not_updated' ],
                                            'header' => $messages[ 'warning' ] ), 'warning');
                                            $success = FALSE;
                                        }
                                
                                    }
                                    
                                    // register
                                    if( $form_data[ 'update_register_temperature' ] === 'register')
                                    {
                                         if( $this -> patients_model -> record_temp_weight( $this -> uri -> segment( 5 ) , 'weight_history' , $form_data['weight']) === TRUE)
                                        {
                                            $success = TRUE;
                                        } else {
                                            olcom_show_dialog('message', array( 'message' => $messages[ 'weight_not_registered' ],
                                            'header' => $messages[ 'warning' ] ), 'warning');
                                            $success = FALSE;
                                        }
                                    }
                                }
                                if( $success === TRUE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages['record_updated'],'header' => $messages['operation_successful']), 'success');
                                }
                                
                            }
                                         
               break;
            case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['confirm_delete'],'');
                            }else{
                                
                                $result = $this -> patients_model -> delete_patient( $this -> uri -> segment( 5 ));
                                
                                if( $result == SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages['record_deleted'],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $return_codes[$result],'header' => $messages['operation_failed']), 'error'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                break;
            }
        }
      }
	/*
	 * 
	 * 
	 */
	 function get_insert_id_data(){
	 	echo  olcomhms_create_id($this -> patients_model -> get_insert_personnel_id());
	 }
	 /*
	  * 
	  * 
	  */
	  function patient_history(){
	  	$patient_id = $this -> uri -> segment( 3 );
		
		if( $patient_id == NULL ){
			redirect('accessengine/access_denied','refresh');
		}
		$general_info = $this -> patients_model -> get_patient_general_info( $patient_id );
		$personnel_info = $this -> patients_model -> get_personnel_info( $patient_id, 'full' );
		
		
		
		$general_info = array_merge( $general_info, $personnel_info );
		
		
		
		
		$patient_history = $this -> load -> view( 'patient_history', array(
			'general_info' => $general_info
		), TRUE );
		$this -> load -> view ( 'main_template' ,array( 'template' => array(
                    'content' => $patient_history,
                    'page_specific_scripts' => $this -> load -> view ('patient_history_specific_scripts','',TRUE)
         )));
	  }
	  /*
	   * 
	   */
	   function get_medications_data(){
	   		$patient_id = $this -> uri -> segment( 3 );
			$limit = $this -> uri -> segment( 4 );
			
			$result_dates  = $this -> patients_model -> get_prescriptions_dates( $patient_id, $limit );
			
			$date_medications = array();
			if( $result_dates != NULL ){
				
				
				foreach( $result_dates as $key => $date ){
					$result_medications = $this -> patients_model -> get_medications_by_date( $patient_id, $date[ 'date'] );
					
					$indication = '&nbsp; ';
					$summary = '';
					foreach( $result_medications as $medication ){
						if( $medication[ 'disease_name' ] != NULL )
						 	$indication .= $medication[ 'disease_name' ].',';
						
						 $summary .= $medication[ 'medicament' ].' Qty:'.$medication[ 'dose' ].'<BR>';
					}
					
					$date_medications[] = array(
						'date' => $date[ 'date' ],
						'indication' => $indication,
						'medicament' => $summary
					);
							
				}
			}
			echo json_encode( $date_medications );
	   }
}
