<?php if(! defined('BASEPATH')) exit('Direct access is restricted');
/*
	Authentication controller 
 
*/

class Authentication extends CI_Controller{
   
    function __construct(){
        parent::__construct();
    }
    
    function index(){
        redirect('accessengine/access_denied','refresh');
    }
    
    function login(){
        
        if( $this -> olcomaccessengine -> is_logged() === FALSE )
        {
            $form_data = $this -> input -> post(NULL);
            
            if(isset($form_data) AND $form_data != NULL)
            {
                
                $login_result = $this -> olcomaccessengine -> login($form_data['username'],$form_data['password']);
                
                if($login_result == 1){
                   
                    $modules = $this -> olcomaccessengine -> load_modules( TRUE );
                     
                    if( $modules != NULL )
                    {
                        if( is_array( $modules ) )
                        {
                            foreach( $modules as $module => $subs)
                            { 
                             redirect( $module.'/index','refresh');
                             break;
                            }    
                        }
                        else
                            {
                                redirect( '/' ,'refresh' );
                            }
                            
                    }else{
                        redirect( '/' , 'refresh' );
                    }
                        
                }
                else if ( $login_result === 0 )
                {
                   
                    $this -> load -> view('login',array('login_error' => TRUE,'inactive' => TRUE));
                }else{
                    $this -> load -> view('login',array('login_error' => TRUE));
                }
            }else {
                $this -> load -> view('login');
            }
        }else{
                    $modules = $this -> olcomaccessengine -> load_modules( TRUE );
                    
                    if( $modules != NULL )
                    {
                       if( is_array( $modules ) )
                        {
                            foreach( $modules as $module => $subs)
                            { 
                              redirect( $module.'/index','refresh');
                              break;
                            }    
                        }
                        else
                            {
                                redirect( '/' ,'refresh' );
                            }
                    }else{
                        redirect( '/' , 'refresh' );
                    }
        }
    }
    function logout(){
        
        $this -> session -> sess_destroy();
        redirect('authentication/login/','refresh') ;
    }
}


