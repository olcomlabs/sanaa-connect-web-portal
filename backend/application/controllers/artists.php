<?php
if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
class Artists extends CI_Controller{
	var $logged = FALSE;
     var $return_codes;
	 var $help;

	function __construct(){
		parent::__construct();
		if($this   ->   olcomaccessengine   ->   is_logged()  === TRUE)
          {
                olcom_is_allowed_module_helper($this  ->  uri  ->  uri_string()) ;
                
                $this  ->  is_logged = TRUE;
           }
           else
           {
            
            if( $this  -> input  ->  is_ajax_request())
            {
                if(preg_match('/data/', $this  ->  uri  ->  uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
		   }
	         $this -> is_logged = FALSE;
	         $this -> load -> config( 'olcom_messages' );
	         $this -> load -> config( 'olcom_labels' );
			     $this -> load -> config( 'olcom_help_text' );
	         $this -> load -> config( 'olcomhms_return_codes' );
	         $this -> return_codes = $this -> config -> item( 'return_codes' );
	         $this -> messages = $this -> config -> item( 'messages' );
	         $this -> labels = $this -> config -> item( 'labels' );
	        $this -> help = $this -> config -> item( 'help_text' );        
	        $this -> load -> library( 'OlcomHmsFx' );
	        $this->load->model('events_model');
          $this->load->model('artist_model');
		

	}

	function index(){

				 
		$modules = $this -> olcomaccessengine -> load_modules( TRUE );
		//print_r( $modules[  $this -> uri -> segment( 1)] );return;
		$modules_configs = $this -> config -> item('modules_configs'  );
		$sub_modules = $modules[  $this -> uri -> segment( 1)];
		$this -> load -> config( 'olcomhms_modules_configs' );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
		
	 
		$launchers = array();
		foreach ($sub_modules as $key => $sub_module) {
			$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
		}
		
		$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
		$data[ 'launchers' ] = $launchers;
		
		$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );	
		
		 $this -> load -> view('main_template',array(
	              'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
	                
	            ));
	}


        function  registered_artists()
            {
                $this -> load -> library('OlcomHmsTemplateDatatable', 
                         array('header' => 'Registered Artists ', 'with_actions' => TRUE, 
                        'create_controller_fx' => NULL, //for create button on view
                         'columns' => array( 'ID#' , 'Artist Name', 'Email','Phone','Category'), 
                         'controller_fx' => 'artists/publishedArtists_data'));
                
                        $datatable = $this -> olcomhmstemplatedatatable -> create_view();
                
                        $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
            }



        function publishedArtists_data()
    {

        $loggedUser = $this->session->userdata('userProfile');

        $where =array('artist.artistId'  => 'artist_category.ac_artistId', 'artist_category.ac_categoryId'=>'category.categoryId');
          
        if( isset($loggedUser) AND $loggedUser != NULL){
          $where[ 'artist.organizationId'] = $loggedUser['organizationId'];  
        }

        $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  => array(
                    'artist'  => array('artistId','displayName','email','phone'),
                    'artist_category'  => array('ac_artistId'),
                    'category'=>array('categoryName'),
                    ),
                    'with_actions'  => TRUE,
                    'hidden_cols'  => array('ac_artistId'),
                    'index_column'  => 'artistId',
                    'controller_fx'  => 'artists/actions/registered_artists',//for actions
                    'id_type'  => '',
                    'where'  => $where,
                    'data_style_format'  => NULL,
                     'cell_ops' => array(
                         
                        ),
                    'order'  => array('displayName'  => 1,'email'  => 2,'phone'=>3,'categoryName'=>4)
                 )
             );
            echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
    }


     /*
     * action function for processing tabledata actions
     */
     function actions(){
      

      
            $this -> load -> config('olcom_messages');
            $dialog_messages = $this -> config -> item('messages');  
      /*
       * 
       * do some permission checking before continuing
       * 
       */
       
      if( $this  ->  olcomaccessengine  ->  is_allowed_module_action($this  ->  uri  ->  uri_string() )  === FALSE){
          
                
          olcom_show_dialog('message',array(
                                            'header'  => $dialog_messages[ 'permission_denied' ],
                                            'message'  => $dialog_messages[ 'permission_denied_'.$this  ->  uri  -> segment(4) ]
                                        ), 'error');
                return ;                        
      }
            
      
      $this -> load -> config('olcom_labels');
      $this -> load -> config('olcom_cookies');
      
      $olcom_cookies = $this -> config -> item('action_cookies');
      $labels = $this -> config -> item('labels');

      $section = $this -> uri -> segment(3);
       
        if( $section === 'registered_artists' )
            { 
            switch($this -> uri -> segment(4)){
              case 'view':
                  
                    $result = $this -> artist_model ->getFullProfile($this->uri->segment(5));
                     
                     unset(  $result['o_dateCreated']);
                     unset($result['password' ]);
                      unset($result['organizationId' ]);
                      $result['active' ] = $result['active'] == 0 ? 'No': 'Yes';
                    olcom_show_dialog('table', array($result),'');
                 
                break;
                
              case 'delete':
                    /*
                     *client-server confirmation 
                     * 
                     * cookies one for action info storage other confirm status
                     */
                     
                    //check for confirm status
                    $confirmed = 0;
                    $cookie_status = $olcom_cookies['action_delete_confirm'];
                    $cookie_action = $olcom_cookies['action_delete'];
                    
                    $confirmed = get_cookie($cookie_status);//returns FALSE where cookie not set
                    
                    if($confirmed  === FALSE){//cookie not set and means just clicked delete ico
                    
                    olcom_actions_cookies('delete', 'create');
                    //load the confirmation dialog
                    olcom_show_dialog('confirm',$dialog_messages['dialog_delete_party'],'');
                  }
                  else{//cookie set 
                        olcom_actions_cookies('delete', 'delete');
                        
                        if($this -> olcomhms -> delete('artist',array(
                        'artistId'  => $this -> uri -> segment(5)
                        )) > 0){
                          
                          olcom_show_dialog('message', array(
                            'header'  => $dialog_messages['operation_successful'],
                            'message'  => $dialog_messages['record_deleted']
                          ), 'success');
                        }else{
                          olcom_show_dialog('message',array(
                            'header'  => $dialog_messages['attention'],
                            'message'  => $dialog_messages['error_deleting_record'],
                          ),'error');
                          
                        }
                    }
                    
                break;
              case 'edit':
                
                    //client server coms
                    /*
                      * cookies 
                      *   olcom-action-edit-save-click [yes ,no]
                      *   olcom-action-edit [action link]
                      */
                   
                    $cookie_save = $olcom_cookies['action_save_click'];
                    $cookie_action = $olcom_cookies['action_edit'];
                    $result = $this -> artist_model ->getFullProfile($this->uri->segment(5));
                    $save_click = 0;
                    $save_click = get_cookie($cookie_save);
                    if($save_click  === FALSE){//just clicked update button
                      //create cookies 
                      olcom_actions_cookies('edit', 'create');  
                        //create update form 
                        $this -> load -> config('olcom_labels');
                        $this -> load -> config('olcom_selects');
                        $labels = $this -> config -> item('labels');
                        $selects = $this -> config -> item('selects');
                        $this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Active/Deactivate Artist',
                                  'header_info'  => '',
                                  'action'  => '#','id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                                  'method'  => 'POST'
                        ));
                       $this -> olcomhmstemplateform -> add_field('checkbox',array(
                          'name'  => 'active',
                          'label'  => $labels['active'],
                          'value'  => 1,
                          'class'  => 'ace-switch ace-switch-2',
                          'checked'  => $result['active']
                    ));
                      olcom_show_dialog('update',array('content'  => $this -> olcomhmstemplateform -> create_form(),
                                        'specific_scripts'  => 'artist_specific_scripts'), '');
                      
                
                        
                      }else{
                      
                         olcom_actions_cookies('edit', 'delete');
                        //update record
                  
                  
                        $data = $this -> input -> post(NULL);
                        if($this -> olcomhms -> edit('artist',$data,
                          array('artistId'  => $this -> uri -> segment(5))) !=  0)
                        {
                          
                          olcom_show_dialog('message',array(
                            'header'  => $dialog_messages['operation_successful'],
                            'message'  => $dialog_messages['record_updated']
                          ), 'success');
                          
                        }else{
                          olcom_show_dialog('message',array(
                            'header'  => $dialog_messages['info'],
                            'message'  => $dialog_messages['no_changes']
                          ), 'info');
                          
                        }
                      }//end else
                break;
              }//end 
            }// end if 
    
  }

	

}