<?php if(!defined('BASEPATH'))exit('Direct access is restricted');

class Appointments extends CI_Controller{
    
    var $return_codes ;
    function __construct(){
        parent::__construct();
        
        if ($this -> olcomaccessengine -> is_logged() === TRUE) {
            
            olcom_is_allowed_module_helper($this -> uri -> uri_string());

            $this -> is_logged = TRUE;
        } else {

            if ($this -> input -> is_ajax_request()) {
                if (preg_match('/data/', $this -> uri -> uri_string()) === 1) {
                    echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                }
            } else {
                redirect('authentication/login', 'refresh');
            }

            $this -> is_logged = FALSE;

            $this -> load -> config('olcomhms_return_codes');
            
            if (!defined('SUCCESS_CODE'))
                define('SUCCESS_CODE', $this -> config -> item('success_code'));
           
            
        }
        $this  -> return_codes = $this -> config -> item( 'return_codes');
        // load models
        $this -> load -> model( 'usermanagement_model' );
        $this -> load -> model( 'appointments_model' );
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');       
    }

	function index()
	{
			
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
    /*
     * 
     * 
     */
     function appointments_calendar() 
     {
         
         $data[ 'css' ] = array(  'fullcalendar'  );
         $data[ 'content' ] = $this -> load -> view ( 'calendar_view', NULL , TRUE );
         $data[ 'page_specific_scripts' ] = $this -> load -> view ( 'calendar_view_specific_scripts' , NULL , TRUE );
         
         $this -> load -> view( 'main_template' ,array(
            'template' => $data
         ));
     }
     /*
      * 
      * 
      * 
      */
      function new_appointment()
      {
        $form_data = $this -> input -> post( NULL );
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){
          
            
            if( ! isset( $form_data[ 'urgent'] )  )
            {
                $form_data[ 'urgent' ] = 0;
            }
            if( ! isset( $form_data[ 'all_day' ] ) )
            {
                $form_data[ 'all_day' ] = 0 ;
            }
            
            
            $logged_user = $this -> olcomaccessengine -> get_logged_in_user( );
            
            if( $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_admin_gid() OR 
                $logged_user[ 'group_id' ] == $this -> olcomaccessengine -> get_super_admin_gid() )
            {
                olcom_server_form_message($messages[ 'requires_employee_access' ],0 );
                return;
            }
            $form_data[ 'employee_id' ] = $logged_user[ 'employee_id' ];
            $form_data[ 'patient_id' ] = olcom_extract_numbers( $form_data[ 'patient_id' ] ); 
            $form_data[ 'date_created' ] = date( 'Y-m-d' );
            
            if( $form_data[ 'start_date' ] > $form_data[ 'end_date' ] )
            {
                olcom_server_form_message($messages[ 'invalid_start_end_dates' ], 0 );
                return;
            }
            
            if( $form_data[ 'start_time'] > $form_data[ 'end_time' ] )
            {
                olcom_server_form_message($messages[ 'invalid_start_end_times'], 0 );
                return;
            }
            
            $appointments = $this -> appointments_model -> get_employee_appointments( $logged_user[ 'employee_id' ] );
            
            if( $appointments != NULL )
            {
                foreach( $appointments as $appointment )
                {
                    // Has to be the same day
                    if( $appointment[ 'start_date' ] == $form_data[ 'start_date'] )
                    {
                        if( $appointment[ 'start_time' ] >=  $form_data[ 'start_time' ] AND $appointment[ 'start_time' ] < $form_data[ 'end_time' ]  )
                        {
                            olcom_server_form_message($messages[ 'you_have_appt_at_time'].$appointment[ 'start_time' ],  0 );
                            return;
                        }
                       if( $appointment[ 'end_time' ] >  $form_data[ 'start_time' ] AND $appointment[ 'end_time' ] < $form_data[ 'end_time' ]  )
                        {
                            olcom_server_form_message($messages[ 'you_have_appt_at_time'].$appointment[ 'end_time' ],  0 );
                            return;
                        }
                      if( $appointment[ 'start_time' ] >  $form_data[ 'start_time' ] AND $appointment[ 'start_time' ] < $form_data[ 'end_time' ]  )
                        {
                            olcom_server_form_message($messages[ 'you_have_appt_at_time'].$appointment[ 'start_time' ],  0 );
                            return;
                        }
                    }
                }
            }
            $result = $this  ->  appointments_model  ->  create_appointment( $form_data );
            
            if( $result === SUCCESS_CODE ){
                olcom_server_form_message( $messages[ 'appointment_created' ], 1);
            }else{
                    olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                }
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array( 
                'with_submit_reset'  => TRUE, 
                'title'  => $labels[ 'new_appointment'] 
            )); 
            $this -> olcomhmstemplateform -> add_field('search',array(
                'name'  => 'patient_id',
                'label'  => $labels['patient'],
                'id' => 'patient_auto_complete',
                'placeholder' => 'Patient ID'  
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'label' => $labels[ 'date_created' ], 
                'readonly' => '',
                'value' => date('M d, Y')
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'name' => 'start_date',
                'label' => $labels[ 'start_date' ],
                'id' => 'start_date',
                 'value' => date('Y-m-d'),
                'readonly' => ''
            ));
            $this -> olcomhmstemplateform -> add_field( 'timepicker' , array(
                'name' => 'start_time',
                'label' => $labels[ 'start_time' ],
                'id' => 'start_time'
               
            )); 
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'name' => 'end_date',
                'label' => $labels[ 'end_date' ],
                'id' => 'end_date',
                'readonly' => '',
                'value' => date('Y-m-d')
            ));
            $this -> olcomhmstemplateform -> add_field( 'timepicker' , array(
                'name' => 'end_time',
                'label' => $labels[ 'end_time' ],
                'id' => 'end_time'
            )); 
            $this -> olcomhmstemplateform -> add_field( 'checkbox' , array(
                'name' => 'urgent',
                'label' => $labels[ 'urgent' ],
                'value' => 1,
                'class' => 'ace-switch ace-switch-5',
            )); 
            $this -> olcomhmstemplateform -> add_field( 'checkbox' , array(
                'name' => 'all_day',
                'label' => $labels[ 'all_day' ],
                'value' => 1,
                'class' => 'ace-switch ace-switch-5',
            )); 
             $this -> olcomhmstemplateform -> add_field( 'textarea' , array(
                'name' => 'extra_info',
                'label' => $labels[ 'extra_info' ]
            )); 
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('appointments_specific_scripts','',TRUE);
            
            $css = array(
                'datetimepicker',
                'bootstrap-timepicker');
            $this -> load -> view('main_template',array(
                'template'  => array( 'css' => NULL, 'content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
      }
    /*
     * 
     * 
     * 
     */
     function patient_autocomplete_data()
     {
        $patient_id = $this -> uri -> segment( 3 );
        $this -> load -> model ( 'patients_model' );
        $patient_general = $this -> patients_model -> get_patient_general_info( olcom_extract_numbers( $patient_id ) );
            if( $patient_general[ 'deceased' ]  == 1 )
            {
                echo json_encode( array( 'result' => 0 ) );
                return ;
            }
        $patients_data = array();
        if( isset( $patient_id ) AND $patient_id != NULL)
        {
            $patients_data = $this -> patients_model -> get_patients_name_id( $patient_id );
            
            if( is_array( $patients_data )  )
            {
                $patients_data = each( $patients_data );     
                echo  json_encode( array( 'result' => 1,'key' => $patients_data[ 'key'],'value' => $patients_data[ 'value']  ));
                return ; 
            }
            echo json_encode( array( 'result' => 0));
            return ;
        }
     }
     /*
      * 
      * 
      * 
      */
      function new_appointment_form_data()
      {
            $messages = $this -> config -> item('messages');
                   
            if( $this -> olcomaccessengine -> is_allowed_module_action( 'appointments/actions/new_appointment/create/1' )  ===  FALSE){
                
                
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                          'message'  => $messages[ 'permission_denied_create' ]
                                        ), 'error');
                return ;                        
            }
          
          if( $this -> input -> is_ajax_request() == TRUE )
          {
               $form_data = $this -> input -> get( NULL );
               
               $messages = $this -> config -> item('messages');
               $labels = $this -> config -> item('labels');
        
              $this -> load -> library('OlcomHmsTemplateForm',array(
                'title'  => $labels[ 'new_appointment'] 
            )); 
            $this -> olcomhmstemplateform -> add_field('search',array(
                'name'  => 'patient_id',
                'label'  => $labels['patient'],
                'id' => 'patient_auto_complete',
                'placeholder' => 'Patient ID'  
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'name' => 'date_created',
                'label' => $labels[ 'date_created' ], 
                'readonly' => '',
                'value' => date('M d, Y')
            ));
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'name' => 'start_date',
                'label' => $labels[ 'start_date' ],
                'id' => 'start_date',
                 'value' => isset( $form_data[ 'start_date' ] ) ? $form_data[ 'start_date' ] : date('Y-m-d'),
                'readonly' => ''
            ));
            $this -> olcomhmstemplateform -> add_field( 'timepicker' , array(
                'name' => 'start_time',
                'label' => $labels[ 'start_time' ],
                'id' => 'start_time',
                'class' => 'span5',
                'value' => isset( $form_data[ 'start_time' ] ) ? $form_data[ 'start_time' ] : date( 'h:i')
               
            )); 
            $this -> olcomhmstemplateform -> add_field( 'text' , array(
                'name' => 'end_date',
                'label' => $labels[ 'end_date' ],
                'id' => 'end_date',
                'readonly' => '',
                'value' =>isset( $form_data[ 'end_date' ] ) ? $form_data[ 'end_date' ] : date('Y-m-d')
            ));
            $this -> olcomhmstemplateform -> add_field( 'timepicker' , array(
                'name' => 'end_time',
                'label' => $labels[ 'end_time' ],
                'id' => 'end_time',
                'class' => 'span5',
                'value' => isset( $form_data[ 'end_time' ] ) ? $form_data[ 'end_time' ] : date( 'h:i') 
            )); 
            $this -> olcomhmstemplateform -> add_field( 'checkbox' , array(
                'name' => 'urgent',
                'label' => $labels[ 'urgent' ],
                'value' => 1,
                'class' => 'ace-switch ace-switch-5',
            )); 
            $this -> olcomhmstemplateform -> add_field( 'checkbox' , array(
                'name' => 'all_day',
                'label' => $labels[ 'all_day' ],
                'value' => 1,
                'class' => 'ace-switch ace-switch-5',
                'checked' => isset( $form_data[ 'all_day' ] ) == TRUE ? ( $form_data[ 'all_day' ] == 'true' ? TRUE : FALSE  ) : FALSE 
            )); 
             $this -> olcomhmstemplateform -> add_field( 'textarea' , array(
                'name' => 'extra_info',
                'label' => $labels[ 'extra_info' ]
            )); 
             
            olcom_show_dialog('form', array('content'  => $this -> olcomhmstemplateform -> create_form(),
                        'specific_scripts'  => 'appointments_specific_scripts' , 'title' => '  '), '');
           }
            
     }
    /*
     * 
     * 
     * 
     */
     function load_appointments_data()
     {
         $result = $this -> appointments_model -> get_appointments( );
         if( $result != NULL )
         {
             $appointments = array();
             $this -> load -> model( 'patients_model' );
             
             foreach( $result as $appointment )
             {
                 $result_patient = $this -> patients_model -> get_personnel_info( $appointment[ 'patient_id' ] );
                 $appointments[] = array(
                    'title' => olcomhms_create_id( $appointment[ 'patient_id' ] ,FALSE , TRUE ).
                    "\n".$result_patient[ 'lastname' ].' '.$result_patient[ 'firstname' ]."\n".$appointment[ 'extra_info' ],
                    'start' => $appointment[ 'start_date' ].' '.$appointment[ 'start_time' ],
                    'end' => $appointment[ 'end_date'].' '.$appointment[ 'end_time' ],
                    'allDay' => $appointment[ 'all_day' ] == 1 ? true : false,
                    'className' => $appointment[ 'urgent' ] == 1 ? 'label-important' : 'label-success',
                    'appointment_id' => $appointment[ 'appointment_id' ]
                 );
             }
             echo json_encode( $appointments );
             return;
         }
         echo json_encode( array() );
     }
    /*
     * 
     * 
     * 
     */
     function delete_appointment_data()
     {
        $this -> load -> config('olcom_messages');
        $messages = $this -> config -> item('messages');
        
        if( $this -> olcomaccessengine -> is_allowed_module_action( 'appointments/actions/appointments_list/delete/'. $this -> uri -> segment( 3 ) )  ===  FALSE)
        {   
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                            'message'  => $messages[ 'permission_denied_delete' ]
                                        ), 'error');
                return ;                        
        } 
         $result = $this -> appointments_model -> delete_appointment( $this -> uri -> segment( 3 ) );
         echo json_encode( array( 'success' => $result ) );
     }
    /*
     * 
     * 
     */
     function appointments_list()
     {
        $labels = $this -> config -> item('labels');

            $this -> load -> library('OlcomHmsTemplateDatatable', array('header' => $labels['appointments'], 'with_actions' => TRUE, 
            'create_controller_fx' => 'appointments/new_appointment',
             'columns' => array($labels['id'], $labels['start_date']  ,$labels[ 'start_time' ] ,$labels[ 'firstname' ],$labels[ 'lastname' ] , $labels[ 'urgent'] ), 'controller_fx' => 'appointments/appointments_list_data'));
    
            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
    
            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));    
     }
     /*
      * 
      * 
      * 
      */
      function appointments_list_data()
      {
          
          $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
          
          $where =  array('appointments.patient_id'  => 'personnel.personnel_id' );
          
          if( isset( $logged_user[ 'employee_id' ] ) )
          {
              $appointments = $this -> appointments_model -> get_employee_appointments( $logged_user[ 'employee_id' ] );
              if( $appointments != NULL )
              {
                  $where = array_merge( $where , array( 'appointments.employee_id' => $logged_user[ 'employee_id' ] ) );
              }  
          }
            
           $this -> load -> library('OlcomHmsDataTablesMultiDBTables',
                array(
                    'tables'  => array(
                        'appointments' => array( 'appointment_id' , 'start_date' , 'start_time', 'urgent' ),
                        'personnel' => array(
                                'firstname' , 'lastname'
                            )
                        ),
                        'with_actions'  => TRUE,
                        'hidden_cols'  => array(),
                        'index_column'  => 'appointment_id',
                        'controller_fx'  => 'appointments/actions/appointments_list',
                        'id_type'  => 'appointment',
                        'where'  => $where,
                        'data_style_format'  => array(
                            'urgent' => array(
                                'yes_no' => TRUE
                            ),
                            'start_date' => array(
                                'pretty_date' => TRUE
                            )
                        ) ,
                       'order'  => array('start_date' => 1 , 'start_time' => 2 ,'firstname' => 3 , 'lastname' => 4 , 'urgent' => 5 )
                     )
                 );
                echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
      }
     /*
      * 
      * 
      * 
      */
      /*
      * 
      * 
      * 
      */
      function actions()
      {
             
        $this -> load -> config('olcom_messages');
        $messages = $this -> config -> item('messages');
        
        if( $this -> olcomaccessengine -> is_allowed_module_action($this -> uri -> uri_string() )  ===  FALSE)
        {   
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                            'message'  => $messages[ 'permission_denied_'.$this -> uri -> segment(4) ]
                                        ), 'error');
                return ;                        
        } 
        //load cookies configurations
       
        
        $section  = $this -> uri -> segment(3);
        $this -> load -> config('olcom_labels');
        $labels  = $this -> config -> item('labels');
        $this -> load -> config('olcom_cookies');
        
        $olcom_cookies  = $this -> config -> item('action_cookies'); 
        $cookie_save = $olcom_cookies['action_save_click'];
        $cookie_status = $olcom_cookies['action_delete_confirm'];
        
        $this -> load -> config( 'olcomhms_return_codes');
        $return_codes = $this -> config -> item( 'return_codes' );
        if( $section === 'appointments_list')
        {
            switch( $this -> uri -> segment( 4 ) )
            {
                case 'view':
                        $result_appointments = $this -> appointments_model -> get_appointments( $this -> uri -> segment( 5 ) );
                        
                        $this -> load -> model( 'employees' );
                        $doctor = $this -> employees -> info( $result_appointments[ 'employee_id' ] ,'basic');
                        $result_appointments[ 'doctor' ] = 'Dr. '.strtoupper( $doctor[ 'lastname' ] ).','.$doctor[ 'firstname' ];
                        unset( $result_appointments[ 'employee_id' ] );
                        unset( $result_appointments[ 'status' ] );
                        $result_appointments[ 'all_day' ] = $this -> load -> view( 'data_style_format' ,array(
                            'format' => array( 'yes_no' => TRUE ,
                            'data' => $result_appointments[ 'all_day' ] == 1 ? 1 : 0 
                            )
                        ) , TRUE );
                        $result_appointments[ 'urgent' ] = $this -> load -> view( 'data_style_format' ,array(
                            'format' => array( 'yes_no' => TRUE ,
                            'data' => $result_appointments[ 'urgent' ]
                            )
                        ) , TRUE );
                        olcom_show_dialog('table',array( $result_appointments ) , '');
                    break;
                    case 'delete' :
                            $confirmed = 0;
                            $cookie_status = $olcom_cookies['action_delete_confirm'];
                            $confirmed = get_cookie($cookie_status);
                            if($confirmed    ===  FALSE){
                                olcom_actions_cookies('delete', 'create');
                                olcom_show_dialog('confirm',$messages['delete_record'],'');
                            }else{
                                
                                $result = $this  ->  appointments_model  ->  delete_appointment( $this  ->  uri  ->  segment( 5 ));
                                
                                if( $result === SUCCESS_CODE )
                                {
                                    olcom_show_dialog('message', array('message' => $messages[ 'record_deleted' ],'header' => $messages['operation_successful']), 'success');
                                }else{
                                    olcom_show_dialog('message', array('message' => $this  ->  return_codes[ $result ],'header' => $messages['operation_failed']), 'warning'); 
                                }
                                olcom_actions_cookies('delete', 'delete');
                            }
                        
                        break;
                    }
               }
        }
}