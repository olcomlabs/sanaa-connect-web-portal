<?php
   /*
    * 
    * 
    * 
    */
    
    
    class Account extends  CI_Controller
    {
        
        var $messages ;
        var $labels;
        var $return_codes ;
        
        function Account()
        {
           parent :: __construct();
           $this -> load -> model( 'usermanagement_model' );
           $this -> load -> model( 'employees') ;
           $this -> load -> config( 'olcom_messages' );
           $this -> messages = $this -> config -> item( 'messages' );
           $this -> load -> config( 'olcom_labels' );
           $this -> labels = $this -> config -> item( 'labels' );
           
           $this -> load -> config( 'olcomhms_return_codes' );
           $this -> return_codes = $this -> config -> item( 'return_codes' );
           
           if( ! defined('SUCCESS_CODE' ) )
           {
               define("SUCCESS_CODE", $this -> return_codes[ 'success_code'] );
           }
        }
        
        
        function profile()
        {
            $logged = $this -> olcomaccessengine -> get_logged_in_user();
            
            if( !isset( $logged[ 'employee_id' ] ) )
            {
                redirect( 'personnel/view_personnel');
            }
            
            $user = $this -> usermanagement_model -> get_users( $logged[ 'employee_id' ] );
            $group = $this -> usermanagement_model -> get_groups( $user[ 'group_id' ] );
            $user = array_merge($user, $group );
            
            $employee = $this -> employees -> info( $logged[ 'employee_id' ] , 'full') ;
            
            
            $form_data = $this -> input -> post( NULL );
            
            if( isset( $form_data ) AND $form_data != NULL )
            {
               
               $update = 0;
               
               if( $this -> employees -> update_personnel( $user[ 'employee_id' ] ,
                array( 'phone' => $form_data[ 'phone' ] ) ) == SUCCESS_CODE )
                {
                    $update = TRUE;
                }
               
               
               if( $form_data[ 'password' ] != 'password'   )
               {
                   $this -> usermanagement_model -> update_user( $user[ 'employee_id'] , array(
                    'password' => md5( $form_data[ 'password' ] ), 'active' => 1 ) ) ;
                   
                   $update = TRUE;
               }
               if( $update == TRUE )
                olcom_server_form_message($this -> messages[ 'account_updated'], 1 ) ;
               
               else {
                   olcom_server_form_message( $this ->return_codes[ 110 ],   1);
               }
               
            }
            else
               {
                  // create form 
                    $this -> load -> library('OlcomHmsTemplateForm',array( 
                        'with_submit_reset'  => TRUE,
                        'title'  => $this ->labels[ 'personnel']
                    ));
                    
                    
                    $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'firstname'],
                        'label' => $this -> labels[ 'firstname' ],
                        'readonly' => ''
                    ));
                    $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'middlename'],
                        'label' => $this -> labels[ 'middlename' ],
                        'readonly' => ''
                    ));
                    $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'lastname'],
                        'label' => $this -> labels[ 'lastname' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'gender'],
                        'label' => $this -> labels[ 'gender' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => olcomhms_create_id($employee[ 'employee_id'],TRUE ,FALSE),
                        'label' => $this -> labels[ 'lastname' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'department_name'],
                        'label' => $this -> labels[ 'department_name' ],
                        'readonly' => ''
                    ));
                    $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'job_title' ],
                        'label' => $this -> labels[ 'job_title' ],
                        'readonly' => ''
                    ));
                    $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => 'phone',
                        'value' => $employee[ 'phone'],
                        'label' => $this -> labels[ 'phone' ],
                        'id' => 'phone'
                        
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'address'],
                        'label' => $this -> labels[ 'address' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'region'],
                        'label' => $this -> labels[ 'region' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $employee[ 'marital_status'],
                        'label' => $this -> labels[ 'marital_status' ],
                        'readonly' => ''
                    ));
                    
                    $this -> olcomhmstemplateform -> add_group_title( $this -> labels[ 'profile'] );
                    
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $user[ 'username'],
                        'label' => $this -> labels[ 'username' ],
                        'readonly' => ''
                    ));
                     $this -> olcomhmstemplateform -> add_field( 'password' , array(
                        'name' => 'password',
                        'value' => 'password',
                        'label' => $this -> labels[ 'password' ] 
                    ));
                    
                     $this -> olcomhmstemplateform -> add_field( 'text' , array(
                        'name' => '',
                        'value' => $user[ 'group_name'],
                        'label' => $this -> labels[ 'group' ],
                        'readonly' => ''
                    ));
                    $this -> load -> view( 'main_template' , array(
                    'template' => array(
                        'content' => $this -> olcomhmstemplateform -> create_form(),
                        'page_specific_scripts' => $this -> load -> view( 'user_profile_specific_scripts' ,'',TRUE)
                    )
                    )); 
               }
            
        }
    }
 