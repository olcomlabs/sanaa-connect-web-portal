<?php
if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
class Supporters extends CI_Controller{
	var $logged = FALSE;
     var $return_codes;
	 var $help;

	function __construct(){
		parent::__construct();
		if($this   ->   olcomaccessengine   ->   is_logged()  === TRUE)
          {
                olcom_is_allowed_module_helper($this  ->  uri  ->  uri_string()) ;
                
                $this  ->  is_logged = TRUE;
           }
           else
           {
            
            if( $this  -> input  ->  is_ajax_request())
            {
                if(preg_match('/data/', $this  ->  uri  ->  uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
		   }
	         $this -> is_logged = FALSE;
	         $this -> load -> config( 'olcom_messages' );
	         $this -> load -> config( 'olcom_labels' );
			     $this -> load -> config( 'olcom_help_text' );
	         $this -> load -> config( 'olcomhms_return_codes' );
	         $this -> return_codes = $this -> config -> item( 'return_codes' );
	         $this -> messages = $this -> config -> item( 'messages' );
	         $this -> labels = $this -> config -> item( 'labels' );
	        $this -> help = $this -> config -> item( 'help_text' );        
	        $this -> load -> library( 'OlcomHmsFx' );
	        $this->load->model('supporters_model');
		

	}

	function index(){

				 
		$modules = $this -> olcomaccessengine -> load_modules( TRUE );
		//print_r( $modules[  $this -> uri -> segment( 1)] );return;
		$modules_configs = $this -> config -> item('modules_configs'  );
		$sub_modules = $modules[  $this -> uri -> segment( 1)];
		$this -> load -> config( 'olcomhms_modules_configs' );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
		
	 
		$launchers = array();
		foreach ($sub_modules as $key => $sub_module) {
			$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
		}
		
		$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
		$data[ 'launchers' ] = $launchers;
		
		$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );	
		
		 $this -> load -> view('main_template',array(
	              'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
	                
	            ));
	}



    function  published_supporters()
            {
                $this -> load -> library('OlcomHmsTemplateDatatable', 
                         array('header' => 'List of Supporters ', 'with_actions' => TRUE, 
                        'create_controller_fx' => 'supporters/create_supporter', //for create button on view
                         'columns' => array( 'ID#' , 'Supporter Name','Supporter Link', 'published Status'), 
                         'controller_fx' => 'supporters/getListOfsupporters'));
                
                        $datatable = $this -> olcomhmstemplatedatatable -> create_view();
                
                        $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
            }




                    function getListOfsupporters()
    {
                     if( $this -> olcomaccessengine -> is_logged() === TRUE)
                          {
                            //load datatable lib
                            $this -> load -> library('OlcomHmsDataTables',array('columns'  =>  array(
                              'supporter'  =>  'supporterId', 'supporterId','supporterName','supporterURL','published',
                  

                    ),

      
                            'index_column'  => 'supporterId',
                            'table_name'  => 'supporter',
                            'controller_fx'  => 'supporters/actions',
                            'id_type'  => '',
                
                            ));
                            
                            //get the data 
                            echo $this -> olcomhmsdatatables -> get_data();
                        }else{
                redirect('authentication/login','refresh');
            }
    }




        function create_supporter(){

        $form_data = $this -> input -> post(NULL);
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){

           // var_dump($_FILES);

                $this->load->library('Services_Cloudinary');
                

                 try{
                $result = $this->services_cloudinary -> upload($_FILES['supporterLogo']['tmp_name'],array());
               // var_dump($result);
                  $supporter = $form_data;
                  $supporter['supporterLogo'] = $result['url'];
                  //$jobs['published_date'] = date('Y-m-d H:i:s');
                 // var_dump($jobs);
            

               

 
                 
                $result = $this->supporters_model->create_supporter($supporter);
                if($result === -1 ){
                  
                  olcom_server_form_message('Supporter Already Exists!', 0);
                }else{
                olcom_server_form_message('Supporter Created!',1);
                }

                
            }catch(Exception $e){

                    olcom_server_form_message($e->getMessage(), 0);
                }
               
                
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array(

                'with_submit_reset'  => TRUE,
                'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                'title'  => 'Create New Supporter',
                'header_info'  => $messages['fill_details'],
                'is_multipart'=>TRUE
            )); 
            
               $this -> load -> config ('olcom_selects');
              $selects = $this -> config -> item( 'selects');

            $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'supporterName',
                'label'  => 'Supporter Name',
            ));

              $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'supporterURL',
                'label'  => 'Supporter Link'
            ));
    

            $this -> olcomhmstemplateform -> add_field('file',array(
                'name'  => 'supporterLogo',
                'label'  => 'Supporter Logo',
            ));
            $this -> olcomhmstemplateform -> add_field( 'select' ,array(
                        'name' => 'published',
                        'label' => 'Choose Status',
                        'choices' => $selects['supporter_status']
                        
                    ));

     
  
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('create_supporter_specific_scripts','',TRUE);
            
            $this -> load -> view('main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
  }




  /*
     * action function for processing tabledata actions
     */
     function actions(){
      
      if($this -> uri -> total_segments()  === 4){
      
            $this -> load -> config('olcom_messages');
             $dialog_messages = $this -> config -> item('messages');  
      /*
       * 
       * do some permission checking before continuing
       * 
       */
       
      if( $this  ->  olcomaccessengine  ->  is_allowed_module_action($this  ->  uri  ->  uri_string() )  === FALSE){
          
                
          olcom_show_dialog('message',array(
                                            'header'  => $dialog_messages[ 'permission_denied' ],
                                            'message'  => $dialog_messages[ 'permission_denied_'.$this  ->  uri  -> segment(3) ]
                                        ), 'error');
                return ;                        
      }
            
      
      $this -> load -> config('olcom_labels');
      $this -> load -> config('olcom_cookies');
      
      $olcom_cookies = $this -> config -> item('action_cookies');
      $labels = $this -> config -> item('labels');
      switch($this -> uri -> segment(3)){
        case 'view':
            
              $result = $this -> olcomhms -> read('supporter',
              array('*'),
              array('supporterId'  => $this -> uri -> segment(4))
              );
                            
              if($result !=  NULL){
              $result = $result -> result_array();  
              //load dialog with data
              olcom_show_dialog('table', $result,'');
            } 
          break;
          
        case 'delete':
              /*
               *client-server confirmation 
               * 
               * cookies one for action info storage other confirm status
               */
               
              //check for confirm status
              $confirmed = 0;
              $cookie_status = $olcom_cookies['action_delete_confirm'];
              $cookie_action = $olcom_cookies['action_delete'];
              
              $confirmed = get_cookie($cookie_status);//returns FALSE where cookie not set
              
              if($confirmed  === FALSE){//cookie not set and means just clicked delete ico
              
              olcom_actions_cookies('delete', 'create');
              //load the confirmation dialog
              olcom_show_dialog('confirm',$dialog_messages['dialog_delete_party'],'');
            }
            else{//cookie set 
                  olcom_actions_cookies('delete', 'delete');
                  
                  if($this -> olcomhms -> delete('supporter',array(
                  'supporterId'  => $this -> uri -> segment(4)
                  )) > 0){
                    
                    olcom_show_dialog('message', array(
                      'header'  => $dialog_messages['operation_successful'],
                      'message'  => $dialog_messages['record_deleted']
                    ), 'success');
                  }else{
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['attention'],
                      'message'  => $dialog_messages['error_deleting_record'],
                    ),'error');
                    
                  }
              }
              
          break;
        case 'edit':
          
              //client server coms
              /*
                * cookies 
                *   olcom-action-edit-save-click [yes ,no]
                *   olcom-action-edit [action link]
                */
             
              $cookie_save = $olcom_cookies['action_save_click'];
              $cookie_action = $olcom_cookies['action_edit'];
              
              $save_click = 0;
              $save_click = get_cookie($cookie_save);
              if($save_click  === FALSE){//just clicked update button
                //create cookies 
                olcom_actions_cookies('edit', 'create');
                // load data
                $result = $this -> olcomhms -> read(
                'supporter','*',array('supporterId'  => $this -> uri -> segment(4))
                );//requested everything
                
                  $result = $result -> result_array();
                  $result = $result[0];
                  //create update form 
                  $this -> load -> config('olcom_labels');
                  $this -> load -> config('olcom_selects');
                  $labels = $this -> config -> item('labels');
                  $selects = $this -> config -> item('selects');
                  $this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Update Supporter',
                            'header_info'  => 'Fill the following Details',
                            'action'  => '#','id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                            'method'  => 'POST'
                  ));
                  //type,options
                  $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'supporterName',
                    'id'  => 'supporterName',
                    'label'  => 'supporter Name',
                    'value'  => $result['supporterName']
                  ));
                  $this -> olcomhmstemplateform -> add_field('text',array(
                    'name'  => 'supporterURL',
                    'id'  => 'supporterURL',
                    'label'  => 'Supporter Link',
                    'value'  => $result['supporterURL']
                  ));
                   $this -> olcomhmstemplateform -> add_field('select',array(
                    'name'  => 'published',
                    'id'  => 'published',
                    'label'  => 'Choose Status',
                    'selected'  => $result['published'],
                     'choices' => $selects['supporter_status']
                  ));
                  
                  olcom_show_dialog('update', array(
                  'content'  => $this -> olcomhmstemplateform -> create_form(),
                  'specific_scripts'  => 'add_personnel_form_scripts'
                  ), '');
          
                  
                }else{
                
                   olcom_actions_cookies('edit', 'delete');
                  //update record
            
                  $data = $this -> input -> post(NULL);
                  if($this -> olcomhms -> edit('supporter',$data,
                    array('supporterId'  => $this -> uri -> segment(4))) !=  0){
                    
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['operation_successful'],
                      'message'  => $dialog_messages['record_updated']
                    ), 'success');
                    
                  }else{
                    olcom_show_dialog('message',array(
                      'header'  => $dialog_messages['info'],
                      'message'  => $dialog_messages['no_changes']
                    ), 'info');
                    
                  }
                }//end else
          break;
        }//end switch
    }
  }






      



}