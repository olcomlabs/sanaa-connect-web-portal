<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personnel extends CI_Controller{
    
    var $is_logged = FALSE;
	/*
	 * 
	 */
	public function __construct(){
		parent::__construct();
        
        if($this  ->  olcomaccessengine  ->  is_logged()  === TRUE){
                olcom_is_allowed_module_helper($this -> uri -> uri_string()) ;
                $this -> is_logged = TRUE;
        }else{
            
            if( $this ->input -> is_ajax_request()){
                if(preg_match('/data/', $this -> uri -> uri_string()) === 1)
                  {
                        echo olcom_empty_dtt();
                  }     
            }
                
            else{
                redirect('authentication/login','refresh');
            }
            $this->is_logged = FALSE;
        }
	}
	
	/*
	 * 
	 * 
	 */
	 function index()
	{
				
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
	 
	 /*
	  * add_personnel fx 
	  * for adding new personnel 
	  * 
	  */
	  function add_personnel(){
	  	
		$post = $this -> input -> post(NULL);
        $this -> load -> config('olcom_messages');
		$messages = $this -> config -> item('messages');
                 
	  	if(count($post)>1 && count($post)<12)
	  	{
	  		/*
			 * 
			 * server side validation
			 */
			 
	  		 //load form_validation lib
	  		 $this -> load -> library('form_validation');
			 if($this -> form_validation -> run('personnel_register')  === TRUE)
			 {
			 	echo json_encode(array('message'  => validation_errors(),'success'  => 0));
			 }
			 else{
			 	 
				/*
				 * 
				 * some extra personnel info checking
				 */
				 
			     	 
				 //save personnel info
				 if( $this ->is_logged == FALSE)
				 {
				     olcom_server_form_message($messages['login_continue'], 0);
                     return;
				 }
				 $personnel_data = $this -> input -> post(NULL);//get all personnel info from form
				 $insert_id = $this -> olcomhms -> create('personnel',$personnel_data);
				 if($insert_id !=  NULL)
				 {
					 	echo json_encode(array('message'  => 'Personnel information saved','success'  => 1));
				 }
				else{
					 	echo json_encode(array('message'  => 'Personnel information not saved','success'  => 0));
				}
			 }
				
	  	}else{
	  		
           
			//load the template form 
			$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_selects');
			
			$labels = $this -> config -> item('labels');
			$selects = $this -> config -> item('selects');
			
			
			$this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Add Personnel',
								'header_info'  => $messages['fill_details'],
								'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
								'with_submit_reset'  => TRUE));
			//type,options
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'firstname',
				'id'  => 'firstname',
				'label'  => $labels['firstname']
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'middlename',
				'id'  => 'middlename',
				'label'  => $labels['middlename']
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'lastname',
				'id'  => 'lastname',
				'label'  => $labels['lastname']
			));
			$this -> olcomhmstemplateform -> add_field('radio',array(
				'name'  => 'gender',
				'id'  => 'gender',
				'label'  => $labels['gender'],
				 'choices'  => $selects['gender_select'],
				 'checked'  => 'Female'
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'dob',
				'id'  => 'date-picker', 
				'size'  => 70,
				'readonly'  => "",
				'data-date-format'  => "yyyy-mm-dd",
				'label'  => $labels['dob']
			));
			$this -> olcomhmstemplateform -> add_field('select',array(
				'name'  => 'marital_status',
				'id'  => 'marital_status',
				'label'  => $labels['marital_status'],
				'choices'  => $selects['marital_status_select'],
				'js'  => 'class = \'olcom-chosen\''
			
			));
			
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'address',
				'id'  => 'address',
				'label'  => $labels['address']
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'phone',
				'label'  => $labels['phone'],
				'id'  => 'phone'
				
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'nok_phone',
				'label'  => $labels['nok_phone'],
				'id'  => 'nok_phone'
				
			));
			$this -> olcomhmstemplateform -> add_field('select',array(
				'name'  => 'region',
				'id'  => 'region',
				'label'  => $labels['region'],
				'choices'  => $selects['region_select'],
				'js'  => 'class = \'olcom-chosen\''
			));
			$this -> olcomhmstemplateform -> add_field('text',array(
				'name'  => 'citizenship',
				'label'  => $labels['citizenship'],
				'id'  => 'citizenship',
				'value' => 'Tanzanian'
				
			));
			 
			$form_specific_scripts = $this -> load -> view('add_personnel_form_scripts',"",TRUE);
	
			$this -> load -> view('main_template',array('template'  => array(
				'content'  => $this -> olcomhmstemplateform -> create_form(),
				'page_specific_scripts'  => $form_specific_scripts
			)));
	  	}
	  	
	  	
	  }
	  /*
	   * view_personnel fx
	   * loads the view
	   * 
	   */
	   function view_personnel(){
	   
			$this -> load -> library(
			'OlcomHmsTemplateDatatable',
			array(
					'header'  => 'Registered Personnel',
					'columns'  => array(
						'ID','Firstname','Lastname','Phone','Address','Region'
					),
					'with_actions'  => TRUE,
					'create_controller_fx' => 'personnel/add_personnel',
					'controller_fx'  => 'personnel/personnel_data'
			)
		);
		//load the main template 
		$datatable_view = $this -> olcomhmstemplatedatatable -> create_view();
		 
		$this -> load -> view('main_template',array(
			'template'  => array(
				'content'  => $datatable_view['datatable'],
				'page_specific_scripts'  => $datatable_view['specific_scripts']
			)
		));
	   }
	
		/*
		 * 
		 * personnel_data fx
		 * loads the data requested by jquery datatables
		 */
		function personnel_data(){
		    
            if( $this -> olcomaccessengine -> is_logged() === TRUE)
            {
    			//load datatable lib
    			$this -> load -> library('OlcomHmsDataTables',array('columns'  => array(
    				'personnel_id','firstname','lastname','phone','address','region','personnel_id'
    			),
    			'index_column'  => 'personnel_id',
    			'table_name'  => 'personnel',
    			'controller_fx'  => 'personnel/actions',
    			'id_type'  => ''
    			));
    			
    			//get the data 
    			echo $this -> olcomhmsdatatables -> get_data();
            }else{
                redirect('authentication/login','refresh');
            }
            
		}

		/*
		 * action function for processing tabledata actions
		 */
		 function actions(){
		 	
			if($this -> uri -> total_segments()  === 4){
			
            $this -> load -> config('olcom_messages');
             $dialog_messages = $this -> config -> item('messages');	
			/*
			 * 
			 * do some permission checking before continuing
			 * 
			 */
			 
			if( $this  ->  olcomaccessengine  ->  is_allowed_module_action($this  ->  uri  ->  uri_string() )  === FALSE){
			    
                
			    olcom_show_dialog('message',array(
                                            'header'  => $dialog_messages[ 'permission_denied' ],
                                            'message'  => $dialog_messages[ 'permission_denied_'.$this  ->  uri  -> segment(3) ]
                                        ), 'error');
                return ;                        
			}
            
			
			$this -> load -> config('olcom_labels');
			$this -> load -> config('olcom_cookies');
			
			$olcom_cookies = $this -> config -> item('action_cookies');
			$labels = $this -> config -> item('labels');
			switch($this -> uri -> segment(3)){
				case 'view':
						
							$result = $this -> olcomhms -> read('personnel',
							array('*'),
							array('personnel_id'  => $this -> uri -> segment(4))
							);
                            
							if($result !=  NULL){
							$result = $result -> result_array();	
							//load dialog with data
							olcom_show_dialog('table', $result,'');
						} 
					break;
					
				case 'delete':
							/*
							 *client-server confirmation 
							 * 
							 * cookies one for action info storage other confirm status
							 */
							 
							//check for confirm status
							$confirmed = 0;
							$cookie_status = $olcom_cookies['action_delete_confirm'];
							$cookie_action = $olcom_cookies['action_delete'];
							
							$confirmed = get_cookie($cookie_status);//returns FALSE where cookie not set
							
							if($confirmed  === FALSE){//cookie not set and means just clicked delete ico
							
							olcom_actions_cookies('delete', 'create');
							//load the confirmation dialog
							olcom_show_dialog('confirm',$dialog_messages['dialog_delete_party'],'');
						}
						else{//cookie set 
									olcom_actions_cookies('delete', 'delete');
									
									if($this -> olcomhms -> delete('personnel',array(
									'personnel_id'  => $this -> uri -> segment(4)
									)) > 0){
										
										olcom_show_dialog('message', array(
											'header'  => $dialog_messages['operation_successful'],
											'message'  => $dialog_messages['record_deleted']
										), 'success');
									}else{
										olcom_show_dialog('message',array(
											'header'  => $dialog_messages['attention'],
											'message'  => $dialog_messages['error_deleting_record'],
										),'error');
										
									}
							}
							
					break;
				case 'edit':
					
							//client server coms
							/*
							  * cookies 
							  * 	olcom-action-edit-save-click [yes ,no]
							  * 	olcom-action-edit [action link]
							  */
						 
							$cookie_save = $olcom_cookies['action_save_click'];
							$cookie_action = $olcom_cookies['action_edit'];
							
							$save_click = 0;
							$save_click = get_cookie($cookie_save);
							if($save_click  === FALSE){//just clicked update button
								//create cookies 
								olcom_actions_cookies('edit', 'create');
								// load data
								$result = $this -> olcomhms -> read(
								'personnel','*',array('personnel_id'  => $this -> uri -> segment(4))
								);//requested everything
								
									$result = $result -> result_array();
									$result = $result[0];
									//create update form 
									$this -> load -> config('olcom_labels');
									$this -> load -> config('olcom_selects');
									$labels = $this -> config -> item('labels');
									$selects = $this -> config -> item('selects');
									$this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Update Personnel',
														'header_info'  => 'Fill the following Details',
														'action'  => '#','id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
														'method'  => 'POST'
									));
									//type,options
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'firstname',
										'id'  => 'firstname',
										'label'  => $labels['firstname'],
										'value'  => $result['firstname']
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'middlename',
										'id'  => 'middlename',
										'label'  => $labels['middlename'],
										'value'  => $result['middlename']
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'lastname',
										'id'  => 'lastname',
										'label'  => $labels['lastname'],
										'value'  => $result['lastname']
									));
									$this -> olcomhmstemplateform -> add_field('radio',array(
										'name'  => 'gender',
										'id'  => 'gender',
										'label'  => $labels['gender'],
										 'choices'  => $selects['gender_select'],
										 'checked'  => $result['gender']
										 
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'dob',
										'id'  => 'date-picker',
										'class'  => 'span8',
										'size'  => 70,
										'readonly'  => "",
										'data-date-format'  => "yyyy-mm-dd",
										'label'  => $labels['dob'],
										'value'  => $result['dob']
									));
									$this -> olcomhmstemplateform -> add_field('select',array(
										'name'  => 'marital_status',
										'id'  => 'marital_status',
										'label'  => $labels['marital_status'],
										'choices'  => $selects['marital_status_select'],
										'selected'  => $result['marital_status'],
										'js'  => 'class = \'olcom-chosen\''
									));
									
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'address',
										'id'  => 'address',
										'label'  => $labels['address'],
										'value'  => $result['address']
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'phone',
										'label'  => $labels['phone'],
										'id'  => 'phone',
										'value'  => $result['phone']
										
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'nok_phone',
										'label'  => $labels['nok_phone'],
										'id'  => 'nok_phone',
										'value'  => $result['nok_phone']
										
									));
									$this -> olcomhmstemplateform -> add_field('select',array(
										'name'  => 'region',
										'id'  => 'region',
										'label'  => $labels['region'],
										'choices'  => $selects['region_select'],
										'selected'  => $result['region'],
										'js'  => 'class = \'olcom-chosen\''
									));
									$this -> olcomhmstemplateform -> add_field('text',array(
										'name'  => 'citizenship',
										'label'  => $labels['citizenship'],
										'id'  => 'citizenship',
										'value'  => $result['citizenship']
									));
									 
									
									olcom_show_dialog('update', array(
									'content'  => $this -> olcomhmstemplateform -> create_form(),
									'specific_scripts'  => 'add_personnel_form_scripts'
									), '');
					
									
								}else{
								
									 olcom_actions_cookies('edit', 'delete');
									//update record
						
									$data = $this -> input -> post(NULL);
									if($this -> olcomhms -> edit('personnel',$data,
										array('personnel_id'  => $this -> uri -> segment(4))) !=  0){
										
										olcom_show_dialog('message',array(
											'header'  => $dialog_messages['operation_successful'],
											'message'  => $dialog_messages['record_updated']
										), 'success');
										
									}else{
										olcom_show_dialog('message',array(
											'header'  => $dialog_messages['info'],
											'message'  => $dialog_messages['no_changes']
										), 'info');
										
									}
								}//end else
					break;
				}//end switch
		}
	}
}

