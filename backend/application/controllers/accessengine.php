<?php  if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );

class Accessengine extends  CI_Controller{
    
    
    function __construct()
    {
        parent :: __construct();
    }
    
    function access_denied()
    {
        $this -> load -> view( 'main_template' ,array(
            'template' => array( 'content' => $this -> load -> view( 'access_denied','',TRUE ) ) ) );
    }
    /*
     * 
     * 
     * 
     */
     function not_found() 
     {
         $this -> load -> view( 'main_template' , array(
            'template' => array( 'content' => $this -> load -> view ( 'not_found', '' , TRUE ))));
     }
}
