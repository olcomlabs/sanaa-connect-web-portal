<?php if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
/*
 *@class - Notifications 
 * 
 * 17-09-2013 23:18
 */
 class Notifications extends CI_Controller
 {
     
     var $logged = FALSE;
     var $return_codes;
     function Notifications()
     {
          
         parent:: __construct();
     }
     /*
       * 
       * 
       * 
       */
       function  notifications_data()
       {
           
           $logged_user = $this -> olcomaccessengine -> get_logged_in_user();
           
           if( isset( $logged_user[ 'employee_id' ] ) AND $logged_user[ 'employee_id' ] != NULL )
           {
               $this -> load -> model( 'notifications_model' );
               $user_notifications = $this -> notifications_model -> get_user_notifications( $logged_user[ 'employee_id' ] );
               
               $notes_html = '';
               if( $user_notifications != NULL )
               foreach( $user_notifications as $key => $user_notification )
               {
                  $icon = '';
                  $message = '';
                  $notification = $this -> notifications_model -> get_notification( $user_notification[ 'note_id' ] );
                  if( $notification != NULL )
                  {
                      if( $notification[ 'is_prescription' ] == 1 )
                      {
                          $icon = 'icon-medkit';
                          $message = 'New Prescription Order';
                      }
                      if( $notification[ 'is_test_request' ]  == 1 )
                      {
                          $icon = 'icon-question';
                          $message = 'New Test Request';
                      }
                      if( $notification[ 'is_test_result' ] ==1 )
                      {
                          $icon = 'icon-check';
                          $message = 'New Test Result';
                          
                      }  
                  }  
                  $user_notifications[ $key ][ 'icon' ] = $icon;
                  $user_notifications[ $key ][ 'message' ] = $message;
               }
               
               $notes_html = $this -> load -> view( 'notification_item' , array( 'notifications' => $user_notifications ) , TRUE );
               
               echo json_encode( array( 
                  'notifications' => $notes_html ,
                  'num_notifications' => count( $user_notifications ) 
               ));
           }
       }
      
 }