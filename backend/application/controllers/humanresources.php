<?php if(!defined('BASEPATH'))exit('Direct access is restricted');

/*
 * 
 * Human Resources Controller 
 * for HMS hr operations 
 * 
 */
 class Humanresources extends  CI_Controller{
 	
	
	function __construct(){
	 
         parent:: __construct();
       
     
         $this -> return_codes = $this -> config -> item( 'return_codes' );
         
          if($this  ->  olcomaccessengine  ->  is_logged()  === TRUE)
          {
              
                olcom_is_allowed_module_helper( $this -> uri -> uri_string() ) ;
                
                $this -> is_logged = TRUE;
           }
           else
           {
            
            if( $this ->input -> is_ajax_request())
            {
                if(preg_match('/data/', $this -> uri -> uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
            
            $this->is_logged = FALSE;
            
            
        }
            if( ! defined( 'SUCCESS_CODE' ))
                define( 'SUCCESS_CODE',$this -> config -> item( 'success_code'));
            
         $this -> load -> config('olcom_messages');
         $this  ->  load  ->  config( 'olcom_labels'); 
         $this -> load -> model( 'employees' );
         $this -> load -> model( 'humanresources_model' );
         $this -> load -> config ('olcomhms_return_codes'); 
	}
	 function index()
	{
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
	 /*
	  * 
	  * 
	  * create_department fx
	  */
	  function create_department(){
	 
			$messages  = $this -> config -> item('messages');
			$labels  = $this -> config -> item('labels');
			
		//check for if form is submitted
		$form_data  = $this -> input -> post(NULL);//get all post values
		
		if(isset($form_data) && $form_data !=  ""){
			
			if($this -> olcomhms -> record_exists(
				'departments',$form_data,FALSE,TRUE,'and')  == FALSE){//record does not exists
			//add date_created	
			$form_data['date_created']  = date('Y-m-d');
			
			if($this -> olcomhms -> create('departments',$form_data) !=  NULL){
					olcom_server_form_message($messages['new_department'], 1);
				}
			}else{
					olcom_server_form_message($messages['record_exists'],0);		
			}
		}else{
			
			
			//create new department form
			
			//load template form library
			$this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Create Department',
								'header_info'  => $messages['fill_details'],
								'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
								'with_submit_reset'  => TRUE));
								
			//form fields
			$this -> olcomhmstemplateform -> add_field('text',
					array(
						'name'  => 'department_name',
						'label'  => $labels['department_name']
					)
				
			);

			$organizations_results = $this->humanresources_model->get_organizations();
			 
			$organizations = array();
			if( $organizations_results != null ){
				foreach ($organizations_results as $key => $organization) {
					$organizations[$organization['organizationId']] = $organization['organizationName'];
				}
			}
			 
			$this -> olcomhmstemplateform -> add_field('select',
                    array(
                        'name'  => 'organizationId',
                        'label'  => 'Select Organization',
                        'choices'  => $organizations,
                        'selected'  => '',
                        
                    )
                );

			
			//load the main template 
			$this -> load -> view('main_template',
				array(
					'template'  => array(
					'content'  => $this -> olcomhmstemplateform -> create_form(),
					'page_specific_scripts'  => $this -> load -> view('create_department_specific_scripts'
					,"",TRUE)
					)
				)
			);
		}
	  }
	/*
	 * 
	 * 
	 * 
	 * departments
	 */
	 function departments(){
	 	$this -> load -> library(
			'OlcomHmsTemplateDatatable',
			array(
					'header'  => 'Departments',
					'columns'  => array(
						'ID','Department Name','Organization','Date Created'
					),
					'with_actions'  => TRUE, 
					'controller_fx'  => 'humanresources/departments_data',
					'create_controller_fx' => 'humanresources/create_department'
			)
		);
		
		//load the main template 
		$datatable_view  = $this -> olcomhmstemplatedatatable -> create_view();
		 
		$this -> load -> view('main_template',array(
			'template'  => array(
				'content'  => $datatable_view['datatable'],
				'page_specific_scripts'  => $datatable_view['specific_scripts']
			)
		));
	 }
 	/*
	 * 
	 * 
	 * 
	 * departments_data fx for jquery datatables
	 */
	 function departments_data(){

	 	$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
            array(
                'tables'  => array(
                    'departments'  => array('department_id','department_name','date_created'),
                    'organization'  => array('organizationName'),
                    ),
                    'with_actions'  => TRUE,
                    'hidden_cols'  => array(''),
                    'index_column'  => 'department_id',
                    'controller_fx'  => 'artist/actions/departments',//for actions
                    'id_type'  => '',
                    'where'  => array('departments.organizationId'  => 'organization.organizationId'),
                    'data_style_format'  => NULL,
                     'cell_ops' => array(
                         
                        ),
                    'order'  => array('department_name'  => 1,'organizationName'  => 2,'date_created'=>3)
                 )
             );
            echo $this -> olcomhmsdatatablesmultidbtables -> get_data();
	 }
	 
	 /*
	  * 
	  * actions 
	  * 
	  */
	  function actions(){
 
        $messages = $this -> config -> item('messages');
        
        if( $this -> olcomaccessengine -> is_allowed_module_action($this -> uri -> uri_string() )  ===  FALSE)
        {   
                olcom_show_dialog('message',array(
                                            'header'  => $messages[ 'permission_denied' ],
                                            'message'  => $messages[ 'permission_denied_'.$this -> uri -> segment(4) ]
                                        ), 'error');
                return ;                        
        } 
	  	//load cookies configurations
	  	$this -> load -> config('olcom_cookies');
		$olcom_cookies  = $this -> config -> item('action_cookies'); 
	  	$section  = $this -> uri -> segment(3);
		$this -> load -> config('olcom_labels');
		$labels  = $this -> config -> item('labels');
		
        $olcom_cookies = $this -> config -> item('action_cookies');
        $cookie_save = $olcom_cookies['action_save_click'];
        $cookie_status = $olcom_cookies['action_delete_confirm'];
        
		if($section   ===  'departments'){
			
			switch($this -> uri -> segment(4)){
				case 'view':
				
						//get department details and number of employees in that department
						$result_dept_info  = $this -> humanresources_model -> get_departments( $this -> uri -> segment( 5 ) );
						$result_dept_employees  = $this -> olcomhms -> read(
							'employees','*',array('department_id'  => $this -> uri -> segment(5))
						);
						
						$num_employees  = $result_dept_employees  == NULL ? 0 : $result_dept_employees -> num_rows();
					
						$result_dept_info['num_employees']  = $num_employees;
						
						olcom_show_dialog('table',array( $result_dept_info ) , '');
						
					break;
			case 'edit':
					
					$save_click = 0;
                    $save_click = get_cookie($cookie_save);
                            
					
					if($save_click  == FALSE){
					olcom_actions_cookies('edit', 'create');
					//get data 
					$result  = $this -> humanresources_model -> get_departments( $this -> uri -> segment( 5 ) );
					//create update form
					$this -> load -> library('OlcomHmsTemplateForm',
						array(
							'title'  => $labels[ 'update_department' ]  
						)); 
					//add field
					$this -> olcomhmstemplateform -> add_field('text',
							array(
								'name'  => 'department_name',
								'value'  => $result['department_name'],
								'label'	  => $labels['department_name']
								)
						);
					
					olcom_show_dialog('update', array(
						'content'  => $this -> olcomhmstemplateform -> create_form() ,
						'specific_scripts'  => 'create_department_specific_scripts'
							), '');		
					}else{
						//modal form submitted
						$form_data  = $this -> input -> post(NULL);
						if(isset($form_data) && $form_data !=  ""){
							
							if($this -> olcomhms -> edit('departments',$form_data,array('department_id'  => $this -> uri -> segment(5)))  == 1){
								olcom_show_dialog('message',
									array('header'  => $messages['operation_successful'],'message'  => $messages['record_updated']),'info');
								
							}else{
								olcom_show_dialog('message', array(
									'message'  => $messages['no_changes'],'header'  => 'Info'), 'info');
							}
						}
						olcom_actions_cookies('edit', 'delete');
					}
				break;	
			case 'delete':
						$cookie_delete_confirm  = get_cookie($olcom_cookies['action_delete_confirm']);
						
						if($cookie_delete_confirm  == FALSE){
							olcom_actions_cookies('delete', 'create');
							olcom_show_dialog('confirm', $messages['dialog_delete_party'],'');
						}else{
							
							if($this -> olcomhms -> delete('departments',
								array('department_id'  => $this -> uri -> segment(5)))  == 1){
									olcom_actions_cookies('delete', 'delete');
									olcom_show_dialog('message', array(
										'message'  => $messages['record_deleted'],
										'header'  => $messages['operation_successful']
									), 'info');
								}
						}
				break;	
				}		
			}
			/*
			 * actions for jobs
			 * 
			 * 
			 */
			 if($section   ===  'job_positions'){
			 	
				switch($this -> uri -> segment(4)){
					case 'view'	:
							$result  = $this -> humanresources_model -> get_jobs( $this -> uri -> segment( 5 ) );
							 
							//get dpt name
							$result_dpt  = $this -> humanresources_model -> get_departments( $result[ 'department_id' ]) ;
							unset($result['department_id']);
                            $result[ 'min_salary' ] = olcom_money_format( $result[ 'min_salary' ] );
                            $result[ 'max_salary' ] = olcom_money_format( $result[ 'max_salary' ] );
							$result['department_name']  = $result_dpt['department_name'];
							olcom_show_dialog('table', array( $result ), '');
						break;
					case 'edit':
					
							$save_click  = get_cookie( $cookie_save );
					
							if( $save_click  == FALSE){
							olcom_actions_cookies('edit', 'create');
							//get data 
							$result  = $this -> humanresources_model -> get_jobs( $this -> uri -> segment( 5 ) );
							//create update form
							$this -> load -> library('OlcomHmsTemplateForm',
								array(
									'title'  => $labels[ 'update_position' ]
								));
							
							//add field
							$this -> olcomhmstemplateform -> add_field('text',
									array(
										'name'  => 'job_title',
										'value'  => $result['job_title'],
										'label'	  => $labels['job_title']
										)
								);
							$this -> olcomhmstemplateform -> add_field('text',
									array(
										'name'  => 'min_salary',
										'value'  => $result['min_salary'],
										'label'	  => $labels['min_salary']
										)
								);
							$this -> olcomhmstemplateform -> add_field('text',
									array(
										'name'  => 'max_salary',
										'value'  => $result['max_salary'],
										'label'	  => $labels['max_salary']
										)
								);
							
							$departments_result  = $this -> humanresources_model -> get_departments( );
							$this -> olcomhmstemplateform -> add_field('select',
								array(
									'name'  => 'department_id',
									'label'  => $labels['department'],
									'choices'  => $departments_result,
									'selected'  => $result['department_id']
								)
							);
							olcom_show_dialog('update', array(
								'content'  => $this -> olcomhmstemplateform -> create_form() ,
								'specific_scripts'  => 'positions_specific_scripts'
									), '');		
							}else{
								//modal form submitted
								$form_data  = $this -> input -> post(NULL);
								if(isset($form_data) && $form_data !=  ""){
									if($form_data['min_salary']>$form_data['max_salary']){
										olcom_show_dialog('message', array(
											'message'  => $messages['invalid_salary_range'],'header'  => $messages['error']), 'error');
									}else{
											if($this -> olcomhms -> edit('jobs',$form_data,array('job_id'  => $this -> uri -> segment(5)))  == 1){
												olcom_show_dialog('message',
													array('header'  => $messages['operation_successful'],'message'  => $messages['record_updated']),'info');
											
										}else{
											olcom_show_dialog('message', array(
												'message'  => $messages['no_changes'],'header'  => 'Info'), 'info');
										}
									}
									
								}
								olcom_actions_cookies('edit', 'delete');
							}
						break;
					case 'delete':
								$cookie_delete_confirm  = get_cookie($olcom_cookies['action_delete_confirm']);
						
								if($cookie_delete_confirm  == FALSE){
									olcom_actions_cookies('delete', 'create');
									olcom_show_dialog('confirm', $messages['dialog_delete_party'],'');
								}else{
									
									if($this -> olcomhms -> delete('jobs',
										array('job_id'  => $this -> uri -> segment(5)))  == 1){
											olcom_actions_cookies('delete', 'delete');
											olcom_show_dialog('message', array(
												'message'  => $messages['record_deleted'],
												'header'  => $messages['operation_successful']
											), 'info');
										}
								}
						break;
				}
			 }
			 
			if($section  == 'employees'){
				switch($this -> uri -> segment( 4 ) )
				{
					case 'view':
							$this -> load -> model( 'employees' );
							$result_employee  = $this -> employees -> info( $this -> uri -> segment( 5 ),'basic',FALSE);
                            
                             $result_employee['active'] = $this -> load -> view('data_style_format',
                                array('format' => array( 'yes_no' => TRUE,'data' => $result_employee['active'])),TRUE);
                            $result_employee[ 'salary' ] = olcom_money_format( $result_employee[ 'salary' ] );
							if($result_employee  !=   NULL)
                            {
                                
                                olcom_show_dialog('table', array( $result_employee ), '');
                            }
								
							else {
								olcom_show_dialog('message', array('message'  => $messages[ 'error_employee' ],'header'  => $labels['error']), 'error');
							}
						break;
						
				case 'edit':
							$cookie_save_click  = get_cookie($olcom_cookies['action_save_click']);
					
						if( $cookie_save_click  == FALSE ){
							olcom_actions_cookies('edit', 'create');
							$this -> load -> config('olcom_selects');
							$selects  = $this -> config -> item('selects');
							$this -> load -> model('employees');
							//get data 
							
							$result_employee  = $this -> employees -> info( $this -> uri -> segment( 5 ),'full',TRUE );
						
                        	if($result_employee !=  NULL){
									$this -> load -> library('OlcomHmsTemplateForm',array(
									'title'  => 'Update Employee'
									));
									
									$this -> olcomhmstemplateform -> add_field('text',
									array(
										'name'  => '',
										'value'  => $result_employee['firstname'].'   '.$result_employee['lastname'],
										'label'  => $labels['employee_name'],
										'readonly'  => ''
									));
									$result_obj  = $this -> olcomhms -> read(
								'departments',array('department_id','department_name'),NULL);
								$this -> olcomhmstemplateform -> add_field('select',
									array(
											'name'  => 'department_id',
											'label'  => $labels['department'],
											'choices'  => olcom_db_to_dropdown($result_obj),
											'js'  => 'class  = \'olcom-chosen\'',
											'selected'  => $result_employee['department_id']
									)
								);
								$this -> olcomhmstemplateform -> add_field('select',
									array(
											'name'  => 'job_id',
											'label'  => $labels['job_title'],
											'choices'  => array(),
											'js'  => 'class  = \'olcom-chosen\' data-placeholder  = \''.
											$result_employee['job_title'].'\'',
											'selected'  => $result_employee['job_id']
									)
								);
								$this -> olcomhmstemplateform -> add_field('text',
								array(
										'name'  => 'salary',
										'label'  => $labels['salary'],
										'value'  => $result_employee['salary']
								));
                               
								$this -> olcomhmstemplateform -> add_field('checkbox',
								array(
								        'name' => 'active',
                                        'label' => $labels[ 'active' ],
                                        'class' => 'ace-switch ace-switch-5',
                                        'value' => 1 ,
                                        'checked' => $result_employee[ 'active' ] == 1 ? TRUE : FALSE 
								));
								olcom_show_dialog('update', array(
									'content'  => $this -> olcomhmstemplateform -> create_form() ,
									'specific_scripts'  => 'employees_specific_scripts'), '');					
							}else
									olcom_show_dialog('message',array('message'  => $messages['error_employee'],
												'header'  => $labels['employee_info']),'error');	
													
							}
							else
							{
								olcom_actions_cookies('edit', 'delete');
								$form_data  = $this -> input -> post( NULL );
								
								if( isset( $form_data['job_id'] ) )
								{
									$result  = $this -> olcomhms -> read('jobs',array('min_salary','max_salary'),array('job_id'  => $form_data['job_id'],
										$form_data['salary'].'>  = '  => 'min_salary',$form_data['salary'].'<  = '  => 'max_salary'),'and');
										
									if($result  == NULL){
											olcom_actions_cookies('edit', 'delete');
											olcom_show_dialog('message',array('message'  => $messages['salary_out_of'],'header'  => $messages['error']),'error');
										break;
									}
								
								}	
                                $form_data[ 'active' ]  = isset( $form_data[ 'active' ] ) ? 1 : 0 ;
                                
                                $result = $this -> employees -> update_employee( $this -> uri -> segment( 5 ) , $form_data );
                                 if( $result === SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $messages['record_updated'],
                                        'header'  => $messages['operation_successful']), 'success');
                                }
                                if( $result !== SUCCESS_CODE)
                                {
                                     olcom_show_dialog('message', array('message'  => $this  ->  return_codes[ $result ],
                                        'header'  => $messages['operation_failed'] ), $result == 108 ? 'warning' : 'info');
                                } 
							}
							
					break;
			case 'delete':
							$cookie_delete_confirm  = get_cookie($olcom_cookies['action_delete_confirm']);
						
								if($cookie_delete_confirm  == FALSE){
									olcom_actions_cookies('delete', 'create');
									olcom_show_dialog('confirm', $messages['dialog_delete_party'],'');
								}else{
									
									if($this -> olcomhms -> delete('employees',
										array('employee_id'  => $this -> uri -> segment(5)))  == 1){
											olcom_actions_cookies('delete', 'delete');
											olcom_show_dialog('message', array(
												'message'  => $messages['record_deleted'],
												'header'  => $messages['operation_successful']
											), 'info');
										}
								}					
					break;
					
				}
			}
	  }

	/*
	 * 
	 * 
	 * create_position
	 */
	 function create_position(){
	 	 
		$labels  = $this -> config -> item('labels');
		$messages  = $this -> config -> item('messages');
		$form_data  = $this -> input -> post(NULL);
		
		if(isset($form_data) && $form_data !=  ""){
			//main template already load 
			
			//check if record exits
			if($this -> olcomhms -> record_exists('jobs',array('job_title'  => $form_data['job_title']),FALSE,TRUE,'and')  == FALSE){
				
				if($form_data['min_salary']>$form_data['max_salary']){
					olcom_server_form_message('Salary range invalid', 0);
				}else{
						if($this -> olcomhms -> create('jobs',$form_data) !=  NULL){
							olcom_server_form_message('Job position created', 1);
						}else{
							olcom_server_form_message('Could not create job position',0);
						}
				}
			
			}else{
				olcom_server_form_message('Position already exists', 0);
			}
		}else{
			//this largely depends on departments so check for registered depts
			$result  = $this -> employees -> get_departments( );
			
				//load form 
			$this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Create Position',
									'with_submit_reset'  => TRUE));
	        $result_obj  = $this -> employees -> get_departments( NULL , 'obj' );
                $this -> olcomhmstemplateform -> add_field('select',
                    array(
                        'name'  => 'department_id',
                        'label'  => $labels['department'],
                        'choices'  => olcom_db_to_dropdown( $result_obj ) !=  NULL ? olcom_db_to_dropdown( $result_obj ):array(),
                        'selected'  => '',
                        
                    )
                );
			$this -> olcomhmstemplateform -> add_field('text',
					array(
							'name'  => 'job_title',
							'label'  => $labels['job_title']
							 
					)
				);
				$this -> olcomhmstemplateform -> add_field('text',
					array(
							'name'  => 'min_salary',
							'label'  => $labels['min_salary']
					)
				);
				$this -> olcomhmstemplateform -> add_field('text',
					array(
							'name'  => 'max_salary',
							'label'  => $labels['max_salary']
					)
				);
				
				$this -> load -> view('main_template',array(
					'template'  => array(
						'content'  => $this -> olcomhmstemplateform -> create_form(),
						'page_specific_scripts'  => $this -> load -> view('positions_specific_scripts','',TRUE)
					)
				));
			
			
		}
	 }
	/*
	 * job_positions for initializing jquery jobs datatables
	 * 
	 */
	 function job_positions(){
	 	
		$this -> load -> config('olcom_labels');
		$labels  = $this -> config -> item('labels');
		
	 	$this -> load -> library(
			'OlcomHmsTemplateDatatable',
			array(
					'header'  => 'Job Positions',
					'columns'  => array(
						$labels['id'],$labels['job_title'],$labels['min_salary'],$labels['max_salary']
					),
					'with_actions'  => TRUE,
					'create_controller_fx' => 'humanresources/create_position',
					'controller_fx'  => 'humanresources/jobs_data'
			)
		);
		
		//load the main template 
		$datatable_view  = $this -> olcomhmstemplatedatatable -> create_view();
		 
		$this -> load -> view('main_template',array(
			'template'  => array(
				'content'  => $datatable_view['datatable'],
				'page_specific_scripts'  => $datatable_view['specific_scripts']
			)
		));
	 }
	/*
	 * 
	 * function jobs_data
	 */	
	 function jobs_data(){
	 	
		$this -> load -> library('OlcomHmsDataTables',
			array(
				'columns'  => array('job_id','job_title','min_salary','max_salary','job_id'),
				'index_column'  => 'job_id',
				'table_name'  => 'jobs',
				'controller_fx'  => 'humanresources/actions/job_positions',//for actions
				'id_type'  => ''
			)
		);
		echo $this -> olcomhmsdatatables -> get_data();	
	 }
	 
	/*
	 * 
	 * 
	 * 
	 * hire_employee
	 */
	 function hire_employee(){
	 
		$this -> load -> config('olcom_selects');
		
		$selects  = $this -> config -> item('selects');
		$labels  = $this -> config -> item('labels');
		$messages  = $this -> config -> item('messages');
		$form_data  = $this -> input -> post( NULL );
		
		if(isset($form_data) && $form_data !=  ""){
			
           
            //check if salary is in range of the position
            $result  = $this -> employees -> get_positions( $form_data[ 'job_id' ]  , NULL );
            
            if( $form_data[ 'salary' ] >= $result[ 'min_salary']  AND $form_data[ 'salary' ] <= $result[ 'max_salary' ] )
            {
                $result = $this -> employees -> hire_employee( $form_data  );
                
                if( $result === SUCCESS_CODE ){
                     olcom_server_form_message( $messages[ 'hired' ], 1);
                 }else{
                    olcom_server_form_message($this  ->  return_codes[ $result ], 0);
                }
            }
            else{
                olcom_server_form_message($messages['salary_out_of'], 0);
                return;
            }
			
			 
		}else{
			//this largely depends on departments so check for registered depts
			$result  = $this -> employees -> get_departments( NULL , 'obj' );
			
				//load form 
			$this -> load -> library('OlcomHmsTemplateForm',array('title'  => 'Hire Employee',
									'with_submit_reset'  => TRUE));
									
									
			$result  = $this -> olcomhms -> get_idle_personnel(array('personnel_id',"concat(firstname,' ',lastname) as name"));
			$this -> olcomhmstemplateform -> add_field('select',
					array(
							'name'  => 'employee_id',
							'label'  => $labels['select_personnel'],
							'choices'  => olcom_db_to_dropdown( $result )
					)
				);
				$result_obj  = $this -> employees -> get_departments( NULL , 'obj' );
				$this -> olcomhmstemplateform -> add_field('select',
					array(
							'name'  => 'department_id',
							'label'  => $labels['department'],
							'choices'  => olcom_db_to_dropdown( $result_obj ),
							'js' => " id = 'department_id' "
					)
				);
				$this -> olcomhmstemplateform -> add_field('select',
					array(
							'name'  => 'job_id',
							'label'  => $labels['job_title'],
							'choices'  => array(),
							'js'  => 'id  = \'job_id\'' 
							
					)
				);
				$this -> olcomhmstemplateform -> add_field('text',
				array(
						'name'  => 'salary',
						'label'  => $labels['salary']
				));
				$this -> olcomhmstemplateform -> add_field('checkbox',
				array(
						'name'  => 'active',
						'label'  => $labels['active'],
						'value'  => 1,
						'class'  => 'ace-switch ace-switch-2'
				));
				$page_scripts  = $this -> load -> view('employees_specific_scripts',
				array(
					'view_data'  => array('class_name'  => '.olcom-chosen')
				),TRUE);
				
				$this -> load -> view('main_template',array(
					'template'  => array(
						'content'  => $this -> olcomhmstemplateform -> create_form(),
						'page_specific_scripts'  => $page_scripts
					)
				));
		}
	 }/*
	  * 
	  * 
	  * jobs_from_dpt fx
	  * @params-department_id
	  */
	  	function jobs_from_dpt(){
	  		$departments  = array();
			$department_id  = $this -> uri -> segment(3);
			if(isset($department_id)){
				$result  = $this -> olcomhms -> read(
					'jobs',array('job_id','job_title'),array('department_id'  => $department_id));
				if($result !=  NULL){
					if($result -> num_rows()>0){
						olcom_server_chosen_ajax(olcom_db_to_dropdown($result));
					}
				}else{
						olcom_server_chosen_ajax(NULL);
				}
			}
	  	}
		
		/*
		 * 
		 * employees datatable initializer
		 */
		 function employees(){
		 	$this -> load -> config('olcom_labels');
    		$labels  = $this -> config -> item('labels');
    		
    	 	$this -> load -> library(
    			'OlcomHmsTemplateDatatable',
    			array(
    					'header'  => 'Employees',
    					'columns'  => array(
    						$labels['id'],$labels['firstname'],$labels['lastname'],$labels['job_title'],$labels['department'],'Organization'
    					),
    					'with_actions'  => TRUE,
    					'create_controller_fx' => 'humanresources/hire_employee',
    					'controller_fx'  => 'humanresources/employees_data'
    			)
		);
		
		//load the main template 
		$datatable_view  = $this -> olcomhmstemplatedatatable -> create_view();
		 
		$this -> load -> view('main_template',array(
			'template'  => array(
				'content'  => $datatable_view['datatable'],
				'page_specific_scripts'  => $datatable_view['specific_scripts']
			)
		));
	 }
	  /*
	  * 
	  * 
	  * 
	  */
	  function employees_data(){
	  	$this -> load -> library('OlcomHmsDataTablesMultiDBTables',
			array(
				'tables'  =>  array(
					'employees'  =>  array('employee_id','job_id','department_id'),
					'personnel'  => array('personnel_id','firstname','lastname'),
					'jobs'  =>  array('job_id','job_title'),
					'departments'  => array('department_id','department_name'),
					'organization' => array('organizationName')
					),
				'with_actions'  => TRUE,
				'hidden_cols'  => array('personnel_id','job_id','department_id'),
				'index_column'  => 'employee_id',
				'controller_fx'  => 'humanresources/actions/employees',//for actions
				'id_type'  => '',
				'where'  => array('employees.employee_id'  => 'personnel.personnel_id',
								'jobs.job_id'  => 'employees.job_id','departments.department_id'  => 'employees.department_id',
								'departments.organizationId' => 'organization.organizationId'),
				'order'  => array('firstname'  => 1,'lastname'  => 2,'job_title'  => 3,'department_name'  => 4,'organizationName'=>5)
			)
		);
		echo $this -> olcomhmsdatatablesmultidbtables -> get_data();	
	  }
	
 }
