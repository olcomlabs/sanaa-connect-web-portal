<?php
if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
class Organizations extends CI_Controller{
	var $logged = FALSE;
     var $return_codes;
	 var $help;

	function __construct(){
		parent::__construct();
		if($this   ->   olcomaccessengine   ->   is_logged()  === TRUE)
          {
                olcom_is_allowed_module_helper($this  ->  uri  ->  uri_string()) ;
                
                $this  ->  is_logged = TRUE;
           }
           else
           {
            
            if( $this  -> input  ->  is_ajax_request())
            {
                if(preg_match('/data/', $this  ->  uri  ->  uri_string()) === 1)
                  {
                        echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                  }     
            }
                
            else
            {
                redirect('authentication/login','refresh');
            }
		   }
         $this -> is_logged = FALSE;
         $this -> load -> config( 'olcom_messages' );
         $this -> load -> config( 'olcom_labels' );
		     $this -> load -> config( 'olcom_help_text' );
         $this -> load -> config( 'olcomhms_return_codes' );
         $this -> return_codes = $this -> config -> item( 'return_codes' );
         $this -> messages = $this -> config -> item( 'messages' );
         $this -> labels = $this -> config -> item( 'labels' );
		 $this -> help = $this -> config -> item( 'help_text' );        
		 $this -> load -> library( 'OlcomHmsFx' );
		 $this->load->model('organizations_model');
		

	}

	function index(){

				 
				$modules = $this -> olcomaccessengine -> load_modules( TRUE );
				//print_r( $modules[  $this -> uri -> segment( 1)] );return;
				$modules_configs = $this -> config -> item('modules_configs'  );
				$sub_modules = $modules[  $this -> uri -> segment( 1)];
				$this -> load -> config( 'olcomhms_modules_configs' );
					//print_r( $modules_configs[ $sub_modules[ 44 ]] );
				
			 
				$launchers = array();
				foreach ($sub_modules as $key => $sub_module) {
					$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
				}
				
				$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
				$data[ 'launchers' ] = $launchers;
				
				$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );	
				
				 $this -> load -> view('main_template',array(
	                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
	                
	            ));
	}

	function  registered_organizations()
	{
		$this -> load -> library('OlcomHmsTemplateDatatable', 
	             array('header' => 'Registered Organizations' , 'with_actions' => TRUE, 
	            'create_controller_fx' => 'organizations/create_organization',
	             'columns' => array( 'ID#' , 'Organization Name', 'Date Created'), 
	             'controller_fx' => 'organizations/registeredOrganization_data'));
	    
	            $datatable = $this -> olcomhmstemplatedatatable -> create_view();
	    
	            $this -> load -> view('main_template', array('template' => array('content' => $datatable['datatable'], 'page_specific_scripts' => $datatable['specific_scripts'])));
	}

	function registeredOrganization_data()
	{
                                     if( $this -> olcomaccessengine -> is_logged() === TRUE)
                          {
    			//load datatable lib
    			$this -> load -> library('OlcomHmsDataTables',array('columns'  => array(
    				'organizationId','organizationName','o_dateCreated','organizationId'
    			),
    			'index_column'  => 'organizationId',
    			'table_name'  => 'organization',
    			'controller_fx'  => 'organizations/actions',
    			'id_type'  => ''
    			));
    			
    			//get the data 
    			echo $this -> olcomhmsdatatables -> get_data();
            }else{
                redirect('authentication/login','refresh');
            }
	}


	function create_organization(){

        $form_data = $this -> input -> post(NULL);
        $this -> load -> config('olcom_messages');
        $this -> load -> config('olcom_labels');
        $messages = $this -> config -> item('messages');
        $labels = $this -> config -> item('labels');
        
        if(isset($form_data) && $form_data!= NULL){
          
                $form_data['o_dateCreated'] = date('Y-m-d H:i:s');
                $result = $this->organizations_model->create_organization($form_data);
                if($result === -1 ){
                 olcom_server_form_message('Organization Already Exists!', 0);
                }else{
                 olcom_server_form_message('Organization Created!',1);
                }
                
            
        }else{
            $this -> load -> library('OlcomHmsTemplateForm',array(
                 
                'with_submit_reset'  => TRUE,
                'id'  => 'olcomhms-template-form','class'  => 'form-horizontal',
                'title'  => 'Register Organization',
                'header_info'  => $messages['fill_details'],
                'is_multipart'=>FALSE
            )); 
            $this -> olcomhmstemplateform -> add_field('text',array(
                'name'  => 'organizationName',
                'label'  => 'Organization Name'
            ));
  
            $form = $this -> olcomhmstemplateform -> create_form();
            
            $form_specific_scripts = $this -> load -> view('create_organization_specific_scripts','',TRUE);
            
            $this -> load -> view('main_template',array(
                'template'  => array('content'  => $form,'page_specific_scripts'  => $form_specific_scripts)
                
            ));
        }
	}
}