<?php
/*
 *
 *
 *
 */
class Reports extends CI_Controller {
    var $labels;
    var $messages;
    var $return_codes;
	var $help;

    var $limited_content;
    function Reports() {
        parent::__construct();
        if ($this -> olcomaccessengine -> is_logged() === TRUE) {

            olcom_is_allowed_module_helper($this -> uri -> uri_string());

            $this -> is_logged = TRUE;
        } else {

            if ($this -> input -> is_ajax_request()) {
                if (preg_match('/data/', $this -> uri -> uri_string()) === 1) {
                    echo "{\"sEcho\":1,\"iTotalRecords\":0,\"iTotalDisplayRecords\":10,\"aaData\":{}}";
                }
            } else {
                redirect('authentication/login', 'refresh');
            }

            $this -> is_logged = FALSE;

        }
        if (!defined('SUCCESS_CODE'))
            define('SUCCESS_CODE', $this -> config -> item('success_code'));

        $this -> load -> model('reports_model');
        $this -> load -> model('finance_model');
        $this -> load -> config('olcom_messages');
		$this -> load -> config('olcom_help_text' );
        $this -> load -> model('patients_model');
        $this -> load -> model('prescriptions_model');
        $this -> load -> model('hospitalizations_model');
        $this -> load -> model('laboratory_model');
        $this -> load -> model('medicaments_model');
        $this -> load -> config('olcom_labels');
        $this -> load -> config('olcomhms_return_codes');
        $this -> return_codes = $this -> config -> item('return_codes');
        $this -> messages = $this -> config -> item('messages');
        $this -> labels = $this -> config -> item('labels');
		$this -> help = $this -> config ->item('help_text' );
        $this -> limited_content = $this -> load -> view('demo', '', TRUE);
    }
	function index()
	{
			$modules = $this -> olcomaccessengine -> load_modules( TRUE );
			//print_r( $modules[  $this -> uri -> segment( 1)] );return;
			$sub_modules = $modules[  $this -> uri -> segment( 1)];
			$this -> load -> config( 'olcomhms_modules_configs' );
			$modules_configs = $this -> config -> item('modules_configs'  );
			//print_r( $modules_configs[ $sub_modules[ 44 ]] );
			
		 
			$launchers = array();
			foreach ($sub_modules as $key => $sub_module) {
				$launchers[ $sub_module ] = $modules_configs[ $sub_module  ];
			}
			
			$data[ 'header' ] = $modules_configs[  $this -> uri -> segment( 1 ) ][ 'menu_text' ];
			$data[ 'launchers' ] = $launchers;
			
			$dashboard = $this -> load -> view( 'dashboard_icons' , $data , TRUE );
			
			
			 $this -> load -> view('main_template',array(
                'template'  => array('content'  => $dashboard,'page_specific_scripts'  => '')
                
            ));
	} 
    /*
     *
     * Top Medicament Used
     * -- Criteria
     *         Based on date
     *         Display Limit
     */
    function top_medicaments() {

        // prepare criteria form
        $this -> load -> library('OlcomHmsTemplateForm', array('with_submit_reset' => TRUE, 'title' => $this -> labels['top_medicaments']));

        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'start_date', 'label' => $this -> labels['start_date'], 'readonly' => '', 'class' => 'start_date', 'value' => '2013-10-27'));
        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'end_date', 'label' => $this -> labels['end_date'], 'readonly' => '', 'class' => 'end_date', 'value' => date('Y-m-d')));
        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'limit', 'label' => $this -> labels['limit'], 'value' => 10));

        $criteria_form = $this -> olcomhmstemplateform -> create_form();
        $criteria_specific_scripts = $this -> load -> view('criteria_form_specific_scripts', '', TRUE);

        $this -> load -> view('main_template', array('template' => array('content' => $criteria_form, 'page_specific_scripts' => $criteria_specific_scripts)));

    }

    /*
     *
     *
     *
     *
     */
    function top_medicaments_data() {

        $form_data = $this -> input -> post(NULL);

        $prescriptions = $this -> reports_model -> get_prescriptions($form_data['start_date'], $form_data['end_date'], $form_data['limit']);

        if ($prescriptions != NULL) {

            // load px model
            $this -> load -> model('prescriptions_model');
            $this -> load -> model('medicaments_model');
            // for the sake of this day 19 Feb 2014 22:22 location : T4 Dar es salaam institute of technology
            // 2023 b the best of luck
            /*
             * Interested in medicament, form,unit, medicament amount , number of times, quantity
             *
             */

            $medicaments = array();
            $medicaments_forms = array();
            $medicaments_units = array();

            $signature_quantity = array();
            $signature_num_times = array();
            foreach ($prescriptions as $prescription) {

                $px = $this -> prescriptions_model -> get_prescription_info($prescription['prescription_id']);

                foreach ($px[ 'lines' ] as $line) {
                    if (!key_exists($line['medicament_id'], $medicaments)) {
                        $result_medicament = $this -> medicaments_model -> get_medicament_info($line['medicament_id']);

                        $medicaments[$line['medicament_id']] = array();
                    }

                    if (!key_exists($line['dose'], $medicaments[$line['medicament_id']])) {
                        // create it
                        $medicaments[$line['medicament_id']][$line['dose']] = array();
                    } else {
                        // move on
                    }

                    if (!key_exists($line['unit_id'], $medicaments[$line['medicament_id']][$line['dose']])) {
                        $medicaments[$line['medicament_id']][$line['dose']][$line['unit_id']] = array();
                    } else {

                    }

                    if (!key_exists($line['form_id'], $medicaments[$line['medicament_id']][$line['dose']][$line['unit_id']])) {
                        $medicaments[$line['medicament_id']][$line['dose']][$line['unit_id']][$line['form_id']] = array('num_times' => 0, 'quantity' => 0);
                    }

                    $signature = $line['medicament_id'] . '-' . $line['dose'] . '-' . $line['unit_id'] . '-' . $line['form_id'];

                    if (!key_exists($signature, $signature_quantity)) {
                        $signature_quantity[$signature] = 0;
                    }

                    $signature_quantity[$signature] += $line['quantity'];

                    if (!key_exists($signature, $signature_num_times)) {
                        $signature_num_times[$signature] = 0;
                    }

                    $signature_num_times[$signature] += 1;
                }
            }

            if (count($signature_quantity) > 0) {
                arsort($signature_quantity);
                // get the signatures
                $signatures = array_keys($signature_quantity);
                $report = array();
                $total_sales = 0;

                foreach ($signatures as $key => $signature) {
                    $signature = explode('-', $signature);

                    $result_medicament = $this -> medicaments_model -> get_medicament_info($signature[0]);

                    $result_unit = $this -> medicaments_model -> get_units($signature[2]);

                    $result_form = $this -> medicaments_model -> get_forms($signature[3]);

                    $report[$key]['medicament'] = strtoupper($result_medicament['active_component']) . ' ' . $signature[1] . ' ' . $result_unit['unit'] . ' ' . $result_form['form'];

                    $signature = implode('-', $signature);
                    $report[$key]['num_times'] = $signature_num_times[$signature];

                    $report[$key]['quantity'] = $signature_quantity[$signature];

                    // get stock info
                    $result_stock = $this -> finance_model -> get_products(NULL, $result_medicament['medicament_id'], NULL, $result_unit['unit_id']);
                	
					if( !empty( $result_stock  ) ){
						$result_stock = $result_stock[0];
	                    // get price info
	                    $result_price = $this -> finance_model -> get_price($result_stock['price_id']);

                    	$report[$key]['sales'] = ($signature_quantity[$signature] * $result_price['selling_price_per_unit']);

                    	$total_sales += $report[$key]['sales'];	
						  $report[$key]['sales'] = olcom_money_format($report[$key]['sales']);
					}
				    
                  

                }

                $total_quantity = 0;
                foreach ($report as $info) {
                    $categories[] = $info['medicament'];
                    $column_series[] = array('name' => $info['medicament'], 'data' => array($info['quantity']));
                    $total_quantity += $info['quantity'];
                }

                foreach ($report as $info) {
                    $pie_series[] = array($info['medicament'], round($info['quantity'] / $total_quantity, 4));
                }

                $table = $this -> load -> view('template_table', array('table_content' => $report, 'footer' => array('Total' => olcom_money_format($total_sales))), TRUE);

                $subtitle = 'From : ' . $form_data['start_date'] . ' To : ' . $form_data['end_date'];
                $column_data = array('categories' => json_encode($categories), 'series' => json_encode($column_series), 'subtitle' => $subtitle);

                $column = $this -> load -> view('column_view', $column_data, TRUE);

                $pie_data = array('subtitle' => $subtitle, 'series' => json_encode($pie_series));
                $pie = $this -> load -> view('pie_view', '', TRUE);
                $pie_scripts = $this -> load -> view('pie_scripts', $pie_data, TRUE);
                echo json_encode(array('content' => $table . $column . $pie, 'specific_scripts' => $this -> load -> view('column_scripts', $column_data, TRUE) . $pie_scripts));
            }

        } else {
            echo json_encode( array( 'content' => 'NO_DATA' ));
        }
    }

    /*
     *
     *
     *
     */
    function top_diseases() {
         // prepare criteria form
        $this -> load -> library('OlcomHmsTemplateForm', array('with_submit_reset' => TRUE, 
        'title' => $this -> labels['top_diseases']));

        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'start_date', 'label' => $this -> labels['start_date'], 'readonly' => '', 'class' => 'start_date', 'value' => '2013-10-27'));
        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'end_date', 'label' => $this -> labels['end_date'], 'readonly' => '', 'class' => 'end_date', 'value' => date('Y-m-d')));
        $this -> olcomhmstemplateform -> add_field('text', array('name' => 'limit', 'label' => $this -> labels['limit'], 'value' => 10 ));

        $criteria_form = $this -> olcomhmstemplateform -> create_form();
        $criteria_specific_scripts = $this -> load -> view('top_diseases_specific_scripts', '', TRUE);

        $this -> load -> view('main_template', array('template' => array('content' => $criteria_form, 'page_specific_scripts' => $criteria_specific_scripts)));
        
    }

    function top_diseases_data()
    {
        
        $form_data = $this -> input -> post( NULL );
        
        $form_data[ 'end_date' ] = explode( '-' , $form_data[ 'end_date' ] );
        $form_data[ 'end_date' ][2] +=1;
        $form_data[ 'end_date' ] = implode( '-' , $form_data[ 'end_date' ] );
        
        $result_diseases = $this -> prescriptions_model -> get_top_diseases( $form_data[ 'start_date' ] , $form_data[ 'end_date' ] , $form_data[ 'limit' ] );
        
        $diseases = array();
        
        if( $result_diseases == NULL )
        {
            echo json_encode( array( 'content' => 'NO_DATA' ));
            return;   
        }
        
        foreach( $result_diseases as $key => $disease )
        {
            
            $result_disease = $this -> prescriptions_model -> get_diseases( $disease[ 'disease_id' ] );
            
            $diseases[ ] = array( 'disease_name' => $result_disease[ 'disease_name' ] , 'cases' => $disease[ 'count' ] );
            
        }
        
        $html = '<h4>Top Disease Cases Report</h4>';
        
        $html  .= $this -> load -> view( 'template_table' , array( 'table_content' => $diseases ) , TRUE );
        
        
        $total_cases = 0;
        foreach( $diseases as $disease )
        {
            $total_cases += $disease[ 'cases' ];
         
        }
        
        $pie_shares = array();
        foreach( $diseases as $disease )
        {
         
            $pie_shares[] = array( $disease[ 'disease_name' ] , round( ($disease[ 'cases' ]/$total_cases) ,2 )*100 );
        } 
        $pie_data = array('subtitle' => 'Cases Relation', 'series' => json_encode( $pie_shares ));
        $pie = $this -> load -> view('pie_view', '', TRUE);
        $pie_scripts = $this -> load -> view('pie2_scripts', $pie_data, TRUE);
        
        echo json_encode( array( 'content' => $html.$pie , 'specific_scripts' => $pie_scripts ));
    }

    /*
     *
     *
     *
     */
    function patient_history() {
        $this -> load -> library('OlcomHmsTemplateForm', array('with_submit_reset' => TRUE, 'title' => $this -> labels['patient_history']));

        $this -> olcomhmstemplateform -> add_field('search', array('name' => 'patient_id', 'label' => $this -> labels['patient_id'], 'id' => 'patient_id','placeholder' => 'Type Patient ID eg. P013'));
        $criteria_form = $this -> olcomhmstemplateform -> create_form();
        $criteria_specific_scripts = $this -> load -> view('patient_history_specific_scripts', '', TRUE);

        $this -> load -> view('main_template', array('template' => array('content' => $criteria_form, 'page_specific_scripts' => $criteria_specific_scripts)));
    }

    /*
     *
     *
     */
    function patient_history_data() {
        $form_data = $this -> input -> post(NULL);
        $patient_id = olcom_extract_numbers($form_data['patient_id']);

        $html = "<a class = 'pull-right' id = 'print' href='" . site_url('reports/print_history/' . $patient_id) . "'><i class = 'icon-print'></i>Print</a><br>";
        $prescriptions = $this -> get_history_data($patient_id);

        if( $prescriptions == NULL )
        {
            echo json_encode( array( 'content' => "<label class = 'label-warnig'>No Records</label>", 'specific_scripts' => '' ));
            return;
        }
        foreach ($prescriptions as $key => $px) {

            //print_r( $px );
            $html .= $this -> load -> view('template_table', array('table_content' => array($px)), TRUE) . '<br><hr>';
        }

        echo json_encode(array('content' => $html, 'specific_scripts' => ''));

    }

    /*
     *
     *
     */

    function print_history() {
        $patient_id = $this -> uri -> segment(3);
        $patient = $this -> patients_model -> get_personnel_info($patient_id);
        
        $this -> load -> helper(array('dompdf', 'file'));
        $prescriptions = $this -> get_history_data($patient_id);
        $html = '';
        foreach ($prescriptions as $key => $px) {
            $html .= $this -> load -> view('print_template_table', array('table_content' => array($px)), TRUE) . '<br><hr>';
        }

         $data = $patient;
         $data = array_merge($data ,array( 'content' => $html , 
            'patient_id' => olcomhms_create_id( $patient_id, FALSE, TRUE ),
            'creation_date' => date( 'Y-m-d H:i:s')
         ) );

        $html  = $this -> load -> view( 'patient_history_template' , $data, TRUE );
        pdf_create($html, $patient_id . '-History' );
    }

    /*
     *
     *
     *
     */

    function get_history_data($patient_id) {

        $patient = $this -> patients_model -> get_personnel_info($patient_id);

        $result_prescriptions = $this -> prescriptions_model -> get_patient_prescriptions($patient_id);
        
        $this -> load -> model ( 'employees' );
        
        
        $prescriptions = array();
        $html = '';
        if ($result_prescriptions != NULL) {

            foreach ($result_prescriptions as $key => $prescription) {
                
                $result = $this -> prescriptions_model -> get_prescription_info($prescription);
                unset($result['patient_id']);
                $employee = $this -> employees -> info( $result[ 'employee_id' ] , 'full');
                
                $result[ 'employee_id' ] = 'Dr.'. $employee[ 'firstname' ].' ' .$employee[ 'lastname' ];
				 
                $result_diagnosis = $this -> prescriptions_model -> get_prescription_diagnosis($prescription );

                //print_r( $result_diagnosis );
                if ($result_diagnosis != NULL) {

                    $result['indication'] = $result_diagnosis['disease_name'];

                    $lines = $result['lines'];
                    unset($result['lines']);
                    $result['lines'] = $lines;
                }
                
                if( is_array( $result[ 'lines' ] ))
                foreach ($result[ 'lines' ] as $key2 => $line) {
                    unset($result['lines'][$key2]['medicament_id']);

                    $result_unit = $this -> medicaments_model -> get_units($line['unit_id']);
                    $result['lines'][$key2]['unit_id'] = $result_unit['unit'];

                    $result_route = $this -> medicaments_model -> get_routes($line['route_id']);
                    $result['lines'][$key2]['route_id'] = $result_route['route'];

                    $result_dosage = $this -> medicaments_model -> get_dosage($line['frequency_id']);

                    $result['lines'][$key2]['frequency_id'] = $result_dosage['dosage'];

                    $result_form = $this -> medicaments_model -> get_forms($line['form_id']);
                    $result['lines'][$key2]['form_id'] = $result_form['form'];
                    
                    unset( $result[ 'lines'][ $key2 ][ 'comments' ] );
                    unset( $result[ 'lines'][ $key2 ][ 'review_date' ] );
                    unset( $result[ 'lines'][ $key2 ][ 'admin_hours' ] );
                    
                    unset( $result[ 'lines'][ $key2 ][ 'allow_substitution']  );
                    unset( $result[ 'lines'][ $key2 ][ 'units']  );
                }
                $prescriptions[] = $result;
            }
            return $prescriptions;
        }
        return NULL;
    }

    /*
     *
     *
     *
     */
    function laboratory_report() {
        $this -> load -> view('main_template', array('template' => array('content' => $this -> limited_content)));
    }

    /*
     *
     *
     *
     */
    function staff_reports() {
        $this -> load -> view('main_template', array('template' => array('content' => $this -> limited_content)));
    }

    /*
     *
     *
     *
     */
    function billing_reports() {
        $this -> load -> view('main_template', array('template' => array('content' => $this -> limited_content)));
    }

    /*
     *
     *
     *
     */
    function hospitalization_report() {
        $this -> load -> view('main_template', array('template' => array('content' => $this -> limited_content)));
    }

    /*
     *
     *
     *
     */
    function asset_status_reports() {
        $this -> load -> view('main_template', array('template' => array('content' => $this -> limited_content)));
    }

}
