<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>

<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>  
<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js" ></script>
<script>
	
	$( function (){
	$('#start_date').datepicker( {
	        format: 'yyyy-m-d',startDate:'<?php echo date('Y-m-d'); ?>' 
	    }).css( 'z-index' , 999999 );
	$('#end_date').datepicker( {
            format: 'yyyy-m-d',startDate:'<?php echo date('Y-m-d'); ?>' 
        }).css( 'z-index' , 999999 );;
        
     $('#start_time').timepicker({
                    minuteStep: 15,
                    showSeconds: false,
                     showInputs: false,
                    showMeridian: false
       });
    $('#end_time').timepicker({
                    minuteStep: 15,
                    showSeconds: false,
                     showInputs: false,
                    showMeridian: false
            });

	hide_spinner();
	
	$( '#patient_auto_complete').keypress( function ( e ) {
	   if( e.which == 13 )
	   {
	       patient_auto_complete ();
	   } 
	});
	
	$('#olcom-search-patient_id').on('click' ,function (e)
        {
           
          patient_auto_complete ();
          e.preventDefault();
          return false;
        });   
        
        function patient_auto_complete()
        {
             $.ajax({
                url: '<?php echo site_url('appointments/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
                dataType: 'json',
                method: 'get',
                success:function (data)
                {
                    
                    if( data.result == 1)
                    {
                         $('#patient_auto_complete').val( data.value ).attr('value',data.key);
                    }
                    else
                    {
                         $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null);
                    }
                   
                }
            });
        }
        <?php echo $this -> load -> view('jquery_ajax',array('data'=>
        array('link' => 'appointments/new_appointment')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                start_date: {
                    required: true
                    } ,
               end_date: {
                    required: true
                    } 
                    ,
               start_time: {
                    required: true
                    } ,
               end_time: {
                    required: true
                    } 
                ,
                patient_id : {
                    required : true
                 }
                },
         
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        });
        
        $( '#olcom-confirm-edit' ).on( 'click' ,function(){
            if( $( '#olcomhms-template-form').valid( ) )
            {
                 var form_data =  $( 'form' ) .serialize();
                 $.ajax( {
                    url : '<?php echo site_url( 'appointments/new_appointment') ; ?>',
                    dataType : 'json',
                    method : 'post' ,
                    data : form_data,
                    success : function ( data )
                    {
                         if( data.success != undefined ){
                             if( data.success == 0 )
                                bootbox .alert( data.message );
                           }
                        console.log( data );
                    } 
                 });
            }
           $('#modal-form-dialog').modal( 'hide' );
        }) 
    }); 
</script>