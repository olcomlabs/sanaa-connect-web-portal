<!-- olcom js -->



<script>
    $(function(){ 
        // preload contents
        
        
        $( '#category_id' ).on( 'change' , function () {
            
             load_equipments();
            
        });
        
        function load_equipments()
        {
              $.ajax({
                url:'<?php echo site_url('laboratory/category_assets_data'); ?>/'+$( '#category_id' ).val(),
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){
                        var serial_number = $( '#serial_number' );
                        
                        serial_number .html( '' );
                        //insert options
                        $.each(data,function(key,value){
                            $( serial_number ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( serial_number ).trigger("liszt:updated");
                    }
                }
            }); 
        }
        
    });
    
</script>