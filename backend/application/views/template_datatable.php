<?php
if (!defined('BASEPATH'))
	exit('Direct access is restriced');
 
?>
<div class="row-fluid"> 
    <div class="widget-header widget-header-flat">
        <h4 class="smaller"><?php echo $info_view['header']; ?> 	&nbsp;&nbsp; 
        	</h4>
        
        <?php  if( isset( $info_view[ 'create_controller_fx' ] ) AND $info_view[ 'create_controller_fx' ] != NULL ): ?>
        
        <div class="widget-toolbar">
        
					&nbsp;&nbsp;
            <a id="olcom-inflated-create-btn" " class="btn btn-mini btn-success" href="<?php echo site_url( $info_view[ 'create_controller_fx' ] ); ?>">
                 <label style="padding-left: 5px;padding-right: 4px;padding-bottom: 3px;"> Create <i class="icon-plus-sign-alt  icon-on-right"></i></label>
                   
            </a>
        </div>
        <?php endif; ?>
    </div>
    <form id='datatable'>
	<table id="hms-datatable<?php echo isset( $info_view[ 'id_suffix' ] ) ? $info_view[ 'id_suffix' ] : NULL ; ?>" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th class="" role="columnheader" rowspan="1" colspan="1" aria-label="">
					<label>
						<input type="checkbox">
							<span class="lbl"></span>
					</label>
				</th>
				<?php
					//datatable columns 
					if(isset($info_view['columns']) && $info_view['columns']!=NULL){
					$i = 0;
						foreach($info_view['columns'] as $column){
							if( $i != 0 )
								echo "<th>".$column."</th>";
							$i++;
						}
					}
				?>
				<!-- the last column is constant ,for actions only -->
				<?php if($info_view['with_actions']==TRUE):?>
					<th class="sorting_disabled"  rowspan="1" colspan="1" aria-label="" style="width: 100px;">Actions</th>
				<?php endif; ?>
			</tr>
		</thead>
	</table>
	<div class = 'row-fluid' style='padding-top: 10px'>
		<span class ='lbl'>With Selected </span>
		<button class="btn btn-small btn-danger" disabled="disabled" id = 'enable-delete'>
			<i class="icon-trash"></i> Delete
		</button>
	</div>
	
	</form>
</div>
<!-- embed modal dialog view for actions -->
<div id='olcom-modal-dialog' style="display: none;">
	
</div>
