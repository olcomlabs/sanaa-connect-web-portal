 <?php
if (!defined('BASEPATH'))
	exit('Direct accces is restricted');
?>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
<script>
	
	$(function(){
    $('.slimScrollPx').slimScroll({
        height : '300px',
        railVisible: true,
    	alwaysVisible: true
    });
    
    load_medications();
    var px_limit = 0;
    function load_medications( ){
    	
    	$.ajax({
    		url : "<?php echo site_url('patients/get_medications_data/'.$this -> uri -> segment( 3 )); ?>/"+ px_limit,
    		dataType : 'json',
    		method : 'get',
    		success : function( data ){
    			
    			if( data.length > 0 ){
    				$.each( data ,function ( key, value ) {
    					console.log( value );
						$("#medication-list").append( "<div class='itemdiv commentdiv'><div class='body'><div class='name blue'>"+
							value.indication+"</div><div class='time'><span class='green'>"+
							value.date +"</span></div><div class='text' ><p>"+
							value.medicament +"</p></div></div></div> ");    					
    				});	
    			}
    			
    			px_limit += 1;
    		}
    	});
    }
    
    $( '#load-more-px' ).on( 'click' , function(){
    	
    		load_medications();	
    	
    	return false;
    });
    
});
</script>