<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>



<script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>

<script>

 $(function (){ 
        
        hide_spinner();

        $( '#cost' ).autoNumeric( 'init' );
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'finance/create_lab_service')),TRUE); ?>
        
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
               
                    selling_price_per_unit:{
                    required: true,
                    maxlength:16
                    },
                    new_selling_price:{
                    required: true,
                    maxlength:16
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        });
</script>