<?php if ( ! defined('BASEPATH'))  exit('Direct access is restricted'); ?>
<div class="row-fluid">
    <div class="widget-header widget-header-flat">
        <h4 class="smaller"><?php echo $info_view['header']; ?></h4>

        <div class="widget-toolbar">
            <button class="btn btn-mini btn-danger" id="olcom-inflated-create-btn">
                  Create
                   <i class="icon-plus-sign-alt  icon-on-right"></i>
            </button>
        </div>
    </div>
    <table id="hms-datatable" class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
        <thead>
            <tr>
                <?php
                
                    //datatable columns 
                    if(isset($info_view['columns']) && $info_view['columns']!=NULL){
                        foreach($info_view['columns'] as $column){
                            echo "<th>".$column."</th>";
                        }
                    }
                ?>
                <!-- the last column is constant ,for actions only -->
                <?php if($info_view['with_actions'] == TRUE):?>
                    <th class="sorting_disabled"  rowspan="1" colspan="1" aria-label="" style="width: 147px;">Actions</th>
                <?php endif; ?>
            </tr>
        </tbody>
    </table>
</div>
<div id='olcom-modal-dialog' style="display: none;">
    
</div>
 