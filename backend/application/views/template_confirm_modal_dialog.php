<div  id='modal-message' class="modal show fade" data-backdrop='static' data-keyboard='false' tabindex="-1">
	<div class="modal-header no-padding">
		<div class="olcom-dialog-<?php echo $modal_view['dialog_type']; ?> olcom-dialog">
		
			<?php echo $modal_view['header']; ?>
		</div>
	</div>

	<div class="modal-body">
		<div class="row-fluid">
		
					<?php

					if (isset($modal_view['message']) && $modal_view['message'] != "") {
				
						echo $modal_view['message'];

					}
					?>
		</div>
	</div>

	<div class="modal-footer">
		<button class="btn btn-small btn-success pull-left" data-dismiss="modal" 
			id='olcom-confirm-yes'>
			<i class="icon-check"></i>
			Yes
		</button>
			<button class="btn btn-small btn-danger pull-left" data-dismiss="modal" id='olcom-confirm-no'>
			<i class="icon-remove"></i>
			No
		</button>
	</div>
</div>