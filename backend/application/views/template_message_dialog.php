<div  id='modal-message' class="modal show fade" tabindex="-1">
	<div class="modal-header no-padding">
		<div class="olcom-dialog olcom-dialog-<?php echo $modal_view['dialog_type']; ?>">
			<button type="button" class="close" data-dismiss="modal">
				&times;
			</button>
			<?php echo $modal_view['header']; ?>
		</div>
	</div>

	<div class="modal-body">
		<div class="row-fluid">
		
					<?php

					if (isset($modal_view['message']) && $modal_view['message'] != "") {
				
						echo $modal_view['message'];

					}
					?>
		</div>
	</div>

	<div class="modal-footer">
	    <?php
	       if( isset( $modal_view[ 'print_link' ] ) AND $modal_view[ 'print_link' ] != NULL )
           {
            ?>
        	    <button class = 'btn btn-small btn-success pull-right'>
        	        <i class = 'icon-print'></i>
        	        Print 
        	    </button>
        <?php 
           }
        ?>
		<button class="btn btn-small btn-danger pull-left" data-dismiss="modal">
			<i class="icon-remove"></i>
			Close
		</button>
	</div>
</div>