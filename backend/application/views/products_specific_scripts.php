<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>


<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script>

 $(function (){ 
        
        hide_spinner();

        $( '#expiry_date' ).datepicker( { format : 'yyyy-m-d'});
        
        $( '#medicament' ).autocomplete({
            serviceUrl:'<?php echo site_url('prescriptions_mngt/medicament_data'); ?>',
            onSelect: function (suggestion){
            $( this ).attr( 'value' , suggestion.data );
            
            }
        });
        
        
        $( '#ppp' ).autoNumeric( 'init' );
        $( '#sppp' ).autoNumeric( 'init' );
        $( '#ppu' ).autoNumeric( 'init' );
        $( '#sppu' ).autoNumeric( 'init' );
        $( '#sppu' ).autoNumeric( 'init' );
        
        $( '#ppp').on( 'keyup' , function(){
           calc_price();
        });
        
        $( '#sppp' ).on( 'keyup' ,function (){
           calc_price(); 
        });
        
        $( '#nop' ).on( 'keyup' , function(){
           calc_price(); 
           
        });
        $( '#upp').on( 'keyup' , function (){
           calc_price(); 
        });
        
        function calc_price( )
        {
            var upp = $( '#upp' ).val();
            var ppp = $( '#ppp').val().replace(',','');
            var nop = $( '#nop' ).val(); 
            var ppu = $( '#ppu' );
            var sppu = $( '#sppu');
        
            var aq = $( '#alertq'); 
            nop = nop*1;
            ppp = ppp*1;
            upp = upp*1;  
            
            aq.val( upp );
            
            var up = Math.ceil(((ppp)/upp));
            ppu.val(up )
            ppu.autoNumeric( 'update' , {});
            
            /*var sup = Math.ceil((( sppp/upp)))
            sppu.val( sup ); 
            sppu.autoNumeric( 'update' , {});
            */
            
        }
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'medicaments/create_product')),TRUE); ?>
        
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                medicament_id: {
                    required: true
                    },
                medicament_amount: {
                    //min:0,
                    required: true,
                    number : true,
                    maxlength:10
                    },
                number_of_packages:{
                    //min:0,
                    required: true,
                    digits : true,
                    maxlength:10
                    },
                unit_per_package: {
                    //min:0,
                    required: true,
                    digits:true,
                    maxlength:10
                    },
                price_per_package: {
                    //min:0,
                    required: true,
                     number : true,
                    maxlength:16
                    },
                selling_price_per_package: {
                    
                    required: true,
                     number : true,
                    maxlength:16
                    },
               price_per_unit: {
                   //min:0,
                    required: true, 
                     number : true,
                    maxlength:16
                    },
               selling_price_per_unit: {
                   //min:0,
                    required: true,
                      number : true,
                    maxlength:16
                    },
                 credit_selling_price_per_unit: {
                   //min:0,
                    required: true,
                      number : true,
                    maxlength:16
                    },
                  credit_selling_price_per_package: {
                   //min:0,
                    required: true,
                      number : true,
                    maxlength:16
                    },
               units_per_package: {
                   //min:0,
                    required: true,
                    digits:true,
                    maxlength:16
                    },
                 expiry_date : {
                     required : true
                 },
                  form_id : {
                     required : true
                 },
                 alert_quantity : {
                     digits : true,
                     required: true
                 },
                 type : {
                     required: true
                 },
                 number : {
                 	required: true
                 }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        });
</script>