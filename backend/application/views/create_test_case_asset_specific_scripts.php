<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        $('#btn-submit').click(function (){
            if($('#olcomhms-template-form').valid()==true){
                show_spinner();
                var form_data = $('form').serialize();
                form_data = String( form_data );
                form_data = form_data +'&serial_numbers'+'='+$( '#serial_number' ).val();
                $.ajax({
                        url:"<?php echo site_url( 'laboratory/create_test_case_assets' ); ?>",
                        data:form_data,
                        dataType:'json',
                        method:'get',
                        success:function(data){
                            form_submit_message(data);
                            hide_spinner();
                            $('#olcomhms-template-form')[0].reset();
                    }
                });
               }
        }); 
        
        
        // preload contents
        load_equipments();
        
        $( '#category_id' ).on( 'change' , function () {
            
             load_equipments();
            
        });
        
        function load_equipments()
        {
              $.ajax({
                url:'<?php echo site_url('laboratory/category_assets_data'); ?>/'+$( '#category_id' ).val(),
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){
                        var serial_number = $( '#serial_number' );
                      
                        //insert options
                        $.each(data,function(key,value){
                            $( serial_number ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( serial_number ).trigger("liszt:updated");
                        }
                    }
                });
            }
            
            load_cases();
            
            $( '#type_id' ).on( 'change' , function () {
                
                 load_cases();
                
            });
            
            function load_cases()
            {
                  $.ajax({
                    url:'<?php echo site_url('laboratory/type_cases_data'); ?>/'+$( '#type_id' ).val(),
                    dataType:'json',
                    success:function (data){
                     
                        if(data.error == undefined){
                            var test_case_id = $( '#test_case_id' );
                          
                            test_case_id.html( '' );
                            //insert options
                            $.each(data,function(key,value){
                                $( test_case_id ).append($('<option></option>').attr('value',key).text(value));
                            });
                            //trigger chosen update
                            $( test_case_id ).trigger("liszt:updated");
                        }
                    }
                }); 
            }
        
         
       }); 
</script>