<?php if(!defined('BASEPATH')) exit('Direct access is restricted');?>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<!-- olcom js -->
<script src="<?php echo base_url(); ?>assets/js/olcom-js.js"></script>

<script type="text/javascript">
	
	$(function (){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'humanresources/create_position')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				job_title: {
					required: true,
					minlength: 4,
					maxlength:40,
					olcom_valid_space_name:true
				},
				min_salary:{
					required:true,
					digits:true,
					minlength:3,
					maxlength:10
				},
				max_salary:{
					required:true,
					
					digits:true,
					minlength:3,
					maxlength:10
				},
					
				department_id:{
						required:true,
						olcom_valid_select:true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
	});
</script>