		$('#btn-submit').click(function (){
			if($('#olcomhms-template-form').valid()==true){
				show_spinner();
				var form_data=$('form').serialize();
				$.ajax({
					url:"<?php echo site_url($data['link']); ?>",
					data:form_data,
					dataType:'json',
					method:'post',
					success:function(data){
						form_submit_message(data);
						hide_spinner();
						
						if( data.success == 1 ){
							window.location.reload( false );
							$('#olcomhms-template-form')[0].reset();
						}
					}
						
				});
			}
		});
		$( '.help-button' ).popover();
		
		$( '#btn-reset' ).click(function(){
			$('.chzn-select').val()
		});
		
