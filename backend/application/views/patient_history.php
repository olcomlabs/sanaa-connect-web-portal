<?php
if (!defined('BASEPATH'))
	exit('Direct accces is restricted');
?>
<div class="page-header position-relative">
	<h4 class="smaller blue"> Patient History Summary </h4>
</div>

<div class = 'row-fluid'>
	<div clas = 'span12'>
		<div class="hide" style="display: block;">
			<div class="user-profile row-fluid" id="user-profile-2">
				<div class="tabbable">
					<ul class="nav nav-tabs padding-18">
						<li class="active">
							<a href="#home" data-toggle="tab"> <i class="green icon-user bigger-120"></i> General Information </a>
						</li>

						<li class="">
							<a href="#medications" data-toggle="tab"> <i class="orange icon-medkit bigger-120"></i> Medications </a>
						</li>

						<li class="">
							<a href="#hospitalizations" data-toggle="tab"> <i class="blue icon-h-sign bigger-120"></i> Hospitalizations </a>
						</li>

						<li class="">
							<a href="#laboratory" data-toggle="tab"> <i class="pink icon-search bigger-120"></i> Laboratory </a>
						</li>
					</ul>

					<div class="tab-content no-border padding-24">
						<div class="tab-pane active" id="home">
							<div class="row-fluid">
								 
								<div class="span6">
									<h4 class="blue"><span class="middle"><?php echo $general_info['firstname'] . ' ' . $general_info['middlename'] . ' ' . $general_info['lastname']; ?></span>,
										<?php echo $general_info['initial'] . '' . $general_info['patient_id']; ?></h4>
									  
									<div class="profile-user-info"> 
									 
										<div class="profile-info-row">
											<div class="profile-info-name">
												Address
											</div>

											<div class="profile-info-value">
												<i class="icon-map-marker light-orange bigger-110"></i>
												<span><?php echo $general_info['address']; ?></span>
												<span><?php echo $general_info['region']; ?></span>
											</div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name">
												Age
											</div>

											<div class="profile-info-value">
												<span><?php echo $general_info['age']; ?></span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">
												Gender
											</div>

											<div class="profile-info-value">
												<span> <?php echo $general_info['gender']; ?> </span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">
												Registration Date
											</div>

											<div class="profile-info-value">
												<span><?php echo $general_info['registration_date']; ?></span>
											</div>
										</div>

										<div class="profile-info-row">
											<div class="profile-info-name">
												Phone #
											</div>

											<div class="profile-info-value">
												<span><?php echo $general_info['phone']; ?> </span>
											</div>
										</div>
										<div class="profile-info-row">
											<div class="profile-info-name">
												N.O.Keen Phone #
											</div>

											<div class="profile-info-value">
												<span> <?php echo $general_info['nok_phone']; ?></span>
											</div>
										</div>
									</div>

									<div class="hr hr-8 dotted"></div>

									
								</div><!--/span-->
								<div class="span6">
									<div class="widget-box transparent">
										<div class="widget-header widget-header-small">
											<h4 class="smaller"><i class="icon-group bigger-110"></i> Family History </h4>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<p>
													<?php echo $general_info['family_history']; ?>
												</p>
											
											</div>
										</div>
									</div>
								</div>
							</div><!--/row-fluid-->

							<div class="space-20"></div>

							<div class="row-fluid">
								<div class="span6">
									<div class="widget-box transparent">
										<div class="widget-header widget-header-small">
											<h4 class="smaller"><i class="icon-info bigger-110"></i> Allergies & Other Critical Info</h4>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<p>
													<?php echo $general_info['allegies_critical_info']; ?>
												</p>
												 
											</div>
										</div>
									</div>
								</div>

								
							</div>
						</div><!--#home-->

						<div class="tab-pane" id="medications">
							<div class="profile-feed row-fluid">
								<div class="span12">
									<div class="slimScrollPx">
										<div class="comments" id = 'medication-list' >
											 	 
										</div>
										  
									</div>
									<div class="hr hr8"></div>
										<div class="center">
												<a href="#" id = "load-more-px">
													Load more
													<i class="icon-arrow-right"></i>
												</a>
											</div>
								</div><!--/span-->
 
							</div><!--/row-->

							<div class="space-12"></div>
 
						</div><!--/#feed-->

						<div class="tab-pane" id="hospitalizations">
							<div class="profile-users clearfix">
								
							</div>

							<div class="hr hr10 hr-double"></div>

							<ul class="pager pull-right">
								<li class="previous disabled">
									<a href="#">← Prev</a>
								</li>

								<li class="next">
									<a href="#">Next →</a>
								</li>
							</ul>
						</div><!--/#hospitalizations-->

						<div class="tab-pane" id="laboratory">
							
						</div><!--/#laboratory-->
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
