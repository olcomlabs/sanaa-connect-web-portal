<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_asset_status')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                status: {
                    required: true,
                    minlength: 4,
                    maxlength:40,
                    olcom_valid_space_name : true
                }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
        
    });
    
</script>