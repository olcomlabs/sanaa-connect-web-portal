<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>

<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){
	  
		
		hide_spinner();
		
	    $('#olcom-search-patient').on('click' ,function (e)
	    {
	      patient_auto_complete();
	      e.preventDefault();
	      return false;
	    });
	       
	    $( '#indication' ).autocomplete({
            serviceUrl:'<?php echo site_url('prescriptions_mngt/indication_data'); ?>',
            onSelect: function (suggestion){
            $( this ).attr( 'value' , suggestion.data );
            }
        });
	    /*
	     * 
	     * 
	     * 
	     * 
	     */
	   function patient_auto_complete()
	   {
	        $.ajax({
                url: '<?php echo site_url('prescriptions_mngt/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
                dataType: 'json',
                method: 'get',
                success:function (data)
                {
                    
                    if( data.result == 1)
                    {
                         $('#patient_auto_complete').val( data.value ).attr('value',data.key);
                    }
                    else
                    {
                         $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null);
                    }
                   
                }
            });
	   }
	   /*
	    * 
	    * 
	    * 
	    */
      $( '#patient_auto_complete').keypress( function ( e ) {
           if( e.which == 13 )
           {
               patient_auto_complete ();
           } 
        });
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'prescriptions_mngt/write_diagnosis')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				patient: {
					required: true
					},
				employee_id: {
					required: true
					},
				complaints:{
					required: true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
		
	});
	
</script>
