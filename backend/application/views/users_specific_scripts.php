<script src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/olcom-js.js"></script>

<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'usermanagement/create_user')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				employee_id: {
					required: true,
					olcom_valid_name:true
					},
				username: {
					required:true,
					olcom_valid_name:true,
					minlength:4,
					maxlength:20
					},
				password:{
					required: true,
					minlength:5,
					maxlength:20
					},
				confirm_password: {
					required: true,
					equalTo:'#password'
				},
				groud_id:{
					required:true,
					olcom_valid_name:true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>