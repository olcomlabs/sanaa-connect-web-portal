<?php if( ! defined( 'BASEPATH' ) ) exit( 'Direct script access is restricted' ); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Sanaa Connect - <?php 
		  $this -> load -> config( 'olcomhms_modules_configs' );
          $modules_configs = $this -> config -> item( 'modules_configs');
          
          $controller = $this -> uri -> segment( 1 );
          if( isset( $controller ) AND $controller != NULL )
          {
              if( $controller == 'accessengine' )
              {
                if( $this -> uri -> segment( 2 ) == 'access_denied'  )
                {
                    echo 'Access Denied';
                }  
                else
                    {
                        echo '404 Not Found';
                    }
              }
                
              else {
                echo $modules_configs[ $controller ][ 'menu_text' ];  
              }
                  
          }    
            
          else {
              echo 'Dashboard';
          }
		 ?> </title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!--basic styles-->

		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.custom.min.css" />
		<!--fonts-->

		<!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" /> -->

		<!--ace styles-->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" />
		<link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/datepicker.css"/>
	    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/daterangepicker.css" />   
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.autocomplete.css" /> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.css" />
       
       <?php 
            if( isset( $template['css' ] ) AND $template[ 'css' ] != NULL )
            {
                foreach( $template[ 'css'] as $css )
                   {
                       ?>
       <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo $css; ?>.css" />
                       <?php
                   }
            }
       ?>
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo base_url(); ?><?php echo base_url(); ?>assets/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
		<link rel="stylesheet" href='<?php echo base_url(); ?>assets/css/olcom-styles.css' />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	</head>

	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a href="#" class="brand">
						<small>
							<i class="icon-leaf"></i>
							British Council - Sanaa Connect Tanzania
						</small>
					</a>

					<ul class="nav ace-nav pull-right">
						

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-bell-alt icon-animated-bell"></i>
								<span class="badge badge-important num_notifications" >0</span>
							</a>
							<ul id = 'notifications' class="pull-right dropdown-navbar navbar-green dropdown-menu dropdown-caret dropdown-closer">
								<li class="nav-header" id = 'num_notifications'>
									 
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo base_url(); ?>assets/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo strtoupper(olcom_get_logged_username()); ?>
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
								<li>
									<a href="#">
										<i class="icon-cog"></i>
										Settings
									</a>
								</li>

								<li>
									<a href="<?php echo site_url( 'account/profile'); ?>">
										<i class="icon-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo site_url('authentication/logout'); ?>">
										<i class="icon-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul><!--/.ace-nav-->
				</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>

		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<div class="sidebar" id="sidebar"> 
						<ul class="nav nav-list">
							<?php
								echo olcomhms_modules_html();
							?>	
						</ul><!--/.nav-list-->


				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>

			<div class="main-content"> 
				<div id="breadcrumbs" class="breadcrumbs">
					<?php
							$sub_modules = get_sub_modules();
							
							if( $sub_modules != NULL ) : 
							 $header = $sub_modules[ 'header' ];
							 $sub_modules = $sub_modules[ 'launchers' ];
						?>
						
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="<?php echo site_url( $this -> uri -> segment( 1 )); ?>/index"><?php echo $header; ?></a>

							<span class="divider">
								|
							</span>
						</li>
						
						<?php $index = 0; foreach( $sub_modules as $key => $sub_module ) :  ?>
						
						<li>
							<a href="<?php echo site_url( $sub_module[ 'link' ] ); ?>"> <?php echo  $sub_module[ 'menu_text' ]; ?></a>

						<?php if( $index != (count( $sub_modules ) - 1)): ?>
							<span class="divider">
								|
							</span>
						<?php endif; ?>
						</li>
						<?php $index++;  endforeach; endif;?>
					</ul>
				</div>
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
									<?php
									
									   if( $this -> olcomaccessengine -> is_logged() === TRUE){
    										if(isset($template['content']) && $template['content']!="")
    										{
    											echo $template['content'];
    										}
                                       }
                                       else
                                       {
                                          
                                       }
									?>
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
					<div id = 'extra_content' class = 'row-fluid'>
					    
					    
					</div>
				</div><!--/.page-content-->

				 
			</div><!--/.main-content-->
		

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--basic scripts-->
 
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
 
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>

		<!--ace scripts-->

		<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script> 
		<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/olcom-js.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
		<script src='<?php echo base_url(); ?>assets/js/jquery.cookie.js'></script>
		<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/date-time/daterangepicker.min.js"></script> 

		<!--inline scripts related to this page-->
		<?php
			if(isset($template['page_specific_scripts']) && $template['page_specific_scripts']!=NULL){
				echo $template['page_specific_scripts'];
			}
		?>
		
		<div id='page-specific-scripts'>
			
		</div>
		<div id = 'extra-specific-scripts'>
		    
		    
		</div>
		<script>
		    $( function (){
		    	
		        $('select').chosen();
		        
		        function notification_update( ) 
		        {
		          $.ajax({
                    url : '<?php echo site_url( 'notifications/notifications_data' ); ?>',
                    dataType : 'json' , 
                    success : function ( data )
                    {
                        $( '#notifications' ).html(  data.notifications );
                        $( '#num_notifications' ).html( "<i class='icon-warning-sign'></i>" + data.num_notifications + ' Notifications');
                        $( '.num_notifications' ).html( data.num_notifications );
                    }
                });    
		        }
				 //notification_update();
			    // setInterval( notification_update , 10000 );
		    });
		</script>
		</div><!--/.main-container-->
	</body>
</html>
