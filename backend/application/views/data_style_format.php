<?php if(!defined('BASEPATH'))exit('Direct access is restricted');?>
<?php 
/*
 * @params pre_text,post_text,label class,data,yes_no 
 * 
 */
if(isset($format['yes_no'])):?>
<span class="label label<?php echo '-';
	if($format['yes_no'] == TRUE)
		echo $format['data'] == 1?'success':'important'; ?> arrowed">
	<?php echo isset($format['pre_txt'])?$format['pre_txt']:''; ?>
	<?php 
		if($format['yes_no'] == true)
			echo $format['data'] == 1?'Yes':'No';?>
	<?php echo isset($format['post_txt'])?$format['post_txt'] : NULL; ?></span>
<?php endif; ?>

<?php if(isset($format['on_off'])):?>
<span class="label label<?php echo '-';
	if($format['on_off'] == TRUE)
		echo $format['data'] == 1?'success':'important'; ?> arrowed">
	<?php echo isset($format['pre_txt'])?$format['pre_txt']:''; ?>
	<?php 
		if($format['on_off'] == true)
			echo $format['data'] == 1 ? 'On':'Off';?>&nbsp;
	<?php echo isset($format['post_txt'])?$format['post_txt']:''; ?></span>
<?php endif;  ?>

<?php if( isset( $format[ 'id_format' ] ) )
{
    echo olcomhms_create_id( $format[ 'data' ] ,$format[ 'id_format' ] == 'employee' ? TRUE : FALSE ,
     $format[ 'id_format' ] == 'patient' ? TRUE : FALSE );
}

if( isset( $format[ 'money'] ) )
{
    echo number_format( $format[ 'data' ] ,2 );
}

if( isset( $format[ 'state' ] ) )
{
    $state = '';
    
    if( $format[ 'data' ] == 1 )
    {
        $state =  '<span class = \'label label-warning\'>Requested</span>';
    }
   if( $format[ 'data' ] == 2 )
    {
        $state =  '<span class = \'label label-success\'>  &nbsp;Posted &nbsp;&nbsp;&nbsp; </span>';
    }
    echo $state;
}

if( isset( $format[ 'stock_status' ] ) )
{
    $state =  '';
    
    if( $format[ 'data' ] == 1 )
    {
        $state =  '<span class = \'label label-warning\'>Depleting</span>';
    }
    else
    {
        $state =  '<span class = \'label label-success\'>  &nbsp;Running &nbsp;&nbsp;&nbsp; </span>';
    }
    echo $state;
}

if( isset( $format[ 'days_left' ] ) )
{
    $state = 'sd';
    
    if( $format[ 'data' ] <= 0 )
    {
        $state =  '<span class = \'label label-important\'>Expired</span>';
    }
    elseif( $format[ 'data' ] > 0 AND $format[ 'data'] <= 60  )
    {
        $state =  '<span class = \'label label-warning\'>'.$format[ 'data' ].'  Days </span>';
    }
    else {
        $state = $format[ 'data']. ' Days';    
    }
    echo $state;
}


if( isset( $format[ 'pretty_date' ] ) )
{
    echo olcom_pretty_date( explode( '-' , $format[ 'data' ] ) );
}

if( isset( $format[ 'prescription' ] ) )
{
    echo olcomhms_create_id( $format[ 'data' ] ,FALSE,FALSE,FALSE,TRUE );
}

if( isset( $format[ 'format' ] ) AND  $format[  'format' ] == 'ward_gender' )
{
     switch( $format[ 'data' ] )
     {
         case 1 :
             echo 'Men Ward';
         break;
         case 2: 
             echo 'Women Ward';
         break;
         case 3:
             echo 'Unisex Ward';
         break;
     }
}
if( isset( $format[ 'bed_status' ] ) )
{
    switch( $format[ 'data' ] )
    {
        case 1 : 
            echo  '<span class = \'label label-success\'>  &nbsp;Free &nbsp;&nbsp;&nbsp; </span>';
          break;
        case 2 :
            echo  '<span class = \'label label-danger\'>  &nbsp;Occupied &nbsp;&nbsp;&nbsp; </span>';
           break;
    }
}

if( isset( $format[ 'admission_type' ] ) ) 
{
    $admission_type = '';
    switch( $format[ 'data' ] )
    {
        case 1 : 
            $admission_type = 'Elective' ;
         break;
         
        case 2 : 
            $admission_type = 'Emergency';
          break;
        case 3: 
            $admission_type = 'Maternity';
          break;
        case 4 : 
            $admission_type = 'Routine';
          break;
        case 5 : 
             $admission_type = 'Urgent';
          break;
    }
    echo $admission_type;
}

if( isset( $format[ 'admission_status' ] ) )
{
    $status = '';
    switch( $format[ 'data' ] )
    {
        case 1 :
                $status = 'Hospitalized';
            break;
        case 2: 
               $status = 'Discharged';
            break;
    }
    echo $status;
 
}
if( isset( $format[ 'bill_type' ] ) )
{
    $bill_type = '';
    switch( $format[ 'data' ] )
    {
        case 1: $bill_type = 'CASH SALE';break;
        case 2: $bill_type = 'PROFOMA INVOICE';break;
    }
    echo $bill_type;
}
if( isset( $format[ 'quantity_status' ] ) ){
	$quantity = '';
	$pieces = explode('/', $format[ 'data' ] );
	 
	$state =  '';
    if(  $pieces[ 3 ] < 0 == 1 OR $pieces[ 0 ] == 1)
    {
        $state =  '<span class = \'label label-important\'>Discarded</span>';
    }
    else if( $pieces[ 2 ]  > $pieces[ 1 ]   == 1 )
    {
    	if( $pieces[ 1 ] == 0 ){
    		$state =  '<span class = \'label label-important\'>Depleted</span>';
    	}
		else $state =  '<span class = \'label label-warning\'>Depleting</span>';
    }
	else {
		
        $state =  '<span class = \'label label-success\'>  &nbsp;Adequate &nbsp;&nbsp;&nbsp; </span>';
    }
    echo $state;
	
}
if( isset( $format[ 'order_status' ] ) ){
	
    if( $format[ 'data' ] == 1 )
    {
        $state =  '<span class = \'label label-warning\'>Draft</span>';
    }
	if( $format[ 'data' ] == 2 )
    {
        $state =  '<span class = \'label label-warning\'>Pending</span>';
    }
	if( $format[ 'data' ] == 3 )
    {
        $state =  '<span class = \'label label-success\'>Accepted</span>';
    }
	if( $format[ 'data' ] == 4 )
    {
        $state =  '<span class = \'label label-success\'>Received</span>';
    }
	if( $format[ 'data' ] == 5 )
    {
        $state =  '<span class = \'label label-danger\'>Cancelled</span>';
    }
	if( $format[ 'data' ] == 6 )
    {
        $state =  '<span class = \'label label-danger\'>Incompleted</span>';
    }
	if( $format[ 'data' ] == -1 )
    {
        $state =  '<span class = \'label label-danger\'>Low Quantity</span>';
    }
    echo $state;
	
}

