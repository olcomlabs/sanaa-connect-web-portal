<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>

<script src='<?php echo base_url(); ?>assets/js/jquery.cookie.js'></script>
 
 

<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){
	    $('.daterangepicker').daterangepicker();
		
		hide_spinner();
		
	    $('#olcom-search-patient').on('click' ,function (e)
	    {
	      patient_auto_complete();
	      e.preventDefault();
	      return false;
	    });   
	    
	     $( '#indication' ).autocomplete({
            serviceUrl:'<?php echo site_url('prescriptions_mngt/indication_data'); ?>',
            onSelect: function (suggestion){
            $( this ).attr( 'value' , suggestion.data );
            }
        });
        
	    function diagnosis_from_patient( patient_id )
        {
            $.ajax({
                url:'<?php echo site_url('prescriptions_mngt/assoc_diagnosis_data'); ?>/'+patient_id,
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){ 
                        var diagnosis = $( '#diagnosis_id' );
                        
                        //clear select content
                        $( diagnosis ).html('');
                        //insert options
                        $.each(data,function(key,value){
                            $( diagnosis ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( diagnosis ).trigger("liszt:updated");
                    }
                    else
                    {   
                        $( diagnosis ).html('');
                        $( diagnosis ).append($('<option></option>'));
                        $( diagnosis ).trigger( 'liszt:updated' );
                    }
                    if( data.error == 1 )
                    {
                        
                        $( diagnosis ).html('');
                        $( diagnosis ).append($('<option></option>'));
                        $( diagnosis ).trigger( 'liszt:updated' );
                    }
                    if( data.result == 0 )
                    {
                        
                        $( diagnosis ).html('');
                        $( diagnosis ).append($('<option></option>'));;
                        $( diagnosis ).trigger( 'liszt:updated' );
                    }
                }
            });
        }
	    /*
	     * 
	     * 
	     * 
	     */
	   function patient_auto_complete()
	   {
	        $.ajax({
                url: '<?php echo site_url('prescriptions_mngt/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
                dataType: 'json',
                method: 'get',
                success:function (data)
                {
                    var diagnosis = $( '#diagnosis_id' );
                    if( data.result == 1)
                    {
                         $('#patient_auto_complete').val( data.value ).attr('value',data.key);
                         diagnosis_from_patient( data.key );
                    }
                    else
                    {
                         $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null);
                         $( diagnosis ).html('');
                        $( diagnosis ).append($('<option></option>'));;
                        $( diagnosis ).trigger( 'liszt:updated' );
                    
                    }
                   
                }
            });
	   }
	   /*
	    * 
	    * 
	    * 
	    */
      $( '#patient_auto_complete').keypress( function ( e ) {
           if( e.which == 13 )
           {
               patient_auto_complete ();
           } 
        });
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'prescriptions_mngt/save_prescription_lines_data')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				patient: {
					required: true
					},
				prescribing_doctor: {
					required: true
					},
				verified:{
					required: true
					},
				salary: {
					required: true,
					digits:true,
					minlength:3,
					maxlength:10
					},
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
		
		$( '.help-button' ).popover();
		
	});
	
</script>
