<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>
<script src="<?php echo base_url(); ?>assets/js/jquery.jeditable.js"></script>
<script>
   
      var request_id = $( '#modal-form-dialog').find('table tr td:first').next().html();
     
      var  trows = $( '#test-result  tr' );
      
      $.each( trows , function ( index ){
          
         if( index > 0 )
         {
             
             var tds = $( this).find( 'td' );
             $.each( tds, function ( index ){
                    if( index > 5 )
                    {
                        
                        // value 
                        
                        if( index == 6 )
                        {
                            $( tds[ index ] ) .editable(
                            function( value , settings )
                            {
                             var  return_val = null;
                             $.ajax( { 
                                 url : '<?php echo site_url( 'laboratory/save_lab_result_data'); ?>',
                                 data : { value : value ,test_case_id : $( tds[ 0 ] ).html() ,request_id : request_id },
                                 dataType : 'json',
                                 success : function ( data ){ 
                                     if( data.type != undefined )
                                       {
                                           $( '#modal-form-dialog' ). modal( 'hide' ); 
                                           $( '#olcom-modal-dialog').html( data.dialog ).show();
                                           $('#modal-message').modal().css({
                                            width:'auto',
                                            'margin-top': function () {
                                                return -($(this).height() / 8);
                                            },
                                            'margin-left': function () {
                                                return -($(this).width() / 2);
                                            }
                                            });
                                            
                                            $( '#modal-message').on( 'hidden' , function (){
                                                
                                                $( '#hms-datatable' ).dataTable().fnReloadAjax();
                                            });
                                     }else
                                     {
                                            $( tds[ index ] ).html( data.value ); 
                                     }
                                            
                                 }
                             });   
                            },{
                                type : 'text',
                                submit : 'Save',
                                width : '80px'
                            });
         
                        }
                        // result text
                        if( index == 7 )
                        {
                            $( tds[ index ] ) .editable(
                            function( value , settings )
                            {
                            
                             $.ajax( { 
                                 url : '<?php echo site_url( 'laboratory/save_lab_result_data'); ?>',
                                 data : { result : value ,test_case_id : $( tds[ 0 ] ).html() ,request_id : request_id },
                                 dataType : 'json',
                                 success : function ( data ){ 
                                     if( data.type != undefined )
                                       {
                                           $( '#modal-form-dialog' ). modal( 'hide' ); 
                                           $( '#olcom-modal-dialog').html( data.dialog ).show();
                                           $('#modal-message').modal().css({
                                            width:'auto',
                                            'margin-top': function () {
                                                return -($(this).height() / 8);
                                            },
                                            'margin-left': function () {
                                                return -($(this).width() / 2);
                                            }
                                            });
                                            
                                            $( '#modal-message').on( 'hidden' , function (){
                                                
                                                $( '#hms-datatable' ).dataTable().fnReloadAjax();
                                            });
                                     }else
                                     {
                                            $( tds[ index ] ).html( data.result ); 
                                     }
                                            
                                 }
                             });   
                            },{
                                type : 'textarea',
                                submit : 'Save',
                                width : '150px'
                            });
         
                        }
                        // remarks
                        if( index == 8 )
                        {
                            $( tds[ index ] ) .editable(
                            function( value , settings )
                            {
                            
                             $.ajax( { 
                                 url : '<?php echo site_url( 'laboratory/save_lab_result_data'); ?>',
                                 data : { remarks : value ,test_case_id : $( tds[ 0 ] ).html() ,request_id : request_id },
                                 dataType : 'json',
                                 success : function ( data ){ 
                                     
                                      if( data.type != undefined )
                                       {
                                           $( '#modal-form-dialog' ). modal( 'hide' ); 
                                           $( '#olcom-modal-dialog').html( data.dialog ).show();
                                           $('#modal-message').modal().css({
                                            width:'auto',
                                            'margin-top': function () {
                                                return -($(this).height() / 8);
                                            },
                                            'margin-left': function () {
                                                return -($(this).width() / 2);
                                            }
                                            });
                                            
                                            $( '#modal-message').on( 'hidden' , function (){
                                                
                                                $( '#hms-datatable' ).dataTable().fnReloadAjax();
                                            });
                                     }else
                                     {
                                            $( tds[ index ] ).html( data.remarks );
                                     }
                                          
                                 }
                             });   
                            },{
                                type : 'textarea',
                                submit : 'Save',
                                width : '150px'
                            });
         
                        }
                    }                         
             });
         }
        
      } ) ;
     
     
     $( '#olcom-confirm-static-update').click( function(){
              $.ajax( { 
                        url : '<?php echo site_url( 'laboratory/post_lab_test_result_data'); ?>',
                        data : { request_id : request_id },
                        dataType : 'json',
                        success : function ( data ){ 
                                   $( '#modal-form-dialog' ). modal( 'hide' ); 
                                   
                                   if( data.type != undefined )
                                   {
                                       $( '#olcom-modal-dialog').html( data.dialog ).show();
                                       $('#modal-message').modal().css({
                                        width:'auto',
                                        'margin-top': function () {
                                            return -($(this).height() / 8);
                                        },
                                        'margin-left': function () {
                                            return -($(this).width() / 2);
                                        }
                                        });
                                        
                                        $( '#modal-message').on( 'hidden' , function (){
                                            
                                            $( '#hms-datatable' ).dataTable().fnReloadAjax();
                                        })
                                   }
                         }
                });
                
                
               // console.log( oTable) ;
     });
      
</script>