<!doctype html>
<html>
    <head>

        <style>
            /* reset */

            {
            border: 0;
            box-sizing: content-box;
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            list-style: none;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
            }

            /* heading */

            h1 {
                font: bold 100% sans-serif;
                letter-spacing: 0.5em;
                text-align: center;
                text-transform: uppercase;
            }

            /* table */

            table {
                font-size: 75%;
                table-layout: fixed;
                width: 100%;
            }
            th, td {
                border-width: 1px;
                padding: 0.5em;
                position: relative;
                text-align: left;
            }
            th, td {
                border-radius: 0.25em;
                border-style: solid;
            }
            th {
                background: #EEE;
                border-color: #BBB;
            }
            td {
                border-color: #DDD;
            }
 
            /* table items */

            table.particulars {
                clear: both;
                width: 100%;
            }
            table.particulars td
            {
                text-align: center
            }
            table.particulars th {
                font-weight: bold;
                text-align: center;
            }

            table.particulars td:nth-child(1) {
               
                width: 12%;
            }
            table.particulars td:nth-child(2) {
                 
            }
            table.particulars td:nth-child(3) {
                 
                width: 12%;
            }
            table.particulars td:nth-child(4) {
                 
                width: 12%;
            }

            /* total */
            .total_additional td
            {
                border-width: 0px;
            }
            
            .total td{
               text-align: right;
               border-width: 1px solid #eee;
            }
            .invoice_header td {
                border-width: 0px;
            }
            .invoice_header {
                border-bottom: 1px solid #eee;
            }
            .patient table
            {
                width: 50%;
            }

            .patient {
                text-align: left;
            }
            .patient td 
            {
                border-width: 0px;
            }
            .additional td {
                border-width: 0px;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <div class = 'invoice_header'>
            <table>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td><h3>HMIS</h3></td>
                        </tr>
                         
                        <tr>
                            <td><small> Street Address :</small></td><td>[]</td>
                        </tr>
                        <tr>
                            <td><small> Phone :</small></td><td>[]</td>
                        </tr>
                        <tr>
                            <td><small> Fax :</small></td><td> [ ]</td>
                        </tr>
                    </table></td>
                    <td>
                    <table>
                        <tr>
                            <td><span style="font-weight: bold;"><?php echo  $sale_invoice; ?></b></td>
                        </tr>
                        <tr>
                            <td><span >Date :</span></td>
                            <td> <?php echo  $bill_date; ?></td>
                        </tr>
                        <tr>
                            <td> Patient ID :</td>
                            <td> <?php echo  $patient_details[ 'patient_id' ]; ?> </td>
                        </tr>
                        <tr>
                            <td>  Bill Number:</td>
                            <td>  <?php echo  $bill_no ?> </td>
                        </tr>
                        
                    </table></td>
                </tr>
            </table>

        </div>
        <br> 
        <div class = 'patient'>
            <table>
                <tr>
                    <th colspan = '2' >Patient Details </th>
                </tr>
                <tr>
                    <td> Full Name : </td>
                    <td> <?php echo  $patient_details[ 'firstname' ].' '.$patient_details[ 'middlename' ].' '.$patient_details[ 'lastname' ]; ?></td>
                </tr>
                <tr>
                    <td> Address :</td>
                    <td> <?php echo  $patient_details[ 'address' ]; ?></td>
                </tr>
                <tr>
                    <td> Phone :</td>
                    <td> <?php echo  $patient_details[ 'phone' ]; ?></td>
                </tr>
            </table>
        </div>
        <br>
        <table class="particulars">
            <thead>
                <tr>
                    
                    <th>Particulars</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
                  foreach( $bill_info as $bill_line )
                  {
                      echo '<tr>'.
                        '<td>'.$bill_line[ 0 ].'</td>'.
                        '<td>'.$bill_line[ 1 ].'</td>'.
                        '<td>'.$bill_line[ 2 ].'</td>'.
                        '<td>'.$bill_line[ 3 ].'</td>'.
                        '</tr>';
                  }  
                ?>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <div >
            <table>
                <tr>
                    <td style ='border: 0px'>
                    <table class = 'additional' >
                        <tr>
                            <td >
                                <h3>Additional information </h3>
                                <p>
                                   
                                    TIN #: 1020203/30303 
                                    
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                    <td style ='border: 0px'>
                    <table >
                        
                        <tr>
                            <th>Total </th>
                            <td style ='background-color:#fff' ><?php echo  $total ?></td>
                        </tr>
                       
                    </table></td>
                </tr>
            </table>

        </div>
        <br>
        <br>
        <br>
    </body>
</html>