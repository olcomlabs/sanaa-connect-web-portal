<script src  ="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ' Top Prescribed Medicaments'
            },
            subtitle: {
                text: '<?php echo  $subtitle ?>'
            },
            xAxis : {
              title : {
                  text : ' Medicaments'
              }  
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Quantity'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: <?php echo  $series ?>
        });
    });
    

        </script>