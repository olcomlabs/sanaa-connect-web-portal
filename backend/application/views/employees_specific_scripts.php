<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>

<script src='<?php echo base_url(); ?>assets/js/jquery.cookie.js'></script>
<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){
		hide_spinner();
		
		
		
		
		var department_id = $('#department_id' );
		
		  department_id  .on('change',function (){
			//console.log('change');
			jobs_from_dpt();
		});
		
		jobs_from_dpt();
		
		function jobs_from_dpt()
		{
		    $.ajax({
                url:'<?php echo site_url('humanresources/jobs_from_dpt'); ?>/'+$( department_id ).val(),
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){ 
                        var job_title = $( '#job_id' );
                        //clear select content
                        $( job_title ).html('');
                        //insert options
                        $.each(data,function(key,value){
                            $( job_title ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( job_title ).trigger("liszt:updated");
                    }
                }
            });
		}
		
		
		
		/*
		 * 
		 * */
		 
	if($.cookie('olcom-action-edit-save-click')=='no'){
		$('#olcomhms-template-form').find('.olcom-chosen').each(function (index,element){
			var selectboxes=$('#olcomhms-template-form').find('.olcom-chosen');
			
			$(this).bind('change',function(){
				if(index == 0){
					$.ajax({
						url:'<?php echo site_url('humanresources/jobs_from_dpt'); ?>/'+$(selectboxes[0]).val(),
						dataType:'json',
						success:function (data){
						 
							if(data.error == undefined){
								 
								//clear select content
								$(selectboxes[1]).html('');
								//insert options
								$.each(data,function(key,value){
									$(selectboxes[1]).append($('<option></option>').attr('value',key).text(value));
								});
								//trigger chosen update
								$(selectboxes[1]).trigger("liszt:updated");
								}
							}
						});
					
					}
				});
			});
		}
	});
</script>

<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'humanresources/hire_employee')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				employee_id: {
					required: true,
					olcom_valid_select:true
					},
				department_id: {
					required: true,
					olcom_valid_select:true
					},
				job_id:{
					required: true,
					olcom_valid_select:true
					},
				salary: {
					required: true,
					digits:true,
					minlength:3,
					maxlength:10
					},
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>