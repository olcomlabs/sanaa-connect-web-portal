
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<!-- olcom js -->
<script src="<?php echo base_url(); ?>assets/js/olcom-js.js"></script>


<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'humanresources/create_department')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				department_name: {
				required: true,
				minlength: 3,
				maxlength:40,
				olcom_valid_space_name:true
					}
				},
				<?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>