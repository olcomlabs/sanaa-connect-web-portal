
    <?php
                if ( isset( $table_content ) && $table_content != NULL) { 
                    $this -> load -> config( 'olcom_labels' );
                    $labels = $this -> config -> item( 'labels' ); 

    ?>
<div class="row-fluid">
    <?php 
    
        if( isset( $title ) ) 
        {
            ?>
            <h3 class="blue"><?php echo $title; ?></h3>
    <?php
        }
     ?>
    <div class="row-fluid">
        <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
            <thead>
                <tr>
                <?php
                    
                    $headers = array_keys( $table_content[ 0 ] );
                    foreach( $headers as $header )
                    {
                        echo "<th>".$labels[ $header ]."</th>";      
                    }
                    
                ?>
                </tr>
            </thead>
            <tbody>
                <?php

                foreach ( $table_content as $key => $row ) {
                    
                    echo "<tr>";
                        foreach( $row as $data )
                        {
                            if( ! is_array( $data ) )
                                echo "<td>".$data."</td>";
                            else
                                {
                                    echo "</tr>".
                                            "<tr>"."<td>".
                                                $this -> load -> view( 'template_table' , array( 'table_content' => $data ,TRUE )).
                                             "</td></tr>";
                                }
                        }
                     echo  "</tr>";

                    }
                
                }

                if( isset( $footer ) )
                {
                   list( $key, $value ) = each( $footer );
                   echo "<tr>".
                            "<td colspan='3'> <b>".$key."</b></td><td>".$value."</td></tr>";
                }
                ?>
                
               
            </tbody>
        </table>
    </div>
