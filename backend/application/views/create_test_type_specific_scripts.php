<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this -> load -> view('jquery_ajax',array('data'=>
        array('link' => 'laboratory/create_lab_test_type')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                test_code: {
                    required: true,
                    minlength :2,
                    maxlength : 10,
                    olcom_valid_name : true
                },
                test_name : {
                      required : true,
                      maxlength : 40,
                      olcom_valid_space_name : true
                    }  
                },
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        }); 
    });
    
</script>