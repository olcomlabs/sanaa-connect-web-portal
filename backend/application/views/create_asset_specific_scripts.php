<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_asset')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                serial_number: {
                    required: true,
                    maxlength : 20,
                    olcom_valid_name_digits : true
                },
                model : {
                      required : true,
                      maxlength : 20,
                      olcom_valid_name_digits : true
                    } 
                    ,
                purchase_price : {
                    required : true,
                    maxlength : 11,
                    digits : true
                } 
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
       
       $( '.datepicker' ).datepicker({ format : 'yyyy-m-d'}); 
    });
    
</script>