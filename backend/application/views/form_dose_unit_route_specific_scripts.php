<!-- olcom js -->


<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'medicaments/'.(isset( $controller ) ? $controller : '#'))),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				form: {
				    required: true,
				    maxlength:100
				    
					},
				dosage: {
				    required: true,
				    maxlength: 100
				}
				,
                unit: {
                    required: true,
                    maxlength: 10
                }
                ,
                description: {
                    required: true,
                    maxlength: 30
                },
                route: {
                    required: true,
                    maxlength: 100
                }
                
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>