<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>




<script>

 $(function (){
     
        hide_spinner();

       

        $( '#olcom-modal-dialog ').find('#olcom-confirm-save').on('click' ,function (e){

        if( $( '#olcom-px-line-form' ).valid() == true ){

	        $( '#modal-form-dialog').modal('hide');
	        var form_data = $( 'form' ).serialize();
	
	        $.ajax({
	            url :"<?php echo site_url('finance/write_bill_line_data'); ?>",
	            data : form_data,
	            dataType : 'html',
	            method : 'post',
	            success : function( data ){
				 
				 get_total_cost();
		        if( data.success == undefined   )
		        {
		        	 
		            $('#olcom-modal-dialog').html(data.dialog).show();
		            
		            $('#modal-message').modal().css({
			            width : 'auto',
			            'margin-top': function () {
			            return -($(this).height() / 8);
			            },
			            'margin-left': function () {
			            return -($(this).width() / 2);
		            }
		         });
		         
		        }
		        
		        //hide_spinner();
		        }
	
	        });
	         
	        }
	        e.preventDefault();
	        return false;
        });
        
		function get_total_cost( )
		   {
		       var total_cost = $( '#total_cost' );
		       $.ajax({
		          url : '<?php echo site_url('finance/get_total_cost_data'); ?>',
		          dataType : 'json',
		          method : 'get' , 
		          success : function ( data )
		          {
		              total_cost.attr( 'value' , data.total_cost );
		          } 
		       });
		   }
		   
		   
        $( '#olcom-px-line-form' ).validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
                rules: {
                medicament_id: {
                required: true
                },
                disease_id : {
                required : true
                },
                start_end_date: {
                required: true
                },
                dose:{
                required: true,
                digits : true,
                maxlength: 8
                },
                quantity: {
                required: true,
                digits:true,
                maxlength:10
                },
                treatment_duration : {
                digits : true,
                maxlength : 10
                },
                refills : {
                digits : true,
                maxlength : 10
                },
                units : {
                digits : true,
                maxlength : 10
            }
        },
    <?php echo $this -> load -> view('jquery_validation_ps', '', TRUE); ?>
        });
        });
</script>