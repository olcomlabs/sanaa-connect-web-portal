<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'hospitalizations/create_bed')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                number : {
                    required: true,
                    maxlength:11,
                    digits : true
                },
                status : {
                    required : true
                },
                ward_id :{
                    required : true
                },
                category_id : {
                        required : true
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
    });
    
</script>