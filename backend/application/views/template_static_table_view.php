<?php
if (!defined('BASEPATH'))
    exit('Direct access is restricted');
 ?>
<div id = 'modal-message' class="modal show fade" tabindex="-1" >
    <div class="modal-header no-padding">
        <div class="olcom-dialog olcom-dialog-info">
            <button type="button" class="close" data-dismiss="modal">
                &times;
            </button>
            Record Details
        </div>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <?php
                if ( isset( $table_content ) && $table_content != NULL) { 
                    $this -> load -> config( 'olcom_labels' );
                    $labels = $this -> config -> item( 'labels' ); 
                    ?>
                    <div class="row-fluid">
                    <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
        
                        <tbody>
                            <?php
       
                                foreach ( $table_content as $key => $value  ) {
                                        echo "<tr><td><b>" . $labels[ $key ] . "</b></td>" .
                                            "<td>" .(strlen( $value ) < 20 ? $value : '<pre class=\'prettyprint prettyprinted\'>'.$value.'</pre>'). "</td>" . "</tr>";
                                    
                                }
                            ?>
                        </tbody>
                    </table>
                    </div>
                    <hr />
        <?php } ?>
        <div class="tabbable">
              <ul id="myTab" class="nav nav-tabs">
                            
               <?php 
                               
                               $keys = array_keys( $static_content );         
                              foreach( $keys as $index => $key ){ ?>
                                
                                             
                                <li class="<?php echo $index == 0 ? 'active' : ''; ?>">
                                       <a href="#<?php echo $key; ?>" data-toggle="tab">
                                           <?php echo $labels[$key]; ?>
                                       </a>
                                </li>
                                            <?php } ?> 
                </ul>

                     <div class="tab-content">
                                            
                           <?php 
                              $count = 0;
                               foreach( $static_content as $key => $content ){ ?>
                               	
                               		<?php if( is_array( $content )   ) { ?>
                                   <div class="tab-pane <?php echo $count == 0 ? 'active' : ''; ?>" id="<?php echo $key; ?>">
                                   	
                                   			<?php if( is_array( $content ) ){ ?>
                                            <table  class="table table-striped table-bordered table-hover" id = '<?php echo isset( $table_id ) ? $table_id : NULL ; ?>'>
                                            <thead>
                                                <tr>
                                                    <?php
                                                    //print_r( $content );
                                                        if( isset( $content ) AND $content != NULL )
                                                        {
                                                            
															
																if(  isset( $content[ 0 ] ) )
                                                                	$columns = array_keys( $content[ 0 ] );
	                                                            else
	                                                                if(  isset( $content[ 1 ] ) )
	                                                                $columns = array_keys( $content[ 1 ] );
	                                                                
	                                                            foreach( $columns as $column )
	                                                            {
	                                                                echo '<th>'.$labels[ $column ].'</th>';
	                                                            }	
															
                                                                
                                                        
                                                    ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <?php
                                                        foreach( $content as $row )
                                                        {
                                                            echo '<tr>';
                                                            foreach( $row as $data )
                                                            {
                                                                echo '<td>'.$data. '</td>';
                                                            }
                                                            echo '</tr>';
                                                        }
                                                    ?>      
                                            </tbody>
                                        </table>
                                        <?php } ?>
                                        
                                  </div><?php }  ?>
                                 <?php
                                      }else{
                                      		?>
                                      		<div class="tab-pane <?php echo $count == 0 ? 'active' : ''; ?>" id="<?php echo $key; ?>">
                                      			<?php 
                                      				if( $content == '' OR $content == NULL )
															echo "<p>--</p>";
													else
                                      					 echo $content;
												?>
                                            </div>
                                            <?php
                                        }
                                      $count++;
                                } ?>
                   </div>
               </div>
       
      </div>
   </div>
   <div class="modal-footer">
        <?php
           if( isset( $print_link ) AND $print_link != NULL )
           {
            ?>
                <a class = 'btn btn-small btn-success pull-left' href= '<?php echo site_url( $print_link ); ?>' target='_blank'>
                    <i class = 'icon-print'></i>
                      Print
                </a>
        <?php 
           }
        ?>
        <button class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
    </div>
</div>
<?php if( isset( $specific_scripts ) )
{
    echo $this -> load -> view ( $specific_scripts , NULL, TRUE );
}
