<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this -> load -> view('jquery_ajax',array('data'=>
        array('link' => 'laboratory/create_lab_test_unit')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                unit_code: {
                    required: true,
                    maxlength : 20
                },
                unit_name : {
                      required : true,
                      maxlength : 20
                    }  
                },
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        }); 
    });
    
</script>