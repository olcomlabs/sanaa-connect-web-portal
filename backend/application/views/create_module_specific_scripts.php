<!-- olcom js -->


<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'usermanagement/create_module')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				module_name: {
				required: true,
				minlength: 4,
				maxlength:40,
				olcom_valid_underscore_name:true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>