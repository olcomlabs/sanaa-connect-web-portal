<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this -> load -> view('jquery_ajax',array('data'=>
        array('link' => 'prescriptions_mngt/create_disease')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                disease_name: {
                    required: true,
                    maxlength : 100
                    } 
                },
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        }); 
    });
    
</script>