<div id="modal-table" class="modal show fade" tabindex="-1">
	<div class="modal-header no-padding">
		<div class="table-header">
			<button type="button" class="close" data-dismiss="modal">
				&times;
			</button>
			<?php echo $modal_view['header']; ?>
		</div>
	</div>

	<div class="modal-body no-padding">
		<div class="row-fluid">
			<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">

				<tbody>
					<?php

					if (isset( $modal_view['content'] ) && $modal_view[ 'content' ] != "") {
						$this -> load -> config( 'olcom_labels');
						$labels = $this -> config -> item( 'labels' );
						
						foreach ($modal_view['content'] as $row) {
							while( list( $key , $value ) = each( $row ) ){
								
                                
								echo "<tr><td><b>" . $labels[ $key ] . "</b></td>" .
							 		"<td>";
							 		
							 		if( preg_match('/span/', $value ) )
                                    {
                                        echo $value;
                                    }
                                   else
                                   {
                                    echo (strlen( strip_tags( $value ) ) < 20 ? $value : '<pre>'.$value.'</pre>');   
                                   }
							 	   echo  "</td>" . "</tr>";
							}
							
						}

					}
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal-footer">
		<button class="btn btn-small btn-danger pull-left" data-dismiss="modal" id='dialog-close'>
			<i class="icon-remove"></i>
			Close
		</button>
	</div>
</div>