<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>



<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>

<script>

 $(function (){
    
        hide_spinner();

         $( '#ordered' ).on( 'keyup',function(){
         	var present = $('#present' ).val() * 1;
         	var received = $( '#received' ).val() *1;
         	
         	var total = $( '#total' ).val( present + received ).val();
         	
         	if( total > $( '#ordered' ).val()*1  ){
         		$( '#total' ).val( 'Ordered items exceed total items' );
         	}
         });
     	$( '#present' ).on( 'keyup',function(){
         	var present = $('#present' ).val() * 1;
         	var received = $( '#received' ).val() *1;
         	
         	var total = $( '#total' ).val( present + received ).val();
         	
         	if( total > $( '#ordered' ).val()*1  ){
         		$( '#total' ).val( 'Ordered items exceed total items' );
         	}
         });
         $( '#received' ).on( 'keyup',function(){
         	var present = $('#present' ).val() * 1;
         	var received = $( '#received' ).val() *1;
         	
         	var total = $( '#total' ).val( present + received ).val();
         	
         	if( total > $( '#ordered' ).val()*1 ){
         		$( '#total' ).val( 'Ordered items exceed total items' );
         	}
         });
        $( '#olcom-modal-dialog ').find('#olcom-confirm-save').on('click' ,function (e){

		 
        if( $( '#olcom-px-line-form' ).valid() == true ){

        $( '#modal-form-dialog').modal('hide');
        var form_data = $( 'form' ).serialize();

        $.ajax({
            url :"<?php echo site_url('medicaments/write_item_order_line_data'); ?>",
            data : form_data,
            dataType : 'json',
            method : 'post',
            success : function( data ){

        if( data.success == undefined   )
        {
            $('#olcom-modal-dialog').html(data.dialog).show();
            $('#modal-message').modal().css({
            width : 'auto',
            'margin-top': function () {
            return -($(this).height() / 8);
            },
            'margin-left': function () {
            return -($(this).width() / 2);
            }
         });
        }
        hide_spinner();
        }

        });
        }
        e.preventDefault();
        return false;
        });

        $( '#olcom-px-line-form' ).validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
                rules: {
                medicament_id: {
                required: true
                },  
                qty_ordered:{
                required: true,
                number : true,
                min:1,
                maxlength: 8
                },
                qty_received: {
                required: true,
                number:true,
                min:1,
                maxlength:10
                },
                qty_present : {
                	required : true,
                	digits : true,
                	maxlength : 10
                },
                qty_total : {
                digits : true,
                maxlength : 10
                }
        },
    <?php echo $this -> load -> view('jquery_validation_ps', '', TRUE); ?>
        });
        
        $( '.help-button' ).popover();
        });
</script>