<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        $( '.start_date' ).datepicker({ format : 'yyyy-m-d' });
        $( '.end_date' ). datepicker( { format : 'yyyy-m-d' } );
        
        
         $('#btn-submit').click(function (){
            if($('#olcomhms-template-form').valid()==true){
                show_spinner();
                var form_data = $('form').serialize();
                
                $.ajax({
                    url:"<?php echo site_url( 'reports/top_medicaments_data' ); ?>",
                    data:form_data,
                    dataType:'json',
                    method:'post',
                    success:function(data){
                       // form_submit_message( data );
                        hide_spinner();
                        //$('#olcomhms-template-form')[0].reset();
                        $( '#extra_content' ).html( data.content );
                        $( '#extra-specific-scripts' ).html( data.specific_scripts );
                    }
                        
                });
            }
        });
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                limit: {
                    required: true,
                    digits : true,
                    maxlength: 100
                },
                start_date : {
                        required : true
                },
                end_date : {
                    required : true
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
    });
    
</script>
