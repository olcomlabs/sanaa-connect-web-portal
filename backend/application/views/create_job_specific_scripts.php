<!-- olcom js -->

 <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.autosize-min.js"></script>
<script>
	$(function(){
		//$('.daterangepicker3').datepicker();
		hide_spinner();
		   

		tinymce.init({ 
			selector:'textarea',
			width:600
			 });
 
		$('#btn-submit').click(function (){
			if($('#olcomhms-template-form').valid()==true){
				show_spinner();
				tinyMCE.get("job_description").save();
				var form_data=$('form').serialize();
				var formData = new FormData($('form')[0]);
			

				$.ajax({
					url:"<?php echo site_url('jobs/create_jobs'); ?>",
					data:formData,
					dataType:'json',
					method:'post',
					cache: false,
			        contentType: false,
			        processData: false,
					success:function(data){
						form_submit_message(data);
						hide_spinner();
						
						if( data.success == 1 ){
							window.location.reload( false );
							$('#olcomhms-template-form')[0].reset();
						}
					}
						
				});
			}
		});
		$( '.help-button' ).popover();
		
		$( '#btn-reset' ).click(function(){
			$('.chzn-select').val()
		});
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				job_title: {
				required: true
					},
				 job_description:{
				 	required:true
				 },
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>