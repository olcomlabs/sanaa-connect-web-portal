<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_maintenance_record')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                maintenance_cost: {
                    required: true, 
                    maxlength:11,
                    digits : true
                },
                maintenance_date : {
                    required : true
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
       
        $( '.datepicker' ). datepicker( { format : 'yyyy-m-d' } );
    });
    
</script>