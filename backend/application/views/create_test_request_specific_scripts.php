<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>




<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){
	    $('.daterangepicker').daterangepicker();
		
		hide_spinner();
		
		
		
		function patient_autocomplete()
		{
		    $.ajax({
                url: '<?php echo site_url('prescriptions_mngt/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
                dataType: 'json',
                method: 'get',
                success:function (data)
                {
                    
                    if( data.result == 1)
                    {
                         $('#patient_auto_complete').val( data.value ).attr('placeholder',data.key);
                    }
                    else
                    {
                         $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null).val( null );
                    }
                   
                }
            });
		}
		/*
		 * 
		 * 
		 * 
		 */
		$( '#patient_auto_complete').keypress( function ( e ) {
           if( e.which == 13 )
           {
               patient_autocomplete ();
           } 
        });
        /*
         * 
         * 
         * 
         */
		$( '#btn-submit').on( 'click' , patient_autocomplete());
		
	    $('#olcom-search-patient_id').on('click' ,function (e)
	    {
	        $.ajax({
	            url: '<?php echo site_url('prescriptions_mngt/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
	            dataType: 'json',
	            method: 'get',
	            success:function (data)
	            {
	                
	                if( data.result == 1)
	                {
	                     $('#patient_auto_complete').val( data.value ).attr('placeholder',data.key);
	                }
	                else
	                {
	                     $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null).val( null );
	                }
	               
	            }
	        });
	      e.preventDefault();
	      return false;
	    });   
      
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'laboratory/create_lab_test_request')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				patient_id: {
					required: true
					},
				employee_id: {
					required: true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
		
	});
	
</script>