<?php if( !defined( 'BASEPATH' ) ) die( 'Direct access is restricted' ); ?>
<span title="" data-content="<?php echo $help[ 'content'  ]; ?>"
	 data-placement="bottom" data-trigger="hover" data-rel="popover" 
	 class="help-button" data-original-title="<?php echo $help[ 'title' ]; ?>">?</span>