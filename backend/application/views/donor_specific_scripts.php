<?php if ( ! defined ('BASEPATH')) exit('Direct access is restricted'); ?>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.autosize-min.js"></script>
<script type="text/javascript" src = '<?php echo base_url(); ?>assets/js/jquery.validate.min.js'></script>
<script type="text/javascript" src = '<?php echo base_url(); ?>assets/js/olcom-js.js'></script>
<script type="text/javascript">


$(function (){


		$('textarea[class*=autosize]').autosize({append : '\n'});

		$('.olcom-chosen').chosen();

		hide_spinner();

		$.mask.definitions['~']='[+-]';
		$('#phone').mask('9999 999 999');

		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'blood/register_donor')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				firstname: {
					required: true,
					minlength:3,
				 	maxlength:20,
					olcom_valid_name:true
					},
				middlename: { 
				 	maxlength:20,
					olcom_valid_select:true
					},
				lastname:{
					required: true,
					minlength:3,
				 	maxlength:20,
					olcom_valid_name:true
					},
			    address:{
                    required: true,
                    maxlength:30
                    },
				age: {
					required: true,
					digits:true,
					maxlength:3
					}, 
				phone: {
				    required: true
				}, 
				gender : {
					required : true
				}

				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
 



</script>