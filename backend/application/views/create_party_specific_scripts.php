<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_maintenance_party')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                name: {
                    required: true,
                    minlength: 3,
                    maxlength:40,
                    olcom_valid_space_name : true
                    },
                 phone : {
                     required : true,
                     
                 }, 
                 email : {
                     email : true
                 }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        $.mask.definitions['~']='[+-]';
        $('#phone').mask('9999 999 999');
        
    });
    
</script>