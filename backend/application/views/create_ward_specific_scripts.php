<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'hospitalizations/create_ward')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                name: {
                    required: true,
                    minlength: 3,
                    maxlength:40,
                    olcom_valid_space_name : true
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
    });
    
</script>