<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        $('#btn-submit').click(function (){
            if($('#olcomhms-template-form').valid()==true){
                show_spinner();
                var form_data = $('form').serialize();
                form_data = String( form_data );
                form_data = form_data +'&serial_numbers'+'='+$( '#serial_number' ).val();
                $.ajax({
                        url:"<?php echo site_url( 'laboratory/create_lab_test_case' ); ?>",
                        data:form_data,
                        dataType:'json',
                        method:'get',
                        success:function(data){
                            form_submit_message(data);
                            hide_spinner();
                            $('#olcomhms-template-form')[0].reset();
                    }
                });
               }
        }); 
        
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                analyte: {
                    required: true,
                    minlength :2,
                    maxlength : 40
                    
                },
                lower_limit : {
                    
                      maxlength : 11,
                      digits : true
                    },
                upper_limit : {
                      maxlength : 11,
                      digits : true
                    }   
                },
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        }); 
        
        
        // preload contents
        load_equipments();
        
        $( '#category_id' ).on( 'change' , function () {
            
             load_equipments();
            
        });
        
        function load_equipments()
        {
              $.ajax({
                url:'<?php echo site_url('laboratory/category_assets_data'); ?>/'+$( '#category_id' ).val(),
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){
                        var serial_number = $( '#serial_number' );
                      
                        //insert options
                        $.each(data,function(key,value){
                            $( serial_number ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( serial_number ).trigger("liszt:updated");
                    }
                }
            }); 
        }
        
    });
    
</script>