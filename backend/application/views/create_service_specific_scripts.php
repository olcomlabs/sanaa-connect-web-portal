<!-- olcom js -->

<script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>

<script>
    $(function(){
        
        hide_spinner();
        $( '#price' ).autoNumeric( 'init' );
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'finance/create_service')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                service_name: {
                    required: true,
                    minlength: 3,
                    maxlength:40,
                    olcom_valid_space_name : true
                    },
                 price : {
                     required : true,
                      number : true,
                 },
                 
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
    });
    
</script>