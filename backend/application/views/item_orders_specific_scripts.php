<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>

<script src='<?php echo base_url(); ?>assets/js/jquery.cookie.js'></script>
 
 

<script>
	 
	var order_lines = [];
	
	$(function (){
	     
		hide_spinner();
		    
       
        
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'medicaments/save_item_order_lines_data')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				patient: {
					required: true
					},
				employee_id: {
					required: true
					},
				verified:{
					required: true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
		
		$( '.help-button' ).popover();
		
	});
	
</script>
