<?php
if (!defined('BASEPATH'))
    exit('Direct access is restricted');
 ?>
<div id = 'modal-message' class="modal show fade" tabindex="-1" >
    <div class="modal-header no-padding">
        <div class="olcom-dialog olcom-dialog-info">
            <button type="button" class="close" data-dismiss="modal">
                &times;
            </button>
            Record Details
        </div>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            
        <?php if( isset( $tab_content ) AND $tab_content !== NULL) { 
                $this -> load -> config( 'olcom_labels' );
                $labels = $this -> config -> item( 'labels' );   
                
                $keys = array_keys( $tab_content ); 
            ?>    
            <?php
                if ( isset( $table_content ) && $table_content != NULL) { ?>
                    <div class="row-fluid">
                    <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
        
                        <tbody>
                            <?php
       
                                foreach ( $table_content as $key => $value  ) {
                                        echo "<tr><td><b>" . $labels[ $key ] . "</b></td>" .
                                            "<td>" .(strlen( $value ) < 20 ? $value : '<pre class=\'prettyprint prettyprinted\'>'.$value.'</pre>'). "</td>" . "</tr>";
                                    
                                }
                            ?>
                        </tbody>
                    </table>
                    </div>
                    <hr />
        <?php } ?>
        <div class="tabbable">
              <ul id="myTab" class="nav nav-tabs">
                            
               <?php 
                                            
                              foreach( $keys as $index => $key ){ ?>
                                             
                                <li class="<?php echo $index == 0 ? 'active' : ''; ?>">
                                       <a href="#<?php echo $key; ?>" data-toggle="tab">
                                           <?php echo $labels[$key]; ?>
                                       </a>
                                </li>
                                            <?php } ?> 
                </ul>

                     <div class="tab-content">
                                            
                           <?php 
                              $count = 0;
                               foreach( $tab_content as $key => $content ){ ?>
                                   <div class="tab-pane <?php echo $count == 0 ? 'active' : ''; ?>" id="<?php echo $key; ?>">
                                            <p> <?php echo $content; ?></p>
                                  </div>
                                 <?php
                                      $count++;
                                      } ?>
                              </div>
                    </div>
        <?php } ?>
      </div>
   </div>
   <div class="modal-footer">
        <button class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
    </div>
</div>
