<!-- olcom js -->


<script>
	$(function(){
		
		hide_spinner();
		
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'medicaments/create_medicament_category')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				category_name: {
				    required: true,
                    minlength: 4,
                    maxlength:40,
                    olcom_valid_space_name:true
					}
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	
</script>