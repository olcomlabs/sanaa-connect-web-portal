<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>



<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>

<script>

 $(function (){
    $('#olcom-modal-dialog').find('.date-range-picker').daterangepicker( {
            format: 'YYYY-MM-DD',startDate:'<?php echo date('Y-m-d'); ?>'
        });
        
        hide_spinner();

        $( '#medicament' ).autocomplete({
            serviceUrl:'<?php echo site_url('prescriptions_mngt/medicament_px_data'); ?>',
            onSelect: function (suggestion){
          	  $( this ).attr( 'value' , suggestion.data );
          	  load_drug_forms( suggestion.data );
          	  load_dose_units( suggestion.data );
          	  load_amounts( suggestion.data );
            }
        });
        $( '#indication' ).autocomplete({
            serviceUrl:'<?php echo site_url('prescriptions_mngt/indication_data'); ?>',
            onSelect: function (suggestion){
            $( this ).attr( 'value' , suggestion.data );
            }
        }); 
        
        $( '#olcom-modal-dialog ').find('#olcom-confirm-save').on('click' ,function (e){

        if( $( '#olcom-px-line-form' ).valid() == true ){

        $( '#modal-form-dialog').modal('hide');
        var form_data = $( 'form' ).serialize();

        $.ajax({
            url :"<?php echo site_url('prescriptions_mngt/write_px_line_data'); ?>",
            data : form_data,
            dataType : 'json',
            method : 'post',
            success : function( data ){

        if( data.success == 'Check this'   )
        {
            $('#olcom-modal-dialog').html(data.dialog).show();
            $('#modal-message').modal().css({
            width : 'auto',
            'margin-top': function () {
            return -($(this).height() / 8);
            },
            'margin-left': function () {
            return -($(this).width() / 2);
            }
         });
        }
        hide_spinner();
        }

        });
        }
        e.preventDefault();
        return false;
        });

        $( '#olcom-px-line-form' ).validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
                rules: {
                medicament_id: {
                required: true
                },
                disease_id : {
                required : true
                },
                start_end_date: {
                required: false
                },
                dose:{
                required: true,
                number : true,
                maxlength: 8
                },
                quantity: {
                required: true,
                number:true,
                maxlength:10
                },
                treatment_duration : {
                digits : true,
                maxlength : 10
                },
                refills : {
                digits : true,
                maxlength : 10
                },
                units : {
                digits : true,
                maxlength : 10
            },
            	frequency_id : {
            		required : true
            	}
        },
    <?php echo $this -> load -> view('jquery_validation_ps', '', TRUE); ?>
        });
        
        $( '.help-button' ).popover();
        });
        
 
       function load_drug_forms( medicament_id )
        {
            $.ajax({
                url:'<?php echo site_url('prescriptions_mngt/drug_forms_data'); ?>/'+medicament_id,
                dataType:'json',
                success:function (data){
                 
                    if(data.error == undefined){ 
                        var drug_forms = $( '#drug_forms' );
                        
                        //clear select content
                        $( drug_forms ).html('');
                        //insert options
                        $.each(data,function(key,value){
                            $( drug_forms ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( drug_forms ).trigger("liszt:updated");
                    }
                    else
                    {   
                        $( drug_forms ).html('');
                        $( drug_forms ).append($('<option></option>'));
                        $( drug_forms ).trigger( 'liszt:updated' );
                    }
                    if( data.error == 1 )
                    {
                        
                        $( drug_forms ).html('');
                        $( drug_forms ).append($('<option></option>'));
                        $( drug_forms ).trigger( 'liszt:updated' );
                    }
                    if( data.result == 0 )
                    {
                        
                        $( drug_forms ).html('');
                        $( drug_forms ).append($('<option></option>'));;
                        $( drug_forms ).trigger( 'liszt:updated' );
                    }
                }
              }); 
            }
            /*
             * 
             * 
             */   
           function load_dose_units( medicament_id )
        	{
	            $.ajax({
	                url:'<?php echo site_url('prescriptions_mngt/dose_units_data'); ?>/'+medicament_id,
	                dataType:'json',
	                success:function (data){
	                 
	                    if(data.error == undefined){ 
	                        var dose_units = $( '#dose_units' );
	                        
	                        //clear select content
	                        $( dose_units ).html('');
	                        //insert options
	                        $.each(data,function(key,value){
	                            $( dose_units ).append($('<option></option>').attr('value',key).text(value));
	                        });
	                        //trigger chosen update
	                        $( dose_units ).trigger("liszt:updated");
	                    }
	                    else
	                    {   
	                        $( dose_units ).html('');
	                        $( dose_units ).append($('<option></option>'));
	                        $( dose_units ).trigger( 'liszt:updated' );
	                    }
	                    if( data.error == 1 )
	                    {
	                        
	                        $( dose_units ).html('');
	                        $( dose_units ).append($('<option></option>'));
	                        $( dose_units ).trigger( 'liszt:updated' );
	                    }
	                    if( data.result == 0 )
	                    {
	                        
	                        $( dose_units ).html('');
	                        $( dose_units ).append($('<option></option>'));;
	                        $( dose_units ).trigger( 'liszt:updated' );
	                    }
                }
        	
        	} ); 
        	
        	}
        /*
         * 
         * 
         */
        function load_amounts( medicament_id )
        	{
	            $.ajax({
	                url:'<?php echo site_url('prescriptions_mngt/medicament_amounts_data'); ?>/'+medicament_id,
	                dataType:'json',
	                success:function (data){
	                 
	                    if(data.error == undefined){ 
	                        var amounts = $( '#amounts' );
	                        
	                        if( data.length == 0 ){
	                        	$('#amounts').replaceWith( "<input name='dose' type='text'>")
	                        }
	                        else{
	                        	//clear select content
		                        $( amounts ).html('');
		                        //insert options
		                        $.each(data,function(key,value){
		                            $( amounts ).append($('<option></option>').attr('value',key).text(value));
		                        });
		                        //trigger chosen update
		                        $( amounts ).trigger("liszt:updated");	
	                        }
	                        
	                        
	                    }
	                    else
	                    {   
	                        $( amounts ).html('');
	                        $( amounts ).append($('<option></option>'));
	                        $( amounts ).trigger( 'liszt:updated' );
	                        console.log( 2);
	                    }
	                    if( data.error == 1 )
	                    {
	                        
	                        $( amounts ).html('');
	                        $( amounts ).append($('<option></option>'));
	                        $( amounts ).trigger( 'liszt:updated' );
	                        console.log( 3);
	                    }
	                    if( data.result == 0 )
	                    {
	                        
	                        $( amounts ).html('');
	                        $( amounts ).append($('<option></option>'));;
	                        $( amounts ).trigger( 'liszt:updated' );
	                        console.log( 4);
	                    }
                }
        	
        	} ); 
        	
        	}
</script>