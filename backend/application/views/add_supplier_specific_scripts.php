<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'finance/add_supplier')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                name: {
                    required: true,
                    minlength: 3,
                    maxlength:40,
                    olcom_valid_space_name : true
                    },
                 phone1 : {
                     required : true,
                     
                 },
                 phone2 : {
                     required : false,
                 }, 
                 email : {
                     email : true
                 },
                 website : {
                 	maxlength: 60
                 },
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        $.mask.definitions['~']='[+-]';
        $('#phone1').mask('9999 999 999');
        $('#phone2').mask('9999 999 999');
        
    });
    
</script>