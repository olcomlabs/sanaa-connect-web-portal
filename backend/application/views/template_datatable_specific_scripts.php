<script src="<?php echo base_url(); ?>assets/js/jquery.cookie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.datatables.pagination.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.datatables.reloadajax.js"></script>

<script type="text/javascript">
            var oTable;
            
			$(function() {
				oTable = $('#hms-datatable<?php echo isset( $info_view[ 'id_suffix' ]  ) ? $info_view[ 'id_suffix' ] : NULL ; ?>').dataTable( {
					'bProcessing':true,
					'bServerSide':true,
				<?php if($info_view['with_actions'] == TRUE): ?>
					"aoColumnDefs": [
			       			 { 
					          "bSortable": false,
					          "aTargets": [ -1 ]  //disable sorting for the last column
			         		}
			    	 ],
			    <?php endif; ?>
					'bFilter':true,
					'bInfo':true,
					'sAjaxSource':'<?php echo site_url($info_view['controller_fx']); ?>',//server-side controller name
					'bPaginate':true,
					'bJqueryUI':true,
					'sPaginationType': 'four_button'
					<?php 
                        if( isset( $info_view[ 'extra_params' ] ) AND $info_view[ 'extra_params' ] != NULL )
                          {
                            ?>
                             ,"fnServerParams": function ( aoData ) {
                            aoData.push( 
                              <?php 
                                 echo $info_view[ 'extra_params' ];
                              ?>      
                             );
                            }
                          <?php         
                          }
                            ?>
					
				}); 
			//add click handler for icons					
			$('#hms-datatable<?php echo isset( $info_view[ 'id_suffix' ]  ) ? $info_view[ 'id_suffix' ] : NULL ; ?> tbody').delegate('a.olcom-action-view','click',action_click);
			$('#hms-datatable<?php echo isset( $info_view[ 'id_suffix' ]  ) ? $info_view[ 'id_suffix' ] : NULL ; ?> tbody').delegate('a.olcom-action-edit','click',action_click);
			$('#hms-datatable<?php echo isset( $info_view[ 'id_suffix' ]  ) ? $info_view[ 'id_suffix' ] : NULL ; ?> tbody').delegate('a.olcom-action-delete','click',action_click);
			
			
			function action_click(){
				//create ajax request based on action
			
					$.ajax({
						url:String($(this).attr('href')),
						type:'get',
						dataType:'json',
						success:function(data){
							var modal_view = $('#olcom-modal-dialog');
							
							if(modal_view != null){
								
								$(modal_view).html(data.dialog).show();//set modal view content
								if(data.type == 'message'){
									 
									$('#modal-message').modal().css({
									    width:'auto',
                                        'margin-top': function () {
                                            return -($(this).height() / 8);
                                        },
                                        'margin-left': function () {
                                            return -($(this).width() / 2);
                                        }
									    });//server returned a message
									    
									$( '#modal-message'). find( '.modal-body' ).css( 'max-height' , function(){
									    
									    var window_height = $( this ).height();
									    return  window_height + window_height * 0.25;
									});
								}
								if(data.type == 'info'){
                                     
                                    $('#modal-message').modal().css({
                                        width:'80%',
                                       'margin-top': function () {
                                            return -($(this).height() / 8);
                                        },
                                        'margin-left': function () {
                                            return -($(this).width() / 2);
                                        }
                                        });//server returned a message
                                }
								if(data.type == 'table'){
									$('#modal-table').modal().css({
									    width:'60%'
									    ,
                                        'margin-top': function () {
                                            return -( $(this).height() / 8);
                                        },
                                        'margin-left': function () {
                                            return -($(this).width() / 2);
									    }
									    });//server returned a modal table
								}
								
								if(data.type == 'update'){
									$('#page-specific-scripts').html(data.specific_scripts);
									$('#modal-form-dialog').modal({
										backdrop:'static',
										keyboard:false
									}).css({
									    width:'auto',
									   // width:'80%',
                                        'margin-top': function () {
                                            return -($(this).height() / 8);
                                        },
                                        'margin-left': function () {
                                            return -($(this).width() / 2);
                                        }
									    });
									    
									    $( '#modal-form-dialog'). find( '.modal-body' ).css( 'max-height' , function(){
                                        $('select').chosen();
                                        var window_height = $( this ).height();
                                        return  window_height + window_height * 0.25;
                                    });
								}
							}
								
						}
					});
					//event.preventBubble=true;
				return false;//prevent page navigation
			}
			/*
			 * 
			 * 
			 * 
			 */
			$('table th input:checkbox').on('click' , function(){
					var that = this;
					if( this.checked == true){
						$('#enable-delete' ).removeAttr('disabled');
					}
					else{
						$('#enable-delete' ).attr('disabled','disabled');
					}
					//console.log($('#enable-delete' ));
					
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
					
				});
			
			$('#hms-datatable<?php echo isset( $info_view[ 'id_suffix' ]  ) ? $info_view[ 'id_suffix' ] : NULL ; ?> tbody ').delegate('.checkbx','click',function(){
				var that = this;
				var oneChecked = false;				
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						
						console.log( this.checked );
						if( this.checked == true ){
							if( oneChecked == false ) {
								oneChecked = true;
							}
						}
						if( this.checked == false ){
							if( oneChecked == false ){
								oneChecked = false;
							}else{
								oneChecked = true;
							}
						}
					});
					//console.log(oneChecked);
					if( oneChecked == true ){
						$('#enable-delete' ).removeAttr('disabled');
					}else{
						$('#enable-delete' ).attr('disabled','disabled');
					}
			});
			
			/*
			 * 
			 * olcomhms client server confirm dialog 
			 * 
			 */
			 $('#olcom-modal-dialog').delegate('#olcom-confirm-yes','click',function (){
			 	
			 		$.cookie('olcom-action-delete-confirm-status','yes',{path:'/'});
			 		var form_data = $('form').serialize();
			 	 
			 		$.ajax(
			 			{
			 				url:$.cookie('olcom-action-delete'),
			 				type:'get',
			 				data: form_data,
			 				dataType:'json',
			 				success:function(data) {
			 					$('#olcom-modal-dialog').html(data.dialog).show();
			 					$('#modal-message').modal();
			 					$('#modal-message').on('hidden',function(){//fired when modal is closed/hidden
			 						oTable.fnReloadAjax();//reload datatable
			 					});
			 				}
			 			}
			 			
			 		);
			 	});
		
			$('#olcom-modal-dialog').delegate('#olcom-confirm-no','click',function (){
			 		$.removeCookie('olcom-action-delete-confirm-status',{path:'/'});
			 		$.removeCookie('olcom-action-delete',{path:'/'}
			 	);
			 });
			 /*
			  * 
			  * 
			 * 
			 * olcomhms client server update dialog 
			 * 
			 */
			 $('#olcom-modal-dialog').delegate('#olcom-confirm-edit','click',function (){
			 	
			 		if($('#olcomhms-template-form').valid()){
			 			//form valid 
			 			var form_data = $('form#olcomhms-template-form').serialize();
			 			
			 			//set cookie for server ack
			 			$.cookie('olcom-action-edit-save-click','yes',{path:'/'});
			 			
			 			//show spinner
			 			show_spinner();
				 		$.ajax(
				 			{
				 				url:$.cookie('olcom-action-edit'),
				 				type:'post',
				 				data:form_data,
				 				dataType:'json',
				 				success:function(data) {
				 					
				 					hide_spinner();
				 					$('#modal-form-dialog').modal('hide');
				 					$('#olcomhms-template-form').remove();
				 					
				 					$('#olcom-modal-dialog').html(data.dialog).show();
				 					$('#modal-message').modal();
				 					$('#modal-message').on('hidden',function(){//fired when modal is closed/hidden
				 						oTable.fnReloadAjax();//reload datatable		
				 					});
				 				}
				 			}
			 			);
			 		}
			 	});
		
			$('#olcom-modal-dialog').delegate('#olcom-confirm-cancel','click',function (){
			 		$.removeCookie('olcom-action-edit-save-click',{path:'/'});
			 		$.removeCookie('olcom-action-edit',{path:'/'});
			 		
			});
		});
		
		
		</script>