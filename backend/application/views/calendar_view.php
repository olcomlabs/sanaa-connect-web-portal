<?php  if( ! defined( 'BASEPATH' ) ) exit( 'Direct script access is restricted' ); ?>

<div class="row-fluid">
    <div class="span9">
        <div class="widget-toolbar">
            <a class="btn btn-mini btn-danger" href = '<?php echo site_url( 'appointments/new_appointment' ); ?>'>
                  New Appointment
                   <i class="icon-plus-sign-alt  icon-on-right"></i>
            </a>
        </div>
        <div class="space"></div>
    
        <div id="calendar"></div>
    </div> 
    <div id = 'calendar-dialogs' style = 'display: none'>
        
    </div>
</div>
<div id = 'page-specific-scripts'>
    
</div>