                <div class="widget-box">
                            <div class="widget-header widget-header-blue widget-header-flat">
                                <h4 class="lighter"><?php echo $template['title']; ?></h4>
                            </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="row-fluid">
                                            <div class="">
                                                
                                                     
                                                        <?php

                                                        /*
                                                         * important data here
                                                         *  form id,class,action,method,enctype
                                                         *
                                                         */
                                                        if (isset($template['form_info']) && $template['form_info'] != "") {
                                                            if ($template['form_info']['is_multipart'] == TRUE) {
                                                                echo form_open_multipart($template['form_info']['action'],
                                                                //other form attributes
                                                                array('id' => $template['form_info']['id'], 'class' => $template['form_info']['class'], 'method' => $template['form_info']['method']));
                                                            } else {
                                                                echo form_open($template['form_info']['action'], array('id' => $template['form_info']['id'], 'class' => $template['form_info']['class'], 'method' => $template['form_info']['method']));
                                                            }
                                                        }
                                                    ?>
                                                                <!-- form fields follows -->
                                                                                    
                                                                    <?php

                                                                    //check for fields array in the template
                                                                    if (isset($template['fields']) && $template['fields'] != "")
                                                                     {
                                                                        $column_count = 0;
                                                                        $num_titles = $template[ 'num_titles' ];
                                                                        $group_titles = $template[ 'group_titles' ];
                                                                        $group_title_index = 0;
                                                                        $row_fluid_opened = FALSE;
                                                                        
                                                                        foreach ( $template['fields'] as $key => $field)
                                                                         {
                                                                             

                                                                            if ($field['type'] != 'hidden'   ) {
                                                                                if ($column_count % 2 == 0) {
                                                                                    
                                                                                    echo '<div class = \'row-fluid\'>';
                                                                                    $row_fluid_opened = TRUE;   
                                                                                }
                                                                                echo '<div class =\'span6\'>';
                                                                                echo "<div class='control-group'>" . form_label($field['options']['label'], 'name', array('class' => 'control-label')) . "<div class='controls'>";
                                                                            }

                                                                            switch($field['type']) {
                                                                                case 'text' :
																					if( isset( $field[ 'options' ][ 'help'  ] ) ){
																						$help_html_text =  $this -> load -> view( 'help_view', array( 'help' => $field[ 'options' ][ 'help' ] ) , TRUE );
																						unset( $field[ 'options' ][ 'help' ] );
																					}
                                                                                    echo form_input($field['options']);
																					if(isset( $help_html_text ) ){
																						echo $help_html_text;
																					}
                                                                                    break;
                                                                                case 'password' :
                                                                                    echo form_password($field['options']);
                                                                                    break;
                                                                                case 'textarea' :
                                                                                    echo form_textarea(array('name' => $field['options']['name'], 'class' => "autosize-transition span10", 'style' => "overflow: hidden; word-wrap: break-word; resize: horizontal; height: 81px; width: 225px;", 'value' => isset($field['options']['value']) ? $field['options']['value'] : ''));
                                                                                    
																					if( isset( $field[ 'options' ][ 'help'  ] ) ){
																						$help_html_area =  $this -> load -> view( 'help_view', array( 'help' => $field[ 'options' ][ 'help' ] ) , TRUE );
																						unset( $field[ 'options' ][ 'help' ] );
																					}	
                                                                                    if(isset( $help_html_area ) ){
																						echo $help_html_area;
																					}
                                                                                    break;
                                                                                case 'select' :
                                                                                     
                                                                                    //selected is the name of the key 
                                                                                    if( isset( $field[ 'options' ][ 'help'  ] ) AND $field[ 'type' ] === 'select'  ){
																						$help_html_select =  $this -> load -> view( 'help_view', array( 'help' => $field[ 'options' ][ 'help' ] ) , TRUE );
																						unset( $field[ 'options' ][ 'help' ] );
																					}	
																					echo form_dropdown($field['options']['name'], $field['options']['choices'], (isset($field['options']['selected']) ? $field['options']['selected'] :  array()), 'class=\'olcom-chosen\' ' . (isset($field['options']['js']) ? $field['options']['js'] : ''));
                                                                                    if(isset( $help_html_select ) ){
																						echo $help_html_select;
																					}
                                                                                    break;
                                                                                
                                                                                case 'radio' :
                                                                                    echo "  <span class=''>";
                                                                                    foreach ($field['options']['choices'] as $key => $choice) :
                                                                                        echo "<label class='blue'>" . form_radio(array('name' => $field['options']['name'], 'value' => $key, 'checked' => (isset($field['options']['checked']) ? ($choice == $field['options']['checked'] ? TRUE : FALSE) : FALSE))) . "<span class='lbl'>" . $choice . "</span>
                                                                                                                </label>";
                                                                                    endforeach;
                                                                                    echo "</span>   ";
                                                                                    break;
                                                                                case 'checkbox' :
                                                                                    echo "  <span class=''>";
                                                                                    echo "<label>" . form_checkbox($field['options']) . '<span class="lbl"> </span>' . "</label>" . "</span>    ";
                                                                                    break;
                                                                                case 'hidden' :
                                                                                    echo form_hidden($field['options']['name'], $field['options']['value']);
                                                                                    break;
                                                                                case 'search' :
                                                                                   
																					if( isset( $field[ 'options' ][ 'help'  ] ) ){
																						$help_html_search =  $this -> load -> view( 'help_view', array( 'help' => $field[ 'options' ][ 'help' ] ) , TRUE );
																						unset( $field[ 'options' ][ 'help' ] );
																					}	
																					echo "<div class='input-prepend'>
                                                                                        <span class='add-on'>
                                                                                            <a href = '#' id ='olcom-search-".$field[ 'options' ][ 'name' ]."'><i class='icon-search'></i></a>
                                                                                        </span>".form_input($field['options'])."</div>";
                                                                                    if(isset( $help_html_search ) ){
																						echo $help_html_search;
																					}
                                                                                  break;
                                                                               case 'timepicker' :
	                                                                               if( isset( $field[ 'options' ][ 'help'  ] ) ){
																							$help_html_time =  $this -> load -> view( 'help_view', array( 'help' => $field[ 'options' ][ 'help' ] ) , TRUE );
																							unset( $field[ 'options' ][ 'help' ] );
																						}	
                                                                                    echo "<div class='input-append bootstrap-timepicker'>
                                                                                        ".form_input($field['options'])."<span class='add-on'>
                                                                                            <i class='icon-time'></i>
                                                                                        </span></div>";
                                                                                   if(isset( $help_html_time ) ){
																						echo $help_html_time;
																					}
                                                                                    break;

                                                                                    case 'file':
                                                                                        echo '<input type="file" name= '.$field["options"]["name"].' id = '.$field["options"]["name"].' />';

                                                                                    break;
                                                                                
                                                                            }

                                                                            
                                                                            if ($field['type'] != 'hidden' ) {
                                                                                echo "</div></div></div>";
                                                                            }
                                                                            
                                                                           

                                                                            if ($column_count % 2 == 1 ) {
                                                                                echo '</div>';
                                                                                $row_fluid_opened = FALSE;
                                                                                //close row-fluid
                                                                            }
                                                                             if( isset( $group_titles ) AND $group_titles != NULL)
                                                                            { 
                                                                                if( isset( $group_titles[ $group_title_index ]) )
                                                                                {
                                                                                                
                                                                                    if( $row_fluid_opened == TRUE )
                                                                                    {   
                                                                                        echo '</div>';    
                                                                                    }
                                                                                    
                                                                                    echo '<span class = \'\' ><label class = \'blue\'>'.$group_titles[ $group_title_index ].'</label>
                                                                                    <div class =\'hr hr-dotted\' ></div></span>';
                                                                                   
                                                                                    if( $row_fluid_opened == TRUE )
                                                                                    {
                                                                                        echo '<div class =\'row-fluid\'>';
                                                                                    }
                                                                                    
                                                                                }
                                                                            }
                                                                            $column_count++;
                                                                            $group_title_index ++;
                                                                        }
                                                                        
                                                                        // closing row-fluid on odd fields
                                                                        if( (count( $template[ 'fields' ] ) ) % 2 == 1)
                                                                        { 
                                                                            echo '</div>';    
                                                                        }            
                                                                        
                                                                    }
                                                               ?>
                                                                                        
                                                                            <?php echo form_close(); ?><!-- close form -->
                                                                         
                                                                            <hr />
                                                                            <?php
                                                                            if (isset($template['inflated_view']) AND $template['inflated_view'] != NULL) {
                                                                                echo $template['inflated_view'];
                                                                            }
                                                                            ?>
                                                                            <?php if($template['with_submit_reset'] == TRUE): ?>
                                                                                <div id='olcom-form-submit-status' >
                                                                                        <div id='olcom-form-spinner'>
                                                                                            <i class="icon-spinner icon-spin blue bigger-125">&nbsp;</i>
                                                                                            Loading...
                                                                                        </div>
                                                                                        <div id='olcom-form-submit-status-message'>
                                                                                            
                                                                                        </div>
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <button class="btn btn-info" type="submit" id='btn-submit'>
                                                                                        <i class="icon-ok bigger-110">&nbsp;</i>
                                                                                        Submit
                                                                                    </button>
                                                
                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                    <button class="btn" type="reset" id='btn-reset'>
                                                                                        <i class="icon-undo bigger-110"></i>
                                                                                        Reset
                                                                                    </button>
                                                                                
                                                                                </div>
                                                                                
                                                                        
                                                                        <?php endif; ?>
                                                                        </div>
                                                                        </div> <!-- row-fluid -->
                                                                    </div><!-- widget-main -->
                                                                    
                                                                </div><!--/widget-body-->
                                                            </div>
                                                   