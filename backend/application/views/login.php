<?php if(!defined('BASEPATH')) exit('Direct access is restricted');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title> British Council - Sanaa Connect Tanzania - Login  </title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!--basic styles-->

		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" /> -->

		<!--ace styles-->

		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.custom.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-ie.min.css" />
		<![endif]-->

		<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

	<body class="login-layout">
		<div class="main-container container-fluid">
			<div class="main-content">
				<div class="row-fluid">
					<div class="span12">
						<div class="login-container">
							<div class="row-fluid">
								<div class="center">
									<h1>
										<i class="icon-leaf green"></i>
										<span class="red"> Sanaa Connect Tanzania </span><br>
										<span class="white">@British Council </span>
										
									</h1>
									 
								</div>
							</div>

							<div class="space-6"></div>

							<div class="row-fluid">
								<div class="position-relative">
									<div id="login-box" class="login-box visible widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header blue lighter bigger">
													<i class="icon-coffee green"></i>
													Account Login
												</h4>

												<div class="space-6"></div>

												<?php echo form_open(site_url('authentication/login'),array('id'=>'olcom-hms-login','method' => 'post')); ?>
													<fieldset>
														<label>
															<span class="block input-icon input-icon-right">
																<input type="text" class="span12" placeholder="Username" name='username' />
																<i class="icon-user"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="password" class="span12" placeholder="Password" name='password'/>
																<i class="icon-lock"></i>
															</span>
														</label>

														<div class="space"></div>
                                                        <div id='olcom-form-submit-status-message'>
                                                            <?php if(isset($login_error) && $login_error != NULL): ?>
                                                            <div class="alert alert-error">
                                                                <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                                                                <strong><i class="icon-remove"></i>Login failed!</strong><p></p>
                                                                <?php if(isset($inactive) && $inactive != NULL){ ?>
                                                                    <p>Inactive account</p><br>
                                                                 <?php } else{ ?>
                                                                    <p>Username or password incorrect</p><br>
                                                                  <?php } ?>
                                                             </div>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="space"></div>
														<div class="clearfix">
															<button id='login-button' type='submit' class="width-35 pull-right btn btn-small btn-primary">
																<i class="icon-key"></i>
																Login
															</button>
														</div>

														<div class="space-4"></div>
													</fieldset>
												<?php echo form_close(); ?>
												 
											</div><!--/widget-main-->

										
										</div><!--/widget-body-->
									</div><!--/login-box-->

									

									
								</div><!--/position-relative-->
							</div>
						</div>
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div>
		</div><!--/.main-container-->

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!--<![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<!--ace scripts-->

		<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>

		<!--inline scripts related to this page-->
        <script src='<?php echo base_url(); ?>assets/js/jquery.validate.min.js'></script>
        <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
		<script src='<?php echo base_url(); ?>assets/js/olcom-js.js'></script>
		<script>
            $(function(){
                
            $('#login-button').click(function (e){
                    
                    if($('#olcom-hms-login').valid()==true){
                       
                            return true;
                        }
                        else{
                            return false;
                        }
                        
                    
                   // e.preventDefault();
                    //return false;
                });
                $('#olcom-hms-login').validate({
                    errorElement: 'span',
                    errorClass: 'help-inline',
                    focusInvalid: false,
                    rules:{
                    username: {
                        required:true,
                        olcom_valid_name:true,
                        minlength:4,
                        maxlength:20
                        },
                    password:{
                        required: true,
                        minlength:4,
                        maxlength:20
                            }
                        },
                        <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
                    
                });
                
                
            });
            </script>
	</body>
</html>
