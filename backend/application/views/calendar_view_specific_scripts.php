<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script> <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>

<!--inline scripts related to this page-->

<script type="text/javascript">
    $(function() {
        
        /* initialize the calendar
         -----------------------------------------------------------------*/

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        var calendar = $('#calendar').fullCalendar({
            buttonText : {
                prev : '<i class="icon-chevron-left"></i>',
                next : '<i class="icon-chevron-right"></i>'
            },
        
            header : {
                left : 'prev,next today',
                center : 'title',
                right : 'month,agendaWeek,agendaDay'
            },
            eventSources : [
                {
                     url : '<?php  echo site_url('appointments/load_appointments_data'); ?>'
                }
            ],
            editable : false,
            selectable : true,
            selectHelper : true,
            select : function(start, end, allDay) {
                 
                var start_date = start.getFullYear() + '-'+ (start.getMonth() + 1 ) + '-' + start.getDate( );
                var end_date = end.getFullYear() + '-' + ( end.getMonth() + 1 ) + '-' + end.getDate();
                var start_time = start.getHours() + ':' + start.getMinutes();
                var end_time = end.getHours() + ':' + end.getMinutes();
                
                $.ajax({
                   url : '<?php echo site_url('appointments/new_appointment_form_data'); ?>',
                   dataType : 'json',
                   data :  'start_date=' + start_date + '&end_date=' + end_date + '&all_day=' + allDay + '&start_time=' + start_time + '&end_time=' + end_time ,
                   method : 'get',
                   success : function ( data )
                   {
                       
                       $( '#calendar-dialogs').html( data.dialog ).show();
                       
                      
                       
                       if( data.type == 'message' )
                       {
                           $( '#modal-message').modal( ).css({
                               
                           });
                       }
                       else
                       {
                            $('#modal-form-dialog').modal(
                                ).css({
                              width:'auto',
                             'margin-top': function () {
                                 return -( $( this ).height( ) / 8);
                               },
                             'margin-left': function () {
                                            return -( $( this  ).width( ) / 2);
                               }
                          });   
                          
                          $( '#modal-form-dialog' ).on( 'hidden' ,function (){
                                calendar.fullCalendar( 'refetchEvents' ); 
                          });
                          $('#page-specific-scripts').html( data.specific_scripts );
                       }
                      
                   } 
                });
               

                calendar.fullCalendar('unselect');

            },
            eventClick : function(calEvent, jsEvent, view) {
               
                var appointment = $("<div class ='well'></div>");
                var title = calEvent.title.split( '\n');
                var patient = title[ 0 ] + ' '+ title[ 1 ];
                var info = title[ 2 ];
                
                patient = $( '<p></p>').html( patient );
                info = $( '<p></p>' ).html( info );
                appointment.append( patient );
                appointment.append( info );
                var div = bootbox.dialog(appointment, [{
                    "label" : "<i class='icon-trash'></i> Delete Appointment",
                    "class" : "btn-small btn-danger",
                    "callback" : function() {
                        
                        calendar.fullCalendar('removeEvents', function( ev ) {
                            $.ajax({
                              url : '<?php echo site_url( 'appointments/delete_appointment_data' );?>/'+calEvent.appointment_id,
                              dataType : 'json',
                              async : false,
                              success : function ( data )
                              {  
                                   
                                   calendar.fullCalendar( 'refetchEvents' );
                                   return true;   
                              } 
                            });
                            return (ev._id == calEvent._id);
                        })
                    }
                }, {
                    "label" : "<i class='icon-remove'></i> Close",
                    "class" : "btn-small"
                }], {
                    // prompts need a few extra options
                    "onEscape" : function() {
                        div.modal("hide");
                    }
                });
            }
        });
       /*
        * 
        * 
        * 
        */
       
        function load_events()
        {
            var events = null;
            $.ajax({
               url : '<?php  echo site_url('appointments/load_appointments_data'); ?>',
               dataType : 'json',
               async : false,
               success : function ( data )
               {
                   events = data; 
               } 
                
            }); 
             return events; 
        }
    });
</script>