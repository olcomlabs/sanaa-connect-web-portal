<!-- olcom js -->



<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this -> load -> view('jquery_ajax',array('data'=>
        array('link' => 'artists/actions/registered_artists')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                active: {
                    required: true,
                     
                    } 
                },
                <?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
            
        }); 
    });
    
</script>