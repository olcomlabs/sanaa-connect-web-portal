<?php
if (!defined('BASEPATH'))
    exit('Direct accces is restricted');
?>


<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/autoNumeric.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script>

 $(function (){ 
        
        hide_spinner();

        $( '#expiry_date' ).datepicker( { format : 'yyyy-m-d'});
        
   		
   		$( '#upp' ).on( 'keyup', function(){
   			var upp = $( '#upp' ).val();
   			$('#alertq' ).val( upp*1 );
   		});
        
        $( '#ppp' ).autoNumeric( 'init' );
        $( '#sppp' ).autoNumeric( 'init' );
        $( '#ppu' ).autoNumeric( 'init' );
        $( '#sppu' ).autoNumeric( 'init' );
        $( '#sppu' ).autoNumeric( 'init' );
    
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'medicaments/add_item_stock')),TRUE); ?>
        
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                item_id: {
                    required: true
                    },
                number_of_packages:{
                    //min:0,
                    required: true,
                    digits : true,
                    maxlength:10
                    },
                unit_per_package: {
                    //min:0,
                    required: true,
                    digits:true,
                    maxlength:10
                    },
                price_per_package: {
                    //min:0,
                    required: true,
                    maxlength:16
                    },
               price_per_unit: {
                   //min:0,
                    required: true, 
                     number : true,
                    maxlength:16
                    },
               selling_price_per_unit: {
                   //min:0,
                    required: true,
                      number : true,
                    maxlength:16
                    },
               units_per_package: {
                   //min:0,
                    required: true,
                    digits:true,
                    maxlength:16
                    },
                 expiry_date : {
                     required : true
                 },
                  form_id : {
                     required : true
                 },
                 alert_quantity : {
                     digits : true,
                     required: true
                 },
                 type : {
                     required: true
                 },
                 number : {
                 	required: true
                 }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        });
</script>