<?php if(!defined('BASEPATH'))exit('Direct access is restricted'); ?>

<script src='<?php echo base_url(); ?>assets/js/jquery.cookie.js'></script>
<script>
	
	$(function (){
		hide_spinner();
				<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'usermanagement/create_module_role')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				
				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
	 
</script>
