<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>




<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){
	    $('#admission_date').datepicker({ format : 'yyyy-m-d' });
		$('#discharge_date' ).datepicker({ format : 'yyyy-m-d' });
		
	 
		hide_spinner();
		
	    $('#olcom-search-patient').on('click' ,function (e)
	    {
	      patient_auto_complete();
	      e.preventDefault();
	      return false;
	    });   
	    /*
	     * 
	     * 
	     * 
	     */
	   function patient_auto_complete()
	   {
	        $.ajax({
                url: '<?php echo site_url('prescriptions_mngt/patient_autocomplete_data'); ?>/'+$('#patient_auto_complete').val(),
                dataType: 'json',
                method: 'get',
                success:function (data)
                {
                    
                    if( data.result == 1)
                    {
                         $('#patient_auto_complete').val( data.value ).attr('value',data.key);
                    }
                    else
                    {
                         $('#patient_auto_complete').attr('placeholder','No match found').attr( 'value' , null);
                    }
                   
                }
            });
	   }
	   /*
	    * 
	    * 
	    * 
	    */
      $( '#patient_auto_complete').keypress( function ( e ) {
           if( e.which == 13 )
           {
               patient_auto_complete ();
           } 
        });
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'hospitalizations/save_inpatient_data')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				patient: {
					required: true
					},
				doctor_id: {
					required: true
					},
				bed_id: {
					required: true,
					olcom_valid_select : true
					},
			    discharge_date : {
			        required : false
			      },
			     admission_date : {
			         required : true
			     }
				},
				<?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		 // preload contents
        //load_beds();
        
        $( '#ward' ).on( 'change' , function () {
            
             load_beds();
            
        });
        load_beds();
        function load_beds()
        {
              $.ajax({
                url:'<?php echo site_url('hospitalizations/ward_empty_beds_data'); ?>/'+$( '#ward' ).val(),
                dataType:'json',
                success:function (data){
                    var bed = $( '#bed' );
                    if( data.error == undefined ){
                        
                        $( bed ).html( '' );
                        //insert options
                        $.each(data,function(key,value){
                            $( bed ).append($('<option></option>').attr('value',key).text(value));
                        });
                        //trigger chosen update
                        $( bed ).trigger("liszt:updated");
                    }
                    else
                    {
                        $( bed ).html( '' );
                        $( bed ).trigger("liszt:updated");
                    }
                    
                }
            }); 
        }
		
	});
	
</script>
