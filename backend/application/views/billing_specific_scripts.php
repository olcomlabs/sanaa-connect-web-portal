<?php if(!defined('BASEPATH')) exit('Direct accces is restricted');?>




<script>
	/*
	 * 
	 * jquery chosen for select elements
	 */
	
	$(function (){ 
	 
		hide_spinner();
		
	    
	      $( '#personnel_auto_complete' ).autocomplete({
            serviceUrl:'<?php echo site_url('finance/search_personnel_data'); ?>',
            onSelect: function (suggestion){
          	  $( this ).attr( 'value' , suggestion.data );
          	   $.cookie('personnel_id' ,$( this ).val() ,'');
          	  oTable.fnReloadAjax( );
          	  get_total_cost();
            }
           
	    });   
	    /*
	     * 
	     * 
	     * 
	     */
	    
	   
	   function get_total_cost( )
	   {
	       var total_cost = $( '#total_cost' );
	       $.ajax({
	          url : '<?php echo site_url('finance/get_total_cost_data'); ?>',
	          dataType : 'json',
	          method : 'get' , 
	          success : function ( data )
	          {
	              total_cost.attr( 'value' , data.total_cost );
	          } 
	       });
	   }
	   /*
	    * 
	    * 
	    * 
	    */
      $( '#total_cost').on('focus', function ( e ) {
             get_total_cost();
        });
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'finance/save_billing_data')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				personnel_id: {
					required: true
					},
				employee_id: {
					required: true
					}
				},
				<?php echo $this -> load -> view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		 
		
	});
	
</script>