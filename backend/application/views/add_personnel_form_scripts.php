
		
		<!-- datepicker -->
		
		
		<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
		<!-- olcom js -->
		<script src="<?php echo base_url(); ?>assets/js/olcom-js.js"></script>
		
		<!--inline scripts related to this page-->
	
		<script type="text/javascript">
			$(function() {
				//load the datepicker
			 	$("#date-picker").datepicker();
				
				
				//hide saving spinner
				hide_spinner();
				
				$('[data-rel=tooltip]').tooltip();
			
				$(".select2").css('width','150px').select2({allowClear:true})
				.on('change', function(){
					$(this).closest('form').validate().element($(this));
				}); 
			
			
				var $validation = true;
			
			
			
			
				$('#olcomhms-template-form').show();
			
				//documentation : http://docs.jquery.com/Plugins/Validation/validate
			
			
				$.mask.definitions['~']='[+-]';
				$('#phone').mask('9999 999 999');
				
				$('#nok_phone').mask('9999 999 999');
				
				/*
				 * 
				 * Form submit
				 */
				$('#btn-submit').click(function(event){
					
					var form_data = $('form').serialize();//serialize form data for transmission
					
					 
					if($('#olcomhms-template-form').valid()){
						
						//reset form 
						$('#olcomhms-template-form')[0].reset();
						//show spinner ico
						show_spinner();
						//send form
						$.ajax(
							{
								url:"<?php echo site_url('personnel/add_personnel'); ?>",
								type:'post',
								data:form_data,
								dataType:'json',
								error:function (xml,status,error){
									//hide spinner
									hide_spinner();
									
									//display error message
									$('#olcom-form-submit-status-message').html(
										"<div class='alert alert-error'>"+
										"<button type='button' class='close' data-dismiss='alert'>"+
											"<i class='icon-remove'></i>"+
										"</button>"+

										"<strong>"+
											"<i class=\"icon-remove\"></i>"+
											"Application error!"+
										"</strong>"+

										status+
										"<br />"+
									"</div>"
									);
									
								},
								success:function(data){
									//hide spinner
									hide_spinner();
									//display server message
								 	//console.log(data.message);
									form_submit_message(data);
								}
							}
						);
						
					}
					return true;
				});
				
				
				
				/*
				 * 
				 * Validation
				 */
				jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\d{4} \d{3} \d{3}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");
			
				///Validate name and name like fields
				jQuery.validator.addMethod("olcom_valid_name",function(value,element){
					
					return this.optional(element)|| /^[A-Za-z]+$/.test(value);
				},"Invalid characters detected");
				
				//form validation 
				$('#olcomhms-template-form').validate({
					errorElement: 'span',
					errorClass: 'help-inline',
					focusInvalid: false,
					rules: { 
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						firstname: {
							required: true,
				 			minlength:3,
				 		 	maxlength:20,
							olcom_valid_name:true
							
						},
						middlename: {
							
				 		 	maxlength:20,
							olcom_valid_name:true
							
						},
						lastname: {
							required: true,
				 			minlength:3,
				 		 	maxlength:20,
							olcom_valid_name:true
							
						},
						phone: {
							required: true,
							phone: 'required'
						},
						nok_phone: {
							required: true,
							phone: 'required'
						},
						dob: {
							required: true,
							date: true
						},
						gender: {
							required: true
						},
						region: {
							required: true
						},
						marital_status: {
							required: true
						},
					 
						citizenship:{
							required:true,
							minlength:4,
							maxlength:30,
							olcom_valid_name:true
						},
						
					 address:{
					 	required:true,
					 	maxlength:100,
					 	minlength:3
					 }
					},
			
					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						
						gender: "Please choose gender"
	
					},
			
					invalidHandler: function (event, validator) { //display error alert on form submit   
						$('.alert-error', $('.login-form')).show();
					},
			
					highlight: function (e) {
						$(e).closest('.control-group').removeClass('info').addClass('error');
					},
			
					success: function (e) {
						$(e).closest('.control-group').removeClass('error').addClass('info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('.controls');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chzn-select')) {
							error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
						}
						else error.insertAfter(element);
					},
			
					submitHandler: function (form) {
						
					},
					invalidHandler: function (form) {
					}
				});
			
				
			
			
			});
		</script>