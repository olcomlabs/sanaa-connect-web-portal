<!DOCTYPE html>
<html>
    <head>
        <style>
            /* reset */

            {
            border: 0;
            box-sizing: content-box;
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            list-style: none;
            margin: 0;
            padding: 0;
            text-decoration: none;
            vertical-align: top;
            }

            /* heading */

            h1 {
                font: bold 100% sans-serif;
                letter-spacing: 0.5em;
                text-align: center;
                text-transform: uppercase;
            }

            /* table */

            table {
                font-size: 75%;
                table-layout: fixed;
                width: 100%;
            }
            th, td {
                border-width: 1px;
                padding: 0.5em;
                position: relative;
            }
            th, td {
                border-radius: 0.25em;
                border-style: solid;
            }
            th {
                background: #EEE;
                border-color: #BBB;
            }
            td {
                border-color: #DDD;
            }

            /* table items */

            table.px {
                clear: both;
                width: 100%;
            }
            table.px th {
                font-weight: bold;
                text-align: center;
            }
            table.px td {

            }
            /*table.px td:nth-child(1) {
                width: 26%;
            }
            table.px td:nth-child(2) {
                width: 38%;
            }
            table.px td:nth-child(3) {
                text-align: right;
                width: 12%;
            }
            table.px td:nth-child(4) {
                text-align: right;
                width: 12%;
            }
            table.px td:nth-child(5) {
                text-align: right;
                width: 12%;
            }

            /* total */
            .total_additional td {
                border-width: 0px;
            }

            .total td {
                text-align: right;
                border-width: 1px solid #eee;
            }
            .px_header td {
                border-width: 0px;
            }
            .px_header {
                border-bottom: 1px solid #eee;
            }
            .patient table {
                width: 50%;
            }

            .patient {
                text-align: left;
            }
            .patient td {
                border-width: 0px;
            }
            .additional td {
                border-width: 0px;
                text-align: left;
            }
            .medicament_px p {
                text-align: center;
                font: bold 80% sans-serif;
            }
        </style>
    </head>

    <body>
        <div class = 'px_header'>
            <table>
                <tr>
                    <td>
                    <table>
                        <tr>
                            <td><h3>HMIS</h3></td>
                        </tr>
                         
                        <tr>
                            <td><small> Street Address :</small></td><td>[]
</td>
                        </tr>
                        <tr>
                            <td><small> Phone :</small></td><td>[]
</td>
                        </tr>
                        <tr>
                            <td><small> Fax :</small></td><td> []</td>
                        </tr>
                    </table></td>
                    <td>
                    <table>
                        <tr>
                            <td><span style = 'font: bold 85% sans-serif' > Patient's Prescription </span></td>
                        </tr>
                        <tr>
                            <td><span >Prescription Date :</span></td>
                            <td> <?php echo $prescription_date ?></td>
                        </tr>
                        
                        <tr>

                            <td> Prescribing Doctor : </td>
                            <td> <?php echo $prescribing_doctor ?> </td>
                        </tr>
                        <tr>
                            <td> Patient ID :</td>
                            <td> <?php echo $patient_id ?></td>

                        </tr>
                        
                    </table></td>
                </tr>
            </table>

        </div>
        <br>
        <div class = 'patient'>
            <table>
                <tr>
                    <th colspan = '2' >Patient Details </th>
                </tr>
                <tr>
                    <td> Full Name : </td>
                    <td> <?php echo $full_name ?> </td>
                </tr>
                <tr>
                    <td> Address :</td>
                    <td> <?php echo $address ?></td>
                </tr>
                <tr>
                    <td> Phone :</td>
                    <td> <?php echo $phone ?></td>
                </tr>
            </table>
        </div>
        <br>
        <div class = 'medicament_px'>

        <?php
            
            if( isset( $lines ) ){
                
              foreach( $lines as $line ) {
             ?>
                <div class = 'line'>
                   
                    <table class="px">
                        <thead>
                            <tr>
                            	<th>Medicament</th>
                                <th>Admin Route</th>
                                <th>Quantity</th>
                                <th>Dosage</th>
                                <th>Allow Substitution</th>
                                <th>Duration</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td> <?php echo $line[ 'active_component' ] ?>  <?php echo $line[ 'dose' ] ?></td>
                              <td> <?php echo $line[ 'route' ] ?></td>
                              <td> <?php echo $line[ 'quantity' ] ?></td>
                              <td> <?php echo $line[ 'dosage' ] ?></td>
                              <td> <?php echo $line[ 'allow_substitution' ] ?></td>
                              <td> <?php echo $line[ 'treatment_duration' ] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            <?php
                } 
            } ?>
        </div>
        <br>
        <br>
        <br>
        <div >
            <table>
                <tr>
                    <td style ='border: 0px'>
                    <table class = 'additional' >
                        <tr>
                            <td ><h3>Additional information </h3>
                            <p>
                                TIN #: 1020203/30303
                            </p></td>
                        </tr>
                    </table></td>
                    <td style ='border: 0px'><b> Remarks </b></td>
                </tr>
            </table>

        </div>
        <br>
        <br>
        <br>
    </body>
</html>