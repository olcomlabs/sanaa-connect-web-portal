<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_maintenance_schedule')),TRUE); ?> 
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                duration: {
                    required: true, 
                    maxlength:11,
                    digits : true
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
       
        
    });
    
</script>