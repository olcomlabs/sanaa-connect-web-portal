<?php if ( ! defined ('BASEPATH')) exit('Direct access is restricted'); ?> 	
<script src="<?php echo base_url(); ?>assets/js/jquery.autosize-min.js"></script>
<script type="text/javascript" src = '<?php echo base_url(); ?>assets/js/jquery.validate.min.js'></script>
<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js" ></script>
<script type="text/javascript">


$(function (){


		$('textarea[class*=autosize]').autosize({append : '\n'});

		$('.olcom-chosen').chosen();

		hide_spinner();

		$.mask.definitions['~']='[+-]';
		$('#phone').mask('9999 999 999');
		$('#dod').datepicker( {
            format: 'yyyy-m-d',startDate:'<?php echo date('Y-m-d'); ?>' 
        }).css( 'z-index' , 999999 );;
        
	     $('#tod').timepicker({
	                    minuteStep: 15,
	                    showSeconds: false,
	                     showInputs: false,
	                    showMeridian: false
	       });
	       
		$( '#deceased' ).on('change',function( data  ){
			
			if( $( '#dod' ).attr( 'disabled' ) == 'disabled' ){
				
				$('#dod' ).attr( 'disabled', 'disabled' );
			}
			else{
				$('#dod' ).removeAttr( 'disabled' );
			}
			
			if( $( '#tod' ).attr( 'disabled' ) == 'disabled' ){
				$('#tod' ).removeAttr( 'disabled' );
			}
			else{
				$('#tod' ).attr( 'disabled', 'disabled' );
			}
			
		} );
		
        $('#olcom-search-patient_id').on('click' ,function (e)
	    {
	      $.ajax({
	         	  url : '<?php echo site_url('patients/get_insert_id_data'); ?>',
		          method : 'get' , 
		          success : function ( data )
		          {
		          	 $( '#patient_id' ).val($('#patient_id').val().toUpperCase()+''+data); 
		          }}); 
	      e.preventDefault();
	      return false;
	    }); 
	    
	    
		<?php echo $this->load->view('jquery_ajax',array('data'=>
		array('link'=>'patients/register_patient')),TRUE); ?>
		
		$('#olcomhms-template-form').validate({
			errorElement: 'span',
			errorClass: 'help-inline',
			focusInvalid: false,
			rules: { 
				
				firstname: {
					required: true,
					minlength:3,
				 	maxlength:20,
					//olcom_valid_name:true
					},
				middlename: { 
				 	maxlength:20,
					//olcom_valid_select:true
					},
				lastname:{
					required: true,
					minlength:3,
				 	maxlength:20,
					//olcom_valid_name:true
					},
			    address:{
                    required: true,
                    maxlength:30
                    },
				age: {
					required: true,
					digits:true,
					maxlength:3
					},
				temperature: {
					digits:true,
					maxlength:6
					},
				phone: {
				    required: true
				},
				weight: {
					digits:true,
					maxlength:3
					},
				gender : {
					required : true
				}

				},
				<?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
			
		});
		
		
	});
 



</script>