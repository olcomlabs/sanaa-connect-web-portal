<div  id='modal-form-dialog' class="modal show fade" tabindex="-1" >
	<div class="modal-header no-padding">
		<div class="olcom-dialog olcom-dialog-info">
			<?php  echo isset( $modal_view[ 'title'] ) ? $modal_view[ 'title' ] :'Record Details'; ?>
		</div>
	</div>

	<div class="modal-body">
		<div class="row-fluid"> 
		
					<?php

					if (isset($modal_view['content']) && $modal_view['content'] != "") {
						
						if( is_array( $modal_view[ 'content' ] ) ){
							$this -> load -> config( 'olcom_labels' );
               				$labels = $this -> config -> item( 'labels' );   
						?>
						<div class="tabbable">
			              <ul id="myTab" class="nav nav-tabs">
			                            
			               <?php 
			                                            
                              $keys = array_keys( $modal_view['content' ] );
							   
                              foreach( $keys as $index => $key ){ ?>
                                             
                                <li class="<?php echo $index == 0 ? 'active' : ''; ?>">
                                       <a href="#<?php echo $key; ?>" data-toggle="tab">
                                           <?php echo $labels[$key]; ?>
                                       </a>
                                </li>
		                                <?php } ?> 
		                        </ul>   
								<div class="tab-content">
		                                            
		                           <?php 
		                              $count = 0;
		                               foreach( $modal_view[ 'content' ] as $key => $content ){ ?>
		                                   <div class="tab-pane <?php echo $count == 0 ? 'active' : ''; ?>" id="<?php echo $key; ?>">
		                                            <p> <?php echo $content; ?></p>
		                                  </div>
		                                 <?php
		                                      $count++;
		                                      } ?>
		                              </div>
		                    </div>
		                <?php 
						
						} // end if
						else{
							echo $modal_view['content'];
						}  
					}
					?>
		</div>
	</div>

	<div class="modal-footer">
	    <?php
	       $save_edit = 'edit';
	       if( isset( $modal_view[ 'save_edit'] ) AND $modal_view[ 'save_edit' ] != NULL )
           {
               $save_edit = $modal_view[ 'save_edit' ];
           }
	    ?>
		<button class="btn btn-small btn-success pull-left"  id='olcom-confirm-<?php echo $save_edit; ?>'>
			<i class="icon-upload"></i>
			Save
		</button>
		<button class="btn btn-small btn-grey pull-left" data-dismiss="modal" id='olcom-confirm-cancel'>
			<i class="icon-remove"></i>
			Cancel
		</button>
	</div>
</div>