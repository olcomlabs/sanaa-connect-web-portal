<?php
if (!defined('BASEPATH'))
    exit('Direct access is restricted');
?>
<?php
if(isset($actions_data) && $actions_data!=NULL):?>
<div class= 'action-buttons'>
    <a class='olcom-action-view blue' href='<?php echo $actions_data['view']; ?>'> <i class='icon-zoom-in bigger-130'></i> </a>
    <a class='olcom-action-edit green' href='<?php echo $actions_data['edit']; ?>'> <i class='icon-pencil bigger-130'></i> </a>
    <a class='olcom-action-delete red' href='<?php echo $actions_data['delete']; ?>'> <i class='icon-trash bigger-130'></i> </a>

</div>
<?php endif; ?>
