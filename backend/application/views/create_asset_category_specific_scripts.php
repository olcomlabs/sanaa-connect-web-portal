<!-- olcom js -->


<script>
    $(function(){
        
        hide_spinner();
        
        <?php echo $this->load->view('jquery_ajax',array('data'=>
        array('link'=>'assets_mngt/create_asset_category')),TRUE); ?>
        
        $('#olcomhms-template-form').validate({
            errorElement: 'span',
            errorClass: 'help-inline',
            focusInvalid: false,
            rules: { 
                category: {
                    required: true,
                    minlength: 4,
                    maxlength:40
                    }
                },
                <?php echo $this->load->view('jquery_validation_ps','',TRUE); ?>
            
        });
        
        
    });
    
</script>