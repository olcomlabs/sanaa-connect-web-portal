<?php
if (!defined('BASEPATH'))
    exit('Direct access is restricted');
 ?>
<div class="error-container">
    <div class="well">
        <h1 class="grey lighter smaller"><span class="blue bigger-125"> <i class="icon-sitemap"></i> 404 </span> Page Not Found </h1>

        <hr />
        <h3 class="lighter smaller">We looked everywhere but we couldn't find it!</h3>

        <div>
            <div class="space"></div>
            <h4 class="smaller">Try one of the following:</h4>

            <ul class="unstyled spaced inline bigger-110">
                <li>
                    <i class="icon-hand-right blue"></i>
                    Re-check the url for typos
                </li>

                <li>
                    <i class="icon-hand-right blue"></i>
                    Read the faq
                </li>

                <li>
                    <i class="icon-hand-right blue"></i>
                    Contact system administrator
                </li>
            </ul>
        </div>

        <hr />
        <div class="space"></div>

    </div>
</div>