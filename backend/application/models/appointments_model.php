<?php if( ! defined( 'BASEPATH')) exit( 'Direct access is resstricted' );
/*
 * 
 * 
 * 
 * 
 */
 class Appointments_model extends Olcomhms
 {
     
     function Appointments_model()
     {
         parent :: __construct();
         
   
         if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
     }
     
     /*
      * 
      * 
      * 
      */
      function create_appointment( $appointment_data )
      {
          if( $appointment_data != NULL )
          {
              if( $this -> create( 'appointments' , $appointment_data ) != 0 )
              {
                  return SUCCESS_CODE;
              }
              return 148;
          }
          return 104;
      }
      /*
       * 
       * 
       * 
       */
       function get_appointments( $appointment_id = NULL )
       {
           if( $appointment_id == NULL )
           {
               $result = $this -> read( 'appointments' , '*' , NULL );
               if( $result != NULL )
               {
                   return $result -> result_array();
               }
               return NULL;
           }
           $result = $this -> read( 'appointments' , '*' , array( 'appointment_id' => $appointment_id ) );
           if( $result != NULL )
           {
               return $result -> row_array();
           }
           return NULL;
       }
       /*
        * 
        * 
        */
        function delete_appointment( $appointment_id )
        {
            if( $appointment_id != NULL )
            {
                if( $this -> delete( 'appointments', array( 'appointment_id' => $appointment_id ) ) != 0 )
                {
                    return SUCCESS_CODE;
                } 
                return 110;
            }
            return 104;
        }
        /*
         * 
         * 
         * 
         */
        function get_employee_appointments( $employee_id )
        {
            if( $employee_id != NULL )
            {
                $result = $this -> read('appointments', '*' , array( 'employee_id'  => $employee_id ) );
                if( $result != NULL )
                {
                   return $result -> result_array();
                }  
                return NULL; 
            }
            return NULL;
        }
      
 }