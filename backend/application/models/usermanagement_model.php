<?php if( ! defined( 'BASEPATH')) exit( 'Direct access is resstricted' );
/*
 * 
 * 
 * 
 * 
 */
 class Usermanagement_model extends Olcomhms{
     
       function Usermanagement_model()
       {
           parent :: __construct();
       }
       /*
        * 
        * 
        * 
        */
       function update_user( $employee_id , $user_data ) 
       {
           if( $user_data != NULL AND $employee_id != NULL )
           {
               if( isset( $user_data[ 'username' ] ) )
                
               if( $this -> record_exists('users', array( 'username' => $user_data[ 'username' ] , 'employee_id !=' => $employee_id ), FALSE , TRUE , 'and' ) == FALSE )
               {
                   if( $this -> edit( 'users'  , $user_data ,array( 'employee_id' => $employee_id ) ) != 0 )
                   {
                       return SUCCESS_CODE;
                   }
                   return 110;
               }
               return 145;
           }
           return 104;
       }
       /*
        * 
        * 
        * 
        */
        function get_groups( $groud_id = NULL )
        {
            
            if( $groud_id != NULL )
            {
                $result = $this -> read( 'groups' , '*' , array( 'group_id' => $groud_id ) , NULL );
                if( $result != NULL )
                {
                    return $result -> row_array( );
                }
                return NULL;
            }
            $result = $this -> read( 'groups' , '*' , NULL );
                if( $result != NULL )
                {
                    return $result -> result_array();
                }
                return NULL;
        }
        /*
         * 
         * 
         */
         function update_group( $group_id , $group_data )
         {
             if( $group_data != NULL AND $group_id != NULL )
             {
                 if($this ->  record_exists('groups',$group_data,FALSE,TRUE,'and')    ===  FALSE)
                 {
                     
                    if($this ->  edit('groups',$group_data,array('group_id'  => $group_id ) ) != 0)
                    {
                        return SUCCESS_CODE;       
                    }
                     return 110;
                 }
                 return 108;
             }
             return 104;
         }
         /*
          * 
          * 
          */
          function delete_user( $employee_id )
          {
              if( $employee_id != NULL )
              {
                  if($this -> delete('users',array('employee_id'  => $employee_id ) ) != 0 )
                  {
                        return SUCCESS_CODE;
                  }
                  return 110;
              }
              return 104;
              
          }
          /*
           * 
           * 
           */
           function get_users( $employee_id = NULL)
           {
               if( $employee_id != NULL )
               {
                   $result = $this -> read( 'users' ,'*' , array( 'employee_id' => $employee_id ) , 'and' );
                   if( $result != NULL )
                   {
                       return $result -> row_array();
                   }
                   return NULL;
               }
               
                $result = $this -> read( 'users' ,'*' , NULL );
                   if( $result != NULL )
                   {
                       return $result -> result_array();
                   }
                   return NULL;
           }
           /*
            * 
            * 
            */
            function sync_group_sub_module_permissions( $group_id  ,$sub_module_id , $permissions )
            {
                if( $group_id != NULL AND $permissions AND $sub_module_id != NULL )
                {
                    if( $this -> edit('sub_module_permissions', $permissions,   array( 'group_id' => $group_id , 'sub_module_id' => $sub_module_id ) ) != 0 )
                    {
                        return SUCCESS_CODE;
                    }
                    return 110;
                }
                return 104;
            }
            /*
             * 
             * 
             */
             function get_modules_sub_modules( $module_id )
             {
                 if( $module_id != NULL )
                 {
                     $result = $this -> read('sub_modules', '*', array( 'module_id' => $module_id ) );
                     if( $result != NULL )
                     {
                         return $result -> result_array();
                     }
                     
                 }
                 return NULL;
             }
             /*
              * 
              * 
              */
              function update_group_module_permissions( $group_id , $permissions )
              {
                  if( $group_id != NULL AND $permissions != NULL )
                  {
                      if( $this -> olcomhms -> edit('module_permissions',$permissions,array('permission_id'  => $this -> uri -> segment( 5) ) ) != 0 )
                      {
                          return SUCCESS_CODE;
                      }
                      return 110;
                  }
                  return 104;
              }
              /*
               * 
               * 
               * 
               */
               function get_group_sub_module_permissions( $group_id , $sub_module_id )
               {
                   if( $group_id != NULL AND $sub_module_id != NULL )
                   {
                       $result = $this -> read('sub_module_permissions', '*', array( 'group_id' => $group_id , 'sub_module_id' => $sub_module_id ) );
                       if( $result != NULL )
                       {
                           return $result -> row_array();
                       }
                       return NULL;
                   }
                   return 104;
               }
 }