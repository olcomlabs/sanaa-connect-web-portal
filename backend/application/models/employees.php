<?php
if (!defined('BASEPATH'))
	exit('Direct access is restricted');
/*
 *
 *
 *
 */
class Employees extends Olcomhms {

	var $employee_id;
	var $job_id;
	var $department_id;
	var $personnel_info;


	/*
	 * 
	 * 
	 * 
	 * 
	 */
	function info($employee_id, $depth,$with_job_dept_ids = FALSE) {
		
		
		$result_employee = $this -> read('employees', array('*'), array('employee_id' =>$employee_id), NULL);
		
		if($result_employee == NULL || $result_employee->num_rows() == 0)
			return NULL;
		
		$result_employee = $result_employee -> row_array();

		$personnel_columns = '*';
		if($depth === 'basic')
			$personnel_columns = array('firstname','lastname');
		if($depth === 'full')
			$personnel_columns = '*';
		
		$result_personnel = $this -> read('personnel',$personnel_columns, array('personnel_id' => $result_employee['employee_id']));

		if($result_personnel == NULL || $result_personnel->num_rows() == 0)
			return NULL;
		$result_personnel = $result_personnel -> row_array();
		
		if($depth==='basic'){
			$result_employee['firstname'] = $result_personnel['firstname'];
			$result_employee['lastname'] = $result_personnel['lastname'];	
		}
		else{
			foreach($result_personnel as $column=>$value){
				$result_employee[ $column ] = $value;
			}
		}
		
		$result_job = $this -> read('jobs', array('job_title'), array('job_id' => $result_employee['job_id']));
		if($result_job == NULL || $result_job -> num_rows() == 0)
			return NULL;
	
		$result_job = $result_job -> row_array();
		$result_employee['job_title'] = $result_job['job_title'];
	
		$result_department = $this -> read('departments', array('department_name'), array('department_id' => $result_employee['department_id']));
		if($result_department == NULL || $result_department -> num_rows() == 0)
			return NULL;
        
		$result_department = $result_department -> row_array();
		$result_employee['department_name'] = $result_department['department_name'];

	
		if($with_job_dept_ids == FALSE){
			unset($result_employee['job_id']);
			unset($result_employee['department_id']);	
		}
		
        
		
		
		$result_employee['employee_id'] = olcomhms_create_id( $result_employee[ 'employee_id' ], TRUE, FALSE);
		
		return $result_employee;
	}
	/*
	 * 
	 * 
	 * get_idle_personnel version of employees without user ids
	 * 
	 */	function get_userless_employees(){
		 	
				$columns = array('employee_id','concat(firstname,\'  \',lastname) as name');
		 		$this -> db -> select('employee_id')->get('users');
				$query = $this -> db -> last_query();
				$this -> db -> select($columns) -> from('personnel,employees')
                 -> where('employees.employee_id NOT IN ('.
				$query.')',NULL,FALSE) -> group_by('employees.employee_id');
				$this -> db -> where(array('employee_id' => 'personnel_id'),NULL,FALSE);
				$result = $this -> db -> get();
				 
				if( $result -> num_rows() != 0 )
				{
					return $result;
				}
				else{
						return NULL;
					
					}
			}
    /*
     * 
     * 
     */
     function update_employee( $employee_id , $employee_data )
     {
         if( $employee_data != NULL AND $employee_id != NULL )
         {
             $is_fired = FALSE;
             if( isset( $employee_data[ 'active' ] ) )
                $is_fired = $employee_data[ 'active' ] == 0 ? TRUE : FALSE;
             
             if( $is_fired == TRUE )
             {
                $this -> edit('users', array( 'active' => 0 ), array( 'employee_id' => $employee_id ) );   
             }
             if( $this -> edit('employees' , $employee_data , array( 'employee_id' => $employee_id ) ) != 0 )
             {
                return SUCCESS_CODE;   
             }
             
             if( $is_fired == TRUE )
             {
                 return SUCCESS_CODE;
             }
             return 110;
         }
         return 110;
     }
     
     function update_personnel( $employee_id , $personnel_data )
     {
         if( isset( $personnel_data ) )
         {
             if( $this -> edit( 'personnel' , $personnel_data , array( 'personnel_id' => $employee_id ) ) != 0 )
             {
                 return SUCCESS_CODE;
             }
             return 110;
         }
     }
    /*
     * 
     * 
     */
     function get_departments( $department_id = NULL , $return = NULL )
     {
         if( $department_id != NULL )
         {
             $result = $this -> read( 'departments' , '*', array( 'department_id' => $department_id ),'and' );
             if( $result != NULL )
             {
                 return $result -> row_array();
             }
             return NULL;
         }
         
         $result = $this -> read( 'departments' , '*' , NULL );
         if( $result != NULL )
         {
             if( $return == 'obj' )
                return $result;
             
             return $result -> result_array();
         }
         return NULL;
     }
     /*
      * 
      * 
      */
      function get_positions( $job_id  = NULL , $department_id = NULL )
      {
          if( $job_id != NULL )
          {
              $result = $this -> read( 'jobs' , '*' , array( 'job_id' => $job_id ) , 'and' );
              if( $result != NULL )
              {
                  return $result -> row_array();
              }
              return NULL;
          }
         if( $department_id != NULL )
          {
              $result = $this -> read( 'jobs' , '*' , array( 'department_id' => $department_id ) , 'and' );
              if( $result != NULL )
              {
                  return $result -> result_array();
              }
              return NULL;
          }
          $result = $this -> read( 'jobs' , '*' , NULL );
          if( $result != NULL )
          {
              return $result -> result_array();
          }
          return NULL;
      }
      /*
       * 
       * 
       * 
       */
       function hire_employee( $employee_data )
       {
           if( $employee_data != NULL )
           {
               if( $this -> record_exists('employees', array( 'employee_id' => $employee_data[ 'employee_id' ] ) , FALSE, TRUE,'and' ) == FALSE )
               {
                   if( $this -> create( 'employees', $employee_data ) )
                   {
                       return SUCCESS_CODE;
                   }
                   return 146;
               }
               return 147;
           }
           return 104;
       }
       /*
	    * 
	    * 
	    */
	    function get_employees( $criteria = NULL){
	    	$result = $this -> db -> select( '*' )
					-> from( 'personnel'  )
					-> join('employees','personnel.personnel_id = employees.employee_id' )
					-> get();
			if( $result -> num_rows() > 0 ){
				return $result -> result_array();
			}
			return NULL;		
			
	    }
       /*
	    * 
	    */
	    function search_personnel( $query ){
	    	$result = $this -> db -> select( '*' )
					-> from( 'personnel' )
					-> join('employees','personnel.personnel_id = employees.employee_id' )
					-> or_like( 'firstname',$query )
					-> or_like( 'middlename' , $query )
					-> or_like( 'lastname', $query )
					-> get( );
					
			if( $result -> num_rows( ) > 0 ){
				return $result -> result_array();
			}
			return NULL;
	    }
}
