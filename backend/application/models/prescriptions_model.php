<?php if( ! defined( 'BASEPATH')) exit( 'Direct access is resstricted' );
/*
 * 
 * 
 * 
 * 
 */
 class Prescriptions_model extends Olcomhms
 {
     
     function Prescriptions_model()
     {
         parent :: __construct();
         if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
     }
     /*
      * 
      * 
      * 
      */
      function add_tempo_prescription_line( $timestamp,$data  )
      {
          if( $timestamp !== NULL and $data !== NULL)
          {    
            // select records not matching given timestamp
            $result = $this -> read('prescription_lines_tempo',array('write_start_timestamp'),
            array('write_start_timestamp !=' => $timestamp ,'prescribing_doctor' => $data[ 'prescribing_doctor' ] ),'and');
            
            if( $result === NULL )
            {
                if( $this -> create('prescription_lines_tempo', $data) !== NULL )
                {
                    return SUCCESS_CODE;
                }
                return 117;
            }
            $result = $result -> row_array();
            // remove old line(s)
            if( $this -> delete('prescription_lines_tempo', array( 'write_start_timestamp' =>
             $result[ 'write_start_timestamp'] )) != 0 )
             {
               return  $this -> add_tempo_prescription_line($timestamp, $data);
             }
             return 118; 
          }
          return 104; //insufficent data
      }
       /*
       * 
       * 
       * 
       */
       function get_indication_by_name( $name , $depth = 'autocomplete')
       {
           if( isset( $name ) AND $name !== NULL )
           {
               if( $depth == 'autocomplete' )
               {
                    $this -> db -> select( array( 'disease_id' ,'disease_name' ))
                            -> like('disease_name',$name );
               }
               
               $result = $this -> db -> get( 'diseases' );
               if( $result -> num_rows() != 0 )
               {
                   return $result -> result_array();                   
               }
               return NULL; 
           }
           return NULL;
       }
      /*
       * 
       * 
       * 
       */
       function get_indication_by_id( $disease_id )
       {
           if( isset( $disease_id ) AND $disease_id !== NULL )
           {
              
               $this -> db -> select( array( 'disease_id' ,'disease_name' ))
                            -> where('disease_id',$disease_id );
               
               $result = $this -> db -> get( 'diseases' );
               if( $result -> num_rows() != 0 )
               {
                   return $result -> row_array();                   
               }
               return NULL; 
           }
           return NULL;
       }
      /*
       * 
       * 
       * 
       */
       function get_temp_px_line_info( $line_id ,$full = FALSE )
       {
           if( $line_id !== NULL )
           {
              if( $full === FALSE )
              $result = $this -> read('prescription_lines_tempo', array( 'allow_substitution','units','route_id','refills',
                                'treatment_duration' , 'admin_hours' ,'review_date','comments' ), array( 'line' => $line_id ) );
              else {
                  $result = $this -> read('prescription_lines_tempo', '*', array( 'line' => $line_id ) );
              }                 
              if( $result === NULL )
              {
                  return 119;
              }
              $result = $result -> row_array();
              
              $result_route = $this -> read( 'administration_routes' ,array( 'route' ), array( 'route_id' => $result[ 'route_id' ] ) );
              
              if( $full == FALSE )
                 {
                  if( $result_route === NULL )
                  {
                      unset( $result[ 'route_id' ] );
                      $result[ 'route_id' ] = NULL;
                  }                        
                  else {
                      $result_route = $result_route -> row_array();
                      $result[ 'route' ] = $result_route[ 'route' ];
                      unset( $result[ 'route_id' ] );
                  }
                }
              
              unset( $result[ 'prescribing_doctor' ] );
              unset( $result[ 'line' ] );
              
              // medicament info 
              if( isset( $result[ 'medicament_id' ] ) AND $result[ 'medicament_id' ] != NULL )
              {
                $result_medicament = $this -> read( 'medicaments' ,array('active_component' ) , array( 'medicament_id' => $result[ 'medicament_id' ] ) ,NULL);
                $result_medicament = $result_medicament -> row_array();
                $result[ 'medicament_id' ] = '( '.$result[ 'medicament_id' ].' ) '.$result_medicament[ 'active_component' ];
                    
              }
              // indication info 
              if( isset( $result[ 'disease_id' ] ) AND $result[ 'disease_id' ] != NULL )
              {
                $result_indication = $this -> read( 'diseases' ,array('name' ) , array( 'disease_id' => $result[ 'disease_id' ] ) ,NULL);
                $result_indication = $result_indication -> row_array();
                $result[ 'disease_id' ] = '( '.$result[ 'disease_id' ]. ' ) '. $result_indication[ 'name' ];
                    
              } 
              if( $full == FALSE )
              {
                $result[ 'allow_substitution' ] = $this->load->view('data_style_format',
                array('format' => array('yes_no' => TRUE,'data' => $result[ 'allow_substitution' ])),TRUE);    
              }
              
              if( $result[ 'review_date' ] == '0000-00-00' )
              {
                  $result[ 'review_date' ] = NULL;
              }
              return $result;    
           }
           return 104;
       }
       /*
        * 
        * 
        * 
        */
        function  delete_px_line( $px_line ){
            if( $px_line != NULL )
            {
                if( $this -> delete('prescription_lines_tempo', array('line' => $px_line ) ) != 0 ) 
                {
                    return SUCCESS_CODE;
                }
                return 110;
            }
            return 104;
        }
        /*
         * 
         * 
         */
         function update_tempo_prescription_line( $line , $data )
         {
             if( $line != NULL AND $data != NULL )
             {
                 
                 if( $this -> edit('prescription_lines_tempo', $data, array( 'line' => $line ) ) !== 0 )
                 {
                     return SUCCESS_CODE;
                 }
                 return 110;
             }
             return 104;
         }
         /*
          * 
          */
          function get_tempo_px_lines( $px_doctor )
          {
              if( $px_doctor != NULL )
              {
                  $result = $this -> read('prescription_lines_tempo', '*', array( 'prescribing_doctor' => $px_doctor ) );
                  if( $result == NULL )
                  {
                      return 123;
                  }
                  return $result -> result_array();
              }
              return NULL;
          } 
         /*
          * 
          * 
          * 
          */
          function write_prescription_lines( $prescribing_doctor , $prescription_data )   
          {
              if( $prescribing_doctor != NULL )
              {
                  $px_tempo_data = $this -> get_tempo_px_lines( $prescribing_doctor );
                  if( is_array( $px_tempo_data ) )
                  {
                  
                    $diagnosis_id = $prescription_data[ 'diagnosis_id' ];
                    
                    unset( $prescription_data[ 'diagnosis_id' ] );
                    
                    $prescription_data[ 'employee_id' ] = $prescribing_doctor;
                    
                    $prescription_id = $this -> create( 'prescriptions', $prescription_data );
                    
                    if( $prescription_id == NULL )
                    {
                        return 120;
                    }
                    else {
                        
                          $is_write_success = TRUE;
                        
                          if( $diagnosis_id != NULL )
                          {
                              $data = array( 'diagnosis_id' => $diagnosis_id , 'prescription_id' => $prescription_id);
                              $this -> create( 'prescriptions_has_patients_diagnosis' , $data );
                          }
                        
                          // write prescription lines
                          foreach( $px_tempo_data as $prescription_line )
                          {
                                //unset none required fields
                                unset( $prescription_line[ 'write_start_timestamp' ] ); 
                                unset( $prescription_line[ 'prescribing_doctor' ] );
                                unset( $prescription_line[ 'line' ] );
                                $prescription_line[ 'prescription_id' ] = $prescription_id;
                                if( $this -> create( 'prescription_lines', $prescription_line ) !== NULL )
                                {
                                    ;
                                }
                                else {
                                    $is_write_success = FALSE;
                                }
                          }
                         if( $is_write_success == TRUE )
                         {
                             $result = $this -> delete('prescription_lines_tempo', array( 'prescribing_doctor' => $prescribing_doctor ) );
                             
                             return olcomhms_create_id( $prescription_id , FALSE,FALSE,FALSE,TRUE );
                         }
                        // revert changes 
                        $this -> delete( 'prescriptions', array( 'prescription_id' => $prescription_id ));
                        
                        return 121;
                    }
                  }
                  return 122;
              }
              return 104;
          }
    /*
     * 
     * 
     * 
     */
     function get_prescription_info( $prescription_id , $prescribing_doctor = NULL )
     {
         if( $prescription_id != NULL )
         {
               $result = $this -> read('prescriptions', array( 'employee_id' , 'patient_id' , 'date'), array( 'prescription_id' => $prescription_id ));
               if( $result != NULL )
               {
                   $result = $result -> row_array();
                   // lines
                   $result_lines = $this -> read( 'prescription_lines' , '*' , array( 'prescription_id' => $prescription_id ));
                   if( $result_lines != NULL )
                   {
                       $result[ 'lines' ] = $result_lines -> result_array(); 
                   }
                   else {
                       $result[ 'lines' ] = NULL;
                   }
                   return $result;
               }
               return 124;
         }
         return 104;
     }
     /*
      * 
      * 
      * 
      */
      function delete_px( $prescription_id )
      {
          if( $prescription_id != NULL )
          {
              if( $this -> delete('prescriptions', array( 'prescription_id' => $prescription_id ) ) != 0 )
              {
                  return SUCCESS_CODE;
              }
              return 110;
          }
          return 104;
      }
     
      /*
       * 
       * 
       */
       function create_disease_category( $category_data )
       {
           if( $category_data != NULL )
           {
               if( $this -> record_exists('disease_categories', $category_data,FALSE, TRUE, 'and' ) == FALSE )
               {
                   if( $this -> create('disease_categories', $category_data ) != 0 )
                   {
                       return SUCCESS_CODE;
                   } 
               }
               return 141;
           }
           return 104;
       }
       /*
        * 
        * 
        * 
        */
        function get_category_diseases( $category_id )
        {
            if( $category_id != NULL )
            {
                $result = $this -> read('diseases', array( 'disease_id' , 'name' ), array( 'category_id' => $category_id ) );
                
                if( $result != NULL )
                {
                    return olcom_db_to_dropdown( $result );
                }
                return NULL;
            }   
            return 104;
        }
        /*
         * 
         * 
         */
         function get_disease_categories( $category_id = NULL )
         {
             
             if( $category_id != NULL )
             {
                $result = $this -> read( 'disease_categories' , '*' , array( 'category_id' => $category_id ) ,NULL );
                if( $result != NULL )
                {
                    return $result -> row_array();
                }    
                return NULL;
             }
             
             $result = $this -> read( 'disease_categories' , '*'  ,NULL );
             
             if( $result != NULL )
             {
                 return olcom_db_to_dropdown( $result );
             }
             return NULL;
         }
        /*
         * 
         * 
         */
         function update_disease_category( $category_id , $category_data )
         {
             if( $category_data != NULL AND $category_data != NULL )
             {
                 if( $this -> record_exists('disease_categories' , array(
                    'category_name' => $category_data[ 'category_name' ], 'category_id !=' => $category_id ) ,FALSE ,TRUE ,'and' ) == FALSE )
                    {
                        if( $this -> edit( 'disease_categories', $category_data , array(
                            'category_id' => $category_id ) ) != 0 )
                        {
                            return SUCCESS_CODE;
                        }
                        return 110;
                    }
                    return 108; 
             }
             return 104;
         }
         /*
          * 
          * 
          */
          function delete_disease_category( $category_id )
          {
              if( $category_id == NULL )
               return 104;
              
              if( $this -> record_exists( 'diseases', array( 'category_id' => $category_id ), FALSE , TRUE , 'and' ) == FALSE )
              {
                 
                 if( $this -> delete('disease_categories' , array( 'category_id' => $category_id ) ) != 0 )
                 {
                     return SUCCESS_CODE;
                 }
                 return 110;
              }
              return 142;
          }
          /*
           * 
           * 
           */
           function create_disease( $disease_data )
           {
               if( $disease_data != NULL )
               {
                   if( $this -> record_exists('diseases' , $disease_data, FALSE , TRUE ,'and' ) == FALSE )
                   {
                       if( $this -> create('diseases' , $disease_data ) != 0 )
                       {
                         return SUCCESS_CODE;  
                       }
                       return 143; 
                   }
                   return 108;
               }
               return 104;
           }
          /*
           * 
           * 
           */
           function get_diseases( $disease_id )
           {
               if( $disease_id != NULL )
               {
                    $result = $this -> read('diseases' , '*', array( 'disease_id'  => $disease_id ) );
                    if( $result != NULL )
                    {
                        return $result -> row_array();
                    }   
                    
                    return NULL; 
               }
               return 104;
           }
           /*
            * 
            * 
            */
             function update_disease( $disease_id , $disease_data )
             {
                 if( $disease_id != NULL AND $disease_data != NULL )
                 {
                     if( $this -> record_exists('diseases' , array(
                        'disease_name' => $disease_data[ 'disease_name' ], 'disease_id !=' => $disease_id  ) ,FALSE ,TRUE ,'and' ) == FALSE )
                        {
                            if( $this -> edit( 'diseases', $disease_data , array(
                                'disease_id' => $disease_id ) ) != 0 )
                            {
                                return SUCCESS_CODE;
                            }
                            return 110;
                        }
                        return 108; 
                 }
                 return 104;
         }
        /*
          * 
          * 
          */
          function delete_disease( $disease_id )
          {
              if( $disease_id == NULL )
               return 104;
              
              if( $this -> record_exists( 'prescription_lines', array( 'disease_id' => $disease_id ), FALSE , TRUE , 'and' ) == FALSE )
              {
                 
                 if( $this -> delete('diseases' , array( 'disease_id' => $disease_id ) ) != 0 )
                 {
                     return SUCCESS_CODE;
                 }
                 return 110;
              }
              return 142;
          }

        /*
         * 
         * 
         * 
         */
         function get_patient_prescriptions( $patient_id )
         {
             if( $patient_id != NULL )
             {
                 $result = $this -> read( 'prescriptions' , array( 'prescription_id' ) ,array( 'patient_id' => $patient_id ) , 'and'    );
                 if( $result != NULL )
                 {
                    $prescriptions = array();
                    foreach( $result -> result_array() as $px )
                    {
                        $prescriptions[] = $px[ 'prescription_id' ];
                    }
                    return $prescriptions;
                 }
             }
             return NULL;
         }
         
         function get_top_diseases( $start_date , $end_date ,$limit )
         {
             $result = $this -> db -> where( array( 'patients_diagnosis.diagnosis_date >=' => $start_date,
                'patients_diagnosis.diagnosis_date <='=> $end_date
             ))
             -> where( array(
                'patients_diagnosis.diagnosis_id' => 'diseases_patient_diagnosis.diagnosis_id'
             ), NULL, FALSE )
             -> select( array( 'disease_id' ,"COUNT( 'disease_id' ) AS count" ))
             -> limit ( $limit )
             -> group_by( 'disease_id')
             -> from( array('patients_diagnosis' , 'diseases_patient_diagnosis'))
             -> get();
             
             if( $result -> num_rows() > 0 )
             {
                 return $result -> result_array();
             }
             return NULL;
         }
         /*
          * 
          * 
          * 
          */
          function get_prescription_diagnosis( $prescription_id )
          {
              if( $prescription_id == NULL )
              {
                  return NULL;
              }
              
              $result = $this -> db -> where( array( 'patients_diagnosis.diagnosis_id' => 
              'prescriptions_has_patients_diagnosis.diagnosis_id' ,'prescriptions_has_patients_diagnosis.diagnosis_id' => $prescription_id  ) ,NULL, FALSE )
              -> from( array( 'prescriptions_has_patients_diagnosis' , 'patients_diagnosis' )) -> get();
              
              if( $result -> num_rows() > 0 )
              {
                  $result =  $result -> row_array();
                  $result_indication = $this -> read( 'diseases_patient_diagnosis', '*',
                   array( 'diagnosis_id' => $result[ 'diagnosis_id'] ) , 'and');
 
                  if( $result_indication != NULL )
                  {
                      $result_indication = $result_indication -> row_array();
                      $result_indication = $this -> get_diseases( $result_indication[ 'disease_id' ] );
                      
                      
                      $result =  array_merge( $result , $result_indication );
                      
                      return(   $result );
                  }
                  
                  return  $result;
              }
              return NULL;
          } 
         /*
          * 
          * 
          * 
          */
          function write_diagnosis( $diagnosis_data )
          {
             
              
              if( $diagnosis_data != NULL )
              {
                  $disease_id = NULL;
                  if( isset( $diagnosis_data[ 'disease_id' ] ) AND $diagnosis_data[ 'disease_id' ] != NULL  )
                  {
                      $disease_id = $diagnosis_data[ 'disease_id' ];
                      unset( $diagnosis_data[ 'disease_id' ] );
					 
                  }
				  else {
					  unset( $diagnosis_data[ 'disease_id' ] );
				  }
                  $result = $this -> create('patients_diagnosis',  $diagnosis_data ) ;
                  if( $result != NULL )
                  {
                      
                      if( $disease_id != NULL ) 
                      {
                          $data[ 'disease_id' ] = $disease_id;
                          $data[ 'diagnosis_id' ] = $result;
                          $this -> create( 'diseases_patient_diagnosis' , $data );
                      }
                      return SUCCESS_CODE;
                  }
                  return 164;
              }
              return 104;
          }
          
          /*
           * 
           * 
           */
           function get_diagnosis( $diagnosis_id = NULL , $patient_id = NULL )
           {
               if( $diagnosis_id == NULL AND $patient_id == NULL )
               {
                   return NULL;
               }
               
               $where = '';
               if( $diagnosis_id != NULL )
               {
                   $where = array( 'diagnosis_id' => $diagnosis_id ) ;
               }
               
               if( $patient_id != NULL )
               {
                   $where = array( 'patient_id' => $patient_id );
               }
               
               $result = $this -> read('patients_diagnosis', '*', $where , 'and');
               
               if( $result != NULL )
               {
                   if( $diagnosis_id != NULL )
                   {
                       return $result -> row_array();
                   }
                   return $result -> result_array();
               }
               return NULL;
           }
           
           /*
            * 
            * 
            * 
            */ 
           function get_diagnosis_disease(  $diagnosis_id = NULL )
           {
               $where = NULL;
               if( $diagnosis_id != NULL )
               {
                   $where[ 'diseases_patient_diagnosis.diagnosis_id' ] = $diagnosis_id;
                   $where[ 'diseases.disease_id' ] = 'diseases_patient_diagnosis.disease_id';
               }
               
               
               $result = $this -> db ->from ( array( 'diseases_patient_diagnosis' , 'diseases' ))
               -> select( array( 'disease_name' , 'category_id','diseases.disease_id' )) 
               -> where ( $where ,NULL,FALSE)
               -> get( );
               
               if( $result -> num_rows() == 0  )
                    return NULL;
               
                return  $result -> row_array();
           }
           /*
            * 
            * 
            * 
            */
            function update_diagnosis( $diagnosis_id , $diagnosis_data )
            {
                if( $diagnosis_data != NULL AND $diagnosis_id != NULL )
                {
                	if( isset( $diagnosis_data[ 'disease_id' ] ) ){
                		$disease[ 'disease_id' ]  = $diagnosis_data[ 'disease_id' ];
						$disease[ 'diagnosis_id' ] = $diagnosis_id;
						if( $this -> record_exists('diseases_patient_diagnosis', array( 'diagnosis_id' => $diagnosis_id ),NULL,TRUE, 'and') == TRUE )
						{
							 $this -> edit( 'diseases_patient_diagnosis', $disease, array( 'diagnosis_id' => $diagnosis_id ));
						}
						else{
							$this -> create( 'diseases_patient_diagnosis', $disease );							
						}
						
						unset( $diagnosis_data[ 'disease_id' ] );
                	}
                    if( $this -> edit( 'patients_diagnosis', $diagnosis_data, array( 'diagnosis_id' => $diagnosis_id) ) != 0 )
                    {
                        return SUCCESS_CODE;
                    }
                    return SUCCESS_CODE;
                }
                return 104;
            }
            /*
             * 
             * 
             * 
             */
             function delete_diagnosis( $diagnosis_id )
             {
                 if( $diagnosis_id != NULL )
                 {
                     if( $this -> delete( 'patients_diagnosis' , array( 'diagnosis_id' => $diagnosis_id ) ) != 0 )
                     {
                         return SUCCESS_CODE;
                     }
                 }
                 return 110;
             }
			/*
			 * 
			 * 
			 */
			 function update_px_line( $line, $data ){
			 	return $this -> edit('prescription_lines', $data, array( 'line' => $line  ) );
			 }
 }

 