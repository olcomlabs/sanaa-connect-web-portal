<?php if(!defined('BASEPATH'))exit('Direct access is restricted');

/*
 * 
 * 
 * Personnel Model definitions
 */
 
 class Olcomhms extends CI_Model{
 	
	//construction fx
	function Olcomhms(){
		parent::__construct();
	}
	
	/*
	 *create fx for create operations 
	 *
	 * @parameters-table_name,data
	 * @return -insert id
	 */
	function  create($table_name,$data){
			if(isset($data) && $data!= ''){
				$this -> db -> limit(1);
				$this -> db -> insert($table_name,$data);
				if($this -> db -> affected_rows()  > 0){
				    
					if($this -> db -> insert_id() != 0 ||$this -> db -> insert_id() != NULL)
						return $this -> db -> insert_id();
					else 
					   return 1;//some tables have no auto inc field
				}
			}
			return NULL;
	}
	/*
	 * read fx for read operations
	 * the result depends on given columns and id
	 * @parameters-$table_name ,$columns,$id
	 * @return array of data
	 * 
	 * usualy one because id is unique
	 */
	 function read($table_name,$columns,$where,$and_or = NULL){
	     
	 	if((isset($columns) && $columns!= NULL) && 
	 	     (isset($table_name) && $table_name!= NULL)){
	 		switch ($columns) {
				 case '*':
					 $this -> db -> select($columns);
					 break;
				 case is_array($columns)  ==  TRUE:
					 $this -> db -> select( implode(',',$columns) );
					break;
			 }
			if( $where != NULL ){
				switch($and_or){
					case 'and':
						foreach($where as $key =>$value)
							$this -> db -> where($key,$value,FALSE);
						break;
					case 'or':
						foreach($where as $key =>$value)
							$this -> db -> or_where($key,$value,FALSE);
						break;
					default:
						$this -> db -> where( $where );
				}
			}
			
			$result = $this -> db -> get($table_name);
			/*$fp = fopen('/var/www/last_query2.txt','w+');
			fwrite($fp, $this -> db -> last_query());
			fclose($fp);*/
			if($result -> num_rows()  ==  0)
				return NULL;
			else
				return $result;
	 	}
	 }
	 
	  /*
	  * 
	  * delete fx
	  * @params = $table,$where
	  * @return affected rows
	  */
	  function delete($table_name,$where){
	  		$this -> db -> where($where);
			$this -> db -> delete($table_name);
			if($affected_rows = $this -> db -> affected_rows() > 0){
				return $affected_rows;
			}else
				return 0;//be supprised 
	  }
	  /*
	   * edit fx
	   *@params = $table_name,$data,$where
	   * @return affected rows
	   */
	   function edit($table_name,$data,$where){
	   	$this -> db -> where( $where );
		$this -> db -> update($table_name,$data);
		if($affected_rows = $this -> db -> affected_rows() > 0){
			return $affected_rows;
		}
		else {
			return 0;
		}
	   }
	   
	   /*
	    * 
	    * record_exists fx
	    * @params-$table_name,$data,$like,$where,$and_or
	    * returns TRUE if exists FALSE otherwise
	    */
	    function record_exists($table_name,$data,$like = FALSE,$where = FALSE,$and_or){
	  
	    	switch($and_or){
				case 'and':
					foreach($data as $key =>$value){
						if($like  ==  TRUE){
							$this -> db -> like($key,$value);
						}
						if($where  ==  TRUE){
							$this -> db -> where($key,$value);
						}
						
					}
				break;
				case 'or':
					foreach($data as $key =>$value){
						if($like  ==  TRUE){
							$this -> db -> or_like($key,$value);
						}
						if($where  ==  TRUE){
							$this -> db -> or_where($key,$value);
						}
					}
					break;
	    	}
			
			$result = $this -> db -> get($table_name);
			/*$fp = fopen('/var/www/dept.txt','w+');
			fwrite($fp, $this -> db -> last_query());
			fclose($fp);*/
			if($result -> num_rows()>0){
				return TRUE;
			}else{
				return FALSE;
			}
	    }
		/*
		 * 
		 * get_idle_personnel
		 * 
		 * @params-array of columns
		 * @return-result obj containing match or NULL if no match
		 */
		 
		 function get_idle_personnel($columns){
		 	
		 		//$this -> db -> select('patient_id') -> get('patients_general');

				//$query  =  $this -> db -> last_query();

				$this -> db -> select('employee_id') -> get('employees');
				$query2  =  $this -> db -> last_query();

				$this -> db -> select($columns) 
				-> from('personnel') 
				//-> where('personnel_id NOT IN ('.$query.')',NULL,FALSE) 
				-> group_by('personnel_id');
				$this -> db -> where('personnel_id NOT IN ('.$query2.')',NULL,FALSE);
				
				$result = $this -> db -> get();

				/*$handle  =  fopen('/var/www/gip.log', 'w+');
				fwrite($handle, $this -> db -> last_query());
				fclose($handle);
                 */
				if($result -> num_rows()!= 0){
					return $result;
				}
				else{
						return NULL;
					
					}
			}
		
		  
		
 }
