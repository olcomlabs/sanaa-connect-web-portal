<?php if( ! defined('BASEPATH')) exit( 'DIrect access is restricted');

class Patients_model extends Olcomhms {
    
    var $error_codes;
    
	function Patients_model(){
		parent:: __construct();
        
        $this -> load -> config ('olcomhms_return_codes');
        $this -> error_codes = $this -> config -> item( 'error_codes');
        
       if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
        
	}
    

	function register_patient($data){

		if( $data !== NULL)
		{
			$general_info = array(
			
					'allegies_critical_info' => $data['allegies_critical_info'],
					'blood_type' => $data['blood_type'].' '.$data['blood_type_sign'],
					'initial' => $data[ 'initial' ]

				);
            $temperature = $data['temperature'];
            $weight = $data['weight'];
            
			unset( $data['temperature'] );
			unset( $data['weight'] );
			unset( $data['allegies_critical_info'] );
			unset( $data[ 'blood_type'] );
			unset( $data ['blood_type_sign'] );
			unset( $data[ 'initial' ] );
			if( $this -> record_exists('personnel', array( 'personnel_id' => $data[ 'personnel_id' ]), FALSE, TRUE, 'and') == TRUE ){
				return 174;
			} 
			//$patient_id = NULL;
			$this -> db -> insert('personnel',$data);
			if( $this -> db -> affected_rows() > 0)
			{
				//$patient_id = $this -> db -> insert_id();

				//if( $patient_id != NULL)
				{
					$general_info['patient_id'] = $data[ 'personnel_id'];
            
                    $this -> db -> insert( 'patients_general', $general_info);
                    
                    if( $this -> db -> affected_rows() > 0)
                    {
                        if( isset($temperature) AND $temperature != NULL)
                        {
                            if($this -> record_temp_weight($patient_id, 'temperature', $temperature) === FALSE)
                            {
                                return 100;
                            }
                        }
                        if( isset($weight) AND $weight != NULL)
                        {
                            if( $this -> record_temp_weight($patient_id, 'weight', $weight) === FALSE)
                            {
                                return 101;
                            }
                        }
                       
                       return SUCCESS_CODE;
                    }
                    
                    return 102;
				} 
			}
			else
			{
				return 103;
			}
		}
		return 104;
	}

	/*


	*/
	function get_temp_weight_history($patient_id,$temp_weight,$all = FALSE)
	{
	     
		$this -> db -> select (array($temp_weight,'history_id'))
		-> from( 'patients_temp_weight_history')
		-> where ('patient_id',$patient_id)
        -> order_by('history_id','desc');
        
		$result = $this -> db -> get();
        
		if( $result -> num_rows() > 0)
		{
			if( $all === FALSE)
			{
              return $result -> row_array();
			}
            else
               {
                    return $result ->   result_array();
               }
		}
		else
		{
			return NULL;
		}
	}
    /*
     * 
     * 
     */
     function set_temp_weight_history($patient_id,$temp_weight, $history_data,$history_id = NULL)
     {
        if( $history_data !== NULL)
        {
           
            // new record
            if( $history_id === NULL)
            {
                $this -> db -> insert('patients_temp_weight_history',array($temp_weight => $history_data,
                'patient_id' => $patient_id));
          
                if( $this -> db -> affected_rows() > 0)
                {
                    return TRUE;   
                }
               return FALSE;    
            }
            // update existing
            $this -> db -> where('history_id',$history_id)
            -> update( 'patients_temp_weight_history',array($temp_weight => $history_data));
           
            if( $this -> db -> affected_rows() > 0 )
            {
                return TRUE;
            }
             return FALSE;   
        }
        return FALSE;
     }
     /*
      * 
      * 
      * 
      * 
      */
      function record_temp_weight($patient_id,$temp_weight,$reading)
        {
            if( isset( $patient_id ) AND $patient_id !== NULL AND isset( $reading ) AND $reading !== NULL)
            {
               
                switch( $temp_weight )
                {
                    case 'temperature':
                        $temp_weight = 'temperature_history';
                    break;
                    
                    case 'weight':
                        $temp_weight = 'weight_history';
                    break;
                }
                // read current history
                
                $patient_temp_weight_history =  $this -> get_temp_weight_history( $patient_id ,$temp_weight );
                
                $reading = array('reading' => $reading,'taken' => date( 'Y-m-d H:i'));
               
                if ( $patient_temp_weight_history === NULL)
                {    
                    // create new history
                     return  $this  ->   set_temp_weight_history( $patient_id, 
                                   $temp_weight, json_encode(array($reading)));
                }
                
           
                // isolate history id and temp history
                $history_id = $patient_temp_weight_history['history_id'];
                $history_size = 0;
                if( isset( $patient_temp_weight_history[ $temp_weight ] ) AND $patient_temp_weight_history[ $temp_weight ] != NULL)
                {
                    $patient_temp_weight_history = $patient_temp_weight_history[ $temp_weight ];
                    $history_size = strlen( $patient_temp_weight_history );
                    $patient_temp_weight_history = json_decode($patient_temp_weight_history);    
                }
                else
                   {
                        $patient_temp_weight_history = array();       
                   }
                
                // current history data size
                
                $reading_size = strlen( json_encode($reading ));
                
                if( $history_size +  $reading_size  < (60 * 1024))
                {
                    if( is_object($patient_temp_weight_history))
                    {
                      $patient_temp_weight_history = json_decode($patient_temp_weight_history); 
                    }
                    array_push($patient_temp_weight_history,json_decode(json_encode($reading)));
                    $patient_temp_weight_history = json_encode($patient_temp_weight_history);
                    
                    return  $this   -> set_temp_weight_history( $patient_id,$temp_weight,$patient_temp_weight_history,$history_id);
                }
                else
                    {
                        return $this   -> set_temp_weight_history( $patient_id , $temp_weight ,json_encode(array($reading)));
                    }
            
        }
    }
    /*
     * 
     * 
     * 
     */
     function get_personnel_info($patient_id,$mode = 'basic')
     {
         if( $patient_id !== NULL)
         {
             $personnel_info = NULL;
             
             if( $mode === 'basic' )
             {
                 $this -> db -> select( array( 'firstname','middlename','lastname','phone','age','gender','address','region'));
             }
             if( $mode === 'full')
             {
                $this -> db -> select('*');    
             }
            $this -> db -> where('personnel_id',$patient_id,FALSE);
            $result = $this -> db -> get('personnel');
            
            if( $result -> num_rows() == 0 )
            {
                return 105;
            }
            return $result -> row_array();
         }
     }
     /*
      * 
      * 
      * 
      * 
      * 
      */
      function get_patient_general_info($patient_id)
      {
          if( $patient_id !== NULL)
          {
              $general_info = NULL;
              
              $this -> db -> select('*');
              $this -> db -> where('patient_id',$patient_id ,FALSE);
              $result = $this -> db -> get( 'patients_general');
              
              if( $result -> num_rows() === 0)
              {
                  return 106;
              }
              return $result -> row_array();
          }
      }
      /*
       * 
       * 
       * 
       * 
       */
       function update_patient_general_info( $patient_id ,$data)
       {
           if( $patient_id !== NULL AND $data !== NULL)
           {
                $this -> db -> where('patient_id' ,$patient_id, FALSE);
                $this -> db -> update( 'patients_general',$data);
                if( $this -> db -> affected_rows() > 0)
                {
                    return SUCCESS_CODE;
                }
                return NULL;
           }
           
       }
       /*
        * 
        * 
        * 
        */
        function update_patient_temp_weight( $patient_id , $temp_weight, $reading)
        {
            if( isset( $patient_id ) AND $patient_id !== NULL AND isset( $reading ) AND $reading !== NULL)
            {
                $patient_temp_weight_history =  $this -> get_temp_weight_history( $patient_id ,$temp_weight );
                $reading = array('reading' => $reading,'taken' => date( 'Y-m-d H:i'));
                if ( $patient_temp_weight_history === NULL)
            {    
                // create new history
                 return  $this  ->   set_temp_weight_history( $patient_id, 
                               $temp_weight, json_encode(array($reading)));
            }
            
            $history_id = $patient_temp_weight_history['history_id'];
            $history_size = 0;
            if( isset( $patient_temp_weight_history[ $temp_weight ] ) AND $patient_temp_weight_history[ $temp_weight ] != NULL)
            {
                $patient_temp_weight_history = $patient_temp_weight_history[ $temp_weight ];
                $history_size = strlen( $patient_temp_weight_history );
                $patient_temp_weight_history = json_decode($patient_temp_weight_history);    
            }
            else
               {
                    $patient_temp_weight_history = array();       
               }
            
            // current history data size
            
            $reading_size = strlen( json_encode($reading ));
            
            if( $history_size +  $reading_size  < (60 * 1024))
            {
                if( count( $patient_temp_weight_history ) > 0 )
                {
                    $current_reading = $patient_temp_weight_history[ count( $patient_temp_weight_history ) - 1] -> reading;
                    $patient_temp_weight_history[ count( $patient_temp_weight_history) - 1] = json_decode(json_encode($reading));
                }
                     
                else {
                    $current_reading = 0;
                    $patient_temp_weight_history[] = json_decode(json_encode($reading));
                }
                if( $current_reading != $reading[ 'reading' ])
                {
                    
                    $patient_temp_weight_history = json_encode($patient_temp_weight_history);
                    return  $this   -> set_temp_weight_history( $patient_id,$temp_weight,$patient_temp_weight_history,$history_id);        
                }
                return TRUE;
            }
            else
                {
                    return $this   -> set_temp_weight_history( $patient_id , $temp_weight ,json_encode(array($reading)));
                }
            
             }
            
        }
        /*
         * 
         * 
         * 
         */
         function delete_patient( $patient_id )
         {
             if( $this -> record_exists('prescriptions', array( 'patient_id' => $patient_id ),FALSE,TRUE, 'and') == TRUE ){
             	return 175;
             }
			 if( $this -> record_exists('bills', array( 'patient_id' => $patient_id ),FALSE,TRUE, 'and') == TRUE ){
             	return 175;
             }
			 if( $this -> record_exists('lab_test_requests', array( 'patient_id' => $patient_id ),FALSE,TRUE, 'and') == TRUE ){
             	return 175;
             }
			 if( $this -> record_exists('patients_diagnosis', array( 'patient_id' => $patient_id ),FALSE,TRUE, 'and') == TRUE ){
             	return 175;
             }
			 
			 
             $this -> db -> where( 'personnel_id', $patient_id) -> delete( 'personnel');
             if( $this -> db -> affected_rows() == 1)
             {
                 return SUCCESS_CODE;
             }
             else {
                 return 107;
             }
         }
         /*
          * 
          *
          * 
          */
          function get_patients_name_id( $search_id = NULL )
          {
              if( $search_id == NULL)
              {
                 return NULL;
              }      
              else {
                      
                  $patient_id = olcomhms_db_id( $search_id );
                  
                  if( $patient_id == NULL)
                  {
                     return NULL;
                  }
                  else {
                    $this -> db -> select( array( 'patient_id' ) )
                        -> where( 'patient_id' ,$patient_id ,FALSE);    
                  }                              
                  
              }
               
              $result = $this -> db -> get( 'patients_general' );
              
              if( $result -> num_rows() > 0 )
                {   
                      $patient_ids = array();
                      foreach( $result -> result_array() as $row )
                      {
                          $patient_ids[ ] = $row[ 'patient_id' ]; 
                      }
                      
                     $this -> db -> select( array( 'personnel_id', 'CONCAT(firstname,\' \',lastname) AS patient_name'))
                           -> where_in('personnel_id' , $patient_ids )
                           -> order_by( 'personnel_id','desc');
                           
                     $result_patient_personnel = $this -> db -> get( 'personnel' );
                   
                     if( $result_patient_personnel -> num_rows() > 0 )
                     {
                         $patients_name_id = array();
                         
                         foreach( $result_patient_personnel -> result_array() as $row )
                         {
                         	$general = $this -> get_patient_general_info( $row[ 'personnel_id' ] );
							
                            $patients_name_id[ $row[ 'personnel_id' ] ] = $general['initial'].olcomhms_create_id( $row[ 'personnel_id' ]).
                                                                           ' '.$row[ 'patient_name' ];
                         }
                         return $patients_name_id;
                     }
                     return 105;      
                 }
                 return 116;
          }
	/*
	 * 
	 * 
	 */
	 function get_insert_personnel_id( ){
	 	$personnel = $this -> db -> select( '*' )-> from( 'personnel' )
		-> order_by( 'personnel_id', 'desc' )
		-> limit( 1 ) -> get();
		
		if( $personnel -> num_rows( )> 0 ){
			$personnel = $personnel -> row_array();
			return $personnel[ 'personnel_id' ] + 1;
		}
		return 1; 
	 }
	 /*
	  * 
	  * 
	  */
	  function search_patient( $query ){
	  	
	  }
	  
	  /*
	   * 
	   *
	   *  
	   */
	   function get_prescriptions_dates( $patient_id, $limit ){
	   	$result = $this -> db  
	   				-> distinct()
	   				-> select( 'date(date) as date' )
					-> from( 'prescriptions' )
					-> where( 'patient_id' ,$patient_id )
					-> limit(5,$limit)
					-> order_by( 'date(date)', 'desc' )
					#-> group_by( 'date(date)')
					-> get(); 
					 
		if( $result -> num_rows() > 0){
			return $result -> result_array();
		}			
		return NULL;
	   }
	   
	   /*
	    * 
	    */
	    function get_medications_by_date( $patient_id, $date ){
	    	$result = $this -> db -> select( array('date( date) as date','disease_name', 'dose', "concat(active_component,' ',
		    	dose,' ',unit,' ',form) as medicament" ))
		    	-> from('prescription_lines')
				-> join('prescriptions','prescriptions.prescription_id = prescription_lines.prescription_id')
				-> where('prescriptions.patient_id',$patient_id)
				-> where( 'date(date)',$date)
				-> join ( 'medicaments','medicaments.medicament_id = prescription_lines.medicament_id')
				-> join ('drug_dose_units',' drug_dose_units.unit_id = prescription_lines.unit_id ')
				-> join ('drug_forms','drug_forms.form_id = prescription_lines.form_id')
				-> join ('prescriptions_has_patients_diagnosis','prescriptions_has_patients_diagnosis.prescription_id = prescriptions.prescription_id','left')
				-> join ('diseases_patient_diagnosis','diseases_patient_diagnosis.diagnosis_id = prescriptions_has_patients_diagnosis.diagnosis_id', 'left')
				-> join ('diseases','diseases.disease_id = diseases_patient_diagnosis.disease_id','left' )
				-> get();
				#echo $this -> db -> last_query();
			if( $result -> num_rows() > 0 ){
				return $result -> result_array();
			}
			return NULL;
	    }
}