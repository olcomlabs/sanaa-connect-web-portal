<?php if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
/*
 * 
 * 
 * 
 */
 class Humanresources_model extends Olcomhms
 {
     
     var $return_codes ;
    function __construct()
    {
        parent:: __construct();
        
        $this -> load -> config ('olcomhms_return_codes');
        $this -> return_codes = $this -> config -> item( 'return_codes');
        
        if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
        
    }  
    /*
     * 
     * 
     * 
     */
     function get_departments( $department_id = NULL )
     {
         if( $department_id == NULL )
         {
             $result  = $this -> read( 'departments' ,array( 'department_id' ,'department_name' ) ,NULL );
             if( $result != NULL )
             {
                 return olcom_db_to_dropdown( $result );
             } 
                 
         }
         if( $department_id != NULL )
         {
            $result = $this -> read( 'departments' , array( 'department_id' , 'department_name' ) , array( 'department_id' => $department_id ) );
            if( $result != NULL )
            {
                return $result -> row_array();
            }   
          
         }
         return NULL; 
     }
     /*
      * 
      * 
      */
      function get_jobs( $job_id = NULL )
      {
          
          if( $job_id != NULL )
          {
              $result = $this -> read( 'jobs' , '*' , array( 'job_id' => $job_id ));
              if( $result != NULL )
              {
                  return $result -> row_array();
              }
              
             return NULL;
          }
          $result = $this -> read( 'jobs' , '*' , NULL );
           if( $result != NULL )
              {
                  return $result -> result_array();
              }
              return NULL;
      }
      /*
       * 
       * 
       * 
       */
        function get_organizations($organizationId=null){
          if( $organizationId == null){
            $result = $this->db->select('*')->get('organization');
             
            return $result->result_array();
          }
          else{
            $result = $this->read('organization','*',array('organizationId' => $organizationId));
            return $result->row_array();
          }
          return null;
        }    
 }