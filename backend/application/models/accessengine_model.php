<?php if( ! defined('BASEPATH')) exit('Direct access is restricted');
/*


*/
class AccessEngine_model extends  CI_Model{

    function AccessEngine_model(){
        parent::__construct();
        
    }
	/*
		@param username
		@param password

		return group id is user is admin or null if not
	*/
	function get_user_info($username,$password){

        $this  ->  db  ->  select('group_id');
        $this  ->  db  ->  where('username',$username);
        $this  ->  db  ->  where('password',$password);
        $this  ->  db  ->  limit(1);
        
		$result_admin = $this  ->  db  ->  get('administrators');
       /* $fp=fopen('/var/www/lastq.txt','w+');
        fwrite($fp,$this  ->  db  ->  last_query());
        fclose($fp);
        */
        if($result_admin !== NULL ){
            if($result_admin  ->  num_rows() === 1){
                 return $result_admin -> row_array();
            }
           
        }
        
        $this  ->  db  ->  select(array('group_id','employee_id','active'));
        $this  ->  db  ->  where('username',$username);
        $this  ->  db  ->  where('password',$password);
        $this  ->  db  ->  limit(1);
            
        $result_user = $this  ->  db  ->  get('users');
       // $fp=fopen('/var/www/lastq.txt','w+');
       // fwrite($fp,$this  ->  db  ->  last_query());
        //fclose($fp);
		 
      if( $result_user !== NULL ){
          
           if($result_user  ->  num_rows() === 1){ 
                 return $result_user -> row_array();
           }
           
    
        }
        //probably not registered
		return NULL;
	}
    /*
     * 
     * 
     * 
     * 
     */
    function log_activity($type,$data,$group_id){
        
        $table = $type === 'admin' ? 'administrators' : 'users';
        $this  ->  db  ->  where('group_id',$group_id);
        $data['last_activity'] = date('Y-m-d H:m:s',$data['last_activity']);
        
        $this  ->  db  ->  update($table,$data);
        if($this  ->  db  ->  affected_rows() === 1){
            return 1;
        }else{
            return 0;
        }
    }
    /*
     * 
     * 
     * 
     */
     function get_modules(){
         $this  ->  load  ->  config('olcom_access_engine');
         $access_config = $this  ->  config  ->  item('access_engine');
         $modules = array();
         $modules_sub_modules = array();
         
        
         //retrieve module names and module ids
         $this  ->  db  ->  select(array('module_id','module_name'));
         $this  ->  db  ->  order_by('module_id','asc');
         $result_modules = $this  ->  db  ->  get('modules');
             
         if($result_modules != NULL AND $result_modules  ->  num_rows() > 0){
               //result to drop down like
               $modules = olcom_db_to_dropdown($result_modules);
                  
                
                 foreach($modules as $module_id => $module_name){
                     
                     //retrieve respective sub modules
                     $this  ->  db  ->  select(array('sub_module_id','sub_module_name'));
                     $this  ->  db  ->  order_by('sub_module_id','asc');
                     $this  ->  db  ->  where('module_id',$module_id,FALSE);
                     $result_sub_modules = $this  ->  db  ->  get('sub_modules');
                
                     $modules_sub_modules[$module_name] = olcom_db_to_dropdown($result_sub_modules);
                 }
                 
                 return $modules_sub_modules;
             }

            return NULL;
             
         
     }
    /*
     * 
     * 
     * 
     * 
     * 
     */
     function get_group_permissions( $group_id ){
         $permissions = NULL;
         $this  ->  db  ->  select(array('read','write','delete','create','module_name','modules.module_id'));
         $this  ->  db  ->  from(array('modules','module_permissions'));
         $this  ->  db  ->  where('modules.module_id','module_permissions.module_id',FALSE);
         $this  ->  db  ->  where('group_id',$group_id,FALSE);
         
         $module_permissions = $this  ->  db  ->  get();
         /*$handle = fopen('/var/www/perms.txt','w+');
         fwrite($handle,$this -> db -> last_query());
         fclose($handle);*/
         if($module_permissions  ->  num_rows() > 0){
             $module_permissions = $module_permissions  ->  result_array();
             $permissions = array();
             
            
             foreach($module_permissions as $key => $module_permission){
                 
                 
                 $permissions[$module_permission['module_name']] = array('read' => $module_permission['read'],
                 'write' => $module_permission['write'], 'create' => $module_permission['create'], 'delete' => $module_permission['delete']);
                 
                 //get sub module permissions
                 $this  ->  db  ->  select(array('read','write','delete','create','sub_module_name'));
                 $this  ->  db  ->  from(array('sub_modules','sub_module_permissions'));
                 $this  ->  db  ->  where('sub_modules.sub_module_id','sub_module_permissions.sub_module_id',FALSE);  
                 $this  ->  db  ->  where('sub_modules.module_id',$module_permission['module_id'],FALSE);
                 $this  ->  db  ->  where('sub_module_permissions.group_id',$group_id,FALSE);
                 
                 $sub_module_permissions = $this  ->  db  ->  get();
                 /*$handle = fopen('/var/www/perms.txt','w+');
                 fwrite($handle,$this -> db -> last_query());
                 fclose($handle);*/
                 if($sub_module_permissions  ->  num_rows() > 0){
                     $sub_modules = array();
                     $sub_module_permissions = $sub_module_permissions  ->  result_array();
                     $i = 0;
                     foreach ($sub_module_permissions as $sub_module_permission) {
                                $i++;
                            $sub_modules[$sub_module_permission['sub_module_name']] = array('read' =>$sub_module_permission['read'],
                                'write' => $sub_module_permission['write'], 'delete' => $sub_module_permission['delete'],
                                'create' => $sub_module_permission['create']
                            );    
                     }
                     
                    $permissions[$module_permission['module_name']]['sub_modules'] = $sub_modules;
                    
                  
                 }
             }
             return $permissions;
         }
         else{
             return NULL;
         }   
     }
    /*
     * @params groud_id , $module_sub_module,module | sub_module
     * 
     * 
     * 
     */
     function get_group_module_permissions($group_id,$module_sub_module_name,$mod_sub){
         
         
         switch($mod_sub){
             case 'module':
                    $this -> db -> select(array('read','write','create','delete'));
                    $this -> db -> from(array('modules,module_permissions'));
                    $this -> db -> where('modules.module_name',$module_sub_module_name);
                    $this -> db -> where('module_permissions.module_id','modules.module_id',FALSE);
                    $this -> db -> where('module_permissions.group_id',$group_id);
                    
                    $result = $this -> db -> get();
                   /* $handle = fopen('/var/www/perm-module.log','w');
                   fwrite($handle,$this->db->last_query());
                    fclose($handle);*/
                    if($result -> num_rows() >0){
                       
                       return $result -> row_array();
                    }
                    return NULL; 
                 break;
                 case 'sub_module':
                    $this -> db -> select(array('read','write','create','delete'));
                    $this -> db -> from(array('sub_modules,sub_module_permissions'));
                    $this -> db -> where('sub_modules.sub_module_name',$module_sub_module_name);
                    $this -> db -> where('sub_module_permissions.sub_module_id','sub_modules.sub_module_id',FALSE);
                    $this -> db -> where('sub_module_permissions.group_id',$group_id);
                    
                    $result = $this -> db -> get();
                    /*$handle = fopen('/var/www/perms.log','w');
                    fwrite($handle,$this->db->last_query());
                    fclose($handle);*/
                    if($result -> num_rows() >0){
                       
                        return $result -> row_array();
                    }
                    return NULL; 
                    
                 break;
         }   
     }
    /*
     * 
     * 
     * 
     */
     function get_profile($personnel_id) 
     {
        $result = $this->db->query("SELECT * FROM personnel JOIN employees ON ".
            "personnel.personnel_id=employees.employee_id JOIN departments ON departments.department_id=employees.department_id JOIN organization ON departments.organizationId = organization.organizationId WHERE personnel.personnel_id=".$personnel_id);
         
        return $result->row_array();
     }
}