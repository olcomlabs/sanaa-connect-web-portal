<?php if( ! defined( 'BASEPATH' )) exit( 'Direct access is restricted' );
/*
 * 
 * 
 * 
 */
 class Notifications_model extends Olcomhms
 {
     
     var $return_codes ;
    function __construct()
    {
        parent:: __construct();
        
        if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));            
    }
    /*
     * 
     * 
     */
     function get_module_id_byname( $module_name )
     {
         if( $module_name != NULL )
         {
             $result = $this -> read( 'modules' , '*' , array( 'module_name' => $module_name ), 'and' );
             
             if( $result == NULL )
             {
                 return NULL;
             }
             $result = $result -> row_array();
             return $result[ 'module_id'];
         }
         return NULL;
     }
     /*
     * 
     * 
     */
     function get_submodule_id_byname( $sub_module_name )
     {
         if( $sub_module_name != NULL )
         {
             $result = $this -> read( 'sub_modules' , '*' , array( 'sub_module_name' => $sub_module_name ), NULL );
             
             if( $result == NULL )
             {
                 return NULL;
             }
             $result = $result -> row_array();
             return $result[ 'sub_module_id'];
         }
         return NULL;
     }
     /*
      * 
      * 
      * 
      */
      function get_submodule_readgroups( $submodule_id )
      {
          if(  $submodule_id != NULL )
          {
              $result = $this -> read( 'sub_module_permissions' , array( 'group_id' ) , array( 'sub_module_id' => $submodule_id , 'read' => 1 ) , NULL );
              if( $result == NULL )
              {
                  return NULL;
              }
              $group_ids = array();
              foreach( $result -> result_array()  as $row )
              {
                  $group_ids[] = $row[ 'group_id' ];
              }
              return $group_ids;
          }
          return NULL;
      }
      /*
       * 
       * 
       * 
       */
       function create_notification( $notification_data )
       {
           if( $notification_data != NULL )
           {
               if( $this -> create( 'notifications', $notification_data ) != 0 )
               {
                   return SUCCESS_CODE;
               }
               return NULL;
           }
           return NULL;
       }
       /*
        * 
        * 
        * 
        */
        function create_users_notifications( $note_id , $users )
        {
            if( $note_id != NULL AND $users != NULL )
            {
               foreach( $users as $user )
               {
                   if( $this -> create('notifications_has_users', array(
                        'note_id' => $note_id , 
                        'users_employee_id'  => $user
                   )) != 0 )
                   {
                       continue;
                   }
               }
               
               return SUCCESS_CODE;
            }
            return NULL;
        }
        /*
         * 
         * 
         */
         function get_user_notifications( $employee_id )
         {
             if( $employee_id != NULL  )
             {
                 $result = $this -> read( 'notifications_has_users' , '*', array( 'users_employee_id' => $employee_id ) );
                 if( $result != NULL )
                 {
                     return $result -> result_array();
                 }
                 return NULL;
             }
             return NULL;
         }
         /*
          * 
          * 
          * 
          */
          function get_notification( $note_id )
          {
              if( $note_id != NULL )
              {
                  $this -> db -> where( array( 'note_id' => $note_id ) )
                    -> order_by( 'note_id' , 'desc' );
                    
                  $result = $this -> db -> get(  'notifications' );
                  
                  if( $result != NULL )
                  {
                      return $result -> row_array( );
                  }
                  return NULL;
              }
              return NULL;
          }
 } 