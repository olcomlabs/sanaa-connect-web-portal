<?php if( ! defined('BASEPATH')) exit( 'DIrect access is restricted');

class Reports_model extends Olcomhms {
    
    var $error_codes;
    
    function Reports_model(){
        parent:: __construct();
        
        $this -> load -> config ('olcomhms_return_codes');
        $this -> error_codes = $this -> config -> item( 'error_codes');
        
       if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
        
    }
    /*
     * 
     * 
     * 
     */
     function get_prescriptions( $start_date , $end_date , $limit )
     {
          
         $end_date = explode( '-' , $end_date );
         $end_date[ 0 ] = $end_date[ 0 ] + 1;
         $end_date = implode('-' ,$end_date );
          
         $result = $this -> db -> where( array( 'date >=' => $start_date , 'date <=' => $end_date ) )
            -> limit ( $limit )
            -> get( 'prescriptions' );
            
         if( $result -> num_rows() > 0 )
         {
             return $result -> result_array( );
         }
         
         return NULL;   
     }   
}
