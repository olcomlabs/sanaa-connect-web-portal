<?php if( ! defined('BASEPATH')) exit( 'DIrect access is restricted');

class Store_model extends Finance_model {
   /*
    * @author - Joseph Luvanda
    * @Package - Olabs Apollo HIS
    * @Contact - lvandazz@gmail.com
    */
 	   
 	   function add_supplier( $supplier ){
 	   	
			
 	   		if( $this -> create('supplier', $supplier) != 0 ){
 	   			return SUCCESS_CODE;
 	   		}
			else{
				return 170;
			}
 	   }
	   /*
	    * 
	    */
	    function get_suppliers($criteria = NULL){
	    	if( $criteria == NULL ){
	    		$result = $this -> read('supplier', '*', NULL);
				
				if( $result != NULL )
					return $result-> result_array();
				return NULL;
	    	}
			else{
				return $this -> read( 'supplier','*',$criteria,'and') -> result_array();
			}
	    }
		/*
		 * 
		 */
		 function update_supplier( $supplier_id, $details ){
		 	
			if( ! $this -> record_exists('supplier', array( 'supplier_id !=' => $supplier_id, 'name' => $details[ 'name' ] ),FALSE,TRUE,'and')){
				if( $this -> edit('supplier', $details, array( 'supplier_id' => $supplier_id )) == 0 ){
					return 110;
				}
				return SUCCESS_CODE; 
			}
			return 108;
		 }
		 /*
		  * 
		  */
		  function create_item( $item_data ){
		  	if( $this -> create( 'support_item', $item_data ) !=  NULL ){
		  		return SUCCESS_CODE;
		  	}
			return 176;
			
		  }
		  /*
		   * 
		   */
		   function get_support_items( $item_id = NULL , $full = FALSE){
		   	if( $item_id != NULL ){
		   		$result = $this -> read( 'support_item', '*', array( 'item_id' => $item_id ) ,'and');
				return $result -> row_array();
		   	}
			$result = $this -> read('support_item', '*', NULL );
			if( $result == NULL ){
				return NULL;
			}
			return $result -> result_array();
		   }
			/*
			 * 
			 */
			 function create_item_stock( $stock_data ){
			 	if( $this -> create( 'item_stock', $stock_data ) != NULL ){
			 		return SUCCESS_CODE;
			 	}
				return 176;
			 }
		/*
		 * 
		 * 
		 */
		 function create_temp_item_line( $item_data ){
		 	//clean up
		 	if( $this -> record_exists('item_line_temp', array( 'employee_id' => $item_data[ 'employee_id' ],
				'write_start_timestamp !=' => $item_data[ 'write_start_timestamp' ]		
			),FALSE, TRUE,'and' ) == TRUE ){
				$this -> delete('item_line_temp', array( 'employee_id' => $item_data[ 'employee_id' ],'write_start_timestamp !=' => $item_data[ 'write_start_timestamp' ] ) );
			}
			if( $this -> record_exists('item_line_temp', array( 'employee_id' => $item_data[ 'employee_id' ], 'item_id' => $item_data[ 'item_id' ] ), FALSE,TRUE,'and')) {
				return 108;	
			} 
			return $this -> create('item_line_temp', $item_data );
			
		 }
		 /*
		  * 
		  */
		  function get_temp_item_line( $line_id ){
		  	$result  = $this -> read('item_line_temp', '*', array( 'line_id' => $line_id ) );
			if($result != NULL ){
				return $result -> row_array();
			}
			return NULL;
		  }
		  /*
		   * 
		   */
		   function edit_temp_line( $line_id , $line_data ){
		   		return  $this -> edit( 'item_line_temp' , $line_data, array( 'line_id' => $line_id ) );
		   }
		   /*
		    * 
		    */
		    function create_item_order( $order_data ){
		    	
				$result = $this -> create('item_order', $order_data );
				if( $result == NULL ){
					return -1;
				}
				return $result;
		    }
			/*
			 * 
			 */
			 function get_temp_items( $data ){
			 	$result = $this -> read( 'item_line_temp', '*',$data,'and' );
				if( $result == NULL ){
					return NULL;
				}
				return $result -> result_array();
			 }
			/*
			 * 
			 * 
			 */
			 function deduct_stock_item( $item_id,$stock_id,$qty){
			 	
			 	$result = $this -> db 
						-> set( 'quantity', "quantity-$qty",FALSE)
						-> where( array( 'item_id' => $item_id, 'stock_id' => $stock_id ))
						-> update( 'item_stock' );
						 
				if( $result ){
					return $stock_id;
				}		
				return -1;

			 }
			/*
			 * 
			 * 
			 */
			 function create_item_line( $line_data ){
			 	
			 	$result = $this -> create('item_line', $line_data );
				if( $result == NULL  ){
					return -1;
				}
				return $result;
			 }
			 /*
			  * 
			  * 
			  */
			  function delete_temp_item_line( $line_id ){
			  	return $this -> delete('item_line_temp', array( 'line_id' => $line_id ) );
			  }
			 /*
			  * 
			  */
			  function get_item_stock( $item_id ){
			  	$result = $this -> read( 'item_stock','*',array( 'item_id' => $item_id ,'is_discarded' => 0), 'and' );
				if( $result != NULL ){
					return $result -> result_array();
				}
				return NULL;
			  }
			  /*
			   * 
			   */
			   function get_item_order( $order_id ){
			   	 $result = $this -> read( 'item_order', '*', array( 'order_id' => $order_id ), 'and' );
				 return $result -> row_array();
			   }
			   /*
			    * 
			    */
			    function get_item_order_lines( $order_id ){
			    	$result = $this -> read( 'item_line', '*',array( 'order_id' => $order_id ), 'and' 	);
					if( $result != NULL ){
						return $result -> result_array();
					}
					return NULL;
			    }
				/*
				 * 
				 */
				 function update_support_item( $item_id,$data ){
				 	if( $this -> record_exists('support_item', array( 'item' => $data['item' ],'item_id !=' => $item_id ),FALSE,TRUE, 'and' ) == TRUE  ){
				 		return 108;
				 	}
					if( $this -> record_exists('item_line' , array( 'item_id' => $item_id ), FALSE,TRUE,'and' ) == TRUE ){
				 		return 177;
				 	}
					if( $this -> record_exists('bill_lines',array( 'particular_id' => $item_id ),FALSE,TRUE,'and' ) == TRUE ){
						return 177;
					}
					if( $this -> record_exists('item_stock', array( 'item_id' => $item_id ),FALSE,TRUE, 'and' ) == TRUE ){
						return 117;
					}
					$this -> edit('support_item', $data, array( 'item_id'  => $item_id));
					return SUCCESS_CODE;
				 }
				/*
				 * 
				 * 
				 */
				 function delete_support_item( $item_id ){
				 	if( $this -> record_exists('item_line' , array( 'item_id' => $item_id ), FALSE,TRUE,'and' ) == TRUE ){
				 		return 177;
				 	}
					if( $this -> record_exists('bill_lines',array( 'particular_id' => $item_id ),FALSE,TRUE,'and' ) == TRUE ){
						return 177;
					}
					if( $this -> record_exists('item_stock', array( 'item_id' => $item_id ),FALSE,TRUE, 'and' ) == TRUE ){
						return 117;
					}
					$this -> delete('support_item' , array( 'item_id' => $item_id ) );
					return SUCCESS_CODE;
				 }
				 /*
				  * 
				  */
				  function delete_item_order( $order_id ){
				  	if( $this -> delete('item_order', array( 'order_id' => $order_id ) ) ){
				  		return SUCCESS_CODE;
				  	}
					return -1;
				  }
				/*
				 * 
				 */
				 function restore_item_stock( $stock_id , $qty ){
				 	$result = $this -> db 
						-> set( 'quantity', "quantity+$qty",FALSE)
						-> where( array( 'stock_id' => $stock_id ))
						-> update( 'item_stock' );
						 
					if( $result ){
						return $stock_id;
					}		
					return -1;
				 }
				 /*
				  * 
				  * 
				  */
				  
				 function delete_supplier( $supplier_id ){
				 	 if( $this -> record_exists('item_stock' , array( 'supplier_id' => $supplier_id ), FALSE,TRUE,'and' ) == TRUE ){
				 		return 177;
				 	}
					 if( $this -> record_exists('packages' , array( 'supplier_id' => $supplier_id ), FALSE,TRUE,'and' ) == TRUE ){
				 		return 177;
				 	}
					$result =  $this -> delete( 'supplier', array( 'supplier_id' => $supplier_id ) ) ;
					if( $result != 0 ){
						return SUCCESS_CODE;
					}
					return 110;
					
				 }
				  
}	
?>