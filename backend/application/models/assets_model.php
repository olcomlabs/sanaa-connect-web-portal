<?php if( ! defined( 'BASEPATH')) exit( 'Direct access is resstricted' );
/*
 * 
 * 
 * 
 * 
 */
 class Assets_model extends Olcomhms
 {
     
     function Assets_model()
     {
         parent :: __construct();
         $this -> load -> config ('olcomhms_return_codes');
         $this -> error_codes = $this -> config -> item( 'error_codes');
        
         if( ! defined ( 'SUCCESS_CODE' ))
            define ('SUCCESS_CODE',$this -> config -> item('success_code'));
     }
     
    /*
     * 
     * 
     * 
     */
     function create_asset_category( $category_data )
     {
         if( $category_data != NULL )
         {
             if( $this -> record_exists('asset_categories', $category_data,FALSE,TRUE,'and') == FALSE )
               {
                 if( $this -> create('asset_categories', $category_data ) !== NULL )
                 {
                     return SUCCESS_CODE;
                 }
                 else{
                     return 125;
                 }
               }
               return 108;
         }
         return 104;
     } 
     /*
     * 
     * 
     * 
     */
     function create_asset_status( $status_data )
     {
         if( $status_data != NULL )
         {
             if( $this -> record_exists('assets_status', $status_data,FALSE,TRUE,'and') == FALSE )
               {
                 if( $this -> create('assets_status', $status_data ) !== NULL )
                 {
                     return SUCCESS_CODE;
                 }
                 else{
                     return 125;
                 }
               }
               return 108;
         }
         return 104;
     }
     /*
      * 
      * 
      */
      function get_asset_category( $category_id  = NULL )
      {
          if( $category_id != NULL )
          {
              $result = $this -> read('asset_categories', '*', array( 'category_id' => $category_id ) ) ;
              if( $result !== NULL )
              {
                  return $result -> row_array();
              }
              return 126;
          }
          $result = $this -> read( 'asset_categories', '*',NULL );
          if( $result != NULL )
          {
              return olcom_db_to_dropdown( $result );
          }
          else {
              return NULL;
          }
          
      }
      /*
       * 
       * 
       */
       function update_asset_category( $category_id , $category_data )
       {
           if( $category_id != NULL AND $category_data != NULL )
           {
               
               $result = $this -> read( 'asset_categories','*',$category_data,NULL );
               if( $result != NULL )
               {
                   $result = $result -> row_array();
                   if( $result[ 'category_id' ] != $category_id )
                   {
                       return 108;
                   }
               }
               
               //update 
               if( $this -> edit('asset_categories', $category_data, array( 'category_id' => $category_id ) ) != 0 )
               {
                   return SUCCESS_CODE;
               }
               return 110;
           }
           return 104;
       } 
       /*
        * 
        * 
        * 
        */
        function delete_asset_category( $category_id )
        {
            if( $category_id != NULL )
            {
                
                if( $this -> delete('asset_categories', array( 'category_id' => $category_id ) ) != 0 )
                {
                    return SUCCESS_CODE;
                }
                return 110;
            }
            return 104;
        } 
        /*
         * 
         * 
         */
         function get_asset_status( $status_id = NULL )
         
         {
             
             if( $status_id  !== NULL )
             {
                 $result = $this -> read( 'assets_status' , '*',array( 'status_id' => $status_id ) ,NULL );
                 if( $result != NULL )
                 {
                     return $result -> row_array();
                 }
                 return 127;
             }
             $result = $this -> read( 'assets_status' , '*',NULL );
             if( $result != NULL )
             {
                 return olcom_db_to_dropdown( $result );
             }
             return NULL;
         }
         /*
          * 
          * 
          */
          function update_asset_status( $status_id , $status_data )
          {
              if( $status_data != NULL AND $status_id != NULL )
              {
                
               
               $result = $this -> read( 'assets_status','*',$status_data,NULL );
               if( $result != NULL )
               {
                   $result = $result -> row_array();
                   if( $result[ 'status_id' ] != $status_id )
                   {
                       return 108;
                   }
               }
               
               //update 
               if( $this -> edit('assets_status', $status_data, array( 'status_id' => $status_id ) ) != 0 )
               {
                   return SUCCESS_CODE;
               }
               return 110;
           }
           return 104;
     
          }
          /*
           * 
           * 
           * 
           */
           function delete_asset_status( $status_id )
           {
               if( $status_id != NULL ) 
               {
                   if( $this -> delete( 'assets_status',array( 'status_id' => $status_id ) ) )
                   {
                       return SUCCESS_CODE;
                   }
                   return 110;
               }
               return 104;
           }
           /*
            * 
            * 
            */
            function create_maintenance_party( $party_data )
            {
                if( $party_data != NULL )
                {
                    if( $this -> record_exists('third_parties', array( 'name' => $party_data[ 'name' ] ,'phone' => $party_data[ 'phone' ] ),FALSE, TRUE, 'or') == FALSE )
                    {
                        if( $this -> create('third_parties', $party_data ) != 0 )
                        {
                            return SUCCESS_CODE;
                        }
                        return 128;
                    }
                    return 108;
                }
                return 104;
            }
            /*
             * 
             * 
             */
             function get_party_info( $party_id = NULL )
             {
                 if( $party_id != NULL ){
                     $result = $this -> read( 'third_parties' , '*', array( 'party_id' => $party_id ) ,NULL );
                     if( $result == NULL )
                     {
                         return 129;
                     }
                     return $result -> row_array();
                 }
                 $result = $this -> read( 'third_parties' , '*', NULL );
                 if( $result != NULL )
                 {
                     return $result -> result_array();
                 }
                 return NULL;
             }
             /*
              * 
              * 
              */
              function update_maintenance_party( $party_id , $party_data ) 
              {
                  if( $party_data != NULL AND $party_id != NULL )
                  {
                      if( $this -> record_exists( 'third_parties', array( 'name' => $party_data[ 'name' ] , 'phone' => $party_data[ 'phone' ] , 'party_id !=' => $party_id ),FALSE,TRUE, 'and' ) == FALSE )
                      {
                          if( $this -> edit( 'third_parties', $party_data, array( 'party_id' => $party_id ) ) != 0 )
                          {
                              return SUCCESS_CODE;
                          }
                          return 110;
                      }
                      return 108;
                  }
                  return 104;
              }
              /*
               * 
               * 
               */
               function delete_party( $party_id  )
               {
                   if( $party_id != NULL )
                   {
                       if( $this -> delete('third_parties' , array( 'party_id' => $party_id ) ) != 0 )
                       {
                           return SUCCESS_CODE;
                       }
                       return 110;
                   }
                   return 104;
               }
               /*
                * 
                * 
                * 
                */
                function create_maintenance_schedule( $schedule_data )
                {
                    if( $schedule_data != NULL )
                    {
                        if( $this -> record_exists('maintenance_schedule',array( 'asset_category_id' => $schedule_data[ 'asset_category_id' ]),FALSE, TRUE , 'and' ) == FALSE )
                        {
                            if( $this -> create('maintenance_schedule' , $schedule_data ) != 0 )
                            {
                                return SUCCESS_CODE;
                            }
                            return 130;
                        }
                        return 108;
                    }
                    return 104;
                }
                /*
                 * 
                 * 
                 */
                 function get_maintenance_schedule( $schedule_id = NULL , $category_id = NULL )
                 {
                     if( $schedule_id != NULL )
                     {
                         $result = $this -> read('maintenance_schedule', '*', array( 'schedule_id' => $schedule_id ), NULL );
                         if( $result != NULL )
                         {
                             $result = $result -> row_array();
                             // asset category 
                             $result_asset_category = $this -> get_asset_category( $result[ 'asset_category_id' ] ); 
                             $result = array_merge( $result , $result_asset_category );
                             
                             return $result;
                         }
                         return NULL;
                     }
                     if( $category_id != NULL )
                     {
                         $result = $this -> read('maintenance_schedule', '*', array( 'asset_category_id' => $category_id ), NULL );
                         if( $result != NULL )
                         {
                             $result = $result -> row_array();
                             // asset category 
                             $result_asset_category = $this -> get_asset_category( $result[ 'asset_category_id' ] ); 
                             $result = array_merge( $result , $result_asset_category );
                             
                             return $result;
                         }
                         return NULL;
                     }
                     return 104;
                 }
                 /*
                  * 
                  * 
                  */
                  function update_maintenance_schedule( $shedule_id , $schedule_data )
                  {
                      if( $schedule_data != NULL AND $shedule_id != NULL )
                      {
                          unset( $schedule_data[ 'asset_category_id'] );
                          if( $this -> edit( 'maintenance_schedule' ,$schedule_data, array( 'schedule_id' => $shedule_id ) ) != 0 )
                          {
                              return SUCCESS_CODE;
                          }
                          return 110;
                      }
                      return 104;
                  }
                   /*
               * 
               * 
               */
               function delete_schedule( $schedule_id  )
               {
                   if( $schedule_id != NULL )
                   {
                       if( $this -> delete('maintenance_schedule' , array( 'schedule_id' => $schedule_id ) ) != 0 )
                       {
                           return SUCCESS_CODE;
                       }
                       return 110;
                   }
                   return 104;
               }
               /*
                * 
                * 
                * 
                */
                function create_asset( $asset_data )
                {
                    if( $asset_data != NULL )
                    {
                        if( $this -> record_exists('assets', array( 'serial_number' => $asset_data[ 'serial_number' ]   ), FALSE,TRUE,'and' ) == FALSE )
                        {
                            $asset_department_data = array();
                            $asset_department_data[ 'date_located' ] = $asset_data[ 'date_located' ];
                            $asset_department_data[ 'department_id'] = $asset_data[ 'department_id' ];
                            
                            unset( $asset_data[ 'date_located'] );
                            unset( $asset_data[ 'department_id' ] ) ;
                            
                            if( $insert_id = $this -> create('assets', $asset_data ) != 0 )
                            {
                              
                                $asset_department_data[ 'assets_serial_number' ] = $asset_data[ 'serial_number' ];
                                // create asset departmet / location 
                                if( $this -> create( 'assets_departments', $asset_department_data) != 0 )
                                {
                                    return SUCCESS_CODE;
                                }
                            }
                            return 132;
                        }
                        return 134;
                    }
                    return 104;
                }   
            /*
             * 
             * 
             * 
             */
             function get_assets( $asset_id = NULL , $category_id = NULL  )
             {
                 
                 if( $asset_id == NULL )
                    {
                        $result = $this -> read('assets' ,'*',NULL  );
                        if( $result != NULL )
                        {
                            return $result -> result_array();
                        }
                        return NULL;
                    }
                    
                  if( $category_id != NULL )
                  {
                      $result = $this -> read( 'assets' , '*' , array( 'category_id' => $category_id ) ,NULL );
                      if( $result != NULL )
                      {
                          return $result -> result_array();
                      }
                      return NULL;
                  }
             }
             /*
              * 
              * 
              */
              function create_maintenance_record( $maintenance_data )
              {
                  if( $maintenance_data != NULL )
                  {
                        if( $this -> create( 'assets_maintenance', $maintenance_data ) != 0 )
                        {
                            return SUCCESS_CODE;          
                        }
                        return 132;   
                  } 
                  return 104;
              }
              /*
               * 
               * 
               */
               function get_maintenance_record( $maintenance_id  )
               {
                   if( $maintenance_id != NULL )
                   {
                       $result = $this -> read( 'assets_maintenance', '*', array( 'maintenance_id' => $maintenance_id ) , 'and');
                       
                       if( $result != NULL )
                       {
                           return $result -> row_array();
                       }
                       return NULL;
                   }
                   return 104;
               }
               /*
                * 
                * 
                * 
                */
                function get_last_asset_maintenance( $serial_number  )
                {
                    if( $serial_number != NULL )
                    {
                        $this -> db -> where( 'serial_number' , $serial_number )
                            -> order_by( 'maintenance_id' , 'desc' )
                            -> limit( 1 );
                         $result = $this -> db -> get( 'assets_maintenance' );
                          
                         if( $result -> num_rows() > 0 )
                         {
                            return $result -> row_array( );                             
                         }
                        
                    }
                    return NULL;
                } 
               /*
                * 
                * 
                * 
                */
                function update_maintenance_record( $maintenance_id , $maintenance_data )
                {
                    if( $maintenance_data != NULL AND $maintenance_id != NULL )
                    {
                        if( $this -> edit( 'assets_maintenance',$maintenance_data , array( 'maintenance_id' => $maintenance_id ) ) != 0 )
                        {
                            return SUCCESS_CODE;
                        }
                        return 110;
                    }
                    return 104;
                }
            /*
             * 
             * 
             * 
             */
             function delete_maintenance_record( $maintenance_id )
             {
                 if( $maintenance_id != NULL )
                 {
                     if( $this -> delete( 'assets_maintenance',array( 'maintenance_id' => $maintenance_id  ) ) != 0 )
                     {
                         return SUCCESS_CODE;
                     }
                     return 110;
                 }
                 return 104;
             }
             /*
              * 
              * 
              */
              function get_asset_info( $serial_number )
              {
                  if( $serial_number != NULL )
                  {
                      $result = $this -> read('assets', '*', array( 'serial_number' => $serial_number ) );
                      if( $result != NULL )
                      {
                          $result_dpt = $this -> read( 'assets_departments' , '*', array( 'assets_serial_number' => $serial_number ),'and' );
                          
                          $result_dpt = $result_dpt -> row_array( );
                          
                          $result_dpt = $this -> read( 'departments' , '*' , array( 'department_id' => $result_dpt[ 'department_id' ] ) , 'and' );
                          
                          $result = $result -> row_array();
                          $result = array_merge(   $result , $result_dpt -> row_array() );
                          
                          return $result;
                      }
                      return NULL;
                  }
                  return NULL;
              }
              /*
               * 
               * 
               */
               function get_asset_total_maintenance( $serial_number )
               {
                   if( $serial_number != NULL )
                   {
                       $this -> db -> select_sum( 'maintenance_cost' )
                        -> where( 'serial_number' , $serial_number );
                        
                       $result = $this -> db -> get( 'assets_maintenance' );
                   
                       if( $result -> num_rows() > 0 )
                       {
                           $result = $result -> row_array( );
                            return $result[ 'maintenance_cost' ];    
                       }
                       
                   }
                   return 0;
               }
               /*
                * 
                * 
                */
                function update_asset_info( $serial_number , $asset_data )
                {
                    if( $asset_data != NULL AND $serial_number != NULL ) 
                    {
                           if( $this -> record_exists('assets', array( 'serial_number' => $asset_data[ 'serial_number' ] , 'serial_number !=' => $serial_number ),FALSE,TRUE,'and' ) == FALSE )
                           {
                               $assets_dpts = array( 'department_id' => $asset_data[ 'department_id' ] , 'date_located' => $asset_data[ 'date_located' ] );
                               unset( $asset_data[ 'department_id' ] );
                               unset( $asset_data[ 'date_located' ] );
                               
                               $is_changed = FALSE;
                               if( $this -> edit('assets' , $asset_data , array( 'serial_number' => $serial_number ) ) != 0 )
                               {
                                   $is_changed = TRUE;
                               }
                               
                               if( $this -> edit( 'assets_departments' ,$assets_dpts ,array( 'assets_serial_number' => $serial_number ) ) != 0 )
                               {
                                   $is_changed = TRUE;
                               }
                               if( $is_changed == TRUE )
                               {
                                   return SUCCESS_CODE;
                               }   
                               else
                                   {
                                       return 110;
                                   }
                           }
                           return 134;
                    }
                    return 104;
                }

        /*
         * 
         * 
         * 
         */
         function delete_asset( $serial_number )
         {
             if( $serial_number != NULL )
             {
                 if( $this -> delete('assets_departments' , array( 'assets_serial_number' =>  $serial_number ) ) != 0 )
                 {
                     $this -> delete( 'assets' , array( 'serial_number' => $serial_number ) );
                     return SUCCESS_CODE;
                 }
                 return 110;
             }
             return 104;
         }
 }