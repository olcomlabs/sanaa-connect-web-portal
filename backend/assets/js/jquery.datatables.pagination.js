$.fn.dataTableExt.oPagination.four_button = {
    "fnInit": function ( oSettings, nPaging, fnCallbackDraw )
    {
        nFirst = document.createElement( 'span' );
        nPrevious = document.createElement( 'span' );
        nNext = document.createElement( 'span' );
        nLast = document.createElement( 'span' );
        
        //nPaging.addClass('paging_bootstrap pagination');
       
        
        nFirst.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sFirst ) );
        nPrevious.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sPrevious ) );
        nNext.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sNext ) );
        nLast.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sLast ) );
        
        
        nFirst.className = "paginate_button first";
        nPrevious.className = "paginate_button previous";
        nNext.className="paginate_button next";
        nLast.className = "paginate_button last";
          
        nPaging.appendChild( nFirst );
        nPaging.appendChild( nPrevious );
        nPaging.appendChild( nNext );
        nPaging.appendChild( nLast );
         
       	
        $(nFirst).click( function () {
        	 
        	$.cookie('display_start',0,{path:'/'});
        	 
        	//console.log($.cookie('display_start'));
        	$.extend(oSettings,{
        		_iDisplayStart:$.cookie('display_start')
        	});
           // oSettings.oApi._fnPageChange( oSettings, "first" );
            fnCallbackDraw( oSettings );
        });
          
        $(nPrevious).click( function() {
        	if($.cookie('display_start')==undefined || $.cookie('display_start')<0){
        		$.cookie('display_start',0,{path:'/'});
        	}
        	var previous_display_start=parseInt($.cookie('display_start')-oSettings._iDisplayLength);
        	
        	if(previous_display_start<0){
        		previous_display_start=0;
        	}
        	$.cookie('display_start',previous_display_start,{path:'/'});
        	
        	$.extend(oSettings,{
        		_iDisplayStart:$.cookie('display_start')
        	});
            //oSettings.oApi._fnPageChange( oSettings, "previous" );
            fnCallbackDraw( oSettings );
        } );
          
        $(nNext).click( function() {
        	if($.cookie('display_start')==undefined || $.cookie('display_start')<0){
        		$.cookie('display_start',0,{path:'/'});
        	}
        	$.cookie('display_start',(parseInt($.cookie('display_start'))+oSettings._iDisplayLength),{path:'/'});
        	
        	var total_records=oSettings.fnRecordsTotal();
        	if($.cookie('display_start')>total_records){
        		$.cookie('display_start',0,{path:'/'});
        	}
			 
        	$.extend(oSettings,{
        		_iDisplayStart:$.cookie('display_start')
        	});
            //oSettings.oApi._fnPageChange( oSettings, "next" );
            fnCallbackDraw( oSettings );
        } );
          
       $(nLast).click( function() {
        	if($.cookie('display_start')==undefined || $.cookie('display_start')<0){
        		$.cookie('display_start',0,{path:'/'});
        	}
        	//console.log('display_start');
        	var display_start_last=parseInt(oSettings.fnRecordsTotal()-oSettings._iDisplayLength);
        	display_start_last=display_start_last<0?0:display_start_last;
        	$.cookie('display_start',display_start_last,{path:'/'});
        	
        	$.extend(oSettings,{
        		_iDisplayStart:$.cookie('display_start')
        	});
           	//oSettings.oApi._fnPageChange( oSettings, "last" );
            fnCallbackDraw( oSettings );
        } );
         
        /* Disallow text selection */
        $(nFirst).bind( 'selectstart', function () { return false; } );
        $(nPrevious).bind( 'selectstart', function () { return false; } );
        $(nNext).bind( 'selectstart', function () { return false; } );
        $(nLast).bind( 'selectstart', function () { return false; } );
    },
     
     
    "fnUpdate": function ( oSettings, fnCallbackDraw )
    {
        if ( !oSettings.aanFeatures.p )
        {
            return;
        }
          
        /* Loop over each instance of the pager */
        var an = oSettings.aanFeatures.p;
        for ( var i=0, iLen=an.length ; i<iLen ; i++ )
        {
            var buttons = an[i].getElementsByTagName('span');
            if ( oSettings._iDisplayStart === 0 )
            {
                buttons[0].className = "prev disabled";
                buttons[1].className = "prev disabled";
            }
            else
            {
                buttons[0].className = "prev enabled";
                buttons[1].className = "prev enabled";
            }
              
            if ( oSettings.fnDisplayEnd() == oSettings.fnRecordsDisplay() )
            {
                buttons[2].className = "next disabled";
                buttons[3].className = "next disabled";
            }
            else
            {
                buttons[2].className = "next disabled";
                buttons[3].className = "next disabled";
            }
        }
    }
};