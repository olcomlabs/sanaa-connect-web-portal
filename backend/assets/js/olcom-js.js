
/*
 *
 * function show_spinner
 */

function show_spinner() {
	$('#olcom-form-spinner').show();
}

/*
 *
 * function hide_spinner
 */
function hide_spinner() {
	$('#olcom-form-spinner').hide();
}

///Validate name and name like fields
jQuery.validator.addMethod("olcom_valid_name", function(value, element) {

	return this.optional(element) || /^[A-Za-z]+$/.test(value);
}, "Invalid characters detected");

jQuery.validator.addMethod("olcom_valid_name_digits", function(value, element) {

    return this.optional(element) || /^[A-Za-z0-9]+$/.test(value);
}, "Invalid characters detected");

jQuery.validator.addMethod("olcom_valid_space_name", function(value, element) {
	
	return this.optional(element) || /^[A-Za-z]+\s+[A-Za-z]+$|^[A-Za-z]+\s+[A-Za-z]+\s+[A-Za-z]+$|^[A-Za-z]+$/.test(value);
}, "Invalid characters  or too many spaces detected");
jQuery.validator.addMethod("olcom_valid_admin_hours", function(value, element) {
    
    return this.optional(element) || /^([0-9]+\s*)+$|^(\d{2}:\d{2}\s*)+$/.test(value);
}, "Invalid characters  detected only integers and spaces allowed");

jQuery.validator.addMethod("olcom_valid_underscore_name", function(value, element) {
	
	return this.optional(element) || /^[a-z_]+$/.test(value);
}, "Invalid characters. Only alphabets and underscores allowed");

jQuery.validator.addMethod("olcom_valid_select", function(value, element) {
	 
 
	return this.optional(element)|| /^[A-Za-z0-9]+$/.test( value );
}, "This field is required");

jQuery.validator.addMethod("olcom_valid_amount", function(value, element) {
     
    if( value < 0 )
        return false;
    
    return this.optional(element)|| /^[A-Za-z]+$/.test( value );
}, "Please enter valid amount");

jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\d{4} \d{3} \d{3}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");

/*
 *
 * Form reset
 *
 */
$('#btn-reset').click(function() {
	$('#olcomhms-template-form')[0].reset();
	
	$('.olcom-chosen').each(function (){
		$(this).html('').trigger('liszt:updated');
	});

});

function form_submit_message(data) {
	if (data.success == 1) {
		$('#olcom-form-submit-status-message').html("<div class=\"alert alert-block alert-success\">" + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">" + "<i class=\"icon-remove\"></i>" + "</button>" + "<p>" + "<strong>" + "<i class=\"icon-ok\"></i>" + "Success!" + "	</strong></p><p>" + data.message + "</p>" + "</div>");
		
	}

	if (data.success == 0) {
		$('#olcom-form-submit-status-message').html("<div class='alert alert-error'>" + "<button type='button' class='close' data-dismiss='alert'>" + "<i class='icon-remove'></i>" + "</button>" + "<strong>" + "<i class=\"icon-remove\"></i>" + "Application error!" + "</strong></p><p>" + data.message + "</p><br />" + "</div>");
	}
}

//chosen 
$('.olcom-chosen').chosen();

