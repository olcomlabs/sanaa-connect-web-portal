<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
set_include_path('Google/');
require_once 'Google/autoload.php';
require_once 'Google/Client.php';
require_once 'Google/Service/YouTube.php';
#session_start();



class Services_Google {
	public $ci;
	public $client;

	function __construct(){
		$this->ci = &get_instance();
		$this->ci->load->config('google_config');
		$this->client =  new Google_Client();
		$this->ci = &get_instance();
		$this->ci->load->config('google_config');
		#$this->client =  new Google_Client();
		$OAUTH2_CLIENT_ID = $this->ci->config->item('g_client_id');
		$OAUTH2_CLIENT_SECRET = $this->ci->config->item('g_client_secret');

		 
		$this->client->setClientId($OAUTH2_CLIENT_ID);
		$this->client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$this->client->setScopes('https://www.googleapis.com/auth/youtube');
		$redirect = filter_var('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'], FILTER_SANITIZE_URL);
		$this->client->setRedirectUri($redirect);

	}

	function setCodeState($code,$state){
		
		$sState = $this->ci->session->userdata('state');
		if (strval($sState) !== strval($state)) {
		    die('The session state did not match.');
		  }

		  $this->client->authenticate($code);
		  $this->ci->session->set_userdata('token',$this->client->getAccessToken());
		
		  $sToken = $this->client->getAccessToken();#$this->ci->session->userdata('token');
		  if (isset($sToken)) {
		    $this->client->setAccessToken($this->ci->session->userdata('token'));
		  }

	}


	function youtube_upload($videoFile,$metadata){

		

		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($this->client);

		/*if (isset($_GET['code'])) {
			$sState = $this->ci->session->userdata('state');
		  if (strval($sState) !== strval($_GET['state'])) {
		    die('The session state did not match.');
		  }

		  $this->client->authenticate($_GET['code']);
		  $this->ci->session->set_userdata('token',$this->client->getAccessToken());
		  header('Location: ' . $redirect);
		}

		$sToken = $this->ci->session->userdata('token');
		if (isset($sToken)) {
		  $this->client->setAccessToken($this->ci->session->userdata('token'));
		}*/

		// Check to ensure that the access token was successfully acquired.
		if ($this->client->getAccessToken()) {
		  try{
		    // REPLACE this value with the path to the file you are uploading.
		    $videoPath = $videoFile; #"../cartoon.mp4";

		    // Create a snippet with title, description, tags and category ID
		    // Create an asset resource and set its snippet metadata and type.
		    // This example sets the video's title, description, keyword tags, and
		    // video category.
		    
		    $snippet = new Google_Service_YouTube_VideoSnippet();
		    $snippet->setTitle($metadata['title']);
		    $snippet->setDescription($metadata['description']);
		    $snippet->setTags($metadata['tags']);

		    // Numeric video category. See
		    // https://developers.google.com/youtube/v3/docs/videoCategories/list 
		    $snippet->setCategoryId("22");

		    // Set the video's status to "public". Valid statuses are "public",
		    // "private" and "unlisted".
		    $status = new Google_Service_YouTube_VideoStatus();
		    $status->privacyStatus = "public";

		    // Associate the snippet and status objects with a new video resource.
		    $video = new Google_Service_YouTube_Video();
		    $video->setSnippet($snippet);
		    $video->setStatus($status);

		    // Specify the size of each chunk of data, in bytes. Set a higher value for
		    // reliable connection as fewer chunks lead to faster uploads. Set a lower
		    // value for better recovery on less reliable connections.
		    $chunkSizeBytes = 1 * 1024 * 1024;

		    // Setting the defer flag to true tells the client to return a request which can be called
		    // with ->execute(); instead of making the API call immediately.
		    $this->client->setDefer(true);

		    // Create a request for the API's videos.insert method to create and upload the video.
		    $insertRequest = $youtube->videos->insert("status,snippet", $video);

		    // Create a MediaFileUpload object for resumable uploads.
		    $media = new Google_Http_MediaFileUpload(
		        $this->client,
		        $insertRequest,
		        'video/*',
		        null,
		        true,
		        $chunkSizeBytes
		    );
		    $media->setFileSize(filesize($videoPath));


		    // Read the media file and upload it chunk by chunk.
		    $status = false;
		    $handle = fopen($videoPath, "rb");
		    while (!$status && !feof($handle)) {
		      $chunk = fread($handle, $chunkSizeBytes);
		      $status = $media->nextChunk($chunk);
		    }

		    fclose($handle);

		    // If you want to make other calls after the file upload, set setDefer back to false
		    $this->client->setDefer(false);


		    

		  } catch (Google_Service_Exception $e) {
		    log_message('info', sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage())));
		  } catch (Google_Exception $e) {
		    log_message('debug',sprintf('<p>An client error occurred: <code>%s</code></p>',htmlspecialchars($e->getMessage())));
		  }

		  $this->ci->session->userdata('token',$this->client->getAccessToken());
		  return $status;
		} else {
		  // If the user hasn't authorized the app, initiate the OAuth flow
		  $state = mt_rand();
		  $this->client->setState($state);
		  $this->ci->session->set_userdata('state' ,$state);

		  $authUrl = $this->client->createAuthUrl();
		  $htmlBody = "<h3>Authorization Required</h3><p>You need to <a href='".$authUrl."'>click here to authorize access</a> before proceeding.<p>";
		  return $htmlBody;
		
	}
}
}