<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {


	public function index($emailsent='', $allfields='')
	{
		$data['title']                =    'Contact';
		$data['headertop']           = 'partials/headerTop';
		$data['navigationmenu']  = 'partials/navigation';
		$data['slider']                  = 'partials/placeholder';
		$data['welcome']             = 'pages/contact';
		$data['event_parties']      = 'partials/placeholder';
		$data['fact_counter']        = 'partials/placeholder';
		$data['upcomingevents']   = 'partials/placeholder';
		$data['bestevent']             = 'partials/placeholder';
		$data['events_screenshots']= 'partials/placeholder';
		$data['featured_artists']       = 'partials/placeholder';
		$data['quote']                       = 'partials/placeholder';
		$data['news']                       = 'partials/placeholder';
		$data['suppoters']                  = 'partials/suppoters';
		$data['footer']                        = 'partials/footer';

		$data['contactdata'] = $this->Pages_model->getContacts();

		

		if($emailsent != ''){
		$data['messagesent'] = $emailsent;
		}else{
		$data['messagesent'] = NULL;
		}

		if($allfields != ''){
		$data['formerror'] = $allfields;
		}else{
		$data['formerror'] = NULL;
		}

		//$config['center'] = '37.4419, -122.1419';
		$config['center'] = '-6.8153264 	, 39.2891559';

		$config['zoom'] = '18';
		$this->googlemaps->initialize($config);

		$marker = array();
		$marker['position'] = '-6.8153264, 39.2891559';
		//$maker['position'] = 'British Council Library';
		$this->googlemaps->add_marker($marker);
		$data['map'] = $this->googlemaps->create_map();

		$data['supportersdata']           =  $this->Pages_model->getSupporters();



		$this->load->view('template',$data);
	}

	public function formEquery()
	{
		 $username = $this->input->post('username');
		 $email = $this->input->post('email');
		 $subject = $this->input->post('subject');
		 $message = $this->input->post('message');

                       $this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
             
                           // Validate inputs
	                if ($this->form_validation->run() == FALSE)
	                {
	                        $allfields = "Please Fill All Fields";
	                        $this->index($allfields);
	                }
	                else
	                {
	             
		 $config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.sanaaconnect.co.tz',
		  'smtp_port' => 465,
		  'smtp_user' => 'info@sanaaconnect.co.tz', // change it to yours
		  'smtp_pass' => 'interface!!1qaz', // change it to yours
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);

		    //  $message = '';
		      $this->load->library('email', $config);

		      $this->email->set_newline("\r\n");
		      $this->email->from($email, $username); // change it to yours
		      $this->email->to('info@sanaaconnect.co.tz');// change it to yours
		      $this->email->subject($subject);
		      $this->email->message($message);
		      if($this->email->send())
		     {
		     $emailsent = 'Thank you for the message, We  will reply to you soon!';
		     $this->index($emailsent);
		     }
		     else
		    {
		     show_error($this->email->print_debugger());
		    }
		}


	}

}