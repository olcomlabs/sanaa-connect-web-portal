<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {
	public function readmore($slideId)
	{
		$data['title']                =    'Home-slider';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/slider';
		$data['welcome']                  =    'partials/slidemoredetails';
		$data['event_parties']           =    'partials/placeholder';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
 
                  
                      $data['welcomedata']              = $this->Pages_model->getWelcome();
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();
                      $data['slidershowadata']         = $this->Pages_model->getSlideShow();
                      $data['slideById']                    = $this->Pages_model->getSlideShowById($slideId);

		$this->load->view('template',$data);
	}
}