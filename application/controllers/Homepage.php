<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {


	public function index($emailfieldsub = '', $emailfield='')
	{
		$data['title']                =    'Home';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/slider';
		$data['welcome']                  =    'partials/welcome';
		$data['event_parties']           =    'partials/events_parties';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/featured_artists';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/news';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

		if($emailfieldsub != ''){
		$data['messagesent'] = $emailfieldsub;
		}else{
		$data['messagesent'] = NULL;
		}

		if($emailfield != ''){
		$data['formerror'] = $emailfield;
		}else{
		$data['formerror'] = NULL;
		}
 
                  
                      $data['welcomedata']              = $this->Pages_model->getWelcome();
                      $data['eventdata']                        = $this->Pages_model->getAllEvents();
                      $data['newsdata']                          = $this->Pages_model->getAllNews();
                      $data['featuredartistsdata']      = $this->ArtistsModel->getFeaturedArtists();
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();
                      $data['slidershowadata']         = $this->Pages_model->getSlideShow();

	          $this->load->view('template',$data);
	}


	public function subscribe(){
	    $email = $this->input->post('email');
	    $this->form_validation->set_rules('email','Email', 'required|valid_email');
	    if($this->form_validation->run() == FALSE){

	    	 $emailfield = "The Email Field is Required!";
	               $this->index($emailfield);
	    }else{
	      $insert = $this->Pages_model->subscribe($email);

	     if($insert){

	     $config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.sanaaconnect.co.tz',
		  'smtp_port' => 465,
		  'smtp_user' => 'info@sanaaconnect.co.tz', // change it to yours
		  'smtp_pass' => 'interface!!1qaz', // change it to yours
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);

		    //  $message = '';
		      $this->load->library('email', $config);

		      $this->email->set_newline("\r\n");
		      $this->email->from('newsletter@sanaaconnect.co.tz', 'Sanaa Connect Tanzania'); // change it to yours
		      $this->email->to($email);// change it to yours
		      $this->email->subject('Sanaa Connect News and Events Subscription');
		      $this->email->message('Thank you for subscribing with Sanaa Connect Tanzania.');
		      if($this->email->send())
		     {
		     $emailfieldsub = "You have been subscribed to our daily News!";
	                 $this->index($emailfieldsub);
		     }
		     else
		    {
		     show_error($this->email->print_debugger());
		    }

	     }else{
	     	echo "database error occured";
	     }
	    }


	}



	public function music()
	{

		 //initializing config settings for pagination
	            $config['base_url'] = base_url().'index.php/homepage/music/';
	             $data['allmusicount']   = $this->Pages_model->getMusic();

	            $config['total_rows'] = count($data['allmusicount']);
	            $config['per_page'] =10;
	            $config['num_links'] = 10;
	              
	              //config for bootstrap pagination class integration
	            
	             $config['full_tag_open'] = '<ul class="pagination">';
	             $config['full_tag_close'] = '</ul>';
	             $config['first_link'] = false;
	             $config['last_link'] = false;
	             $config['first_tag_open'] = '<li>';
	             $config['first_tag_close'] = '</li>';
	             $config['prev_link'] = '&laquo';
	             $config['prev_tag_open'] = '<li class="prev">';
	            $config['prev_tag_close'] = '</li>';
	            $config['next_link'] = '&raquo';
	            $config['next_tag_open'] = '<li>';
	            $config['next_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li>';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="active"><a href="#">';
	           $config['cur_tag_close'] = '</a></li>';
	           $config['num_tag_open'] = '<li>';
	           $config['num_tag_close'] = '</li>';
	            
	           $this->pagination->initialize($config); 



		$data['title']                =    'Home-music';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'partials/public_music';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

 
                  
                      $data['welcomedata']              = $this->Pages_model->getWelcome();
                      $data['eventdata']                   = $this->Pages_model->getAllEvents();
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();

                       $limit  = $config['per_page'];
                      $offset = $this->uri->segment(3);
                      $data['allmusic']     = $this->Pages_model->getMusic($limit,$offset);
                      $data['mostpopularmusic']           = $this->Pages_model->getMostPopularMusic();


	          $this->load->view('template',$data);
	}


	public function specificmusic($albumId)
	{

		$data['title']                =    'Home-music';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'partials/public_musicSpecific';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

 

                      $data['supportersdata']           =  $this->Pages_model->getSupporters();

                      $data['specificMusic'] = $this->Pages_model->getSpecificMusic($albumId);
                      $data['mostpopularmusic']           = $this->Pages_model->getMostPopularMusic();


	          $this->load->view('template',$data);
	}








	public function video()
	{

	            //initializing config settings for pagination
	            $config['base_url'] = base_url().'index.php/homepage/video/';
	             $data['allvideount']   = $this->Pages_model->getVideo();

	            $config['total_rows'] = count($data['allvideount']);
	            $config['per_page'] =1;
	            $config['num_links'] = 10;
	              
	              //config for bootstrap pagination class integration
	            
	             $config['full_tag_open'] = '<ul class="pagination">';
	             $config['full_tag_close'] = '</ul>';
	             $config['first_link'] = false;
	             $config['last_link'] = false;
	             $config['first_tag_open'] = '<li>';
	             $config['first_tag_close'] = '</li>';
	             $config['prev_link'] = '&laquo';
	             $config['prev_tag_open'] = '<li class="prev">';
	            $config['prev_tag_close'] = '</li>';
	            $config['next_link'] = '&raquo';
	            $config['next_tag_open'] = '<li>';
	            $config['next_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li>';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="active"><a href="#">';
	           $config['cur_tag_close'] = '</a></li>';
	           $config['num_tag_open'] = '<li>';
	           $config['num_tag_close'] = '</li>';
	            
	           $this->pagination->initialize($config); 



		$data['title']                =    'Home-video';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'partials/public_video';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

 
                  
                      $data['welcomedata']              = $this->Pages_model->getWelcome();
                      $data['eventdata']                   = $this->Pages_model->getAllEvents();
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();

                       $limit  = $config['per_page'];
                      $offset = $this->uri->segment(3);
                      $data['allvideo']     = $this->Pages_model->getVideo($limit,$offset);
                       $data['mostpopularvideos']           = $this->Pages_model->getMostPopularVideos();

	          $this->load->view('template',$data);
	}




	public function gallery()
	{
		//initializing config settings for pagination
	            $config['base_url'] = base_url().'index.php/homepage/gallery/';
	             $data['allimages']   = $this->Pages_model->getImages();

	            $config['total_rows'] = count($data['allimages']);
	            $config['per_page'] =100;
	            $config['num_links'] = 10;
	              
	              //config for bootstrap pagination class integration
	            
	             $config['full_tag_open'] = '<ul class="pagination">';
	             $config['full_tag_close'] = '</ul>';
	             $config['first_link'] = false;
	             $config['last_link'] = false;
	             $config['first_tag_open'] = '<li>';
	             $config['first_tag_close'] = '</li>';
	             $config['prev_link'] = '&laquo';
	             $config['prev_tag_open'] = '<li class="prev">';
	            $config['prev_tag_close'] = '</li>';
	            $config['next_link'] = '&raquo';
	            $config['next_tag_open'] = '<li>';
	            $config['next_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li>';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="active"><a href="#">';
	           $config['cur_tag_close'] = '</a></li>';
	           $config['num_tag_open'] = '<li>';
	           $config['num_tag_close'] = '</li>';
	            
	           $this->pagination->initialize($config); 

		$data['title']                =    'Home-gallery';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'partials/public_gallery';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

 
                  
                      $data['welcomedata']              = $this->Pages_model->getWelcome();
                    
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();

                      $limit  = $config['per_page'];
                      $offset = $this->uri->segment(3);
                      $data['allimages']     = $this->Pages_model->getImages($limit,$offset);

	          $this->load->view('template',$data);
	}



	public function search()
	{

                      $searchword  =  $this->input->post('search');
		$data['title']                =    'Home';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		 $data['welcome']                 = 'partials/search_result'; // View name 
		$data['event_parties']           =    'partials/placeholder';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

                          

		 $data['supportersdata']           =  $this->Pages_model->getSupporters();

		 $data['searchresult']           =  $this->Pages_model->getSearchResult($searchword);

	           
	           
	            $this->load->view('template',$data);
	}
}
