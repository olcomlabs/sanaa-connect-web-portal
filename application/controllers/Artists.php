<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artists extends CI_Controller {

         /**
          * 
          * @return browse random
          */
	public function index()
	{ 
                       // Get artists subcategory
		$categories = $this->ArtistsModel->getArtistMainCategories();
                       // Get all artist for public view
                       $allartists   = $this->ArtistsModel->getAllArtists();
                      
		$viewData = array(
			'categories' => $categories,
			'allartists'     => $allartists,
			);
		$artistsHtml = $this->load->view('pages/artists/artists',$viewData,true);
		log_message('debug','********* This is template data *******************'.print_r($viewData,true));
		$data = $this->load_template();
		$data['artists'] = $artistsHtml;
		$data['welcome'] = null;
		$data['title' ] = 'Artists';
		#log_message('debug','********* This is template data *******************'.print_r($data,true));
		$this->load->view('template',$data);
	}
	

         /**
          * @return Return artists profile
          */
	public function profile($identifier)
	{
	

		#$profile = $this->ArtistsModel->getPublicArtistProfile($artistName);
		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		//var_dump($profile);
		if($profile == null ){
			redirect('/artists');
		}
		$data = $this->load_template();
		//load artist profile html with given data
		$popularAlbums = $this->ArtistsModel->getMostPopularAlbums(null,$profile['artistId']);


		
		$artistId = $this->uri->segment(3); 
		$artist = array(
			 'artist_profile' => $profile,
			 'contents' => '',
			 'artistId' => $artistId,
			 'popularAlbums' => $popularAlbums 	
			);

                        
		$data['profile']  = $this->load->view('pages/artists/publicprofile',$artist,true);
		unset($data['welcome']);
		$this->load->view('template',$data);
		
	}

           /**
            * 
            * @return works of current artists
            */
	public function works()
	{
	// get logged in artist
		$identifier =$this->session->userdata('authenticated' );#'dd7facc67039113a6c7f9d3c2a7c1252';;
		if( $identifier == NULL ){
			$identifier = $this->uri->segment(3); 
		} 

		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);	
		
		if($profile == null ){
			redirect('/artists');
		}
		$data = $this->load_template();
		//load artist profile html with given data
		$tracks=null;
		$graphics=null;
		if( preg_match('/usic/', $profile['categoryName'],$match)){
			$tracks = $this->ArtistsModel->getWorks(null,$profile[ 'artistId']);
		}
		
		if( preg_match('/[p|P]hoto|[f|F]ine|[P|p]aint/', $profile['categoryName'],$match)){
			$graphics = $this->ArtistsModel->getWorks(null,$profile[ 'artistId']);
		}
		$artist = array(
			 'artist_profile' => $profile,
			 'tracks' => $tracks,
			 'graphics' => $graphics	
			); 

		$data['profile']  = $this->load->view('pages/artists/works',$artist,true);
		unset($data['welcome']);
		$this->load->view('template',$data);
	}

            /**
             * [works description]
             * @return latest blog post
             */
	public function blogposts()
	{
		$data['title']                =    'Artists';
		$data['headertop']           = 'partials/headerTop';
		$data['navigationmenu']  = 'partials/navigation';
		$data['slider']                  = 'partials/placeholder';
		$data['welcome']             = 'pages/artists/profile';
		$data['artistcontents']             = 'pages/artists/blogpost';
		$data['event_parties']      = 'partials/placeholder';
		$data['fact_counter']        = 'partials/placeholder';
		$data['upcomingevents']   = 'partials/placeholder';
		$data['bestevent']             = 'partials/placeholder';
		$data['events_screenshots']= 'partials/placeholder';
		$data['featured_artists']       = 'partials/placeholder';
		$data['quote']                       = 'partials/placeholder';
		$data['news']                       = 'partials/placeholder';
		$data['suppoters']                  = 'partials/suppoters';
		$data['footer']                        = 'partials/footer';

		$this->load->view('template',$data);
	}

           /**
            * [photos description]
            * @return photos of current artists
            */
	public function photos()
	{
		$data = $this->load_template();
		$this->load->view('template',$data);
	}




	
	function myProfile(){

		// get logged in artist
		$identifier = $this->session->userdata('authenticated' );
		if( $identifier == NULL ){
			redirect('/hauth/login');
		}

		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		if($profile == null ){
			redirect('/artists');
		}
		$data = $this->load_template();
		//load artist profile html with given data
		
		$popularAlbums = $this->ArtistsModel->getMostPopularAlbums(null,$profile['artistId']);


		$artist = array(
			 'artist_profile' => $profile,
			 'contents' => '',
			 'popularAlbums' => $popularAlbums	
			);
                     
		$data['profile']  = $this->load->view('pages/artists/profile',$artist,true);
		unset($data['welcome']);
		$this->load->view('template',$data);

	}

	function add_album(){

		//decide which cdn to load 

		$identifier = $this->session->userdata('authenticated');
		if( $identifier == null ){
			redirect('/');


			return;
		}

		$formData = $this->input->post(null);
		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		if( isset($formData)  AND $formData != null )
		{
			$this->load->library('Services_Cloudinary');
			log_message('info','******************** Services_Cloudinary Loaded. *******');
			log_message('info','******************** Services_Cloudinary Uploading image . *******'. $_FILES['albumCover']['tmp_name']);

			try{
				$result = $this->services_cloudinary -> upload($_FILES['albumCover']['tmp_name'],array());	
				if(is_array( $result)){
					//save to db 
					$album = array(
						'albumName' => $formData['albumName'],
						'albumCover' => $result['url'],
						#'coverID'  => $result['public_id'],
						'a_description' => $formData['description'], 
						'artist_artistId' => $profile['artistId'],
						'a_dateCreated'=>date('Y-m-d')
						);
		
					$this->ArtistsModel->saveAlbum($album);
					redirect('/artists/albums');
				}
				else{
					show_error('Could not upload cover photo');
				}
			}
			catch(Exception $e ){
				show_error( $e->getMessage());
			}
			
			

		}
		else{
			$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
			//identify artist category

			

			$templateData = $this->load_template();
			//load artist profile html with given data

			$artist = array(
			 'artist_profile' => $profile,
			 'contents' => '',
			 'function' => 'add_album'	
			);
			$addAlbumData['authorizeUrl'] = ' ';
			$addAlbumData = array_merge($addAlbumData,$artist);

			 $templateData['content']  = $this->load->view('pages/artists/add_album',$addAlbumData,true);
			 unset($templateData['welcome']);


			 $this->load->view('template',$templateData);
		}
		

		

	}

	function scCallback(){


		log_message('info','******************** Sound cloud Callback data *******'.print_r($_GET,true));
		if(! isset($_GET['code']))
		{
			redirect('/');
		}
		$identifier = $this->session->userdata('authenticated');
		$this->load->config('soundcloud_config');
		 //load soundcloud library 
		 $clientId = $this->config->item('scClientId');
		 $clientSecret = $this->config->item('scClientSecret');
		 $callback = $this->config->item('callback');
		 $soundcloudConfigs=array('clientId' =>$clientId, 'clientSecret'=>$clientSecret,
		  'callback'=>$callback,'development' => false);
		 log_message('info','******************** These are souncloud configs *******'.print_r($soundcloudConfigs,true));
		 $this->load->library('Services_Soundcloud', $soundcloudConfigs); 

		 
		 try {
		
		   $this->services_soundcloud->setCurlOptions(array(
		            CURLOPT_SSL_VERIFYHOST => false,
		            CURLOPT_SSL_VERIFYPEER => false
		        ));

		   $accessToken = $this->services_soundcloud->accessToken($_GET['code']);
		} catch (Exception $e) {

		log_message('info','******************** Sound cloud Exception - accessToken *******'.print_r($e->getMessage(),true));


		   $accessToken = $this->services_soundcloud->accessToken($_GET['code']);
		   log_message('info','******************** Sound cloud  - _GET data *******'.print_r($_GET,true));
		} catch (Exception $e) {

		log_message('info','******************** Sound cloud  - accessToken *******'.print_r($e->getMessage(),true));

		  show_error($e->getMessage());
		}
		try {
		    $me = json_decode($this->services_soundcloud->get('me'), true);
		} catch (Exception $e) { 
		log_message('info','******************** Sound cloud Exception *******'.print_r($e->getMessage(),true));
		  show_error($e->getMessage());
		}
		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
				if($profile == null ){
					redirect('/artists');
				}
				
		$artist = array(
			 'artist_profile' => $profile,
			 'contents' => '',
			 'function' => 'add_album'	
		);

		 $scUserData =  array( 
		                'sc_accessToken' => $accessToken['access_token'],
		                'sc_profileId' => $me['id'],
		                'sc_username' => $me['username'],
		                'sc_name' => $me['full_name'],
		                'sc_avatar' => $me['avatar_url'],
		                'a_artistId' => $profile['artistId']

		 );
		 $this->ArtistsModel->saveSoundcloudProfile($scUserData);

		 log_message('info','******************** Sound cloud  - userData *******'.print_r($scUserData,true));

		 $scUserData = array_merge($artist,$scUserData);
		$this->session->set_userdata($scUserData);
		$templateData = $this -> load_template();
		$formData = $this->load->view('pages/artists/soundcloud_upload',$scUserData,true);
		$templateData['content'] = $formData;
		unset($templateData['welcome'] );
		$this->load->view('template',$templateData);

	}

	function soundcloud_upload(){
 

		$identifier = $this->session->userdata('authenticated');
		$albumId = $this->session->userdata('albumId');
		 
		if( $identifier == null ){
			redirect('/');
		}
		else{ 

			$access_token = $_POST['access_token'];// $this->input->post('access_token');

			try {
				
			
			$this->load->config('soundcloud_config');
			 //load soundcloud library 
			 $clientId = $this->config->item('scClientId');
			 $clientSecret = $this->config->item('scClientSecret');
			 $callback = $this->config->item('callback');
			 $soundcloudConfigs=array('clientId' =>$clientId, 'clientSecret'=>$clientSecret, 'callback'=>$callback,'development' => false);
			 log_message('info','******************** These are souncloud configs *******'.print_r($soundcloudConfigs,true));
			 $this->load->library('Services_Soundcloud', $soundcloudConfigs); 
			 $this->services_soundcloud->setAccessToken($access_token);

			  $mytrack = array(
			    'track[title]' => $_POST["audioname"],
			    'track[asset_data]' => '@'.$_FILES["audiofile"]["tmp_name"] 
			     );
			     
			   $this->services_soundcloud->setCurlOptions(array(
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSL_VERIFYHOST => false,
				));

			  $track = json_decode($this->services_soundcloud->post('tracks', $mytrack));
			  $message  = '<p><b>Congrats your file successfully uploaded to <a target="_blank" href="'.$track->permalink_url.'">'.$track->permalink_url.'</a>';
			  $profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
				if($profile == null ){
					redirect('/artists');
				}
				
				
			log_message('info','******************** SoundClooud Upload Results*******'.print_r($track,true));
			$workDetails = array(
				'aa_artistId' => $profile['artistId'],
				'a_albumId' => $albumId,
				'w_location'=>$track->permalink_url,
				'w_dateCreated'=>date('Y-m-d'),
				'cdnId' => $track->id
				);
			  
			   $this->ArtistsModel->addWork($workDetails);
			$success = array(
				 'artist_profile' => $profile,
				'contents' => '',
				 'function' => 'add_album',
				 'message' =>$message
			);

			$templateData = $this->load_template();
			unset($templateData['welcome']);

			 $successData = $this->load->view('pages/artists/soundcloud_success',$success,true);
			 $templateData[ 'content'] = $successData;
			 $this->load->view('template',$templateData);

			 } catch (Exception $e) {
				show_error($e->getMessage());
			}
		
		}
	}

	function load_template(){

		$data['title']                     =    'Artists';
		$data['headertop']           = 'partials/headerTop';
		$data['navigationmenu']  = 'partials/navigation';
		$data['slider']                  = 'partials/placeholder';
		$data['welcome']             = 'pages/artists/profile';
		$data['artistcontents']      = 'pages/artists/photos';
		$data['event_parties']      = 'partials/placeholder';
		$data['fact_counter']        = 'partials/placeholder';
		$data['upcomingevents']   = 'partials/placeholder';
		$data['bestevent']             = 'partials/placeholder';
		$data['events_screenshots']= 'partials/placeholder';
		$data['featured_artists']       = 'partials/placeholder';
		$data['quote']                       = 'partials/placeholder';
		$data['news']                       = 'partials/placeholder';
		$data['suppoters']                  = 'partials/suppoters';
		$data['footer']                        = 'partials/footer';
	
                  

	         $data['supportersdata']           =  $this->Pages_model->getSupporters();

		return $data;
	}

	function albums(){
		// get logged in artist
		$identifier = $this->session->userdata('authenticated' );#'dd7facc67039113a6c7f9d3c2a7c1252';#$this->session->userdata('authenticated' );
		$profile = null;
		$albums = null;
		if( $identifier != NULL ){

			$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
				if($profile == null ){
					redirect('/hauth/login');
				} 
		}
		else{
			$profile  = $profile = $this->AuthModel->getUserProfile(null,null,null,$this->uri->segment(3));
			
		}

		$albums = $this->ArtistsModel->getAlbums($profile[ 'artistId']);

		$data = $this->load_template();
		//load artist profile html with given data

		$albumsData = array(
			 'artist_profile' => $profile,
			 'contents' => $albums	
			);

		$data['profile']  = $this->load->view('pages/artists/albums',$albumsData,true);
		unset($data['welcome']);
		$this->load->view('template',$data);
	}

	function view_album(){
		$loggedIn = $this->session->userdata('authenticated');

		$albumId = $this->uri->segment(3); 
		$albumData = $this->ArtistsModel->getAlbums(null,$albumId);
		$data = $this->load_template();

		if( $loggedIn == null ){ 
			$identifier = $this->uri->segment(4);
			$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		}else{
			$profile = $this->AuthModel->getUserProfile(null,null,null,$loggedIn);
		}
		
		if($profile == null ){
			redirect('artists');
		}
		$this->session->set_userdata('albumId',$albumId);
		
		$add_work = '';
		$tracks = null;
		$graphics = null;
		$videos = null;
		if( isset($loggedIn)){
			 

			if( preg_match('/usic/', $profile['categoryName'],$match)){

				//load soundcloud
				// load soundcloud config
				 $this->load->config('soundcloud_config');
				 //load soundcloud library
				 
				 $clientId = $this->config->item('scClientId');
				 $clientSecret = $this->config->item('scClientSecret');
				 $callback = $this->config->item('callback');
				 $soundcloudConfigs=array('clientId' =>$clientId, 'clientSecret'=>$clientSecret,
				  'callback'=>$callback,'development' => false);
				 log_message('info','******************** These are souncloud configs *******'.print_r($soundcloudConfigs,true));
				 $this->load->library('Services_Soundcloud', $soundcloudConfigs); 
				 //instatiate soundcloud 
				 
				 $authorizeUrl = $this->services_soundcloud->getAuthorizeUrl();

				 if( $authorizeUrl ==  null ){
				 	redirect('/');
				 	return;
				 }
				 $add_work = "<a href = '  $authorizeUrl;  '><img src='".base_url('assets/images/btn-connect-sc-l.png')."'' /></a>"; 
				  $tracks = $this->ArtistsModel->getWorks($albumId);
				
			}elseif(preg_match('/[p|P]hoto|[f|F]ine|[P|p]aint/',$profile['categoryName'],$match)){
				
				$add_work = "<a   href ='".site_url('artists/cloudinary_upload')."'><strong>+Upload Your Graphic</strong></a>";
				$graphics = $this->ArtistsModel->getWorks($albumId);		


				
			}elseif(preg_match('/[v|V]ideo|[a|A]nimation|[C|c]lip/',$profile['categoryName'],$match)){
				
				$add_work = "<a   href ='".site_url('artists/youtube_upload')."'><strong>+Upload Your Video</strong></a>";
				$videos = $this->ArtistsModel->getWorks($albumId);	
				
			}

		
		}
		else{
			if( preg_match('/usic/', $profile['categoryName'],$match)){
				$tracks = $this->ArtistsModel->getWorks($albumId);
			}elseif(preg_match('/[p|P]hoto|[f|F]ine|[P|p]aint/',$profile['categoryName'],$match)){
				$graphics = $this->ArtistsModel->getWorks($albumId);
			}elseif(preg_match('/[v|V]ideo|[a|A]nimation|[C|c]lip/',$profile['categoryName'],$match)){
				$videos = $this->ArtistsModel->getWorks($albumId);	
			}
		}

		$albumData = array(
			'artist_profile' => $profile,
			'album_data' => $albumData,
			'add_work' => $add_work,
			'tracks' => $tracks,
			'graphics'=>$graphics,  
			'videos'=>$videos
		);



		$data['profile'] = $this->load->view('pages/artists/view_album',$albumData,true);
		unset($data['welcome']);
		$this->load->view('template',$data);

	}


	function cloudinary_upload(){
		$albumId = $this->session->userdata('albumId');
		$loggedIn = $this->session->userdata('authenticated');
		$formData = $this->input->post(null);
		$data = $this->load_template();

		$profile = $this->AuthModel->getUserProfile(null,null,null,$loggedIn);
		if($profile == null ){
			redirect('/artists');
		}

		if(isset($formData) AND $formData != null){
			$this->load->library('Services_Cloudinary');
			log_message('info','******************** Services_Cloudinary Loaded. *******');
			log_message('info','******************** Services_Cloudinary Uploading image . *******'. $_FILES['albumCover']['tmp_name']);

			try{
				$result = $this->services_cloudinary -> upload($_FILES['graphicFile']['tmp_name'],array());	
				
				if(is_array( $result)){
					//save to db 
					 $workDetails = array(
					'aa_artistId' => $profile['artistId'],
					'a_albumId' => $albumId,
					'w_location'=>$result['url'],
					'w_dateCreated'=>date('Y-m-d'),
					'cdnId' => $result['public_id'],
					'w_description' => $formData['description']
					);
				  	 
				 	$this->ArtistsModel->addWork($workDetails);
					$success = array(
						 'artist_profile' => $profile,
						'contents' => '',
						 'function' => 'add_album',
						 'message' =>'Graphic upload successful'
					);

					$templateData = $this->load_template();
					unset($templateData['welcome']);

					 $successData = $this->load->view('pages/artists/cloudinary_success',$success,true);
					 $templateData[ 'content'] = $successData;
					 $this->load->view('template',$templateData);
					redirect("/artists/view_album/$albumId");
				}
				else{
					show_error('Could not upload cover photo');
				}
			}
			catch(Exception $e ){
				show_error( $e->getMessage());
			}
			
		}else{
			
			$pageData = array(
				'artist_profile' => $profile,
			);

			$uploadForm = $this->load->view('pages/artists/cloudinary_upload', $pageData,true);
			unset($data['welcome']);
			unset($data['profile']);
			$data['content' ] = $uploadForm;

			$this->load->view('template',$data);
		}
	}

	function youtube_upload(){
		$albumId = $this->session->userdata('albumId');
		$loggedIn = $this->session->userdata('authenticated');
		$formData = $this->input->post(null);
		$data = $this->load_template();

		$profile = $this->AuthModel->getUserProfile(null,null,null,$loggedIn);
		if($profile == null ){
			redirect('/artists');
		}
		$this->load->library('Services_Google');
		
		$ytFormData = array(
				'artist_profile' => $profile,
				'content' => 'null'
			);

		if(!isset($_GET['code'])){
		  $state = mt_rand();
		  $this->services_google->client->setState($state);
		  $this->session->set_userdata('state' ,$state);

		  $authUrl = $this->services_google->client->createAuthUrl();
		  $htmlBody = "<h3>Authorization Required</h3><p>You need to <a href='".$authUrl."'>click here to authorize access</a> before proceeding.<p>";
		  $ytFormData['authorize'] = $htmlBody;
		}
		  if(isset($formData) AND $formData!=null){
			 
			 $tags = explode(',',$_POST['tags']);
			 $tags[] = 'sanaaconnect';
			 $payload = array(
			 	 'title' => $_POST['title'],
			 	 'tags' => explode(',',$_POST['tags']),
			 	 'description' => $_POST['description']
			 	);
			 if(isset($_GET['code']))
			 {
			 	$this->services_google->setCodeState($_GET['code'],$_GET['state']);	
			 }
			 $status = $this->services_google->youtube_upload($_FILES['videoFile']['tmp_name'],$payload);

			 if( !is_array( $status) AND $status != null){

			 	if(is_object($status)){
			 		$ytFormData['authorize'] = '<h3>Youtube video upload successful</h3>'.
			 		'<p>Check it out <a target =_blank href = "https://www.youtube.com/watch?v='.$status->id.'">Click here'."</a></p>";
			 		
			 		$workDetails = array(
						'aa_artistId' => $profile['artistId'],
						'a_albumId' => $albumId,
						'w_location'=>'https://www.youtube.com/watch?v='.$status->id,
						'w_dateCreated'=>date('Y-m-d'),
						'cdnId' => $status->id,
						'w_description' => $formData['description']
					);
				  	 
				 	$this->ArtistsModel->addWork($workDetails);
			 	}else{
			 		$ytFormData['authorize'] = $status;	
			 	}
			 	
			 	$ytHtml = $this->load->view('pages/artists/yt_upload',$ytFormData,true);
			 }else{
			 	
			 	$ytHtml = $this->load->view('pages/artists/yt_upload',$ytFormData,true);
			 }

		}
		else{
				
			$ytHtml = $this->load->view('pages/artists/yt_upload',$ytFormData,true);

			 
		}
		
		$data['content'] = $ytHtml;
		unset($data['welcome']);
		unset($data['profile']);
		$this->load->view('template',$data);
	}



	public function editprofile($identifier)
	{
		  #$profile = $this->ArtistsModel->getPublicArtistProfile($artistName);
		$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		//var_dump($profile);
		if($profile == null ){
			redirect('/artists');
		}
		$data = $this->load_template();
		//load artist profile html with given data
		//$popularAlbums = $this->ArtistsModel->getMostPopularAlbums(null,$profile['artistId']);


		
		$artistId = $this->uri->segment(3); 
		$artist = array(
			 'artist_profile' => $profile,
			 'contents' => '',
			 'artistId' => $artistId,
			// 'popularAlbums' => $popularAlbums 	
			);

                        
		$data['profile']  = $this->load->view('pages/artists/editprofile',$artist,true);
		unset($data['welcome']);
		$this->load->view('template',$data);
	}


	public function updateInfo()
	{
                 		//decide which cdn to load 

		$identifier = $this->session->userdata('authenticated');
		if( $identifier == null ){
			redirect('/');
			return;
		}

		$formData = $this->input->post(null);
		//var_dump($formData);
		//$profile = $this->AuthModel->getUserProfile(null,null,null,$identifier);
		if( isset($formData)  AND $formData != null )
		{ 
			if(empty($_FILES['photoURL']['tmp_name']) OR empty($_FILES['coverPhoto'])){
				//$identifier = $this->session->userdata('authenticated');
				 $this->session->set_flashdata('uploaderror','Profile Photo or Cover Photo not Selected!');
				 redirect('artists/myProfile');
				
			}
			$this->load->library('Services_Cloudinary');
			//log_message('info','******************** Services_Cloudinary Loaded. *******');
			//log_message('info','******************** Services_Cloudinary Uploading image . *******'. $_FILES['coverPhoto']['tmp_name']);
                                  //var_dump($_FILES);
			try{
				$result1 = $this->services_cloudinary ->upload($_FILES['coverPhoto']['tmp_name'],array());
				$result2 = $this->services_cloudinary ->upload($_FILES['photoURL']['tmp_name'],array());
				//var_dump($result1);	

				if(is_array($result1) AND is_array($result2)){
					$identifier =$formData['identifier'];
					//save to db 
					$info = array(
						'artistName' => $formData['artistName'],
						'CoverPhoto' => $result1['url'],
						'photoURL' => $result2['url'],
						 'firstName'  =>$formData['firstName'],
						  'lastName'  =>$formData['lastName'],
						 'gender'      => $formData['gender'],
						 'age'      => $formData['age'],
						 'email'      => $formData['email'],
						 'address'      => $formData['address'],
						  'city'      => $formData['city'],
						  'country'      => $formData['country'],
						   'phone'      => $formData['phone'],
						  'password' => md5($formData['password']),
						    'description'      => $formData['description'],
						    'linkedin'      => $formData['linkedin'], 
						    'webSiteURL'      => $formData['webSiteURL'], 
						    'linkedin'      => $formData['linkedin'], 
						);
		
					$this->AuthModel->updateArtistProfile($info,$identifier);
					redirect('artists/myProfile');
				}
				else{
					//show_error('Could not upload cover photo');
				}
			}
			catch(Exception $e ){
				show_error( $e->getMessage());
			}
			
			

		}
		else{
		    "you did not set data";
		}
	}
}
