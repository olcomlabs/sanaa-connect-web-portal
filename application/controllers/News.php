<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function index()
	{


		 //initializing config settings for pagination
	            $config['base_url'] = base_url().'index.php/news/index';
	             $data['allmusicount']   = $this->Pages_model->getAllNews();

	            $config['total_rows'] = count($data['allmusicount']);
	            $config['per_page'] =10;
	            $config['num_links'] = 10;
	              
	              //config for bootstrap pagination class integration
	            
	             $config['full_tag_open'] = '<ul class="pagination">';
	             $config['full_tag_close'] = '</ul>';
	             $config['first_link'] = false;
	             $config['last_link'] = false;
	             $config['first_tag_open'] = '<li>';
	             $config['first_tag_close'] = '</li>';
	             $config['prev_link'] = '&laquo';
	             $config['prev_tag_open'] = '<li class="prev">';
	            $config['prev_tag_close'] = '</li>';
	            $config['next_link'] = '&raquo';
	            $config['next_tag_open'] = '<li>';
	            $config['next_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li>';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="active"><a href="#">';
	           $config['cur_tag_close'] = '</a></li>';
	           $config['num_tag_open'] = '<li>';
	           $config['num_tag_close'] = '</li>';
	            
	           $this->pagination->initialize($config); 


		$data['title']                          =    'News';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'pages/news/index';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                  =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                         =    'partials/placeholder';
		$data['news']                          =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
	
                  
                           $limit  = $config['per_page'];
                          $offset = $this->uri->segment(3); 
		$data['newsdata']      = $this->Pages_model->getAllNews($limit,$offset);
		$data['supportersdata']           =  $this->Pages_model->getSupporters();

		$this->load->view('template',$data);
	}



	public function readmore($newsId)
	{
		$data['title']                =    'News';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'pages/news/newspecific';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
		$data['map']                          =    null;   
                  

		$data['specificNews'] = $this->Pages_model->getSpecificNews($newsId);
		$data['supportersdata']           =  $this->Pages_model->getSupporters();
		$data['mostpopularnews']           = $this->Pages_model->getMostPopularNews();

		$this->load->view('template',$data);
	}
}