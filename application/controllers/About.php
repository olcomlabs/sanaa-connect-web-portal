<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {


	public function index()
	{
		$data['title']                =    'About';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'pages/about';
		$data['event_parties']           =    'partials/placeholder';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials//placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials//placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
	
                  
                  
                      $data['aboutusdata']              = $this->Pages_model->getAboutus();

                       $data['aboutIntroduction']              = $this->Pages_model->getAboutusIntro();
                  
                      $data['supportersdata']           =  $this->Pages_model->getSupporters();

		$this->load->view('template',$data);
	}
}