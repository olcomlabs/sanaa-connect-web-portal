<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CI_Controller {


	public function index() 
	    { 
                         
		
		 $data['title']                =    'Home';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		 $data['welcome']                 = 'errors/html/404'; // View name 
		$data['event_parties']           =    'partials/placeholder';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';

                          

		 $data['supportersdata']           =  $this->Pages_model->getSupporters();

	           
	           
	            $this->load->view('template',$data);
}
}
