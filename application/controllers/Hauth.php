<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HAuth extends CI_Controller {

	function __construct(){
		parent::__construct();
		#$this->load->model('AuthModel');
	}

	public function index()
	{
	
		$data = $this->load_template();
		$data['supportersdata']           =  $this->Pages_model->getSupporters();
		$this->load->view('template',$data);
		
	}

	public function login($provider =null)
	{

		if($provider == null){
			$input = $this->input->post(null);
			if(isset($input) AND count($input) > 0 ){
				log_message('debug', "*************** Loggin in with these data ****".print_r($input,true));
				if(preg_match("/@/", $input['username'],$match)){
					$profile = $this->AuthModel->getUserProfile($input['username'],$input['password']);
				}else{
					$profile = $this->AuthModel->getUserProfile(null,$input['password'],$input['username']);
				}
				
				if($profile != null AND $profile['active'] == 1){ 


					$this->session->set_userdata('authenticated',$profile['identifier']);
					$this->session->set_userdata('identifier',$profile['identifier']);
					redirect('/');
				}else{
					$data = $this->load_template();
					unset($data['welcome']);
					redirect('hauth/login');
				}

				return;	
			}
			else{
				redirect('hauth');
			}
		}
		log_message('debug', "controllers.HAuth.login($provider) called");

		try
		{
			log_message('debug', 'controllers.HAuth.login: loading HybridAuthLib');
			$this->load->library('HybridAuthLib');


			if ($this->hybridauthlib->providerEnabled($provider))
			{
				log_message('debug', "controllers.HAuth.login: service $provider enabled, trying to authenticate.");
				$service = $this->hybridauthlib->authenticate($provider);
				$data = $this->load_template();

				if ($service->isUserConnected())
				{
					log_message('debug', 'controller.HAuth.login: user authenticated.');


					#log_message('info','FILE:'.__FILE__.' LINE  '.__LINE__.' FUNCTION '.__FUNCTION__.'*********** Calling getUserProfile ***',TRUE);
					$user_profile = $service->getUserProfile();

					log_message('info', 'controllers.HAuth.login: user profile:'.PHP_EOL.print_r($user_profile, TRUE));
   
					$saved_profile = $this->AuthModel->getUserProfile(null,null,null,$user_profile->identifier);
  					$this->session->set_userdata('identifier' ,$user_profile->identifier);

					if($saved_profile == null){

					//artist profile does not exist, first time login
					//auto active profile 
					  $user_profile->active = 0;
					  $user_profile->dateCreated = date('Y-m-d H:i:s');
					  $this->AuthModel->saveUserProfile($user_profile);
					  
					   log_message('info', 'FILE:'.__FILE__.' LINE  '.__LINE__.' FUNCTION '.__FUNCTION__.'*************************** user profile:'.PHP_EOL.print_r($saved_profile, TRUE));
				 

					//load the cats
					$categories = $this->ArtistsModel->getArtistCategories();
					log_message('info',__FILE__.' '.__LINE__.' '.__FUNCTION__.'*********** Result ***'.print_r($categories ,true ),TRUE);
					$form_data[ 'categories' ] = $categories;
							
					//did we get bio
					if(  $user_profile->description == null ){
						$form_data['description' ] = 1;
					}
					//did we get email
					if(  $user_profile->email == null ){
						$form_data['email' ] = 1;
					}
					 
					 //did we get phone
					if(  $user_profile->phone == null ){
						$form_data['phone' ] = 1;
					}
					log_message('info',__FILE__.' '.__LINE__.' '.__FUNCTION__.'*********** This is form data ***'.print_r($form_data ,true ),TRUE);
					// redirect user to set new password	
					$data['profile'] = $this->load->view( 'pages/password',$form_data,true);;
					unset($data['welcome' ]);
						

					}else{
						// profile already saved 
						$this->session->set_userdata('authenticated' ,$user_profile->identifier);
						redirect('/');
					}

					log_message('info',__FILE__.' '.__LINE__.' '.__FUNCTION__.'*********** this is the data variable ***'.print_r($data ,true ),TRUE);
					$this->load->view('template',$data);
				}
				else // Cannot authenticate user
				{
					$data = $this->load_template();
					$data  = 'Cannot authenticate user';
					$this->load_view('template',$data);
				}
			}
			else // This service is not enabled.
			{
				log_message('error', 'controllers.HAuth.login: This provider is not enabled ('.$provider.')');
				show_404($_SERVER['REQUEST_URI']);
			}
		}
		catch(Exception $e)
		{
			$error = 'Unexpected error';
			
			switch($e->getCode())
			{
				case 0 : $error = 'Unspecified error.'; break;
				case 1 : $error = 'Hybriauth configuration error.'; break;
				case 2 : $error = 'Provider not properly configured.'; break;
				case 3 : $error = 'Unknown or disabled provider.'; break;
				case 4 : $error = 'Missing provider application credentials.'; break;
				case 5 : log_message('debug', 'controllers.HAuth.login: Authentification failed. The user has canceled the authentication or the provider refused the connection.');
				         //redirect();
				         if (isset($service))
				         {
				         	log_message('debug', 'controllers.HAuth.login: logging out from service.');
				         	$service->logout();
				         }
				         show_error('User has cancelled the authentication or the provider refused the connection.');
				         break;
				case 6 : $error = 'User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.';
				         break;
				case 7 : $error = 'User not connected to the provider.';
				         break;
			}

			if (isset($service))
			{
				$service->logout();
			}

			log_message('error', 'controllers.HAuth.login: '.$error);
			show_error('Error authenticating user.');
		}
	}

	public function endpoint()
	{

		log_message('debug', 'controllers.HAuth.endpoint called.');
		log_message('info', 'controllers.HAuth.endpoint: $_REQUEST: '.print_r($_REQUEST, TRUE));

		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			log_message('debug', 'controllers.HAuth.endpoint: the request method is GET, copying REQUEST array into GET array.');
			$_GET = $_REQUEST;
		}

		log_message('debug', 'controllers.HAuth.endpoint: loading the original HybridAuth endpoint script.');
		require_once APPPATH.'/third_party/hybridauth/index.php';

	}
	/**
	 *
	 *
	 */
	function load_template(){

		$data['title']                =    'Login';
		$data['headertop']           = 'partials/headerTop';
		$data['navigationmenu']  = 'partials/navigation';
		$data['slider']                  = 'partials/placeholder';
		$data['welcome']             = 'pages/login';
		$data['event_parties']      = 'partials/placeholder';
		$data['fact_counter']        = 'partials/placeholder';
		$data['upcomingevents']   = 'partials/placeholder';
		$data['bestevent']             = 'partials/placeholder';
		$data['events_screenshots']= 'partials/placeholder';
		$data['featured_artists']       = 'partials/placeholder';
		$data['quote']                       = 'partials/placeholder';
		$data['news']                       = 'partials/placeholder';
		$data['suppoters']                  = 'partials/suppoters';
		$data['footer']                        = 'partials/footer';


		return $data;
	}

	function setPassword(){

		$input  = $this->input->post(NULL); 
		log_message('info', 'FILE:'.__FILE__.' LINE  '.__LINE__.' FUNCTION '.__FUNCTION__.'*****************IIIINNNNPPUUUTTTTT********** user profile:'.PHP_EOL.print_r($input, TRUE));
		 if( isset($input) AND $input != null ){
		 	$identifier = $this->session->userdata('identifier');
		 	$saved_profile = $this->AuthModel->getUserProfile(null,null,$identifier);
		 	log_message('info', 'FILE:'.__FILE__.' LINE  '.__LINE__.' FUNCTION '.__FUNCTION__.'*****************55555555555555********** user profile:'.PHP_EOL.print_r($saved_profile, TRUE));
		 	$saved_profile['password'] = md5($input['password']);
		 	$saved_profile['categoryId' ]= $input['categoryId'];
		 	$saved_profile['email' ] = $input['email'];
		 	$saved_profile['description' ] = $input['description'];
		 	$saved_profile['phone'] = $input['phone'];
		 	

		 	 unset($saved_profile['ac_categoryId' ]);
  			 unset($saved_profile['ac_artistId']);
   			unset($saved_profile['ac_dateCreated']);
    			unset($saved_profile['categoryName']);
   			unset($saved_profile['cat_DateCreated']);
    			unset($saved_profile['cat_dateChanged']);

		 	$this->AuthModel->updateUserProfile($saved_profile);
		 	$this->session->set_userdata('authenticated',$identifier);
		 	redirect('/');
		 }
	}

	function logout(){
		$this->session->unset_userdata(array('identifier','authenticated'));
		redirect('/');
	}

	function register(){
		$form_data = $this->input->post(null);
		$categories = $this->ArtistsModel->getArtistMainCategories();
		$organizations = $this->ArtistsModel->getOrganizations();
		$subcategories = $this->ArtistsModel->getArtistSubCategories();
		$supportersdata           =  $this->Pages_model->getSupporters();
		$register_data = array(
				'categories' => $categories,
				'organizations'=>$organizations,
				'supportersdata'=>$supportersdata
				);
		if(isset($form_data) AND count($form_data) > 0 ){
			$form_data['password'] = md5($form_data['password']);
			$form_data['identifier' ] = md5(date('Y-m-dhis'));
			
			if(empty($_FILES['profilePhoto']['tmp_name']) OR !isset($_FILES['profilePhoto']['tmp_name'])){

		            $data = $this->load_template();
			    unset($data['welcome']);
			    $register_data['error'] = 'Please provide a profile photo!';
			    $register = $this->load->view('partials/register',$register_data,true);
			    $data['profile'] = $register;
			     $this->load->view('template',$data);
			     return;
			}
                                  $this->load->library('Services_Cloudinary');
			log_message('info','******************** Services_Cloudinary Loaded. *******');
			log_message('info','******************** Services_Cloudinary Uploading image . *******'. $_FILES['profilePhoto']['tmp_name']);
			$uploadResult = $this->services_cloudinary -> upload($_FILES['profilePhoto']['tmp_name'],array());

			$form_data['photoURL'] = $uploadResult['url'];
            $form_data['active'] = 0;
            // Register Artist model
			$result = $this->AuthModel->registerArtist($form_data);

			if($result == -1){
			    $data = $this->load_template();
			    unset($data['welcome']);
			    $register_data['error'] = 'Provided email ,artist name,phone already exists';
			    $register = $this->load->view('partials/register',$register_data,true);
			    $data['profile'] = $register;
			     $this->load->view('template',$data);
			     return;
			}
			else{
				$data = $this->load_template();
				unset($data['welcome']);
				$register = $this->load->view('partials/register',array('error' => 'Thank you for registering with us, Your selected organization is reviewing your application'),true);
				$data['profile'] = $register;
				$this->load->view('template',$data);
				return;
			}
			$this->session->set_userdata('authenticated',$form_data['identifier']);
			$this->session->set_userdata('identifier',$form_data['identifier']);
		 	redirect('/');
		}
		else{
			$data = $this->load_template();
			unset($data['welcome']);

			
			
			$register = $this->load->view('partials/register',$register_data,true);
			$data['profile'] = $register;
			$this->load->view('template',$data);
		}
	}
}

/* End of file hauth.php */
/* Location: ./application/controllers/hauth.php */
