<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {


public function index()
	{
		 //initializing config settings for pagination
	            $config['base_url'] = base_url().'index.php/jobs/index';
	             $data['alljobscount']   = $this->Pages_model->getAllJobs();

	            $config['total_rows'] = count($data['alljobscount']);
	            $config['per_page'] =10;
	            $config['num_links'] = 10;
	              
	              //config for bootstrap pagination class integration
	            
	             $config['full_tag_open'] = '<ul class="pagination">';
	             $config['full_tag_close'] = '</ul>';
	             $config['first_link'] = false;
	             $config['last_link'] = false;
	             $config['first_tag_open'] = '<li>';
	             $config['first_tag_close'] = '</li>';
	             $config['prev_link'] = '&laquo';
	             $config['prev_tag_open'] = '<li class="prev">';
	            $config['prev_tag_close'] = '</li>';
	            $config['next_link'] = '&raquo';
	            $config['next_tag_open'] = '<li>';
	            $config['next_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li>';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="active"><a href="#">';
	           $config['cur_tag_close'] = '</a></li>';
	           $config['num_tag_open'] = '<li>';
	           $config['num_tag_close'] = '</li>';
	            
	           $this->pagination->initialize($config); 



		$data['title']                =    'Jobs';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'pages/jobs/index';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
	
                  
                           $limit  = $config['per_page'];
                          $offset = $this->uri->segment(3); 
		 $data['jobs']      = $this->Pages_model->getAllJobs($limit,$offset);
		 $data['supportersdata']           =  $this->Pages_model->getSupporters();

		$this->load->view('template',$data);
	}


	public function readmore($jobId)
	{
		$data['title']                =    'Jobs';
		$data['headertop']                =    'partials/headerTop';
		$data['navigationmenu']       =    'partials/navigation';
		$data['slider']                       =    'partials/placeholder';
		$data['welcome']                  =    'partials/placeholder';
		$data['event_parties']           =    'pages/jobs/jobspecific';
		$data['fact_counter']             =    'partials/placeholder';
		$data['upcomingevents']       =    'partials/placeholder';
		$data['bestevent']                 =    'partials/placeholder';
		$data['events_screenshots']   =    'partials/placeholder';
		$data['featured_artists']         =    'partials/placeholder';
		$data['quote']                        =    'partials/placeholder';
		$data['news']                         =    'partials/placeholder';
		$data['suppoters']                  =    'partials/suppoters';
		$data['footer']                        =    'partials/footer';
		$data['map']                          =    null;   
                  

		$data['specificJob'] = $this->Pages_model->getSpecificJobs($jobId);
		$data['supportersdata']           =  $this->Pages_model->getSupporters();
		$data['mostpopularjobs']           = $this->Pages_model->getMostPopularJobs();

		$this->load->view('template',$data);
	}


}