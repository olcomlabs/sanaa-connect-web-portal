<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model{


       /**
        * @return welcome message details
        */
	public function getWelcome()
	{
		$result = $this->db->select('*')
		                              ->limit(1)
		                             ->get( 'welcome');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}



         /**
          * @return all list of jobs from database
          */
	public function getAllJobs($limit=NULL,$offset=NULL)
	{
		$result = $this->db->select('*')
		                                ->limit($limit,$offset)
		                                 ->order_by('jobId', 'DESC')
		                                -> get( 'vacancies');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


          /**
          * @return specific jobs from database
          */
	public function getSpecificJobs($jobId)
	{
		$result = $this->db->select('*')
		                             ->where('jobId',$jobId)
		                             ->get( 'vacancies');
		if($result -> num_rows() > 0 ){
                                       $resultUpdate= $this->db->query("UPDATE vacancies SET viewCount = viewCount+1 WHERE jobId = $jobId LIMIT 1");
			return $result -> result_array();
		}
		return null;
	}

	// Get most popular jobs
	public function getMostPopularJobs()
	{
		$result = $this->db->select('*')
		                             ->order_by('viewCount', 'DESC')
		                             ->limit('3')
		                             ->get( 'vacancies');

		if($result -> num_rows() > 0 ){
			return $result -> result_array();
		}
		return null;
	}




	 /**
	  * @return all events from database
	  */
	 public function getAllEvents($limit=NULL,$offset=NULL)
		{
			$result = $this->db->select('*')
			                               ->limit($limit,$offset)
			                                ->order_by('eventId', 'DESC')
			                                ->get( 'events');
			if($result -> num_rows() > 0 ){

				return $result -> result_array();
			}
			return null;
		}



          /**
          * @return specific event from database
          */
	public function getSpecificEvent($eventId)
	{
		$result = $this->db->select('*')
		                             ->where('eventId',$eventId)
		                             ->get( 'events');

		if($result -> num_rows() > 0 ){

		$resultUpdate= $this->db->query("UPDATE events SET viewCount = viewCount+1 WHERE eventId = $eventId LIMIT 1");

			return $result -> result_array();
		}
		return null;
	}

	// Get most popular Event
	public function getMostPopularEvent()
	{
		$result = $this->db->select('*')
		                             ->order_by('viewCount', 'DESC')
		                             ->limit('3')
		                             ->get( 'events');

		if($result -> num_rows() > 0 ){
			return $result -> result_array();
		}
		return null;
	}

          

           /**
	  * @return all news from database
	  */
	 public function getAllNews($limit=NULL,$offset=NULL)
		{
			$result = $this->db->select('*') 
			                              ->limit($limit,$offset)
			                             ->order_by('newsId', 'DESC')
			                             ->get( 'news');
			if($result -> num_rows() > 0 ){

				return $result -> result_array();
			}
			return null;
		}


        /**
          * @return specific news from database
          */
	public function getSpecificNews($newsId)
	{
		$result = $this->db->select('*')
		                             ->where('newsId',$newsId)
		                             ->get( 'news');
		if($result -> num_rows() > 0 ){

		$resultUpdate= $this->db->query("UPDATE news SET viewCount = viewCount+1 WHERE newsId = $newsId LIMIT 1");	

			return $result -> result_array();
		}
		return null;
	}


	// Get most popular news
	public function getMostPopularNews()
	{
		$result = $this->db->select('*')
		                             ->order_by('viewCount', 'DESC')
		                             ->limit('3')
		                             ->get( 'news');

		if($result -> num_rows() > 0 ){
			return $result -> result_array();
		}
		return null;
	}


	 /**
          * @return all suppoters
          */
	public function getSupporters()
	{
		$result = $this->db->select('*') -> get( 'supporter');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}




	 /**
          * @return all list of jobs from database
          */
	public function getAboutus()
	{
		$result = $this->db->select('*') -> get( 'about');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	 /**
          * @return all list of jobs from database
          */
	public function getAboutusIntro()
	{
		$result = $this->db->select('aboutIntroduction')
		                              ->limit('1')
		                              ->order_by('aboutId','ASC')
		                             ->get( 'about');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	public function getSlideShow()
	{
		$result = $this->db->select('*') -> get( 'slideshow');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}

	public function getSlideShowById($slideId)
	{

		$result = $this->db->select('*') 
		                            ->where('slideId', $slideId)
		                            -> get( 'slideshow');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}

	public function subscribe($email)
	{             
		  $emaildata  = array('email'=>$email);
		  $this->db->insert('emailToSubscribe',$emaildata);
		   if($this->db->affected_rows()>0){
		         return TRUE;
		          }else{
		           return FALSE;
		         }
	}



	public function getContacts()
	{
		$result = $this->db->select('*') -> get( 'contact');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	public function getMusic($limit=NULL,$offset=NULL)
	{
		$result = $this->db->select('*') 
		                                ->like('w_location', 'soundcloud')
		                                ->join('album', 'album.albumId =work.a_albumId')
		                                ->limit($limit,$offset)
		                                ->ORDER_BY('workId','DESC')
		                                 -> get( 'work');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	// Get most popular music
	public function getMostPopularMusic()
	{
		$result = $this->db->select('*')
		                                 ->like('w_location', 'soundcloud')
		                                  ->join('album', 'album.albumId =work.a_albumId')
		                                 ->order_by('viewCount', 'DESC')
		                                 ->limit('3')
		                                 ->get( 'work');

		if($result -> num_rows() > 0 ){
			return $result -> result_array();
		}
		return null;
	}




	        /**
          * @return specific music from database
          */
	public function getSpecificMusic($albumId)
	{
		$result = $this->db->select('*')
		                             ->where('a_albumId',$albumId)
		                             ->get( 'work');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	public function getVideo($limit=NULL,$offset=NULL)
	{
		$result = $this->db->select('*') 
		                                ->like('w_location', 'youtube')
		                                ->join('album', 'album.albumId =work.a_albumId')
		                                ->join('artist', 'artist.artistId =album.artist_artistId')
		                                ->limit($limit,$offset)
		                                 -> get( 'work');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


         // Get most popular videos
	public function getMostPopularVideos()
	{
		$result = $this->db->select('*')
		                                 ->like('w_location', 'youtube')
		                                  ->join('album', 'album.albumId =work.a_albumId')
		                                 ->order_by('viewCount', 'DESC')
		                                 ->limit('3')
		                                 ->get( 'work');

		if($result -> num_rows() > 0 ){
			return $result -> result_array();
		}
		return null;
	}





	public function getImages($limit=NULL,$offset=NULL)
	{
		$result = $this->db->select('*') 
		                                ->like('w_location', 'cloudinary')
		                                ->join('album', 'album.albumId =work.a_albumId')
		                                ->join('artist', 'artist.artistId =album.artist_artistId')
		                                ->order_by('workId', 'DESC')
			                    ->limit($limit,$offset)
		                                 -> get( 'work');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}
     

// For searching
     public function getSearchResult($searchword)
	{
		$result = $this->db->like('w_description',$searchword)
		                            ->or_like('w_location',$searchword)
		                            -> get( 'work');
		if($result -> num_rows() > 0 ){
                                  
			return $result -> result_array();
		}
		return null;
	}


     


}