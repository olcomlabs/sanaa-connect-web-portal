<?php

class AuthModel extends CI_Model
{

	function __construct(){
		parent::__construct();
	}

	function getUserProfile($email = null, $password = null , $displayName = null, $id = null){

		log_message('info','FILE:'.__FILE__.' LINE : '.__LINE__.' FUNCTION: '. __FUNCTION__."========***** getting user profile:  with email =$email  , id = $id+=========", TRUE);
		$result = $this->db->select('*')->from('artist' ) -> join('artist_category', 'artist.artistId = artist_category.ac_artistId','left') -> join('category','artist_category.ac_categoryId = category.categoryId','left');
		if( $email != null ){
			#log_message('info','========***** SQL query+========='.$this->db->last_query(), TRUE);
			$result ->where('email',$email	);
		}
		 if($password != null){
			#log_message('info','========***** SQL query+========='.$this->db->last_query(), TRUE);
			$result ->where('password',md5($password));
		}
		if($displayName != null){
			#log_message('info','========***** SQL query+========='.$this->db->last_query(), TRUE);
			$result ->where('displayName',$displayName);
		}
		 if($id != null){
			#log_message('info','========***** SQL query+========='.$this->db->last_query(), TRUE);
			$result ->where('identifier',$id);
		}
		elseif($displayName == null AND $email == null){
			return null;
		}

		$result  = $result->limit(1)->get();
		log_message('info','========***** SQL query+========='.$this->db->last_query(), TRUE);
		log_message('info','========***** Got some data ========='.print_r($result->row_array(),true), TRUE);
		if($result->num_rows() ==1 ){
			return $result->row_array();
		}
		return null;
	}

	function saveUserProfile($profile){ 
 		log_message('info','========***** saving user profile: +========='.print_r($profile,true), TRUE);
 		$result = $this->db->select('*')->where('identifier',$profile->identifier)->get('artist');
 		if($result->num_rows() > 0 ){
 		     return $result->row_array();
 		}
		
		$this->db->insert('artist',$profile);
		return null;
	}

	function updateUserProfile($profile){
		if( is_array($profile)){
			if( isset($profile['categoryId'])){
				log_message('info','FILE:'.__FILE__.' LINE : '.__LINE__.' FUNCTION: '. __FUNCTION__."========***** This is the profile +=========".print_r( $profile,true), TRUE);
				$this->db->insert('artist_category',array('ac_categoryId' => $profile['categoryId'], 'artist_subcatId' => $profile['artist_subcatId'],'ac_artistId' => $profile['artistId'],'ac_dateCreated' => date('Y-m-s H:i:s')));
				unset($profile['categoryId']);
			}
			$this->db->set($profile)->where('identifier',$profile['identifier'])->update('artist');	
		}else{
			$this->db->set($profile)->where('identifier',$profile->identifier)->update('artist');
		}
		
		return true;
	}
	
	function registerArtist($profile){


		$profileCheck = $this->db ->where(
			'email' ,$profile[ 'email']) -> or_where('phone' , $profile['phone']) -> or_where( 'displayName', $profile['displayName'])->select('*')->get('artist');
		if( $profileCheck -> num_rows()){
			return -1;
		}
		$categoryId = $profile['categoryId'];
		unset($profile['categoryId']);
		unset($profile['subcategoryId']);
		unset($profile['password2']);
		unset($profile['submit-form']);
		$profile['active'] = 0;
		$result = $this->db->insert('artist',$profile); 
		$this->db->insert('artist_category',array('ac_categoryId' => $categoryId, 'ac_artistId' => $this->db->insert_id(),'ac_dateCreated' => date('Y-m-s')));
		return $profile['identifier'];
	}



	function updateArtistProfile($info,$identifier){
		if( is_array($info)){
		
			$result = $this->db->set($info)->where('identifier',$identifier)->update('artist');
			if($result){
			return true;
		           }	
		}
		
		return false;
	}

}