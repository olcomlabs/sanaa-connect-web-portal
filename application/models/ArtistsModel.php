<?php

class ArtistsModel extends CI_Model{
	
	//
	function getArtistCategories(){
		$result = $this->db->select('*') 
		                             ->from('artist_category')
		                             #->join('artist_subcategory','artist_category.artist_subcatId=artist_subcategory.artist_subcatId')
		                             ->get();
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}


	// Get main category
	function getArtistMainCategories(){
		$result = $this->db->select('*') -> get( 'category');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}



	// Get main category
	function getOrganizations(){
		$result = $this->db->select('*') -> get( 'organization');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}

 


	// Get subcategory
	function getArtistSubCategories(){
		$result = $this->db->select('*') -> get( 'artist_subcategory');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}



	// Get all artists for public view
	function getAllArtists(){
		$result = $this->db->select('*') 
		                             ->join('artist_category','artist.artistId=artist_category.ac_artistId')
		                             ->join('category','category.categoryId=artist_category.ac_categoryId')
		                             ->get( 'artist');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}



	// Get artist public details
	function getPublicArtistProfile($artistId){
		$result = $this->db->select('*') 
		                             ->where('artistId', $artistId)
		                             ->get( 'artist');
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null; 
}


// Return featured artists
	function getFeaturedArtists()
	{
		$result = $this->db->query("SELECT * FROM artist a JOIN artist_category ac ON a.artistId=ac.ac_artistId JOIN ".
				" category c ON ac.ac_categoryId=c.categoryId ORDER BY artistId DESC LIMIT 3");

			
		if($result -> num_rows() > 0 ){

			return $result -> result_array();
		}
		return null;
	}

 
	function saveAlbum($data){

		return $this->db->insert('album',$data);
	}

	function saveSoundcloudProfile($data){
		$result  = $this->db->where('sc_profileId',$data['sc_profileId']) ->get('soundcloud_profile');
		if($result->num_rows()>0 ){
			return 0;//profile exists
		}
		$result = $this->db->insert('soundcloud_profile',$data);
		return 1;
	}

	function getAlbums($artistId = null,$albumId = null){
		log_message('info',"****************** Fetching Albums with artistId : $artistId , AlbumID : $albumId");
		if(  isset($artistId)){
			$result = $this->db->where( 'artist_artistId',$artistId)->order_by('albumId','desc')->get('album');
			$resultUpdate = $this->db->query("UPDATE album SET viewCount = viewCount+1 WHERE artist_artistId = $artistId LIMIT 1");
			
			if($result->num_rows()> 0 ){
				return $result -> result_array();
			}
			return null;
		}

		if(isset($albumId)){
			$result = $this->db->where('albumId',$albumId)->order_by('albumId','desc')->get('album');
			$resultUpdate = $this->db->query("UPDATE album SET viewCount = viewCount+1 WHERE albumId = $albumId LIMIT 1");
			
			if($result->num_rows()>0 ){
				return $result->row_array();
			}
			return null;
		}

		$result = $this->db->get('album');
		if($result->num_rows() > 0 ){
			return $result->result_array();
		}
		return null;

	}

	function addWork($data){
		return $this->db->insert('work',$data);
	}

	function getWorks($albumId,$artistId=null){
		
		if( $albumId != null ){
			$result = $this->db->select( '*' ) -> where('a_albumId', $albumId) -> order_by('workId', 'desc')->get('work');
			$resultUpdate = $this->db->query("UPDATE work SET w_viewCount = w_viewCount+1 WHERE a_albumId = $albumId LIMIT 1");
			if( $result->num_rows()>0 ){
				return $result->result_array();
			}
			return null;
		}

		if( $artistId != null){
			$result = $this->db->select( '*' ) -> where('aa_artistId', $artistId) -> order_by('workId', 'desc')->limit(10)->get('work');
			$resultUpdate = $this->db->query("UPDATE work SET w_viewCount = w_viewCount+1 WHERE aa_artistId = $artistId LIMIT 1");
			if( $result->num_rows()>0 ){
				return $result->result_array();
			}
			return null;	
		}
		
	}

	function getMostPopularAlbums( $albumId=null,$artistId=null){

		if( isset($albumId)){
			$result = $this->db->select('*')->where('albumId',$albumId)
				->order_by('viewCount','desc')->limit(5)->get('album');
			return $result->result_array();
		}
		if( isset($artistId)){
			$result = $this->db->select('*')->where('artist_artistId',$artistId)
				->order_by('viewCount','desc')->limit(5)->get('album');
			return $result->result_array();
		}
		return null;
	}

	function getMostPopularWorks( $workId=null,$artistId=null){

		if( isset($albumId)){
			$result = $this->db->select('*')->where('workId',$workId)
				->order_by('w_viewCount','desc')->limit(5)->get('work');
			return $result->result_array();
		}
		if( isset($artistId)){
			$result = $this->db->select('*')->where('aa_artistId',$artistId)
				->order_by('w_viewCount','desc')->limit(5)->get('work');
			return $result->result_array();
		}
		return null;
	}
 
}