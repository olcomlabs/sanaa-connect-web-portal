<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>SanaaConnect  <?php echo isset($title) ? '- '.$title: '';?></title>
<!-- Stylesheets -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/revolution-slider.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/owl.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/sanaastyle.css" rel="stylesheet">

<!-- Artists profile style -->
<!-- <link href="<?php echo base_url();?>assets/css/dayday/dayday.css" rel="stylesheet"> -->
<link href="<?php echo base_url();?>assets/css/dayday/timeline.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/dayday/animate.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/dayday/big-cover.css" rel="stylesheet">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->

<!--<script type="text/javascript" src="scripts/soundcloud.player.api.js"></script>-->
<!-- google map for contact page -->
<?php if(isset($map)){
  echo $map['js']; 
}else{
  echo $map = NULL;
}
 ?>

</head>

<body>

<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	
    <!-- Main Header -->
    <header class="main-header style-one">
    	<!-- Header Top -->
    	<?php $this->load->view($headertop);?>
       <!--  End header Top -->
        
        <!-- Search Box -->
        <div class="search-box toggle-box">
        	<div class="auto-container clearfix">
                
                <!-- Search Form -->
                <div class="search-form">
                	<?php echo form_open('homepage/search')?>
                        <div class="form-group">
					<input type="search" name="search"  placeholder="Search">
                            <button class="search-submit" type="submit"><span class="f-icon flaticon-magnifying-glass16"></span></button>
                        </div>
                    <?php echo form_close();?>
                </div>
            
            </div>
        </div>





        

        <!-- Header navigation Lower -->
            <?php $this->load->view($navigationmenu);?>
       <!--  End Header navigation Lower -->
        
        
    </header>
    <!--End Main Header -->
    


    <!-- Main Slider -->
       <?php $this->load->view($slider); ?>
    <!--  End Main Slider -->

     <!-- Welcome message -->

         <?php 
          if( isset($welcome) AND $welcome != null )
              $this->load->view($welcome);
            else if(isset($profile)){
                echo $profile;
              }              
              elseif(isset($artists)){
                echo $artists;
              }
              elseif(isset($content)){
                echo $content;
              }
               ?>
    <!-- End welcome message -->
    
    
   <!--Events and Parts-->
      <?php $this->load->view($event_parties); ?>
   <!--Events and Parts-->

   <!--News Area-->
    <?php $this->load->view($news);?>
    <!--End News Area-->
    
    <!--Facts Counter-->
    <?php $this->load->view($fact_counter);?>
    <!--End Facts Counter-->
    
    
      <!--upcoming Events Area-->
        <?php $this->load->view($upcomingevents);?>
      <!--End upcoming Events Area-->
    
    <!--Best Event-->
        <?php $this->load->view($bestevent);?>
    <!-- End Best Event-->
    
    <!--Events with Screenshots-->
      <?php $this->load->view($events_screenshots);?>
    <!-- End Events with Screenshots-->


    <!--Featured artst-->
      <?php $this->load->view($featured_artists);?>
     <!--End Featured artst-->
    
    <!--Quote section-->
     <?php $this->load->view($quote);?>
   <!-- End Quote section-->
        
    </section>
    
    
    
    <!--Suppoters Logos-->
    <?php $this->load->view($suppoters)?>
    <!-- End Suppoters Logos-->
    
    <!--Main Footer-->
    <?php $this->load->view($footer);?>
<!--Main Footer End-->	
    
</div>
<!--End pagewrapper-->
<script src="<?php echo base_url();?>assets/js/jquery.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/revolution.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bxslider.js"></script>
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mixitup.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url();?>assets/js/wow.js"></script>
<script src="<?php echo base_url();?>assets/js/script.js"></script>
<script src="<?php echo base_url();?>assets/js/myscript.js"></script>
<script src="<?php echo base_url();?>assets/js/dayday/dayday.js"></script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
 <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
 <script>

  $(function(){
    tinymce.init({ 
      selector:'textarea',
   
       });
  });
 </script>

</body>
</html>
