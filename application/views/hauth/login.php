 <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
        <div class="auto-container text-center">
            <h1>Contact Form</h1>
            <ul class="bread-crumb"><li><a href="#">Home</a></li> <li>Contact Us</li></ul>
        </div>
    </section>
    

    <!--Info Strip-->
    <section class="info-strip">
        <div class="auto-container">
            <div class="row clearfix">
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-block text-center wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                        <div class="icon hvr-radial-out"><span class="f-icon flaticon-pointing8"></span></div>
                        <h4>Address</h4>
                        <p>Dar es Salaam, Tanzania</p>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-block text-center wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                        <div class="icon hvr-radial-out"><span class="f-icon flaticon-email103"></span></div>
                        <h4>Email</h4>
                        <p>info@sanaaconnect.co.tz</p>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-block text-center wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms">
                        <div class="icon hvr-radial-out"><span class="f-icon flaticon-telephone46"></span></div>
                        <h4>Phone</h4>
                        <p>(255) 803-456, (235) 806-352</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>


        <!--Contact Us Area-->
    <section class="contact-us-area">
        <div class="auto-container">
            <div class="row clearfix">
                 
                 <!--Contact Form-->
                <div class="col-md-7 col-sm-6 col-xs-12 contact-form wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <h2>Write in details</h2>
                       <form>

                           <div class="field-container clearfix">
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="username" value="" placeholder="Username*">
                            </div>
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="email" name="email" value="" placeholder="Email*">
                            </div>
                            
                            <div class="clearfix"></div>
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="subject" value="" placeholder="Subject*">
                            </div>
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <textarea name="message" placeholder="Message"></textarea>
                            </div>
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" name="submit-form" class="primary-btn hvr-bounce-to-left"><span class="btn-text">Send Message</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                            </div>
                            
                        </div>
                      
                      </form>
         
                </div>

                  <!--Map Area-->
                <div class="col-md-5 col-sm-6 col-xs-12 map-area wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
               
                </div>
            
            </div>
        </div>
    </section>