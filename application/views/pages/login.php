 <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
        <div class="auto-container text-center">
            <h1>Login</h1>
            <ul class="bread-crumb"><li><a href="#">Home</a></li> <li>Login</li></ul>
        </div>
    </section>
   


        <!--Contact Us Area-->
    <section class="contact-us-area">
        <div class="auto-container">
            <div class="row clearfix">
                 
                 <!--Contact Form-->
                <div class="col-md-5 col-sm-6 col-xs-12 contact-form wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <h2>Enter Login Details</h2>
                    <?php 

                        if(isset($error)){
                            ?>
                            <div class = 'label lbl-error'><?php echo $error; ?></div>
                            <?php
                        }
                        ?>
                       <form method='post' action = '<?php echo site_url("hauth/login"); ?>'>

                           <div class="field-container clearfix">
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="username" value="" placeholder="Username/Email*">
                            </div>
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password" value="" placeholder="Password*">
                            </div>
                            
                            <div class="clearfix"></div> 
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" name="submit-form" class="primary-btn hvr-bounce-to-left"><span class="btn-text">Submit</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                                <a href="<?php echo site_url('/hauth/register'); ?>" name="register" class="primary-btn hvr-bounce-to-right"><span class="btn-text">Register</span> <strong class="icon"><span class="f-icon flaticon-arrows110"></span></strong></a>
                            </div>
                            
                        </div>
                      
                      </form>
         
                </div>

                  <!--Map Area-->
                <div class="col-md-7 col-sm-6 col-xs-12 map-area wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <!--<h2>or Use Social Networks</h2>
                    
                    <a class="primary-btn hvr-bounce-to-left" href="<?php echo site_url();?>/hauth/login/Facebook"><span class="btn-text">Facebook</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a>
                    <a class="primary-btn hvr-bounce-to-left"><span class="btn-text">Google+</span> <strong class="icon"><span class="fa fa-google-plus11"></span></strong></a>
                    <a class="primary-btn hvr-bounce-to-left"><span class="btn-text">Twitter</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a>
                </div>-->
                
                </div>
            
            </div>
        </div>
    </section>