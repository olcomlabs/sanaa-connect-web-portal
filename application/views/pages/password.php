 <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
        <div class="auto-container text-center">
            <h1>Login</h1>
            <ul class="bread-crumb"><li><a href="#">Home</a></li> <li>Login</li></ul>
        </div>
    </section>
   


        <!--Contact Us Area-->
    <section class="contact-us-area">
        <div class="auto-container">
            <div class="row clearfix">
                 
                 <!--Contact Form-->
                <div class="col-md-6 col-sm-6 col-xs-12 contact-form wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <h2>Enter Your Password </h2>
                       <form method='post' action = '<?php echo site_url('hauth/setPassword'); ?>'>

                           <div class="field-container clearfix">
                            

                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <select name = 'categoryId' >
                                    <option value = ''> Select Your Category </option>
                                    <?php
                                        foreach($categories as $category){
                                            echo "<option  value =".$category['categoryId'].">".$category['categoryName']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <?php if(isset($email) AND $email != null ): ?>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="email" name="email"  placeholder="Email*">
                            </div>
                            <?php endif; ?>
                            <?php if(isset($phone) AND $phone != null ): ?>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="phone"  placeholder="Phone*">
                            </div>
                            <?php endif; ?>

                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password"  placeholder="Password*">
                            </div>
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password2"  placeholder="Confirm Password*">
                            </div>
                            <?php if(isset($description) AND $description != null ): ?>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <textarea name="description"  placeholder="Bio*"></textarea>
                            </div>
                            <?php endif; ?>
                            <div class="clearfix"></div> 
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" name="submit-form" class="primary-btn hvr-bounce-to-left"><span class="btn-text">Submit</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                                <button type="reset" name="" class="primary-btn hvr-bounce-to-right"><span class="btn-text">Reset</span> <strong class="icon"><span class="f-icon flaticon-arrows110"></span></strong></button>
                            </div>
                            
                        </div>
                      
                      </form>
         
                </div>

                  <!--Map Area-->
                <div class="col-md-7 col-sm-6 col-xs-12 map-area wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                  
                </div>
            
            </div>
        </div>
    </section>