<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/events/events_banner.jpg);">
        <div class="auto-container text-center">
            <h1>Events</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li><a href="<?php echo site_url('events')?>">Back to Events</a></li> </ul>
        </div>
    </section>






     <section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                 <?php if(isset($specificEvent)){foreach($specificEvent as $key=>$event){?>
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 class=" sec-title wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                        <?php echo $event['eventTitle'] ?>
                        </h2>
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Image -->
                            <img class="img-responsive" src="<?php 
                                if( isset($event['eventPhotoURL'] ) && $event[ 'eventPhotoURL'] != NULL){
                                  echo  $event['eventPhotoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/events/eventsplaceholder.jpg';                                  
                                }
                                ?>" alt="">
                            <div class="post-content wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;"> 
                                <!-- Meta -->
                                  <?php echo  $event['eventDescriptions'] ?>  

                                    <br>
                     
                                 <div class="project-info" style="position:relative;">
                                        <ul class="clearfix">
                                            <li><strong>Location:</strong>  <?php echo $event['eventLocation']?></li>
                                            <li><strong>Date :</strong> <?php echo $event['eventDate']?></li>
                          
                                        </ul>
                                    </div>

       
                  <div class="share-btn" style="padding-bottom:20px;padding-top:20px;">
                                Share on :<br>
                                  <!-- AddToAny BEGIN -->
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                    <a class="a2a_button_reddit"></a>
                                    <a class="a2a_button_tumblr"></a>
                                    <a class="a2a_button_pinterest"></a>
                                    <a class="a2a_button_linkedin"></a>
                                    </div>
                                    <!-- AddToAny END -->
                                </div>
                            </div>
                        </div>
                    </div><!-- End Post -->     
                </div><!-- Blog Left Side Ends -->
                <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No events matches.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>

                 <?php   } ?>
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div class="search wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <form>
                                <input type="search" name="name" placeholder="SEARCH..">
                                <input type="submit" value="submit">
                            </form>
                        </div>
                        
                        <!-- Popular Post -->

                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular Events</h2>
                          
                            <ul class="popular-list">
                                <!-- Item -->
                                  <?php if(isset($mostpopular)){
                                       foreach($mostpopular as $key=>$popularevent)
                                           
                                        {?>
                                <li>
                                    <!-- Post Image -->
                                    <a href="<?php echo site_url('events/readmore/'.$popularevent['eventId'])?>"><img width="82" height="84" src="<?php 
                                if( isset($popularevent['eventPhotoURL'] ) && $popularevent[ 'eventPhotoURL'] != NULL){
                                  echo  $popularevent['eventPhotoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/events/eventsplaceholder.jpg';                                  
                                }
                                ?>" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="<?php echo site_url('events/readmore/'.$popularevent['eventId'])?>"><?php echo $popularevent['eventTitle']?></a></h3>
                                        <div class="posted-date"><?php echo $popularevent['eventDate']?></div>
                                    </div>
                                </li>
                                 <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No most popular Event.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>

                 <?php   } ?>
                            </ul>
                             
                        </div><!-- Popular Post Ends-->
                      


 
                        
                        
                        <!-- Category Posts -->
                        <div class="category widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>Events Categories</h2>
                            <ul class="category-list">
                                <li>
                                    <h3><a href="">Musics</a></h3>
                                </li>
                                <li>
                                    <h3><a href="">Dances</a></h3>
                                </li>
                          
                             
                            </ul>
                        </div><!-- Category Ends-->
                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>