<section id="blog" class="blog-area section">
        <div class="auto-container">
             <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft;">
                <h2>My <span> Blog Posts</span></h2>
            </div>



            <div class="row">
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">Recently Blog POst</h2>
                
                    </div><!-- End Post -->	
                    
                    <!-- Author Section -->
                    <div class="author wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <!-- Image -->
                        <img src="<?php echo base_url();?>assets/images/blog/author/d.jpg" alt="">
                            <div class="author-comment">
                                <h5>John Michaels</h5>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure.</p>
                                <div class="share-btn">
                                    <a href="#" class="btn fb-bg">Share on <b>Facebook</b></a>
                                    <a href="#" class="btn twitter-bg">Share on <b>Twitter</b></a>
                                </div>
                            </div>						
                    <div class="clear"></div>							
                    </div><!-- Author Section Ends-->

                    <!-- Comment Section -->
                    <div class="post-comments">
                        <!-- Heading -->
                        <div class="title-head wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">3 comments</div>
                            <ul class="comment-list">
                                <!-- Item -->
                                <li class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                    <!-- Image -->
                                    <img src="<?php echo base_url();?>assets/images/blog/comments/1.jpg" alt="">
                                    <!-- Comments -->
                                    <div class="comment-details">
                                        <div class="comments">
                                            <!-- Comments Meta-->
                                            <div class="comment-meta">
                                                <!-- Author Name-->
                                                <div class="user-name">
                                                    Jenna
                                                </div>
                                                <div class="posted-date">
                                                    July 19, 2014  <span> 5:50 AM</span>
                                                </div>
                                            </div>
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure.	</p>
                                        </div>
                                        <a href="#" class="btn reply">reply</a>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                    <!-- Image -->
                                    <img src="<?php echo base_url();?>assets/images/blog/comments/2.jpg" alt="">
                                    <!-- Comments -->
                                    <div class="comment-details">
                                        <div class="comments">
                                            <!-- Comments Meta-->
                                            <div class="comment-meta">
                                                <!-- Author Name-->
                                                <div class="user-name">
                                                    July
                                                </div>
                                                <div class="posted-date">
                                                    July 19, 2014  <span> 5:50 AM</span>
                                                </div>
                                            </div>
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure.	</p>
                                        </div>
                                        <a href="#" class="btn reply">reply</a>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                    <!-- Image -->
                                    <img src="<?php echo base_url();?>assets/images/blog/comments/3.jpg" alt="">
                                    <!-- Comments -->
                                    <div class="comment-details">
                                        <div class="comments">
                                            <!-- Comments Meta-->
                                            <div class="comment-meta">
                                                <!-- Author Name-->
                                                <div class="user-name">
                                                    Marry
                                                </div>
                                                <div class="posted-date">
                                                    July 19, 2014  <span> 5:50 AM</span>
                                                </div>
                                            </div>
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure.	</p>
                                        </div>
                                        <a href="#" class="btn reply">reply</a>
                                    </div>
                                </li>
                            </ul>
                    </div>
                   
                    
                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">

                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular posts</h2>
                            <ul class="popular-list">
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/1.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/2.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/3.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/4.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- Popular Post Ends-->
                        
                        <!-- Newest Posts -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>Newest posts</h2>
                            <ul class="popular-list">
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/1.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/2.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/3.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/4.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">Lorem ipsum blog post</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- Newest Post Ends-->
                        
                        
                      
                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>


        </div>
 </section>