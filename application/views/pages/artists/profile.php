<div class="auto-container">

              <div class="row"> <!-- start row for use profile -->
                 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo $artist_profile['coverPhoto']; ?>');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php   echo  $artist_profile['photoURL']; ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                               
                                
                        </div>

                </div>
        <!--    <div class="alert alert-danger alert-dismissible fade in" role="alert"> 
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                       <h4 class="text-center">There was</h4> 
                                </div>
 -->
             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li class="active"><?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier == null ){?>
                                                          <a href="<?php echo site_url('artists/myprofile')?>"><i class="fa fa-tasks"></i> About</a>
                                                     <?php }else{?>

                                                         <a href="<?php echo site_url('artists/profile').'/'.$artist_profile['identifier']?>"><i class="fa fa-tasks"></i> About</a>
                                                        <?php }?></li>
                                                          <li><a href="<?php echo site_url('artists/works')?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                    <!--       <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li> -->
                                                           <li>
                                                           <?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier != null ){?>
                                                           <a href="<?php echo site_url('artists/editprofile').'/'.$artist_profile['identifier']?>"><i class="fa fa-pencil"></i> Edit Profile</a>
                                                           <?php } ?>
                                                           </li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->
        

        <section id="blog" class="blog-area section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                            <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft; padding:0px;">
                                 <h2>About <span><?php  ?></span></h2>
                             </div>
                    <!-- Post -->
                    <div class="post-item wow animated" data-animation="fadeInUp" data-animation-delay="300" style="visibility: visible;">
                        <!-- Post Title -->
                
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           
                            <div class="post-content">  
                            <h4>Contact Information</h4><hr>
                                <!-- Text -->
                                <p>
                                 Full Name: <?php echo $artist_profile['firstName'].' '.$artist_profile['lastName']; ?>
                                </p>
                                <p>
                                    Gender: <?php echo $artist_profile['gender']; ?>
                                </p>
                                <p>
                                    Address: <?php echo $artist_profile['address']; ?>
                                </p>
                                <!-- Text -->
                                <p>
                                    Phone: <?php echo $artist_profile['phone']; ?>
                                </p>
                                <p>
                                    Email: </span> <?php echo $artist_profile['email']; ?>
                                </p>
                                <p>
                                    Instagram: <?php echo $artist_profile['instagram']; ?>
                                </p>
                                <p>  
                                   Profile: <a href = '<?php echo $artist_profile['profileURL']; ?>'><?php echo $artist_profile['profileURL']; ?></a>
                                </p>
                                <p>
                                 LinkedIn: <a href='<?php echo $artist_profile['linkedin']; ?>'><?php echo $artist_profile['linkedin']; ?></a>
                                </p> 
                                <!-- Text -->
                                <p>
                            
                               
                            </div>
                        </div>
                    </div><!-- End Post -->
                    
        


                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">

                        
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular works</h2>
                            <ul class="popular-list">

                                <?php


                                if( isset($popularAlbums))
                                {

                                  foreach($popularAlbums as $album){
                                    ?>
                                    <!-- Item -->
                                    <li>
                                        <!-- Post Image -->
                                        <a href="#"><img width='84px' height='86px' src="<?php echo $album['albumCover']; ?>" alt=""></a>
                                        <!-- Details -->
                                        <div class="content">
                                            <h3><a href="#"><?php echo $album['a_description']; ?></a></h3>
                                            <div class="posted-date"><?php echo $album['a_dateCreated' ]; ?></div>
                                        </div>
                                    </li>
                                <?php 
                              }
                            }
                            ?>
               
                  
                            </ul>
                        </div><!-- Popular Post Ends-->
                        
                        
                        

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>

       






            
    