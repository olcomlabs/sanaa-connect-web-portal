<div class="row"> <!-- start row for use profile -->
                 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php 
                                if( isset($artist_profile['coverPhoto'] ) && $artist_profile[ 'coverPhoto'] != NULL){
                                  echo  base_url().$artist_profile['coverPhoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/artists/profile/nocover.jpg';                                  
                                }
                                ?>');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php 
                                if( isset($artist_profile['photoURL'] )AND $artist_profile[ 'photoURL'] != NULL){
                                  echo  $artist_profile['photoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/empty_profile.gif';                                  
                                }
                                ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                                
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li>
                                                    
                                                         <?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier == null ){?>
                                                          <a href="<?php echo site_url('artists/myprofile')?>"><i class="fa fa-tasks"></i> About</a>
                                                     <?php }else{?>

                                                         <a href="<?php echo site_url('artists/profile').'/'.$artist_profile['identifier']?>"><i class="fa fa-tasks"></i> About</a>
                                                        <?php }?>
                                                          </li>
                                                          <li><a class="active" href="<?php echo site_url('artists/works').'/'.$artist_profile['identifier']?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                          <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li>
                                                           <li><a href="<?php echo site_url('artist/connections')?>"><i class="fa fa-share-alt"></i> Connection</a></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->  