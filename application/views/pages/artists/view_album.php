<div class="auto-container">

              <div class="row"> <!-- start row for use profile -->
                 
    <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo $artist_profile['coverPhoto']; ?>');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php   echo  $artist_profile['photoURL']; ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                               
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li ><?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier == null ){?>
                                                          <a href="<?php echo site_url('artists/myprofile')?>"><i class="fa fa-tasks"></i> About</a>
                                                     <?php }else{?>

                                                         <a href="<?php echo site_url('artists/profile').'/'.$artist_profile['identifier']?>"><i class="fa fa-tasks"></i> About</a>
                                                        <?php }?></li>
                                                          <li class="active"><a href="<?php echo site_url('artists/works')?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                     <!--      <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li> -->
                                                           <li><?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier != null ){?>
                                                           <a href="<?php echo site_url('artists/editprofile').'/'.$artist_profile['identifier']?>"><i class="fa fa-pencil"></i> Edit Profile</a>
                                                           <?php } ?></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->

  <section id="blog" class="blog-area section">
        <div class="auto-container"> 

            <div class="row">
                <!-- Blog Left Side Begins -->
                  <div class="service-tabs style-two three-column">
                
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;">
                   <li class="tab-btn hvr-bounce-to-left"  >
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/add_album'); ?>'><strong>Add Album</strong></a>
                    
                    </li>
                    <!--Active Btn-->
                    <li class="tab-btn hvr-bounce-to-left" data-id="#architecture-tab">
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                          <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/albums').'/'.$artist_profile['identifier'];; ?>'><strong>Albums</strong></a>
                    
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#renovation-tab">
                       <!-- <div class="icon"><span class="flaticon-hammer55"></span></div>-->
                        <strong>Most Viewed </strong>
               
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#construction-tab">
                        <!--<div class="icon"><span class="flaticon-barrow"></span></div> -->
                        <strong>Search</strong>
                      
                    </li>
                   
                </ul>

                <div class="col-md-9">
                            <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft; padding:0px;">
                                 <h2><?php echo $album_data['albumName']; ?></h2><br><br>
                                  <div style ='float:right'><?php echo $add_work; ?></div>
                             </div> 
                    <!-- Post -->
                    <div class="post-item wow animated" data-animation="fadeInUp" data-animation-delay="300" style="visibility: visible;">
                        <!-- Post Title -->
                
                  <?php 

                  if( isset($tracks) and $tracks != null and count($tracks)>0){

                    foreach ($tracks as $key => $track) {
                    
                    ?>
                    <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           
                            <div class="post-content">    
                                    <iframe width="100%" height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo $track['cdnId']; ?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                
                            </div> 
                        </div> 
                      <?php
                    }//end foreach
                  } ?>
                  <?php 

                   if( isset($videos) and $videos != null and count($videos)>0){

                    foreach ($videos as $key => $video) {
                    
                    ?>
                    <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           
                            <div class="post-content">    
                                     <iframe id="ytplayer" type="text/html" width="640" height="390"
                                  src="http://www.youtube.com/embed/<?php echo $video['cdnId'];?>?autoplay=0&origin=http://sanaaconnect.co.tz" frameborder="0"/></iframe>
                                  <p><label>Description:</label><br><?php echo $video['w_description']; ?></p>
                            </div> 
                        </div> 
                      <?php
                    }//end foreach
                  } ?>
                  </div>   

                  <?php

                    if( isset($graphics)and $graphics != null and count($graphics)>0){
                      ?>

                      <section class="our-projects with-margin">
                        <div class="auto-container">
                            
                              <!--<div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft;">
                                  <h2>Medical <span>Screenshots</span></h2>
                              </div>
                                  
                              <div class="sec-text wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour, or randomised words which don't look even slightly believable</p>
                              </div>
            
            <!--Filters Nav 
            <ul class="filter-tabs clearfix anim-3-all">
                <li class="filter active" data-role="button" data-filter="all"><span class="btn-txt">All</span></li>
                <li class="filter" data-role="button" data-filter="buildings"><span class="btn-txt">medical</span></li>
                <li class="filter" data-role="button" data-filter="hospital"><span class="btn-txt">Hospital</span></li>
                <li class="filter" data-role="button" data-filter="school"><span class="btn-txt">health</span></li>
                <li class="filter" data-role="button" data-filter="isolation"><span class="btn-txt">science</span></li>
                <li class="filter" data-role="button" data-filter="mall"><span class="btn-txt">hospital</span></li>
                <li class="filter" data-role="button" data-filter="others"><span class="btn-txt">Others</span></li>
            </ul>
        </div>
        
        <!--Projects Container-->
                      <div class="projects-container filter-list clearfix wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
                        
                        <?php

                          foreach($graphics as $graphic ){

                        ?>
                          <article class="project-box mix mix_all mall architecture" style="display: inline-block;  opacity: 1;">
                            <figure class="image"><img src="<?php echo $graphic['w_location']; ?>" alt=""><a href="<?php echo $graphic['w_location']; ?>" class="lightbox-image zoom-icon"></a></figure>
                              <div class="text-content">
                                <div class="text">
                                    <span class="cat"></span>
                                      <h4></h4>

                                  </div>
                                  <?php echo $graphic['w_description']; ?>
                              </div>
                          </article>
                          <?php } ?>
                          
                      </div>
                  </section>
                      <?php

                    }
                  ?>
                </div><!-- END col-md-8 -->
                <!-- Blog Sidebar Begins -->
                <div class="col-md-3">
                    <div class="sidebar">
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>Album Details</h2>
                            <p>
                            	<div class = 'img' style = 'width:20%'></div>
                           	<table>
                           		<tr>
                           			<td><strong>Release Date:</strong></td><td><?php echo $album_data['a_dateCreated']; ?></td>
                           		</tr>
                           		<tr>
                           			<td><strong>Views:</strong></td><td><?php echo $album_data['viewCount']; ?></td>
                           		</tr>
                           		<tr>
                           			<td colspan=2><strong>Description</strong></td>
                           		</tr>
                           		<tr>
                           			<td colspan=2><?php echo $album_data['a_description']; ?></td>
                           		</tr>

                           	</table>   

                           </p>

                        </div><!-- Popular Post Ends-->
                      </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div> 

        </div>




            
    