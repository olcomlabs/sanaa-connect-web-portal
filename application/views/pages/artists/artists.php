    
    <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
        <div class="auto-container text-center">
            <h1>Artists</h1>
            <ul class="bread-crumb"><li><a href="<?php echo site_url()?>">Home</a></li> <li>Artists</li></ul>
        </div>
    </section>

<section class="our-projects">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Artist <span>Board</span></h2>
            </div>
         
             <ul class="filter-tabs clearfix anim-3-all">
                <li class="filter" data-role="button" data-filter="all"><span class="btn-txt">All</span></li>
            <?php
            
            if( isset($categories)){
                foreach ($categories as $key => $category) {
                    ?>
                    <li class="filter" data-role="button" data-filter="<?php echo strtolower( $category['categoryName']); ?>"><span class="btn-txt"><?php echo $category['categoryName']; ?></span></li>
                    <?php    
                }
            }
            ?>
            <!--Filters Nav-
            <ul class="filter-tabs clearfix anim-3-all">
                <li class="filter" data-role="button" data-filter="all"><span class="btn-txt">All</span></li>
                <li class="filter" data-role="button" data-filter="musician"><span class="btn-txt">Musician</span></li>
                <li class="filter" data-role="button" data-filter="fineart"><span class="btn-txt">Fine Arts</span></li>
                <li class="filter" data-role="button" data-filter="craft"><span class="btn-txt">Craft</span></li>
                <li class="filter" data-role="button" data-filter="photographer"><span class="btn-txt">Photographer</span></li>
                <li class="filter" data-role="button" data-filter="sound"><span class="btn-txt">Sound Engineer</span></li>
                <li class="filter" data-role="button" data-filter="actor"><span class="btn-txt">Actors</span></li>
              <li class="filter" data-role="button" data-filter="others"><span class="btn-txt">Others</span></li>
            </ul>-->
        </div>

        
        <!--Projects Container-->
        <div class="projects-container filter-list clearfix">

        <?php
             if( isset($allartists)){
                //var_dump($allartists);

             foreach($allartists as $key => $artists) {?>
            <article class="project-box mix mix_all <?php echo $artists['categoryName'];  ?>">
                <figure class="image">
                     <img style="width:400px; height:340px;" src="<?php 
                                             if( isset($artists['photoURL'] ) && $artists[ 'photoURL'] != NULL)
                                             {
                                             echo  $artists['photoURL']; 
                                            }
                                        else{
                                         echo base_url().'assets/images/artists/profile/profile-photo-placeholder.jpg';                              
                                           }
                                 ?>" alt="">
                          <a href="<?php 
                                             if( isset($artists['photoURL'] ) && $artists[ 'photoURL'] != NULL)
                                             {
                                             echo  $artists['photoURL']; 
                                            }
                                        else{
                                         echo base_url().'assets/images/artists/profile/profile-photo-placeholder.jpg';                              
                                           }
                                 ?>" class="lightbox-image zoom-icon"></a>
                </figure>
                <div class="text-content">
                    <div class="text">
                    <h4 style="font-size:13px;">
                         <a href="<?php echo site_url('artists/profile/'.$artists['identifier'])?>"><?php echo $artists['displayName'];  ?></a>
                    </h4>
                        <span class="cat"><?php echo $artists['categoryName'];  ?></span>
                        
                        <div class="social"><a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Facebook" class="fa fa-facebook-f"></a>   <a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Twitter" class="fa fa-twitter"></a>   <a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Google+" class="fa fa-google-plus"></a></div>
                    </div>

                </div>
            </article>
            <?php }} else{ ?>

                    <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No artist registered yet!.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>

            
        </div>
    </section>