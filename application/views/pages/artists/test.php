    
    <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
    	<div class="auto-container text-center">
        	<h1>Artists</h1>
            <ul class="bread-crumb"><li><a href="<?php echo site_url()?>">Home</a></li> <li>Artists</li></ul>
        </div>
    </section>

<section class="our-projects">
    	<div class="auto-container">
    	       <div class="row">
                    <div class="service-tabs style-two three-column">
                
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;">
                    <!--Active Btn-->
                    <li class="tab-btn hvr-bounce-to-left" data-id="#audio-tab">
                        <div class="icon"><span class="flaticon-wall20"></span></div>
                        <strong>Audio</strong>
                    
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#video-tab">
                        <div class="icon"><span class="flaticon-hammer55"></span></div>
                        <strong>Videos</strong>
               
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#image-tab">
                        <div class="icon"><span class="flaticon-barrow"></span></div>
                        <strong>Images</strong>
                      
                    </li>
                    
   
                </ul>
                
                <!--Tabs Content-->
                <div class="tab-content wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">


                    
                    <!--Audio / Active Tab-->
                    <div class="tab clearfix" id="audio-tab" style="display: none;">
                        
                        <!--Posts Container-->
                        <div class="posts-container clearfix">
                       <form id="myForm" action="">
                            <input type="file" name="useraudio" placeholder="browser audio file">
                            <button type="submit" onclick="SC.upload()">Upload</button>
                       </form>
                       <script>
			function SC.upload(){
			    document.getElementById("myForm").submit();
			     alert(val());
			}
	         </script>
                        
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">

                                   <div class="audio-wrapper">
                                <iframe height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/106329682&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                  </div>
                                    
                                </div>
                            </article><!--Post End-->

                        </div><!--Posts Container End-->
                    </div><!--Tab End-->


                    
                    <!--video Tab-->
                    <div class="tab clearfix" id="video-tab" style="display: none;">
                    videoooo
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="image-tab" style="display: none;">Imagees
                        
                    </div><!--Tab End-->

                    
                </div>
                
            </div>
            </div>
    	</div>
 </section>