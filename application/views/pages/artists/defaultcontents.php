        <section id="blog" class="blog-area section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                            <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft; padding:0px;">
                                 <h2>About <span><?php echo $artist_profile['displayName']; ?></span></h2>
                             </div>
                    <!-- Post -->
                    <div class="post-item wow animated" data-animation="fadeInUp" data-animation-delay="300" style="visibility: visible;">
                        <!-- Post Title -->
                
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           
                            <div class="post-content">  
                            <h4>Contact Information</h4><hr>
                                <!-- Text -->
                                <p>
                                    <span class="f-icon flaticon-telephone46"></span> <?php echo $artist_profile['phone']; ?>
                                </p>
                                <p>
                                    <span class="f-icon flaticon-email103"></span> <?php echo $artist_profile['email']; ?>
                                </p>


                                <h4>Story/Biography</h4><hr>
                                <!-- Text -->
                                <p>
                                <?php echo $artist_profile['description']; ?>
                                </p>
                            
                               
                            </div>
                        </div>
                    </div><!-- End Post -->
                    
                    <!-- Post -->
                    <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft; padding:0px;">
                                 <h2>Latest <span>Blog Post</span></h2>
                             </div>
                    <div class="post-item wow animated" data-animation="fadeInUp" data-animation-delay="300" style="visibility: visible;">
                        <!-- Post Title -->
                        <!-- <h2 class="wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;"><a href="#">Latest Blog Post</a></h2> -->
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Image -->
                            <a href="blog-detail.html"><img class="img-responsive" src="<?php echo base_url();?>assets/images/blog/2.jpg" alt="blog"></a>
                            <div class="post-content">  
                                <!-- Text -->
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                
                            </div>
                        </div>
                    </div><!-- End Post -->

                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">

                        
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular works</h2>
                            <ul class="popular-list">
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/1.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">work1</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/blog/popular-post/2.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#">work2</a></h3>
                                        <div class="posted-date">July 19, 2014</div>
                                    </div>
                                </li>
               
                  
                            </ul>
                        </div><!-- Popular Post Ends-->
                        
                        
                        

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>