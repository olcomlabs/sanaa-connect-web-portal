<div class="auto-container">

              <div class="row"> <!-- start row for use profile -->
                 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo base_url();?>assets/images/artists/profile/fashion.jpg');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php 
                                if( isset($artist_profile['photoURL'] )AND $artist_profile[ 'photoURL'] != NULL){
                                  echo  $artist_profile['photoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/empty_profile.gif';                                  
                                }
                                ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                                
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li ><a href="<?php echo site_url('artists/myProfile')?>"><i class="fa fa-tasks"></i> About</a></li>
                                                          <li class="active"><a href="<?php echo site_url('artists/works')?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                          <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li>
                                                           <li><a href="<?php echo site_url('artist/connections')?>"><i class="fa fa-share-alt"></i> Connection</a></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->

             <section id="blog" class="blog-area section">
        <div class="auto-container">
            <div class="row">
 
              <div class="service-tabs style-two three-column">
                
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;">
                   <li class="tab-btn hvr-bounce-to-left"  >
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/add_album'); ?>'><strong>Add Work</strong></a>
                    
                    </li>
                    <!--Active Btn-->
                    <li class="tab-btn hvr-bounce-to-left" data-id="#architecture-tab">
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <strong>Albums</strong>
                    
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#renovation-tab">
                       <!-- <div class="icon"><span class="flaticon-hammer55"></span></div>-->
                        <strong>Most Viewed </strong>
               
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#construction-tab">
                        <!--<div class="icon"><span class="flaticon-barrow"></span></div> -->
                        <strong>Search</strong>
                      
                    </li>
                    
                   
                </ul> 
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                            <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft; padding:0px;">
                                 <h2>Upload <span>Music</span></h2>
                             </div>
                    <!-- Post -->
                    <div class="post-item wow animated" data-animation="fadeInUp" data-animation-delay="300" style="visibility: visible;">
                        <!-- Post Title -->
                
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           
                            <div class="post-content">  
                            <h4>Soundcloud music upload</h4><hr>
                                <!-- Text -->
                                <p>
                                  <form action="<?php echo site_url('artists/soundcloud_upload'); ?>" method="post" enctype="multipart/form-data">
 
                                      <input type="hidden" name="access_token" value="<?php echo $sc_accessToken; ?>" /> 
                                      <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                      Audio Name:<input type="text" name="audioname" placeholder="My audio" /><br /><br /> 
                                    </div>
                                <div class="clearfix"></div>                             
                                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                       Audio File: <input type="file" name="audiofile" id="audiofile" />
                                  </div>
                                  <div class="clearfix"></div> 
                                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" name="submit-form" class="primary-btn hvr-bounce-to-left"><span class="btn-text">Submit</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                                
                            </div>

                                    </form>
                                    <div class="clearfix"></div> 
                                    <div class="clearfix"></div> 
                                    <p>&nbsp;
                                </p>
                                </div>
                            
                        </div>
                    </div><!-- End Post -->

                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">

                        
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>Music upload  Requirements</h2>
                            <p>
                                Artist must have and soundcloud account in order to upload music. If you don't have an account ,don't worry just click on the but  and create one</p>
                        </div><!-- Popular Post Ends-->
                        
                        
                        

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>
       

        </div>




            
    