<div class="auto-container">
<div class="row"> <!-- start row for use profile -->
                 
             <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo $artist_profile['coverPhoto']; ?>');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php   echo  $artist_profile['photoURL']; ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                               
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                             <li>
                                                    
                                                         <?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier == null ){?>
                                                          <a href="<?php echo site_url('artists/myprofile')?>"><i class="fa fa-tasks"></i> About</a>
                                                     <?php }else{?>

                                                         <a href="<?php echo site_url('artists/profile').'/'.$artist_profile['identifier']?>"><i class="fa fa-tasks"></i> About</a>
                                                        <?php }?>
                                                          </li>
                                                          <li><a class="active" href="<?php echo site_url('artists/works').'/'.$artist_profile['identifier']?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                          <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li>
                                                           <li><?php
                                                       $ci = &get_instance();
                                                    $identifier = $ci->session->userdata('authenticated'); 
                                                     if(isset($identifier)  AND $identifier != null ){?>
                                                           <a href="<?php echo site_url('artists/editprofile').'/'.$artist_profile['identifier']?>"><i class="fa fa-pencil"></i> Edit Profile</a>
                                                           <?php } ?></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->  
        </div>
<section id="blog" class="blog-area section">
        <div class="auto-container">
        <div class="row">
        <div class="col-md-12 post-content">
 
              <?php if(isset($artist_profile)){?>     

                      <?php echo form_open_multipart('artists/updateInfo')?>

                       <?php 
                        if( isset($error) AND $error != null){
                            ?>
                                <div class 'label red'><?php echo $error; ?></div>
                            <?php
                        }
                    ?>
                      <div class="col-md-4">
                          <h2>Basic Details</h2><hr>
                     
                                       <div class="form-group">
                                      <label for="exampleInputEmail1">Fist Name</label>
                                      <input type="text" name="firstName" class="form-control" value="<?php echo $artist_profile['firstName']?>" placeholder="FIrst Name" required>
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Last Name</label>
                                      <input type="text" name="lastName" value="<?php echo $artist_profile['lastName']?>" class="form-control"  placeholder="Last Name" required>
                                    </div>

                                      <div class="form-group">
                                      <label for="exampleInputPassword1">Gender</label>
                                      <select name="gender"  class="form-control" required>
                                           <option value="">Choose Gender</option>
                                           <option value="male">Male</option>
                                           <option value="female">Female</option>
                                      </select>
                                      
                                    </div>

                                     <div class="form-group">
                                      <label for="exampleInputPassword1">Age</label>
                                      <input type="text" name="age" value="<?php echo $artist_profile['age']?>" class="form-control"  placeholder="age">
                                    </div>

                                      <div class="form-group">
                                      <label for="exampleInputPassword1">Artist Name</label>
                                      <input type="text" name="artistName" value="<?php echo $artist_profile['artistName']?>" class="form-control"  placeholder="Artist Name">
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Email</label>
                                      <input type="email" name="email" value="<?php echo $artist_profile['email']?>" class="form-control"  placeholder="Email Address" required>
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Address</label>
                                      <input type="text" name="address" value="<?php echo $artist_profile['address']?>" class="form-control"  placeholder="Address">
                                    </div>

                                     <div class="form-group">
                                      <label for="exampleInputPassword1">City / Region</label>
                                      <input type="text" name="city" value="<?php echo $artist_profile['city']?>" class="form-control"  placeholder="City">
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Country</label>
                                      <input type="text" name="country" value="<?php echo $artist_profile['country']?>" class="form-control"  placeholder="Country">
                                    </div>

                                      <div class="form-group">
                                      <label for="exampleInputPassword1">Password</label>
                                      <input type="password" name="password" class="form-control"  placeholder="password" required>
                                    </div>



                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Phone Number</label>
                                      <input type="text" name="phone" value="<?php echo $artist_profile['phone']?>" class="form-control"  placeholder="Phone Number" required>
                                    </div>


                 
                      </div>

                      <div class="col-md-8">
                          <h2>Proffesional Details</h2><hr>

                              <div class="form-group">
                                      <label for="exampleInputPassword1">Profile Photo</label>
                                      <input type="file" name="photoURL" value="<?php echo $artist_profile['photoURL']?>" class="form-control"  placeholder="Profile Photo" required>
                             </div>

                              <div class="form-group">
                                      <label for="exampleInputPassword1">Cover Photo</label>
                                      <input type="file" name="coverPhoto" value="<?php echo $artist_profile['coverPhoto']?>" class="form-control"  placeholder="Cover Photo" required>
                             </div>

                            <div class="form-group">
                                      <label for="exampleInputPassword1">LinkedIn Profile Link</label>
                                      <input type="text" name="linkedin" value="<?php echo $artist_profile['linkedin']?>" class="form-control"  placeholder="linkedin">
                            </div>

                             <div class="form-group">
                                      <label for="exampleInputPassword1">Website Link</label>
                                      <input type="text" name="webSiteURL" value="<?php echo $artist_profile['webSiteURL']?>" class="form-control"  placeholder="Website Link">
                            </div>


                            <div class="form-group">
                                     
                                      <input type="hidden" name="identifier" value="<?php echo $artist_profile['identifier']?>" class="form-control">
                            </div>



                          <div class="form-group">
                              <label for="exampleInputPassword1">Biography Descriptions</label>
                             <textarea name="description"  class="form-control"><?php echo $artist_profile['description']?></textarea>
                          </div>

                           <div class="form-group">
                              
                            <button type="submit" class="btn btn-primary">Update</button>
                          </div>
                      </div>

             <?php echo form_close();?>

                <?php } else{?>
                             <div class="subscribe-area">
                              <div class="subs-box">
                  
                                           <div class="alert alert-success" role="alert">
                                            <h4 class="text-center">Sorry!, We can't find your informations.</h4> 
                                           </div>                       
                                    
                                       </div>
                            </div>
                <?php } ?>
              
          
          </div>
          </div>
        </div>
</section>