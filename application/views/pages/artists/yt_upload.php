<div class="auto-container">

              <div class="row"> <!-- start row for use profile -->
                 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo base_url();?>assets/images/artists/profile/fashion.jpg');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php 
                                if( isset($artist_profile['photoURL'] )AND $artist_profile[ 'photoURL'] != NULL){
                                  echo  $artist_profile['photoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/empty_profile.gif';                                  
                                }
                                ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                                
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li ><a href="<?php echo site_url('artists/myProfile')?>"><i class="fa fa-tasks"></i> About</a></li>
                                                          <li class="active"><a href="<?php echo site_url('artists/works')?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                          <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li>
                                                           <li><a href="<?php echo site_url('artist/connections')?>"><i class="fa fa-share-alt"></i> Connection</a></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->
              <div class="row clearfix">
         
             <section class="blog-area section">
        <div class="auto-container">
          <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft;">
                <h2>Add <span> Work</span></h2>
            </div>
          <div class = 'row'>
             <div class="service-tabs style-two three-column">
                
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;">
                   <li class="tab-btn hvr-bounce-to-left"  >
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/add_album'); ?>'><strong>Add Album</strong></a>
                    
                    </li>
                    <!--Active Btn-->
                    <li class="tab-btn hvr-bounce-to-left" data-id="#architecture-tab">
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/albums'); ?>'><strong>Albums</strong></a>
                    
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#renovation-tab">
                       <!-- <div class="icon"><span class="flaticon-hammer55"></span></div>-->
                        <strong>Most Viewed </strong>
               
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#construction-tab">
                        <!--<div class="icon"><span class="flaticon-barrow"></span></div> -->
                        <strong>Search</strong>
                      
                    </li>
                    
                   
                </ul>
                <section class="contact-us-area">
        <div class="auto-container">
            <div class="row clearfix">
                 
                 <!--Contact Form-->
                <div data-wow-duration="1000ms" data-wow-delay="0ms" class="col-md-7 col-sm-6 col-xs-12 contact-form wow fadeInLeft animated" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInLeft;">
                    
                   
                       <?php 

                       if( isset($authorize)){
                        echo $authorize;
                       }else{

                        ?>
                         <h2>Video Details</h2>
                       <form method = 'post' enctype="multipart/form-data" action ='<?php 
                          if( isset($_GET['code']))
                          $url =  site_url('artists/youtube_upload').'?state='.$_GET['state'].'&code='.$_GET['code'];
                          else
                            $url =  site_url('artists/youtube_upload');

                          echo $url;
                        ?>'>

                           <div class="field-container clearfix"> 
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text"   value="" name="title" placeholder='Title'>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text"   value="" name="tags" placeholder ='Tags eg bongo,hiphop'>
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label>Video File</label><input type="file"   value="" name="videoFile">
                            </div>
                            
                            <div class="clearfix"></div> 
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <textarea placeholder="Description*" name="description"></textarea>
                            </div>
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button class="primary-btn hvr-bounce-to-left" name="submit-form" type="submit"><span class="btn-text">Create</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                            </div>
                            
                        </div>
                      
                      </form>
                      <?php 

                    }
                    ?>
         
                </div>

                  <!--Map Area-->
                <div data-wow-duration="1000ms" data-wow-delay="0ms" class="col-md-5 col-sm-6 col-xs-12 map-area wow fadeInRight animated" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInRight;">
                    <h2> My Previous Albums</h2>
                    
                    
                </div>
            
            </div>
        </div>
    </section>
                </div>
              </div>
          </div>
            <div class="row clearfix">
             <div class="col-md-5 col-sm-6 col-xs-12 map-area wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <h2> </h2>
              </div>
            
            </div>
        </div>
    </section>

        </div>




            
    