<section class="page-banner" style="background-image:url(<?php echo base_url() ?>assets/images/background/about/about-us-banner.jpg);">
    	<div class="auto-container text-center">
        	<h1>About Us</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li><a href="<?php echo site_url('about');?>">About Us</a></li></ul>
        </div>
    </section>




    <section class="about-us-area">
        <!--About Upper-->
        <div class="about-upper text-center">
            <div class="auto-container">
            
                 <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                    <h2>About <span>Sanaa Connect</span></h2>
                </div>
                    
                <div class="sec-text wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                <?php if(isset($aboutIntroduction)){ foreach($aboutIntroduction as $key=>$aboutintro) {?>
                    <p><?php echo $aboutintro['aboutIntroduction'] ?></p>
                          <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No about us Introduction for now.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>
                </div>

                
            </div>
        </div>
        

        <!--About Lower-->
        <div class="about-lower">
            <div class="auto-container">
                <div class="row clearfix">
  <?php if(isset($aboutusdata)){ foreach($aboutusdata as $key=>$about){?>
                    <!--Text Blocks-->
                    <div class="col-md-12 col-sm-6 col-xs-12 text-blocks">
                        <div class="auto-container">
                            <div class="row clearfix">
                            
                                <article class="col-md-12 col-sm-12 col-xs-12 text-block wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                                    <h3><?php echo $about['aboutTitle']?></h3>
                                    <div class="text"><?php echo $about['aboutDescriptions']?></div>
                                </article>
     
                            
                            </div>
                        </div>
                    </div><!--Text Blocks End-->

                       <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No about us for now.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>
                


                </div>
            </div>
        </div><!--About Lower End-->
        
    </section>