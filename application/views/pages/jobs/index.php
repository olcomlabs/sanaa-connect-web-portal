<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/page-banner-bg-2.jpg);">
    	<div class="auto-container text-center">
        	<h1>Jobs</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li>Jobs</li> </ul>
        </div>
    </section>

     <section class="top-services">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2><span>The jobs board</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p>Browse for a job of your career at different places, if you cant find it today come tomorrow.</p>
            </div>
            
            <div class="row clearfix">
                <?php if(isset($jobs)){foreach($jobs as $key=>$job){?>
                <!--Post-->
                <article class="col-md-4 col-sm-6 col-xs-12 post wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                	<div class="post-inner">
                    
                        <figure class="image">
                            <img style="width:340px; height:220px;" class="img-responsive" src="<?php 
                                if( isset($job['job_coverphoto'] ) && $job[ 'job_coverphoto'] != NULL){
                                  echo  $job['job_coverphoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/jobs/jobsplaceholder.png';                                  
                                }
                                ?>" alt="" />
                            <span class="curve"></span>
                        </figure>
                          <div class="content">
                            <div class="inner-box">
                                <h3><?php echo $job['job_title'] ?></h3>
                                <div class="text">
                                    
                                <?php $jobdata = $job['job_description'] ?>
                                  <?php $readmore = word_limiter($jobdata, 20); ?>
                                  <?php echo $readmore;?>
                                </div>
                         <!--         <div class="alert alert-info" role="alert">
                                     <span>Event Date:</span><b> December 5, 2015.</b>
                                </div> -->
                                <a href="<?php echo site_url('jobs/readmore/'.$job['jobId'])?>" class="primary-btn hvr-bounce-to-left"><span class="btn-text">READ MORE</span></a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } }else{ ?>

                    	 <div class="subscribe-area">
	            	<div class="subs-box">
		
		                         <div class="alert alert-success" role="alert">
		                         	<h4 class="text-center">Sorry!, No jobs for now.</h4> 
		                         </div>                       
		                  
	                       </div>
                      </div>


                 <?php   } ?>
                
            </div>
        </div>
	</section>
         <section class="our-projects">
    <div style="visibility: visible; animation-name: fadeInRight;" class="post-nav wow fadeInRight animated" data-animation="fadeInUp" data-animation-delay="300">
                <?php echo   $this->pagination->create_links();?>
    </div>
    </section>