<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/page-banner-bg-2.jpg);">
    	<div class="auto-container text-center">
        	<h1>Jobs</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li><a href="<?php echo site_url('jobs')?>">Back to Jobs</a></li> </ul>
        </div>
    </section>


     <section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <?php if(isset($specificJob)){foreach($specificJob as $key=>$job){?>
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><?php echo $job['job_title']?></h2>
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Image -->
                            <img class="img-responsive" src="<?php 
                                if( isset($job['job_coverphoto'] ) && $job[ 'job_coverphoto'] != NULL){
                                  echo  $job['job_coverphoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/jobs/jobsplaceholder.png';                                  
                                }
                                ?>" alt="<?php echo $job['job_title']?>">
                            <div class="post-content wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;"> 
                                <!-- Meta -->
                                <div class="posted-date"><?php echo $job['published_date']?>   /   <span>by</span>John</div>
                                  <?php echo $job['job_description']?>            
                                <div class="share-btn" style="padding-bottom:20px;padding-top:20px;">
                                Share on :<br>
                                     <!-- AddToAny BEGIN -->
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                    <a class="a2a_button_reddit"></a>
                                    <a class="a2a_button_tumblr"></a>
                                    <a class="a2a_button_pinterest"></a>
                                    <a class="a2a_button_linkedin"></a>
                                    </div>
                                    <!-- AddToAny END -->
                                </div>
                            </div>
                        </div>
                    </div><!-- End Post -->     
                </div><!-- Blog Left Side Ends -->
                <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No jobs matches.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>

                 <?php   } ?>
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div class="search wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <form>
                                <input type="search" name="name" placeholder="SEARCH..">
                                <input type="submit" value="submit">
                            </form>
                        </div>
                        
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular jobs</h2>
                            <ul class="popular-list">
                                <!-- Item -->
                                                       <?php if(isset($mostpopularjobs)){
                                       foreach($mostpopularjobs as $key=>$popularjobs)
                                           
                                        {?>
                                <li>
                                    <!-- Post Image -->
                                    <a href=""><img style="width:82px; height:84px;" src="<?php 
                                if( isset($popularjobs['job_coverphoto'] ) && $popularjobs[ 'job_coverphoto'] != NULL){
                                  echo  $popularjobs['job_coverphoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/jobs/jobsplaceholder.png';                                  
                                }
                                ?>" alt="<?php echo $popularjobs['job_title']?>"></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="<?php echo site_url('jobs/readmore/'.$popularjobs['jobId'])?>"><?php echo $popularjobs['job_title']?></a></h3>
                                        <div class="posted-date"><?php echo $popularjobs['published_date']?></div>
                                    </div>
                                </li>
                                <!-- Item -->
       
                                <?php } }else{ ?>

                         <div class="subscribe-area">
                        <div class="subs-box">
            
                                     <div class="alert alert-success" role="alert">
                                        <h4 class="text-center">Sorry!, No most popular Jobs.</h4> 
                                     </div>                       
                              
                               </div>
                          </div>

                 <?php   } ?>
       
      
                            </ul>
                        </div><!-- Popular Post Ends-->
 

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>