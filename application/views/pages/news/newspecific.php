<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/news/news_banner2.jpg);">
        <div class="auto-container text-center">
            <h1>news</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li><a href="<?php echo site_url('news')?>">Back to news</a></li> </ul>
        </div>
    </section>



    <section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <?php if(isset($specificNews)){foreach($specificNews as $key=>$new){?>
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><?php echo $new['newsTitle']?></h2>
                        <div class="post wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Image -->
                            <img class="img-responsive" src="<?php 
                                if( isset($new['newsCoverPhoto'] ) && $new[ 'newsCoverPhoto'] != NULL){
                                  echo  $new['newsCoverPhoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/news/newsplaceholder.jpg';                                  
                                }
                                ?>" alt="">
                            <div class="post-content wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;"> 
                                <!-- Meta -->
                                <div class="posted-date"><?php echo $new['newsPublishDate']?>   /   <span> by </span><?php echo $new['publishedBy']?></div>
                                  <?php echo $new['newsDescriptions']?>            
                                <div class="share-btn" style="padding-bottom:20px;padding-top:20px;">
                                Share on :<br>
                                     <!-- AddToAny BEGIN -->
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                    <a class="a2a_button_reddit"></a>
                                    <a class="a2a_button_tumblr"></a>
                                    <a class="a2a_button_pinterest"></a>
                                    <a class="a2a_button_linkedin"></a>
                                    </div>
                                    <!-- AddToAny END -->
                                </div>
                            </div>
                        </div>
                    </div><!-- End Post -->     
                </div><!-- Blog Left Side Ends -->
                <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No news matches.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>

                 <?php   } ?>
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div class="search wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <form>
                                <input type="search" name="name" placeholder="SEARCH..">
                                <input type="submit" value="submit">
                            </form>
                        </div>
                        
                        <!-- Popular Post -->
                        <div class="blog/popular-post widget wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <!-- Title -->
                            <h2>most popular news</h2>
                            <ul class="popular-list">
                            <?php if(isset($mostpopularnews)){
                                       foreach($mostpopularnews as $key=>$popularnews)
                                           
                                        {?>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href=""><img style="width:82px; height:84px;" src="<?php 
                                if( isset($popularnews['newsCoverPhoto'] ) && $popularnews[ 'newsCoverPhoto'] != NULL){
                                  echo  $popularnews['newsCoverPhoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/news/newsplaceholder.jpg';                                  
                                }
                                ?>" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="<?php echo site_url('news/readmore/'.$popularnews['newsId'])?>"><?php echo $popularnews['newsTitle']?></a></h3>
                                        <div class="posted-date">Posted on: <?php echo $popularnews['newsPublishDate']?></div>
                                    </div>
                                </li>
                                <!-- Item -->


                            <?php } }else{ ?>

                         <div class="subscribe-area">
                        <div class="subs-box">
            
                                     <div class="alert alert-success" role="alert">
                                        <h4 class="text-center">Sorry!, No most popular News.</h4> 
                                     </div>                       
                              
                               </div>
                          </div>

                 <?php   } ?>
       
      
                            </ul>
                        </div><!-- Popular Post Ends-->
 

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>