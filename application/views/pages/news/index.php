<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/news/news_banner2.jpg);">
    	<div class="auto-container text-center">
        	<h1>News</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li> <li>News</li> </ul>
        </div>
    </section>

     <section class="top-services">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2><span>The news board</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p>Browse for the lates news around the global all about artists.</p>
            </div>
            
            <div class="row clearfix">
                <?php if(isset($newsdata)){foreach($newsdata as $key=>$new){?>
                <!--Post-->
                <article class="col-md-4 col-sm-6 col-xs-12 post wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                	<div class="post-inner">
                    
                        <figure class="image">
                            <img class="img-responsive" src="<?php 
                                if( isset($new['newsCoverPhoto'] ) && $new[ 'newsCoverPhoto'] != NULL){
                                  echo  $new['newsCoverPhoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/news/newsplaceholder.jpg';                                  
                                }
                                ?>" alt="" />
                            <span class="curve"></span>
                        </figure>
                        <div class="content">
                            <div class="inner-box">
                                <h3><?php echo $new['newsTitle']?></h3>
                                <div class="text">
                                     <?php $news =  $new['newsDescriptions'] ?>
                                     <?php $shortednews = word_limiter($news, 10); ?>
                                     <?php echo $shortednews; ?>
                                </div>
                                <a href="<?php echo site_url('news/readmore/'.$new['newsId'])?>" class="primary-btn hvr-bounce-to-left" ><span class="btn-text">READ MORE</span></a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } }else{ ?>

                    	 <div class="subscribe-area">
	            	<div class="subs-box">
		
		                         <div class="alert alert-success" role="alert">
		                         	<h4 class="text-center">Sorry!, No news for now.</h4> 
		                         </div>                       
		                  
	                       </div>
                      </div>


                 <?php   } ?>
                
            </div>
        </div>
	</section>
        <section class="our-projects">
    <div style="visibility: visible; animation-name: fadeInRight;" class="post-nav wow fadeInRight animated" data-animation="fadeInUp" data-animation-delay="300">
                <?php echo   $this->pagination->create_links();?>
    </div>
    </section>