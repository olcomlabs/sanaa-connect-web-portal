
<section class="testimonials-area" style="background-image:url(<?php echo base_url()?>images/parallax/image-1.jpg);">
    	<div class="auto-container clearfix">
        	<h2 style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: tada; font-size: 10em;" class="wow tada animated" data-wow-delay="0ms" data-wow-duration="1500ms"><span>404</span></h2>
       
        	<!--Slider-->
            <div style="max-width: 100%; font-size: 1.5em;color:#ffffff;" class="bx-wrapper text-center">Sorry! The page you are looking does not exist.</div>
               <br>
            <div style="max-width: 100%; font-size: 1.5em;" class="bx-wrapper text-center">
            	<div class="link-btn">
                    <a class="primary-btn hvr-bounce-to-left" href="<?php echo site_url()?>"><span class="btn-text">Go Home</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a>
                   
                </div>
            </div>
            

            
        </div>
        
    </section>
