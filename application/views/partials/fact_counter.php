<section class="fact-counter fact-counter-one" style="background-image:url(<?php echo base_url();?>assets/images/parallax/image-1.jpg);">
    	<div class="inner clearfix">
        	
            <!--Column-->
        	<div class="column one odd wow fadeIn" data-wow-delay="0s" data-wow-duration="100ms">
            	<div class="content bg">
                	<div class="count-text" data-speed="1000" data-stop="98"></div>
                    <span>Happy Moments</span>
                </div>
            </div>
            
            <!--Column-->
            <div class="column one even wow fadeIn" data-wow-delay="0s" data-wow-duration="100ms">
            	<div class="content bg-1">
                	<div class="count-text" data-speed="1000" data-stop="73"></div>
                    <span>Lightning</span>
                </div>
            </div>
            
            <!--Column-->
            <div class="column one odd wow fadeIn" data-wow-delay="0s" data-wow-duration="100ms">
            	<div class="content bg-2">
                	<div class="count-text" data-speed="2000" data-stop="258"></div>
                    <span>Lovely Clients</span>
                </div>
            </div>
            
            <!--Column-->
            <div class="column one even wow fadeIn" data-wow-delay="0s" data-wow-duration="100ms">
            	<div class="content bg-3">
                	<div class="count-text" data-speed="1000" data-stop="36"></div>
                    <span>Awards</span>
                </div>
            </div>
            
        </div>
    </section>