    <section class="news-area">
    	<div class="auto-container">
        	
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Latest <span>News</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                </div>
            
            <!--News Slider-->
            <div class="news-slider">
            
            <?php if(isset($newsdata)){foreach($newsdata as $key=>$news){?>
            	<!--Slide Item-->
                <article class="slide-item">
                	<figure class="image" ><img style="height:230px; width:210px;" src="<?php 
                                if( isset($news['newsCoverPhoto'] ) && $news[ 'newsCoverPhoto'] != NULL){
                                  echo  $news['newsCoverPhoto']; 
                                }
                                else{
                                    echo base_url().'assets/images/news/newsplaceholder.jpg';                                  
                                }
                                ?>" alt=""></figure>
                    <div class="content-box">
                    	<div class="text-content">
                        	<h3><a href="#"><?php echo $news['newsTitle'] ?></a></h3>
                            <div class="info">By <em><a href="#"><?php echo $news['publishedBy']?></a></em> at <?echo $news['newsPublishDate']; ?></div>
                            <div class="text">
                                <?php $new =  $news['newsDescriptions'] ?>
                                <?php $shortednews = word_limiter($new, 10); ?>
                                <?php echo $shortednews; ?>
                            </div>
                            <div class="link-btn"><a class="primary-btn hvr-bounce-to-left" href="<?php echo site_url('news/readmore/'.$news['newsId'])?>"><span class="btn-text">READ MORE</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a></div>
                        </div>
                    </div>
                </article>
                <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No Events for now.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>

                
            </div>
            
        </div>
    </section>