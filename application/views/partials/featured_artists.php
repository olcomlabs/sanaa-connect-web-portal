<section class="project-details">
        <div class="auto-container">
            
          <div style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft;" class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Featured <span>Artists</span></h2>
            </div>

            <div style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;" class="sec-text wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1000ms">
                <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour, or randomised words which don't look even slightly believable</p>-->
            </div>
            
            <div class="row clearfix">
                
                 <?php if(isset($featuredartistsdata)){ foreach($featuredartistsdata as $key=>$featuredartist) { ?>
                <!--Column-->
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <!--Post-->
                    <article style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;" class="post wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms">
                        <div class="post-inner">
                        
                            <figure class="image">
                                             <img style="width:340px; height:220px;" src="
                                    <?php 
                                             if( isset($featuredartist['photoURL'] ) && $featuredartist[ 'photoURL'] != NULL)
                                             {
                                             echo  $featuredartist['photoURL']; 
                                            }
                                        else{
                                         echo base_url().'assets/images/artists/profile/profile-photo-placeholder.jpg';                              
                                           }
                                 ?>"
                         alt="<?php echo $featuredartist['displayName']?>"  title="<?php echo $featuredartist['displayName']?>">
                                <span class="curve"></span>
                            </figure>
                            <div class="content">
                                <div class="inner-box">
                                    <h3><a href="<?php echo site_url('artists/profile/'.$featuredartist['identifier'])?>"><?php echo $featuredartist['displayName'];  ?></a> <span><?php echo strtoupper($featuredartist['categoryName']); ?></span></h3>
                                <!--     <div class="text">There are many variations of  the passages of Lorem . . .</div> -->
                                    <div class="social"><a href="#" class="fa fa-facebook-f"></a>   <a href="#" class="fa fa-twitter"></a>   <a href="#" class="fa fa-google-plus"></a>   <a href="#" class="fa fa-linkedin"></a></div>
                                </div>

                            </div>
                            
                        </div>
                    </article>
                </div>
                      <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No featured for now.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>



                
            </div>
        </div>
    </section>