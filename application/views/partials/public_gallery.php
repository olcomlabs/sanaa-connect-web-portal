    
    <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/gallery/gallerybg.png);">
        <div class="auto-container text-center">
            <h1>Photo Gallery</h1>
            <ul class="bread-crumb"><li><a href="<?php echo site_url()?>">Home</a></li> <li>Browse</li><li>Gallery</li></ul>
        </div>
    </section>

<section class="our-projects">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Photo Gallery</span></h2>
            </div>
         
             <ul class="filter-tabs clearfix anim-3-all">
                <li class="filter" data-role="button" data-filter="all"><span class="btn-txt">All</span></li>

                <li class="filter" data-role="button" data-filter="musician"><span class="btn-txt">Musician</span></li>
                <li class="filter" data-role="button" data-filter="fineart"><span class="btn-txt">Fine Arts</span></li>
                <li class="filter" data-role="button" data-filter="craft"><span class="btn-txt">Craft</span></li>
                <li class="filter" data-role="button" data-filter="photographer"><span class="btn-txt">Photographer</span></li>
                <li class="filter" data-role="button" data-filter="sound"><span class="btn-txt">Sound Engineer</span></li>
                <li class="filter" data-role="button" data-filter="actor"><span class="btn-txt">Actors</span></li>
              <li class="filter" data-role="button" data-filter="others"><span class="btn-txt">Others</span></li>
            </ul>
        </div>

        
        <!--Projects Container-->
        <div class="projects-container filter-list clearfix">

        <?php
             if( isset($allimages)){
                //var_dump($allartists);

             foreach($allimages as $key => $images) {?>
            <article class="project-box mix mix_all ">
                <figure class="image">
                     <img style="width:500px; height:340px;" src="<?php echo $images['w_location'];  ?>" alt="">
                          <a href="<?php echo $images['w_location'];  ?>" class="lightbox-image zoom-icon"></a>
                </figure>
                <div class="text-content">
                    <div class="text">
                    <h4 style="font-size:13px;">
                         <a href="#"><?php echo $images['displayName'];  ?></a>
                    </h4>
                        <span class="cat"><?php echo $images['albumName'];  ?></span>
                        
                        <div class="social"><a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Facebook" class="fa fa-facebook-f"></a>   <a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Twitter" class="fa fa-twitter"></a>   <a href="#" data-toggle="tooltip" data-placement="top" title="Follow on Google+" class="fa fa-google-plus"></a></div>
                    </div>

                </div>
            </article>
            <?php }} else{ ?>

                    <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No artist registered yet!.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>

            
        </div>

    </section>
    <section class="our-projects">>
    <div style="visibility: visible; animation-name: fadeInRight;" class="post-nav wow fadeInRight animated" data-animation="fadeInUp" data-animation-delay="300">
                <?php echo   $this->pagination->create_links();?>
    </div>
    </section>