<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/video/videobanner.jpg);">
      <div class="auto-container text-center">
          <h1>Videos</h1>
            <ul class="bread-crumb"><li><a href="<?php echo site_url()?>">Home</a></li> <li>Browse</li><li>Video</li></ul>
        </div>
        </div>
    </section>


  <section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 style="visibility: visible; animation-name: fadeInLeft;" class="wow fadeInLeft animated">Videos</h2>
                        <div style="visibility: visible; animation-name: fadeInUp;" class="post wow fadeInUp animated">
                          
                          

                                    <?php if(isset($allvideo)){ foreach($allvideo as $key=>$video){?>
      <div style="visibility: visible; animation-name: fadeInUp;" class="post wow fadeInUp animated">
                            <!-- Image -->
                            <div class="video-container">
                             <iframe width = "785" height = "350" id="player" type="text/html"  src="http://www.youtube.com/embed/<?php echo $video['cdnId']?>?"
  frameborder="0" allowfullscreen >
                          </iframe>
                          </div>
                            <div class="post-content">  
                              <span><?php echo $video['w_description']?></span>
                                <!-- Text -->
                                <div class="text-muted"><span>by</span> <a href="<?php echo site_url('artists/profile/'.$video['identifier'])?>" class="text-muted"><?php echo $video['displayName']?></a> </div>
                                  <div class="text-muted">
                                  <?php
                                       $now = time(); // or your date as well

                                       $your_date = strtotime($video['w_dateCreated']);
                                       $datediff =  abs($your_date-$now);
                                       $ago =  floor($datediff/(60*60*24));

                                  ?>
                                     <?php echo $video['w_viewCount']?> views | <?php echo $ago  ?> days ago
                                  </div>
                            </div>
                        </div>


                        <?php } } else{?>
                        
                                   <div class="subscribe-area">
                                          <div class="subs-box">
                              
                                                       <div class="alert alert-success" role="alert">
                                                        <h4 class="text-center">Sorry!, No List of Videos for now.</h4> 
                                                       </div>                       
                                                
                                                   </div>
                                                </div>

                        <?php } ?>
            <div style="visibility: visible; animation-name: fadeInRight;" class="post-nav wow fadeInRight animated" data-animation="fadeInUp" data-animation-delay="300">
                <?php echo   $this->pagination->create_links();?>
            </div>

                        
                        </div>
                    </div><!-- End Post --> 
                    


                    
                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="search wow fadeInUp animated">
                            <form>
                                <input name="name" placeholder="SEARCH.." type="search">
                                <input value="submit" type="submit">
                            </form>
                        </div>
                        
                        <!-- Popular Post -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="blog/popular-post widget wow fadeInUp animated">
                            <!-- Title -->
                            <h2>most popular videos</h2>
                            <ul class="popular-list">
                              <?php if(isset($mostpopularvideos)){ foreach($mostpopularvideos as $key=>$popularvideo){?>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="blog-detail.html#">
                                          <div class="video-container">
                             <iframe width = "82" height = "84" id="player" type="text/html"  src="http://www.youtube.com/embed/<?php echo $popularvideo['cdnId']?>?"
  frameborder="0" allowfullscreen >
                          </iframe>
                          </div>
                                    </a>
                                    <!-- Details -->
                                <div>  
                              <span><?php echo $popularvideo['w_description']?></span>
                                <!-- Text -->
                                <div class="text-muted"><span>by</span> <a href="<?php echo site_url('artists/profile/'.$video['identifier'])?>" class="text-muted"><?php echo $video['displayName']?></a> </div>
                                  <div class="text-muted">
                                  <?php
                                       $now = time(); // or your date as well

                                       $your_date = strtotime($popularvideo['w_dateCreated']);
                                       $datediff =  abs($your_date-$now);
                                       $ago =  floor($datediff/(60*60*24));

                                  ?>
                                     <?php echo $video['w_viewCount']?> views | <?php echo $ago  ?> days ago
                                  </div>
                            </div>

                                </li>
                                 <?php } } else{?>
                                    
                                               <div class="subscribe-area">
                                                      <div class="subs-box">
                                          
                                                                   <div class="alert alert-success" role="alert">
                                                                    <h4 class="text-center">Sorry!, No Most Popular Video for now.</h4> 
                                                                   </div>                       
                                                            
                                                               </div>
                                                            </div>
 
                                    <?php } ?>
                              
                            </ul>
                        </div><!-- Popular Post Ends-->
                        

                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>