<section class="our-team-area">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Featured <span>Artists</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour, or randomised words which don't look even slightly believable</p>
            </div>
            
            <div class="row clearfix">
            
                <article class="col-md-4 col-sm-6 col-xs-12 team-member">
                    <figure class="image"><img src="<?php echo base_url();?>assets/images/resource/team-image-1.png" alt=""></figure>
                    <div class="content-box wow rotateInUpRight" data-wow-delay="0ms" data-wow-duration="700ms">
                    	<div class="inner hvr-bounce-to-bottom">
                            <div class="text-content">
                                <h4>MASUM RANA</h4>
                                <div class="info">
                                    <p>CEO, Microsoft</p>
                                </div>
                                <p>There are many variations of passages of Lorem Ipsum </p>
                            </div>
                            <div class="social-links">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                
                <article class="col-md-4 col-sm-6 col-xs-12 team-member">
                    <figure class="image"><img src="<?php echo base_url();?>assets/images/resource/team-image-2.png" alt=""></figure>
                    <div class="content-box wow rotateInUpRight" data-wow-delay="300ms" data-wow-duration="700ms">
                    	<div class="inner hvr-bounce-to-bottom">
                            <div class="text-content">
                                <h4>RASHED KABIR</h4>
                                <div class="info">
                                    <p>Businessman</p>
                                </div>
                                <p>There are many variations of passages of Lorem Ipsum </p>
                            </div>
                            <div class="social-links">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                
                <article class="col-md-4 col-sm-12 col-xs-12 team-member">
                    <figure class="image"><img src="<?php echo base_url();?>assets/images/resource/team-image-3.png" alt=""></figure>
                    <div class="content-box wow rotateInUpRight" data-wow-delay="600ms" data-wow-duration="700ms">
                    	<div class="inner hvr-bounce-to-bottom">
                            <div class="text-content">
                                <h4>MAHFUZ RIAD</h4>
                                <div class="info">
                                    <p>Pediatrician</p>
                                </div>
                                <p>There are many variations of passages of Lorem Ipsum </p>
                            </div>
                            <div class="social-links">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                
            </div>
        </div>
    </section>