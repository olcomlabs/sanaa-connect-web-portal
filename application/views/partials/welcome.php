     <section class="top-services" style="padding-top:30px;">
        <div class="auto-container">
            <?php if(isset($welcomedata)){ foreach($welcomedata as $key=>$welcome){ ?>
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2><?php echo $welcome['welcome_title']?></h2>
            </div>

               <div style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;" class="sec-text wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p><?php echo $welcome['welcome_descriptions']?></p>
            </div>
             <?php }}else{?>
                Welcome data not set already
             <?php } ?>
        </div>
        <div class="row clearfix"></div>
  </section>