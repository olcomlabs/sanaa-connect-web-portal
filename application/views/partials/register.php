 <!-- Page Banner -->
    <section class="page-banner" style="background-image:url(<?php echo base_url();?>assets/images/background/page-banner-bg-2.jpg);">
        <div class="auto-container text-center">
            <h1>Login</h1>
            <ul class="bread-crumb"><li><a href="#">Home</a></li> <li>Login</li></ul>
        </div>
    </section>
   


        <!--Contact Us Area-->
    <section class="contact-us-area">
        <div class="auto-container">
            <div class="row clearfix">
                 
                 <!--Contact Form-->
                <div class="col-md-6 col-sm-6 col-xs-12 contact-form wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <h2>Artist Registration</h2>
                    <?php 
                        if( isset($error) AND $error != null){
                            ?>
                                <div class 'label red'><?php echo $error; ?></div>
                            <?php
                        }
                    ?>
                       <form method='post' action = '<?php echo site_url('hauth/register'); ?>' enctype="multipart/form-data"> 
                            
                             
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <select name = 'categoryId' id="categoryType">
                                    <option value = ''> Select Your Category </option>
                                    <?php
                                        foreach($categories as $category){
                                            echo "<option  value =".$category['categoryId'].">".$category['categoryName']."</option>";
                                        }
                                    ?>
                                </select>

                            </div>

                                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <select name = 'organizationId' id="organization">
                                    <option value = ''> Select Your Organization</option>
                                    <?php
                                        foreach($organizations as $organizations){
                                            echo "<option  value =".$organizations['organizationId'].">".$organizations['organizationName']."</option>";
                                        }
                                    ?>
                                </select>

                            </div> 


                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="displayName"  placeholder="Artist Name*">
                            </div>  
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="email" name="email"  placeholder="Email*">
                            </div>  
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="phone"  placeholder="Phone*">
                            </div> 
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password"  placeholder="Password*">
                            </div>
                            
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password2"  placeholder="Confirm Password*">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                       Profile Photo: <input type="file" name="profilePhoto" id="profilePhoto" />
                                  </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <textarea name="description"  placeholder="Bio*"></textarea>
                            </div> 
                            <div class="clearfix"></div> 
                            
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <button type="submit"  class="primary-btn hvr-bounce-to-left"><span class="btn-text">Submit</span> <strong class="icon"><span class="f-icon flaticon-letter110"></span></strong></button>
                                <button type="reset" name="" class="primary-btn hvr-bounce-to-right"><span class="btn-text">Reset</span> <strong class="icon"><span class="f-icon flaticon-arrows110"></span></strong></button>
                            </div>
                            
                        </div>
                      
                      </form>
         
                </div>

                  <!--Map Area-->
                <div class="col-md-7 col-sm-6 col-xs-12 map-area wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                  
                </div>
            
            </div>
        </div>
    </section>