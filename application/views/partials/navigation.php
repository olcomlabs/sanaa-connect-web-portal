<div class="header-lower">
<div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo"><a href="<?php echo site_url()?>"><img src="<?php echo base_url();?>assets/images/sanaalogo.png" alt="Sanaa Connect" title="Sanaa Connect"></a></div>
                
                <!--Right Container-->
                <div class="right-cont clearfix">
                	<div class="search-btn">
                    	<div class="f-icon flaticon-magnifying47"></div>
                        <span class="curve"></span>
                    </div>
                    
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">                                                                                              
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo site_url()?>">Home</a></li>

                             <li><a href="#">Browse</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo site_url('artists');?>">Artists</a></li>
                                 
                                        <li><a href="<?php echo site_url('homepage/music');?>">Music</a>
                                              <!--<ul class="submenu">
                                                <li><a href="">Taarab</a></li>
                                                <li><a href="">Bongo Flava</a></li>
                                                <li><a href="">Hip Hop</a></li>
                                                <li><a href="">Mdundiko</a></li>
                                            </ul>-->
                                        </li>
                                        <li><a href="<?php echo site_url('homepage/video');?>">Videos</a>
                                                   
                                              <!--  <ul class="submenu">
                                                <li><a href="">Taarab</a></li>
                                                <li><a href="">Bongo Flava</a></li>
                                                <li><a href="">Hip Hop</a></li>
                                                <li><a href="">Mdundiko</a></li>
                                            </ul>-->
                                              
                                        </li>
                                        <!-- <li><a href="<?php echo site_url('artists/albums'); ?>">Albums</a></li> -->
                                          <li><a href="<?php echo site_url('homepage/gallery');?>">Gallery</a>
                                    </ul>
                                </li>


                                <li><a href="<?php echo site_url('events')?>">Events</a></li>

                                 <li><a href="<?php echo site_url('news')?>">News</a></li>



                                <!-- <li class="dropdown"><a href="">Gallery</a></li>
                               
                                <li><a href="">Blog</a></li> -->


                                 <li><a href="<?php echo site_url('jobs')?>">Jobs</a></li>
                                <li><a href="<?php echo site_url('about');?>">About Us</a></li>
                                <li><a href="<?php echo site_url('contact')?>">Contact Us</a></li>
                            </ul>
                            <div class="clearfix"></div>
        
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                </div>
                
            </div>
             </div>