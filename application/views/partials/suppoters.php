<section class="client-logo wow fadeIn" data-wow-delay="300ms" data-wow-duration="1000ms">
    	<div class="auto-container">
        <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2><span>Supported By</span></h2>
            </div>
        	<div class="slider-container">
            	
                <ul class="slider">
                <?php if(isset($supportersdata)){ foreach($supportersdata as $key=>$supporter) {?>
                	<li><a href="<?php echo $supporter['supporterURL']?>" target="_blank" ><img style="width:200px; height:64px;" src="<?php echo $supporter['supporterLogo']?>" alt="<?php echo $supporter['supporterName']?>"  title="<?php echo $supporter['supporterName']?>"></a></li>
                  <?php } }else{ ?>

                         <div class="subscribe-area">
                            <div class="subs-box">
                
                                         <div class="alert alert-success" role="alert">
                                            <h4 class="text-center">Sorry!, No supporters for now.</h4> 
                                         </div>                       
                                  
                            </div>
                      </div>


                 <?php   } ?>

                </ul>
                
            </div>
        </div>
    </section>