<section class="main-slider">
 <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                  <?php if(isset($slidershowadata)){foreach($slidershowadata as $key=>$slideshow){?>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?php echo $slideshow['slideshowImageURL'];?>"  data-saveperformance="off"  data-title="<?php echo $slideshow['slideshowTitle']; ?>"> <!-- MAIN IMAGE --> 
                    <img src="<?php echo $slideshow['slideshowImageURL'];?>"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-60"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="big-title"><h2 style="color:white"> <?php echo $slideshow['slideshowTitle']; ?></h2></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="100"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-text"><p style="color:white">
                        <?php

                         $slide =  $slideshow['slideshowDescriptions'];
                         $slideshort = word_limiter($slide,20);
                         echo $slideshort;

                        ?>

                        </p></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="170"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                       <div class="link-btn"><a href = "<?php echo site_url('slideshow/readmore/'.$slideshow['slideId'])?>" class="primary-btn hvr-bounce-to-left">
                          <span class="btn-text ">LEARN MORE</span>
                             <strong class="icon"><span class="f-icon flaticon-right11">
                         
                </span></strong></a></div></div>
                    
                    
                    </li>
                     <?php } }else{?>
                          

                          <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="<?php echo base_url();?>assets/images/main-slider/image-1.jpg"  data-saveperformance="off"  data-title="We are Awsome"> <!-- MAIN IMAGE --> 
                    <img src="<?php echo base_url();?>assets/images/main-slider/fineartbanner.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-60"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="big-title"><h2 style="color:white"> WE <br>ARRANGE <br>BEAUTIFUL EVENTS</h2></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="100"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-text"><p style="color:white">Lorem Ipsum is simply dummy text of the printing <br>and typesetting industry</p></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="170"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="link-btn"><a class="primary-btn hvr-bounce-to-left"><span class="btn-text ">LEARN MORE</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a></div></div>
                    
                    
                    </li>

                    
                    <li data-transition="slidedown" data-slotamount="1" data-masterspeed="1000" data-thumb="<?php echo base_url();?>assets/images/main-slider/image-1.jpg"  data-saveperformance="off"  data-title="We are Awsome"> <!-- MAIN IMAGE --> 
                    <img src="<?php echo base_url();?>assets/images/main-slider/handcrafts.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="-60"
                    data-speed="1500"
                    data-start="500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="big-title"><h2>WE <br>MAKE IT <br>HAPPEN</h2></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="100"
                    data-speed="1500"
                    data-start="1000"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-text"><p>Lorem Ipsum is simply dummy text of the printing <br>and typesetting industry</p></div></div>
                    
                    <div class="tp-caption lfb tp-resizeme"
                    data-x="left" data-hoffset="15"
                    data-y="center" data-voffset="170"
                    data-speed="1500"
                    data-start="1500"
                    data-easing="easeOutExpo"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="link-btn"><a class="primary-btn hvr-bounce-to-left"><span class="btn-text">LEARN MORE</span> <strong class="icon"><span class="f-icon flaticon-right11"></span></strong></a></div></div>
                    
                    
                    </li>


                     <?php }?>
                    
                </ul>
                
               <div class="tp-bannertimer"></div>
            </div>
        </div>
        </section>