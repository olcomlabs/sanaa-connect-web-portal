 <section class="services-area">
    	<div class="auto-container">
        	
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Upcoming <span>Events</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour, or randomised words which don't look even slightly believable</p>
            </div>
            
            
            <!--Service Tabs-->
            <div class="service-tabs clearfix">
            	
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                	<!--Active Btn-->
                	<li class="tab-btn hvr-bounce-to-bottom active-btn" data-id="#architecture-tab">
                    	<div class="icon"><span class="flaticon-wall20"></span></div>
                        <strong>Mark's Weeding</strong>
                        <span class="skill">SKILL - 85% </span>
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-bottom" data-id="#renovation-tab">
                    	<div class="icon"><span class="flaticon-hammer55"></span></div>
                        <strong>Masum's Birthday</strong>
                        <span class="skill">SKILL - 82% </span>
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-bottom" data-id="#construction-tab">
                    	<div class="icon"><span class="flaticon-barrow"></span></div>
                        <strong>5thY Celebration</strong>
                        <span class="skill">SKILL - 78% </span>
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-bottom" data-id="#isolation-tab">
                    	<div class="icon"><span class="flaticon-saw6"></span></div>
                        <strong>Birth Day Party</strong>
                        <span class="skill">SKILL - 92% </span>
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-bottom"  data-id="#sanitary-tab">
                    	<div class="icon"><span class="flaticon-pipe9"></span></div>
                        <strong>Nodi's New Phone</strong>
                        <span class="skill">SKILL - 76% </span>
                    </li>
                </ul>
                
                <!--Tabs Content-->
                <div class="tab-content wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                	
                    <!--Tab / Active Tab-->
                    <div class="tab active-tab clearfix" id="architecture-tab">
                    	<!--Content Box-->
                        <div class="content-box">
                        	<h4>MARK'S WEEDING</h4>
                            
                            <!--Tab Text-->
                            <div class="tab-text">
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <p>Randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                            
                            <!--Skill Meter-->
                            <h4>Event Quality</h4>
                            <div class="skill-meter">
                            	<div class="progress-bar"><span class="percent">85%</span></div>
                            </div>
                            
                            <!--Work Process-->
                            <div class="work-process">
                            	<h4>Major Expertise</h4>
                                <br>
                                <div class="row clearfix">
                                	
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">1</div>
                                        <span>Field</span>
                                        <br>
                                        <span class="work">Measuring </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">2</div>
                                        <span>Layout</span>
                                        <br>
                                        <span class="work">Experiment </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">3</div>
                                        <span>Final</span>
                                        <br>
                                        <span class="work">Output </span>
                                    </div>
                                    
                                </div>
                            	
                            </div>
                            
                        </div><!--Content Box End-->
                        
                        <!--Featured Person-->
                        <article class="featured-box">
                        	<div class="box-inner">
                    
                                <figure class="image">
                                    <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/featured-image-1.jpg" alt="" />
                                    <span class="curve"></span>
                                </figure>
                                <div class="content">
                                    <div class="inner-box">
                                        <h3>Rashed Kabir <span>Event Organizer</span></h3>
                                        <div class="text">There are many variations of  the passages of Lorem . . .</div>
                                        <div class="social"><a href="#" class="fa fa-facebook-f"></a> &ensp; <a href="#" class="fa fa-twitter"></a> &ensp; <a href="#" class="fa fa-google-plus"></a> &ensp; <a href="#" class="fa fa-linkedin"></a></div>
                                    </div>
                                </div>
                                
                            </div>
                        </article><!--Featured Person End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="renovation-tab">
                    	<!--Content Box-->
                        <div class="content-box">
                        	<h4>MASUM'S BIRTHDAY</h4>
                            
                            <!--Tab Text-->
                            <div class="tab-text">
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <p>Randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                            
                            <!--Skill Meter-->
                            <h4>Event Quality</h4>
                            <div class="skill-meter">
                            	<div class="progress-bar"><span class="percent">85%</span></div>
                            </div>
                            
                            <!--Work Process-->
                            <div class="work-process">
                            	<h4>MAJOR EXPERTISE</h4>
                                <br>
                                <div class="row clearfix">
                                	
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">1</div>
                                        <span>Field</span>
                                        <br>
                                        <span class="work">Measuring </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">2</div>
                                        <span>Layout</span>
                                        <br>
                                        <span class="work">Experiment </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">3</div>
                                        <span>Final</span>
                                        <br>
                                        <span class="work">Output </span>
                                    </div>
                                    
                                </div>
                            	
                            </div>
                            
                        </div><!--Content Box End-->
                        
                        <!--Featured Person-->
                        <article class="featured-box">
                        	<div class="box-inner">
                    
                                <figure class="image">
                                    <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/featured-image-1.jpg" alt="" />
                                    <span class="curve"></span>
                                </figure>
                                <div class="content">
                                    <div class="inner-box">
                                        <h3>Masum Rana <span>Event Organizer</span></h3>
                                        <div class="text">There are many variations of  the passages of Lorem . . .</div>
                                        <div class="social"><a href="#" class="fa fa-facebook-f"></a> &ensp; <a href="#" class="fa fa-twitter"></a> &ensp; <a href="#" class="fa fa-google-plus"></a> &ensp; <a href="#" class="fa fa-linkedin"></a></div>
                                    </div>
                                </div>
                                
                            </div>
                        </article><!--Featured Person End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="construction-tab">
                    	<!--Content Box-->
                        <div class="content-box">
                        	<h4>5TH Year CELEBRATION</h4>
                            
                            <!--Tab Text-->
                            <div class="tab-text">
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <p>Randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                            
                            <!--Skill Meter-->
                            <h4>Event Quality</h4>
                            <div class="skill-meter">
                            	<div class="progress-bar"><span class="percent">85%</span></div>
                            </div>
                            
                            <!--Work Process-->
                            <div class="work-process">
                            	<h4>MAJOR EXPERTISE</h4>
                                <br>
                                <div class="row clearfix">
                                	
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">1</div>
                                        <span>Field</span>
                                        <br>
                                        <span class="work">Measuring </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">2</div>
                                        <span>Layout</span>
                                        <br>
                                        <span class="work">Experiment </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">3</div>
                                        <span>Final</span>
                                        <br>
                                        <span class="work">Output </span>
                                    </div>
                                    
                                </div>
                            	
                            </div>
                            
                        </div><!--Content Box End-->
                        
                        <!--Featured Person-->
                        <article class="featured-box">
                        	<div class="box-inner">
                    
                                <figure class="image">
                                    <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/featured-image-1.jpg" alt="" />
                                    <span class="curve"></span>
                                </figure>
                                <div class="content">
                                    <div class="inner-box">
                                        <h3>Muhibbur Rashid <span>Event Organizer</span></h3>
                                        <div class="text">There are many variations of  the passages of Lorem . . .</div>
                                        <div class="social"><a href="#" class="fa fa-facebook-f"></a> &ensp; <a href="#" class="fa fa-twitter"></a> &ensp; <a href="#" class="fa fa-google-plus"></a> &ensp; <a href="#" class="fa fa-linkedin"></a></div>
                                    </div>
                                </div>
                                
                            </div>
                        </article><!--Featured Person End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="isolation-tab">
                    	<!--Content Box-->
                        <div class="content-box">
                        	<h4>BIRTH DAY PARTY</h4>
                            
                            <!--Tab Text-->
                            <div class="tab-text">
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <p>Randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                            
                            <!--Skill Meter-->
                            <h4>Event Quality</h4>
                            <div class="skill-meter">
                            	<div class="progress-bar"><span class="percent">85%</span></div>
                            </div>
                            
                            <!--Work Process-->
                            <div class="work-process">
                            	<h4>MAJOR EXPERTISE</h4>
                                <br>
                                <div class="row clearfix">
                                	
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">1</div>
                                        <span>Field</span>
                                        <br>
                                        <span class="work">Measuring </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">2</div>
                                        <span>Layout</span>
                                        <br>
                                        <span class="work">Experiment </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">3</div>
                                        <span>Final</span>
                                        <br>
                                        <span class="work">Output </span>
                                    </div>
                                    
                                </div>
                            	
                            </div>
                            
                        </div><!--Content Box End-->
                        
                        <!--Featured Person-->
                        <article class="featured-box">
                        	<div class="box-inner">
                    
                                <figure class="image">
                                    <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/featured-image-1.jpg" alt="" />
                                    <span class="curve"></span>
                                </figure>
                                <div class="content">
                                    <div class="inner-box">
                                        <h3>Tafseer Hussain <span>Event Organizer</span></h3>
                                        <div class="text">There are many variations of  the passages of Lorem . . .</div>
                                        <div class="social"><a href="#" class="fa fa-facebook-f"></a> &ensp; <a href="#" class="fa fa-twitter"></a> &ensp; <a href="#" class="fa fa-google-plus"></a> &ensp; <a href="#" class="fa fa-linkedin"></a></div>
                                    </div>

                                </div>
                                
                            </div>
                        </article><!--Featured Person End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="sanitary-tab">
                    	<!--Content Box-->
                        <div class="content-box">
                        	<h4>NODI'S NEW PHONE</h4>
                            
                            <!--Tab Text-->
                            <div class="tab-text">
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <p>Randomised words which don't look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                            
                            <!--Skill Meter-->
                            <h4>Event Quality</h4>
                            <div class="skill-meter">
                            	<div class="progress-bar"><span class="percent">85%</span></div>
                            </div>
                            
                            <!--Work Process-->
                            <div class="work-process">
                            	<h4>MAJOR EXPERTISE</h4>
                                <br>
                                <div class="row clearfix">
                                	
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">1</div>
                                        <span>Field</span>
                                        <br>
                                        <span class="work">Measuring </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">2</div>
                                        <span>Layout</span>
                                        <br>
                                        <span class="work">Experiment </span>
                                    </div>
                                    
                                    <!--Step-->
                                	<div class="col-md-4 col-sm-4 col-xs-12 step">
                                    	<div class="num img-circle">3</div>
                                        <span>Final</span>
                                        <br>
                                        <span class="work">Output </span>
                                    </div>
                                    
                                </div>
                            	
                            </div>
                            
                        </div><!--Content Box End-->
                        
                        <!--Featured Person-->
                        <article class="featured-box">
                        	<div class="box-inner">
                    
                                <figure class="image">
                                    <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/featured-image-1.jpg" alt="" />
                                    <span class="curve"></span>
                                </figure>
                                <div class="content">
                                    <div class="inner-box">
                                        <h3>John James <span>Event Organizer</span></h3>
                                        <div class="text">There are many variations of  the passages of Lorem . . .</div>
                                        <div class="social"><a href="#" class="fa fa-facebook-f"></a> &ensp; <a href="#" class="fa fa-twitter"></a> &ensp; <a href="#" class="fa fa-google-plus"></a> &ensp; <a href="#" class="fa fa-linkedin"></a></div>
                                    </div>

                                </div>
                                
                            </div>
                        </article><!--Featured Person End-->
                        
                    </div><!--Tab End-->
                    
                </div>
                
            </div>
            
        </div>
    </section>