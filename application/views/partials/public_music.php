<section class="page-banner" style="background-image:url(<?php echo base_url()?>assets/images/background/music/musicbanner.jpg);">
      <div class="auto-container text-center">
          <h1>Music</h1>
            <ul class="bread-crumb"><li><a href="<?php echo base_url()?>">Home</a></li><li>Browse</li> <li>Music</li> </ul>
        </div>
    </section>


  <section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <div class="col-md-8">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 style="visibility: visible; animation-name: fadeInLeft;" class="wow fadeInLeft animated">List of Music</h2>
                        <div style="visibility: visible; animation-name: fadeInUp;" class="post wow fadeInUp animated">
                          
                            <div style="visibility: visible; animation-name: fadeInUp;" class="post-content wow fadeInUp animated"> 

                                    <?php if(isset($allmusic)){ foreach($allmusic as $key=>$music){?>
                                    <!-- Author Section -->
                                    <div style="visibility: visible; animation-name: fadeInUp;" class="wow fadeInUp animated">
                                        <!-- Image -->
                                       <div class="post-content">    
                                    <iframe width="100%" height="166" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/<?php echo $music['cdnId']; ?>&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true&amp;show_comments=false&amp;show_user=false&amp;show_reposts=false"></iframe>
                                
                                     </div>
                                            
                                    <div class="clear"></div>             
                                    </div><!-- Author Section Ends-->


                                    <?php } } else{?>
                                    
                                               <div class="subscribe-area">
                                                      <div class="subs-box">
                                          
                                                                   <div class="alert alert-success" role="alert">
                                                                    <h4 class="text-center">Sorry!, No List of Music for now.</h4> 
                                                                   </div>                       
                                                            
                                                               </div>
                                                            </div>
 
                                    <?php } ?>
<div style="visibility: visible; animation-name: fadeInRight;" class="post-nav wow fadeInRight animated" data-animation="fadeInUp" data-animation-delay="300">
                                      <?php echo   $this->pagination->create_links();?>
            </div>

                            </div>
                        </div>
                    </div><!-- End Post --> 
                    




                    
                </div><!-- Blog Left Side Ends -->
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-4">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="search wow fadeInUp animated">
                            <form>
                                <input name="name" placeholder="SEARCH.." type="search">
                                <input value="submit" type="submit">
                            </form>
                        </div>
                        
                        <!-- Popular Post -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="blog/popular-post widget wow fadeInUp animated">
                            <!-- Title -->
                            <h2>most popular Album</h2>
                            <ul class="popular-list">
                            <?php if(isset($mostpopularmusic)){ foreach($mostpopularmusic as $key=>$popularmusic){?>
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="<?php echo site_url('homepage/specificmusic/'.$popularmusic['albumId'])?>"><img width="82" height="84" src="<?php 
                                if( isset($popularmusic['albumCover'] ) && $popularmusic[ 'albumCover'] != NULL){
                                  echo  $popularmusic['albumCover']; 
                                }
                                else{
                                    echo base_url().'assets/images/events/eventsplaceholder.jpg';                                  
                                }
                                ?>" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="<?php echo site_url('homepage/specificmusic/'.$popularmusic['albumId'])?>"><?php echo $popularmusic[ 'albumName'] ?></a></h3>
                                      <!--   <div class="posted-date">Released : July 19, 2014</div> -->
                                    </div>
                                </li>



                                    <?php } } else{?>
                                    
                                               <div class="subscribe-area">
                                                      <div class="subs-box">
                                          
                                                                   <div class="alert alert-success" role="alert">
                                                                    <h4 class="text-center">Sorry!, No Most Popular Music for now.</h4> 
                                                                   </div>                       
                                                            
                                                               </div>
                                                            </div>
 
                                    <?php } ?>
                            
                
        
                              
                            </ul>
                        </div><!-- Popular Post Ends-->
                        

                        
                        
                        <!-- Category Posts -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="category widget wow fadeInUp animated">
                            <!-- Title -->
                            <h2>Categories</h2>
                            <ul class="category-list">
                                <li>
                                    <h3><a href="blog-detail.html#">HIPHOP</a></h3>
                                </li>
                                <li>
                                    <h3><a href="blog-detail.html#">BongoFlava </a></h3>
                                </li>
                                <li>
                                    <h3><a href="blog-detail.html#">Others</a></h3>
                                </li>

                            </ul>
                        </div><!-- Category Ends-->
                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>