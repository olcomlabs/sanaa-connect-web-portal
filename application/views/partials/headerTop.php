<div class="header-top">
        	<div class="auto-container clearfix">
            	
                <!-- Top Left -->
                <div class="top-left pull-left clearfix">
                	<div class="email pull-left"><a href="info@sanaaconnect.co.tz"><span class="f-icon flaticon-email145"></span> info@sanaaconnect.co.tz</a></div>
                    <div class="phone pull-left"><a href="#"><span class="f-icon flaticon-phone325"></span> +255 264 234 233</a></div>
                </div>
                
                <!-- Top Right -->
                <nav class="top-right pull-right">
                	<ul>
                    	
                        <?php
                            $ci = &get_instance();
                            $identifier = $ci->session->userdata('authenticated'); 
                            $profile = $ci->AuthModel->getUserProfile(null,null,null,$identifier);

                            if( isset($identifier) AND $identifier != null ){
                             ?>
                             <li><a href="#"><?php echo $profile['displayName']; ?></a></li> 
                            <li><a href="<?php echo site_url();?>/artists/myProfile">My Profile</a></li>
                             <li><a href="<?php echo site_url();?>/hauth/logout">Logout</a></li>       
                             <?php
                            }else{
                            ?>
                            <li><a href="<?php echo site_url();?>/hauth">Login</a></li>
                        <?php } ?>
                    </ul>
                </nav>
                
            </div>
        </div>