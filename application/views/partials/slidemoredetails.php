<section id="blog" class="blog-area single section">
        <div class="auto-container">
            <div class="row">
                <!-- Blog Left Side Begins -->
                <?php if(isset($slideById)){foreach($slideById as $key=>$slideshow){?>
                <div class="col-md-12">
                    <!-- Post -->
                    <div class="post-item">
                        <!-- Post Title -->
                        <h2 style="visibility: visible; animation-name: fadeInLeft;" class="wow fadeInLeft animated">
                            <?php $title = $slideshow['slideshowTitle'];
                                 echo  strip_tags($title);
                             ?>
                        </h2>
                        <div style="visibility: visible; animation-name: fadeInUp;" class="post wow fadeInUp animated">
                            <!-- Image -->
                            <img class="img-responsive" src="<?php echo $slideshow['slideshowImageURL'];?>" alt="">
                            <div style="visibility: visible; animation-name: fadeInUp;" class="post-content wow fadeInUp animated"> 
                                <!-- Meta -->
                                <div class="posted-date">July 19, 2014   /   <span>by</span> <a href="blog-detail.html#">John</a>   /   <a href="blog-detail.html#">12 Comments</a></div>
                                <!-- Text -->
                                <p><?php echo $slideshow['slideshowDescriptions']; ?></p>
                                <div class="share-btn" style="padding-bottom:20px;padding-top:20px;">
                                Share on :<br>
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                    <a class="a2a_button_reddit"></a>
                                    <a class="a2a_button_tumblr"></a>
                                    <a class="a2a_button_pinterest"></a>
                                    <a class="a2a_button_linkedin"></a>
                                    </div>
                                    <!-- AddToAny END -->
                                </div>
                            </div>
                        </div>
                    </div><!-- End Post --> 
                
            </div>
            <?php }}else{?>
                No slider related to that.
            <?php }?>
        
        </div>
    </section>