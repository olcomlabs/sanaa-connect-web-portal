<section class="our-projects">
    	<div class="auto-container">
        	
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2>Events With <span>Screenshots</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour, or randomised words which don't look even slightly believable</p>
            </div>
            
            <!--Filters Nav-->
            <ul class="filter-tabs clearfix anim-3-all">
                <li class="filter" data-role="button" data-filter="all"><span class="btn-txt">All</span></li>
                <li class="filter" data-role="button" data-filter="buildings"><span class="btn-txt">Weeding</span></li>
                <li class="filter" data-role="button" data-filter="hospital"><span class="btn-txt">Music</span></li>
                <li class="filter" data-role="button" data-filter="school"><span class="btn-txt">Official</span></li>
                <li class="filter" data-role="button" data-filter="isolation"><span class="btn-txt">Birthday</span></li>
                <li class="filter" data-role="button" data-filter="mall"><span class="btn-txt">Ceremony</span></li>
                <li class="filter" data-role="button" data-filter="others"><span class="btn-txt">Others</span></li>
              <li class="filter" data-role="button" data-filter="others"><span class="btn-txt">Others</span></li>
            </ul>
        </div>

        
        <!--Projects Container-->
        <div class="projects-container filter-list clearfix">
        	
            <article class="project-box mix mix_all mall architecture">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-1.jpg" alt=""><a href="images/resource/proj-image-1.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Birthday Event</span>
                        <h4>Birthday Party</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all school other buildings">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-2.jpg" alt=""><a href="images/resource/proj-image-2.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Weeding</span>
                        <h4>Weeding Event</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all isolation architecture">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-3.jpg" alt=""><a href="images/resource/proj-image-3.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Ceremony </span>
                        <h4>Ceremoney Event</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all buildings hospital others">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-4.jpg" alt=""><a href="images/resource/proj-image-4.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Music  </span>
                        <h4>Band Music Party</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all buildings architecture">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-5.jpg" alt=""><a href="images/resource/proj-image-5.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Acupuncture </span>
                        <h4>Acupuncture Image</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all school mall isolation buildings">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-6.jpg" alt=""><a href="images/resource/proj-image-6.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Angioplasty</span>
                        <h4>Angioplasty Image</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all mall architecture others">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-1.jpg" alt=""><a href="images/resource/proj-image-1.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Acupuncture</span>
                        <h4>Acupuncture Image</h4>
                    </div>
                </div>
            </article>
            
            <article class="project-box mix mix_all mall isolation hospital buildings architecture others">
            	<figure class="image"><img src="<?php echo base_url();?>assets/images/resource/proj-image-7.jpg" alt=""><a href="images/resource/proj-image-7.jpg" class="lightbox-image zoom-icon"></a></figure>
                <div class="text-content">
                	<div class="text">
                    	<span class="cat">Social</span>
                        <h4>Social Walefare</h4>
                    </div>
                </div>
            </article>
            
        </div>
    </section>