 <section class="top-services" style="padding-top:0px;">
        <div class="auto-container">
            
            <div class="sec-title wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2><span>Featured Events</span></h2>
            </div>
                
            <div class="sec-text wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in<br>some form, by injected humour,
                 or randomised words which don't look even slightly believable</p>-->
            </div>
            
            <div class="row clearfix">
                
                         <?php if(isset($eventdata)){foreach($eventdata as $key=>$event){?>
                <!--Post-->
                <article class="col-md-4 col-sm-6 col-xs-12 post wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                    <div class="post-inner">
                    
                        <figure class="image">
                            <img  style = "width: 340px; height:220px;" class="img-responsive" src="<?php 
                                if( isset($event['eventPhotoURL'] ) && $event[ 'eventPhotoURL'] != NULL){
                                  echo  $event['eventPhotoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/events/eventsplaceholder.jpg';                                  
                                }
                                ?>" alt="" />
                            <span class="curve"></span>
                        </figure>
                        <div class="content">
                            <div class="inner-box">
                                <h3><?php echo $event['eventTitle'] ?></h3>
                                <div class="text">
                                    
                                <?php $eventdata = $event['eventDescriptions'] ?>
                                  <?php $readmore = word_limiter($eventdata, 10); ?>
                                  <?php echo $readmore;?>
                                </div>
                                
                                     <span>Event Date:</span><b> December 5, 2015.</b>
                                
                                <a href="<?php echo site_url('events/readmore/'.$event['eventId'])?>" class="primary-btn hvr-bounce-to-left"><span class="btn-text">READ MORE</span></a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } }else{ ?>

                         <div class="subscribe-area">
                    <div class="subs-box">
        
                                 <div class="alert alert-success" role="alert">
                                    <h4 class="text-center">Sorry!, No Events for now.</h4> 
                                 </div>                       
                          
                           </div>
                      </div>


                 <?php   } ?>
                

                
            </div>
        </div>
	</section>