 <section class="testimonials-area" style="background-image:url(<?php echo base_url();?>assets/images/parallax/image-1.jpg);">
    	<div class="auto-container clearfix">
        	<h2 class="wow tada" data-wow-delay="0ms" data-wow-duration="1500ms">TESTI<span>MONIALS</span></h2>
            <div class="icon-quote"><span class="fa fa-quote-left"></span></div>
        	<!--Slider-->
            <ul class="slider">
        		<li class="slide-item">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</li>
                <li class="slide-item">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</li>
                <li class="slide-item">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</li>
            </ul>
            
            <!--Custom-Pager-->
            <div class="custom-pager clearfix">
            	
            	<a href="#" class="pager-item active" data-slide-index="0">
                	<span class="testi-thumb"><img class="img-circle" src="<?php echo base_url();?>assets/images/resource/testi-thumb-1.jpg" alt="" title=""></span>
                    <strong>Jhone Doe</strong>MD/Apple
                </a>
                <a href="#" class="pager-item" data-slide-index="1">
                	<span class="testi-thumb"><img class="img-circle" src="<?php echo base_url();?>assets/images/resource/testi-thumb-2.jpg" alt="" title=""></span>
                    <strong>Grey White</strong>CEO/Google Inc
                </a>
                <a href="#" class="pager-item" data-slide-index="2">
                	<span class="testi-thumb"><img class="img-circle" src="<?php echo base_url();?>assets/images/resource/testi-thumb-3.jpg" alt="" title=""></span>
                    <strong>White More</strong>PM/Amazon
                </a>
                
            </div>
            
        </div>