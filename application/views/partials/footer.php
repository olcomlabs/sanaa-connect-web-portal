 <footer class="main-footer">
        <div class="auto-container">
        	
            <!--Subscribe Area-->
            <div class="subscribe-area">
                       <?php if(isset($messagesent) && $messagesent != NULL){?>
                                 
                                 <div class="alert alert-success alert-dismissible fade in" role="alert"> 
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                       <h4 class="text-center"><?php echo $messagesent;?></h4> 
                                </div>


                                 <?php } ?>

                              <?php if(isset($formerror) && $formerror != NULL){ ?>
        
                                 <div class="alert alert-danger alert-dismissible fade in" role="alert"> 
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                       <h4 class="text-center"><?php echo $formerror;?></h4> 
                                </div>
                                <?php }?>
            	<div class="subs-box">
                
                      <div class="row clearfix">
                        <h2 class="col-md-6 col-sm-12 col-xs-12 pull-left">Subcribe Our <span>News</span></h2>
                        
                        <!--Form Box-->
                        <div class="form-box col-md-6 col-sm-12 col-xs-12  pull-right">
                            <?php echo form_open('homepage/subscribe')?>
                                <div class="form-group">
                                    <input type="email" name="email" id="email"  placeholder="Enter Email">
                                    <button type="submit" ><span class="icon fa fa-paper-plane"></span></button>
                                </div>
                            <?php echo form_close();?>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <!--Footer Widget Area-->
            <div class="footer-widget-area clearfix">
            
            	<!--Footer Widget-->
                <article class="footer-widget about-widget col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">
                	<h3>Sanaa Connect</h3>
                    <div class="widget-content">
                    	<p>Sanaa Connect is the web portal for helping Tanzania Artists to advertise themselves around the world.</p>
                        <p>Also it's the place where you can find works of all kind of artists living in Tanzania.</p>
                    </div>
                </article>
                
                <!--Footer Widget-->
                <article class="footer-widget quick-links col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                	<h3>Quick Links</h3>
                    <div class="widget-content">
                    	<ul>
                            <li><span class="fa fa-angle-right"></span> <a href="<?php echo site_url('artists')?>">Artists</a></li>
                            <li><span class="fa fa-angle-right"></span> <a href="<?php echo site_url('news')?>">News</a></li>
                            <li><span class="fa fa-angle-right"></span> <a href="<?php echo site_url('events')?>">Events</a></li>
                            <li><span class="fa fa-angle-right"></span> <a href="#">Support</a></li>
                        </ul>
                    </div>
                </article>
                
                <!--Footer Widget-->
                <article class="footer-widget address col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms">
                	<h3>Address</h3>
                    <div class="widget-content">
                    	<ul class="info">
                        	<li><span class="fa fa-map-marker"></span> &ensp; British Council</li>
                            <li><span class="fa fa-phone"></span> &ensp; (+255) 756 545 224</li>
                            <li><span class="fa fa-envelope"></span> &ensp; info@sanaaconnect.co.tz</li>
                        </ul>
                        
                        <div class="social">
                             <a class="img-circle fa fa-facebook-f" target="blank" href="http://www.facebook.com/sanaaconnect"></a> 
                            <a class="img-circle fa fa-twitter" target="blank" href="http://www.twitter.com/sanaaconnect"></a> 
                             <a class="img-circle fa fa-google-plus" target="blank" href="https://plus.google.com/sanaaconnect"></a>
                             <a class="img-circle fa fa-instagram" target="blank" href="https://www.instagram.com/sanaaconnect/"></a>
                       </div>
                    </div>
                </article>
      
                
            </div>
        </div>
        
        <!--Footer Bottom-->
        <div class="footer-bottom">
        	<div class="auto-container text-center">&copy; <?php echo date('Y');?> <a href="#">Sanaa Connect</a></div>
        </div>
        
	</footer>