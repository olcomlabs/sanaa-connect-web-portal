    <section class="we-are-best">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <div class="col-md-6 col-sm-6 col-xs-12 image-side">
                	<figure class="image"><img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/about-us-image-1.jpg" alt="" title=""></figure>
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12 text-side">
                	<h2 class="wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms">The Best <span>Event theme</span> Ever!!</h2>
                    <div class="text wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                    	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    </div>
                    <div class="link-btn wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1000ms"><a class="primary-btn light hvr-bounce-to-left" href="contact-us.html"><span class="btn-text">Book Your Position</span> <strong class="icon"><span class="f-icon flaticon-new100"></span></strong></a></div>
                </div>
                
            </div>
        </div>
    </section>