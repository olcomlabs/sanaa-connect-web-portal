<section class="page-banner">
    	<div class="auto-container text-center">
        	<h1>Search Results</h1>
           
        </div>
    </section>

<section id="blog" class="blog-area section">
        <div class="auto-container">
            <div class="row">
                
                
                <!-- Blog Sidebar Begins -->
                <div class="col-md-12">
                
                    <div class="sidebar">
                    
                        <!-- Search -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="search wow fadeInUp animated">
                          <?php echo form_open('homepage/search')?>
                                <input  name="search" placeholder="SEARCH.." type="search">
                                <input value="submit" type="submit">
                            <?php echo form_close();?>
                        </div>
                       
                        <!-- Popular Post -->
                        <div style="visibility: visible; animation-name: fadeInUp;" class="blog/popular-post widget wow fadeInUp animated">
                            <!-- Title -->
                            <?php if(isset($searchresult)){ ?>
                            <h2><?php echo count($searchresult) ?> Result(s)</h2>
                            <?php }?>
                            <ul class="popular-list">

                            <?php if(isset($searchresult)){ foreach($searchresult as $key=>$search) {?>
                          
                                <!-- Item -->
                                <li>
                                    <!-- Post Image -->
                                    <a href="blog.html#"><img src="images/blog/popular-post/1.jpg" alt=""></a>
                                    <!-- Details -->
                                    <div class="content">
                                        <h3><a href="#"></a></h3>
                                        <div class="posted-date"></div>
                                    </div>
                                </li>
                                <!-- Item -->


                                <?php } }else{ ?>

                    	 <div class="subscribe-area">
	            	<div class="subs-box">
		
		                         <div class="alert alert-success" role="alert">
		                         	<h4 class="text-center">Sorry!, No results found related to your search.</h4> 
		                         </div>                       
		                  
	                       </div>
                      </div>


                 <?php   } ?>
                

          
                            
                            </ul>
                        </div><!-- Popular Post Ends-->
                        

                                            <!-- Pagination -->
                    <div style="visibility: hidden; animation-name: none;" class="post-nav wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
                            <ul class="pagination">
                            <li><a href="blog.html#">«</a></li>
                            <li><a href="blog.html#">1</a></li>
                            <li><a href="blog.html#">2</a></li>
                            <li><a href="blog.html#">3</a></li>
                            <li class="active"><a href="blog.html#">4</a></li>
                            <li><a href="blog.html#">5</a></li>
                            <li><a href="blog.html#">»</a></li>
                        </ul>
                    </div>	<!-- Pagination Ends-->
                        
                       
                        
                    </div>
                    
                </div><!-- Blog Sidebar Ends -->
                
            </div>
        
        </div>
    </section>
