-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 21, 2015 at 10:51 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sanaaconnect`
--

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`admin_id`, `username`, `password`, `email`, `last_activity`, `user_agent`, `ip_address`, `group_id`, `user_data`) VALUES
(3, 'super', '493bee4ce0e3098cff288c60a47eb1aa', NULL, NULL, NULL, NULL, 66, NULL);

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`albumId`, `artist_artistId`, `albumName`, `a_dateCreated`, `albumCover`, `viewCount`, `a_description`) VALUES
(1, 10, 'Test Album II', '2015-12-15', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450206164/p0mhhqd320cn2rszyrjk.jpg', 3, 'Test album Descript'),
(2, 10, 'My new album', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450338099/cddwbm7c6izwitpbc3fp.jpg', 1, 'Test album'),
(3, 10, 'Anti paper', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450361960/tmijjv1afmencn3va1bt.jpg', 1, 'My test album'),
(4, 11, 'Xmass season', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373559/pzgnfq0fshl78t8jdrul.jpg', 0, 'Xmass and my people'),
(5, 12, 'Xmass season', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450380390/p4hp9odgb9bnmcqvfujo.jpg', 0, 'Xmass and my people'),
(6, 14, 'Michano', '2015-12-19', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450540592/htvz0wcch2w61fmwfjdl.jpg', 9, 'Mikaliz'),
(7, 15, 'Xmass season', '2015-12-20', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450601260/amjqneigtgnobz3p4t2y.jpg', 15, 'Xmass season clips'),
(8, 15, 'xmas season', '2015-12-20', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450602820/wbp9ifi12ws8ib3q73gy.jpg', 19, 'xmas and amazing people');

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artistId`, `firstName`, `language`, `lastName`, `artistName`, `email`, `instagram`, `phone`, `age`, `description`, `dateCreated`, `active`, `password`, `coverPhoto`, `linkedin`, `identifier`, `gender`, `address`, `city`, `profileURL`, `webSiteURL`, `displayName`, `birthDay`, `birthMonth`, `birthYear`, `emailVerified`, `zip`, `country`, `region`, `photoURL`) VALUES
(4, '', NULL, '', NULL, 'luvandazz@gmail.com', NULL, '+255 756 444 333', NULL, 'Lorem ipsum dolar min.', NULL, 1, 'e012a61c6d1a4f875a3ca17df394be98', NULL, NULL, '10201392248126047', '', NULL, NULL, '', '', 'Cody Junior', NULL, NULL, NULL, '', NULL, NULL, '', 'https://graph.facebook.com/10201392248126047/picture?width=150&height=150'),
(5, NULL, NULL, NULL, NULL, 'test@test.com', NULL, 'fsdfs', NULL, 'fghfghfg', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', 'assets/images/artists/profile/coverphoto.jpg', NULL, '431116a1421a2f9535b8c0df88a0a3d8', NULL, NULL, NULL, NULL, NULL, 'Diamond Platinumz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost/sanaaconnect/assets/images/artists/featured/diamond.jpg'),
(7, NULL, NULL, NULL, NULL, 'photo@photo.com', NULL, '3838383838', NULL, 'Passionate photo grapher', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '834fc1e23d5c2d640bda6f6f64c37414', NULL, NULL, NULL, NULL, NULL, 'Damian Solo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost/sanaaconnect/assets/images/artists/featured/damian.jpg'),
(8, NULL, NULL, NULL, NULL, 'sdfsdfsd@fdfdssd.krhfg', NULL, 'ssdf', NULL, 'dfsdfsdfsd', NULL, 1, 'dcd989387b401ac29bf44755f31c0952', NULL, NULL, '4d5eb61378ab06359b38ed52417a8302', NULL, NULL, NULL, NULL, NULL, 'sfsdfs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, 'test@test.com3', NULL, 'sdfsdfsdf', NULL, 'sfsdfsfsdfsd', NULL, 1, '84d9cfc2f395ce883a41d7ffc1bbcf4e', NULL, NULL, 'b19ec0132dc3d3d4f24996cfca0d2740', NULL, NULL, NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, 'king@kong.com', NULL, '2323232323', NULL, 'King kong for real', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'dd7facc67039113a6c7f9d3c2a7c1252', NULL, NULL, NULL, NULL, NULL, 'kingkong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, 'genic@photo.com', NULL, '43040404', NULL, 'Photo genic mwenyewe', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'ae440ed56ba7e562c163c7bfbde068ef', NULL, NULL, NULL, NULL, NULL, 'photogen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, 'geni@tonic.com', NULL, '4949494949', NULL, 'Photographer with passion to life''s moments', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'd8f6d381078e630c02c80faa023265cc', NULL, NULL, NULL, NULL, NULL, 'genitonic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, 'keny@test.com', NULL, '32462346235', NULL, 'Test', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '27799898f477c7bb6cee9c44ad9c6693', NULL, NULL, NULL, NULL, NULL, 'Kenny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, 'niki@dot.com', NULL, '3939393939', NULL, 'fine any how', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '7d81bafe33105d38979d38504f18aa5c', NULL, NULL, NULL, NULL, NULL, 'niki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://res.cloudinary.com/olcomlabs/image/upload/v1450536276/hbpx6ybgqu3idspglhsq.jpg'),
(15, NULL, NULL, NULL, NULL, 'vide@o.com', NULL, '93939393', NULL, 'Video grapher for real', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '610d4aff5198f5b5222cc2df1bb36c42', NULL, NULL, NULL, NULL, NULL, 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://res.cloudinary.com/olcomlabs/image/upload/v1450601213/d3kopbgeidmodavtwf3y.jpg');

--
-- Dumping data for table `artist_category`
--

INSERT INTO `artist_category` (`ac_categoryId`, `ac_artistId`, `ac_dateCreated`) VALUES
(1, 4, '2015-12-14 00:00:00'),
(1, 7, '2015-12-28 00:00:00'),
(1, 10, '2015-12-14 00:00:00'),
(1, 14, '0000-00-00 00:00:00'),
(3, 15, '0000-00-00 00:00:00');

--
-- Dumping data for table `artist_subcategory`
--

INSERT INTO `artist_subcategory` (`artist_subcatId`, `artist_subcatName`) VALUES
(1, 'musician'),
(2, 'craftMan'),
(3, 'video Producer'),
(4, 'actor');

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `categoryName`, `cat_DateCreated`, `cat_dateChanged`) VALUES
(1, 'musician', '2015-12-08 00:00:00', NULL),
(2, 'fine Artist', '2015-12-14 00:00:00', NULL),
(3, 'video grapher', '2015-12-20 11:45:26', '2015-12-20 11:45:26');

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eventId`, `eventTitle`, `eventCategory`, `eventLocation`, `eventDescriptions`, `eventDate`, `eventPostedBy`, `eventPhotoURL`, `memberId`) VALUES
(1, 'British Dance Edition 2016', NULL, 'Wales Millennium Centre ', 'British Dance Edition is the biennial showcase and initiative of the National Dance Network hosted in different cities across the UK. It is renowned for presenting the best of British dance and is a market place for the buying and selling of dance product.\r\nBritish Dance Edition 2016 takes place in Wales Millennium Centre, set in the heart of Cardiff Bay and the thriving Capital of Wales. The four-day industry focused showcase for British Dance will also be hosted by Coreo Cymru, Chapter, National Dance Company of Wales and Riverfront Theatre, Newport who are looking forward to extending a warm Welsh welcome to UK and international promoters and presenters.\r\nThe 2016 showcase will present over 30 leading UK companies and independent artists and offers a dynamic, diverse and high quality platform that inspires exchanges, debates and new partnerships.\r\nRegistration opens from November 2015.', '2015-12-24', 'Calore Blake', 'assets/images/events/sanaaeventd.jpg', 1),
(2, 'The Nyama Choma Festival – December edition', NULL, 'UDSM SPORTS GROUNDS', '<p>Welcome To Our Meat World.This time around Nyama Choma is on the 6th December, 2014.</p><p>\r\nIt is the one and only event that offers a unique experience every four months in Dar es Salaam. One of a kind BBQ extravaganza showcases different BBQ pitmasters all styling different bbq skills;attend and get served by the best bbq pitmasters from around the city of Dar es Salaam ,enjoy your drink at our beer garden and music from the finest djs in the entertainment industry.</p>\r\n\r\n<p>The Nyama Choma Festival – December edition is here, get your tickets for Tshs 5000 now or Tshs 7000 on 7th of December</p>', '2015-12-06', 'Peter Thomas', 'assets/images/events/nyamachomaevent.jpg', 1),
(3, 'Swahili Fashion Week', NULL, 'SEA CLIFF HOTEL', '<p>The SIXTH Swahili Fashion Week is around the corner. East and Central Africa’s largest Fashion Platform.</p>\r\n\r\n<p>Swahili Fashion Week is now THE platform for fashion and accessory designers from Swahili speaking countries and the African continent to showcase their talent, market their creativity and network with clientele and the international fashion industry. This is all aimed at emphasizing to the region that fashion is an income generating creative industry, meanwhile promoting a “Made in Africa” concept.</p>\r\n\r\n<p>Swahili Fashion Week 2013 will feature more than 50 Designers from East and Central Africa and beyond with the Awards on the last day.\r\nSwahili Fashion Week – 5th to 8th Dec 2013</p>', '2015-12-06', 'John Doe', 'assets/images/events/swahilifashion.png', 1);

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`jobId`, `job_title`, `job_description`, `job_key_skills`, `job_contact_information`, `job_coverphoto`, `published_date`) VALUES
(1, 'sdfsffdsfds', '\n\nThe SIXTH Swahili Fashion Week is around the corner. East and Central Africa’s largest Fashion Platform.\n\nSwahili Fashion Week is now THE platform for fashion and accessory designers from Swahili speaking countries and the African continent to showcase their talent, market their creativity and network with clientele and the international fashion industry. This is all aimed at emphasizing to the region that fashion is an income generating creative industry, meanwhile promoting a “Made in Africa” concept.\n\nSwahili Fashion Week 2013 will feature more than 50 Designers from East and Central Africa and beyond with the Awards on the last day. Swahili Fashion Week – 5th to 8th Dec 2013\n', 'xcvbxcvgxhfjfgjfggjyyuiuiuku', 'gukghkjhjkhj', 'assets/images/jobs/jobcover.jpg', '2015-12-01 00:00:00');

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberId`, `memberName`, `memberEmail`, `memberPassword`, `memberPhoneNumber`, `memberProfilePhoto`, `memberIsApproved`, `memberLocation`) VALUES
(1, 'Peter Thomas', 'peter88tom@gmail.com', 'interface', '34635747567', NULL, 1, 'Kigamboni');

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`) VALUES
(16, 'personnel'),
(18, 'humanresources'),
(19, 'usermanagement'),
(21, 'news'),
(22, 'organizations'),
(23, 'events'),
(24, 'artists');

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`newsId`, `newsTitle`, `newsDescriptions`, `newsPublishDate`, `publishedBy`, `newsCoverPhoto`, `personnel_id`) VALUES
(1, 'BLOG HEADLINE HERE', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in\nsome form, by injected humour, or randomised words which don''t look even slightly believable', '2015-12-10', 'john Doe', 'assets/images/news/news-image-1.jpg', 1),
(2, 'New Video from Jux, One More Night', 'One More Night is a song about how he cannot go a day without a mystery woman he would love to have that one more night with. Produced by The Industry’s Nahreel, the song is a smooth feel of his insatiable struggles with wanting a girl’s affection/attention and not getting enough if it.', '2015-12-08', 'Kavishe Jonson', 'assets/images/news/jux.jpg', 1);

--
-- Dumping data for table `personnel`
--

INSERT INTO `personnel` (`personnel_id`, `firstname`, `middlename`, `lastname`, `gender`, `dob`, `marital_status`, `address`, `nok_phone`, `phone`, `region`, `citizenship`, `age`, `registration_date`) VALUES
(1, 'Sanaa', '', 'Connect', 'Male', '2015-12-21', 'Single', 'dar', '3333 333 333', '3444 444 443', 'Arusha', 'tanzanian', NULL, '2015-12-21 09:33:13');

--
-- Dumping data for table `soundcloud_profile`
--

INSERT INTO `soundcloud_profile` (`sc_profileId`, `sc_name`, `sc_avatar`, `sc_accessToken`, `sc_username`, `a_artistId`) VALUES
('190243713', 'Peter Thomas', 'https://i1.sndcdn.com/avatars-000191082288-xmx26t-large.jpg', '1-167353-190243713-b2ec2b9c8486e', 'Peter Thomas', 10);

--
-- Dumping data for table `sub_modules`
--

INSERT INTO `sub_modules` (`sub_module_id`, `sub_module_name`, `module_id`) VALUES
(57, 'modules', 0),
(59, 'sub_modules', 0),
(60, 'view_personnel', 0),
(61, 'groups', 0),
(62, 'users', 0),
(63, 'employees', 0),
(64, 'departments', 0),
(65, 'modules', 19),
(66, 'sub_modules', 19),
(67, 'users', 19),
(68, 'groups', 19),
(69, 'employees', 18),
(70, 'view_personnel', 16),
(71, 'published', 21),
(72, 'create_article', 21);

--
-- Dumping data for table `supporter`
--

INSERT INTO `supporter` (`supporterId`, `supporterName`, `supporterURL`, `supporterLogo`, `published`) VALUES
(1, 'European Union', 'http://europa.eu/index_en.htm', 'assets/images/supporters/1.png', 1),
(2, 'British Council Tanzania', 'https://www.britishcouncil.or.tz/', 'assets/images/supporters/2.png', 1),
(3, 'ALLIANCE FRANCAISE - ARUSHA', 'http://www.aftarusha.org/', 'assets/images/supporters/3.png', 1),
(4, 'Nafasi Art Space', 'http://nafasiartspace.org/s/', 'assets/images/supporters/4.png', 1);

--
-- Dumping data for table `welcome`
--

INSERT INTO `welcome` (`welcomeId`, `welcome_title`, `welcome_descriptions`) VALUES
(1, 'Welcome to Sanaa Connect.', 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.');

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`workId`, `a_albumId`, `aa_artistId`, `w_dateCreated`, `w_location`, `cdnId`, `w_description`, `w_viewCount`) VALUES
(2, 1, 10, '2015-12-15', 'http://soundcloud.com/peter-thomas-432181533/test-music', '237703300', NULL, 7),
(3, 1, 10, '2015-12-16', 'http://soundcloud.com/peter-thomas-432181533/fsdfhdfgh', '237722948', NULL, 0),
(4, 2, 10, '2015-12-17', 'http://soundcloud.com/peter-thomas-432181533/test-audio', '237943105', NULL, 1),
(5, 4, 11, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373626/jwcgaozxeimxnilsf6mn.jpg', 'jwcgaozxeimxnilsf6mn', 'Brother Jo', 0),
(6, 4, 11, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373692/ky2fsb5nvtkwumloi6vo.jpg', 'ky2fsb5nvtkwumloi6vo', 'Brother Jo', 0),
(7, 5, 12, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450380492/hmvog6t4ooc0pibeu7gx.jpg', 'hmvog6t4ooc0pibeu7gx', 'Lines and vines ', 0),
(11, 8, 15, '2015-12-20', 'https://www.youtube.com/watch?v=CwA1IpOiCHU', 'CwA1IpOiCHU', 'Minions', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
