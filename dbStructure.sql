-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2015 at 05:35 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sanaaconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `user_agent` varchar(120) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`admin_id`, `username`, `password`, `email`, `last_activity`, `user_agent`, `ip_address`, `group_id`, `user_data`) VALUES
(3, 'super', '493bee4ce0e3098cff288c60a47eb1aa', NULL, NULL, NULL, NULL, 66, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `albumId` int(11) NOT NULL,
  `artist_artistId` int(11) NOT NULL,
  `albumName` varchar(45) DEFAULT NULL,
  `a_dateCreated` date DEFAULT NULL,
  `albumCover` text,
  `viewCount` int(11) DEFAULT '0',
  `a_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`albumId`, `artist_artistId`, `albumName`, `a_dateCreated`, `albumCover`, `viewCount`, `a_description`) VALUES
(1, 10, 'Test Album II', '2015-12-15', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450206164/p0mhhqd320cn2rszyrjk.jpg', 3, 'Test album Descript'),
(2, 10, 'My new album', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450338099/cddwbm7c6izwitpbc3fp.jpg', 1, 'Test album'),
(3, 10, 'Anti paper', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450361960/tmijjv1afmencn3va1bt.jpg', 1, 'My test album'),
(4, 11, 'Xmass season', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373559/pzgnfq0fshl78t8jdrul.jpg', 0, 'Xmass and my people'),
(5, 12, 'Xmass season', '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450380390/p4hp9odgb9bnmcqvfujo.jpg', 0, 'Xmass and my people'),
(6, 14, 'Michano', '2015-12-19', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450540592/htvz0wcch2w61fmwfjdl.jpg', 9, 'Mikaliz'),
(7, 15, 'Xmass season', '2015-12-20', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450601260/amjqneigtgnobz3p4t2y.jpg', 16, 'Xmass season clips'),
(8, 15, 'xmas season', '2015-12-20', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450602820/wbp9ifi12ws8ib3q73gy.jpg', 21, 'xmas and amazing people');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `artistId` int(11) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `artistName` varchar(100) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `age` varchar(45) DEFAULT NULL,
  `description` text,
  `dateCreated` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `password` varchar(100) DEFAULT NULL,
  `coverPhoto` text,
  `linkedin` varchar(100) DEFAULT NULL,
  `identifier` varchar(45) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `profileURL` text,
  `webSiteURL` text,
  `displayName` varchar(45) DEFAULT NULL,
  `birthDay` char(10) DEFAULT NULL,
  `birthMonth` char(10) DEFAULT NULL,
  `birthYear` char(10) DEFAULT NULL,
  `emailVerified` char(10) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `photoURL` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artistId`, `firstName`, `language`, `lastName`, `artistName`, `email`, `instagram`, `phone`, `age`, `description`, `dateCreated`, `active`, `password`, `coverPhoto`, `linkedin`, `identifier`, `gender`, `address`, `city`, `profileURL`, `webSiteURL`, `displayName`, `birthDay`, `birthMonth`, `birthYear`, `emailVerified`, `zip`, `country`, `region`, `photoURL`) VALUES
(4, '', NULL, '', NULL, 'luvandazz@gmail.com', NULL, '+255 756 444 333', NULL, 'Lorem ipsum dolar min.', NULL, 1, 'e012a61c6d1a4f875a3ca17df394be98', NULL, NULL, '10201392248126047', '', NULL, NULL, '', '', 'Cody Junior', NULL, NULL, NULL, '', NULL, NULL, '', 'https://graph.facebook.com/10201392248126047/picture?width=150&height=150'),
(5, NULL, NULL, NULL, NULL, 'test@test.com', NULL, 'fsdfs', NULL, 'fghfghfg', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', 'assets/images/artists/profile/coverphoto.jpg', NULL, '431116a1421a2f9535b8c0df88a0a3d8', NULL, NULL, NULL, NULL, NULL, 'Diamond Platinumz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost/sanaaconnect/assets/images/artists/featured/diamond.jpg'),
(7, NULL, NULL, NULL, NULL, 'photo@photo.com', NULL, '3838383838', NULL, 'Passionate photo grapher', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '834fc1e23d5c2d640bda6f6f64c37414', NULL, NULL, NULL, NULL, NULL, 'Damian Solo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://localhost/sanaaconnect/assets/images/artists/featured/damian.jpg'),
(8, NULL, NULL, NULL, NULL, 'sdfsdfsd@fdfdssd.krhfg', NULL, 'ssdf', NULL, 'dfsdfsdfsd', NULL, 1, 'dcd989387b401ac29bf44755f31c0952', NULL, NULL, '4d5eb61378ab06359b38ed52417a8302', NULL, NULL, NULL, NULL, NULL, 'sfsdfs', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, 'test@test.com3', NULL, 'sdfsdfsdf', NULL, 'sfsdfsfsdfsd', NULL, 1, '84d9cfc2f395ce883a41d7ffc1bbcf4e', NULL, NULL, 'b19ec0132dc3d3d4f24996cfca0d2740', NULL, NULL, NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, 'king@kong.com', NULL, '2323232323', NULL, 'King kong for real', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'dd7facc67039113a6c7f9d3c2a7c1252', NULL, NULL, NULL, NULL, NULL, 'kingkong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, 'genic@photo.com', NULL, '43040404', NULL, 'Photo genic mwenyewe', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'ae440ed56ba7e562c163c7bfbde068ef', NULL, NULL, NULL, NULL, NULL, 'photogen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, 'geni@tonic.com', NULL, '4949494949', NULL, 'Photographer with passion to life''s moments', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, 'd8f6d381078e630c02c80faa023265cc', NULL, NULL, NULL, NULL, NULL, 'genitonic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, 'keny@test.com', NULL, '32462346235', NULL, 'Test', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '27799898f477c7bb6cee9c44ad9c6693', NULL, NULL, NULL, NULL, NULL, 'Kenny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, 'niki@dot.com', NULL, '3939393939', NULL, 'fine any how', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '7d81bafe33105d38979d38504f18aa5c', NULL, NULL, NULL, NULL, NULL, 'niki', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://res.cloudinary.com/olcomlabs/image/upload/v1450536276/hbpx6ybgqu3idspglhsq.jpg'),
(15, NULL, NULL, NULL, NULL, 'vide@o.com', NULL, '93939393', NULL, 'Video grapher for real', NULL, 1, '098f6bcd4621d373cade4e832627b4f6', NULL, NULL, '610d4aff5198f5b5222cc2df1bb36c42', NULL, NULL, NULL, NULL, NULL, 'video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://res.cloudinary.com/olcomlabs/image/upload/v1450601213/d3kopbgeidmodavtwf3y.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `artist_category`
--

CREATE TABLE `artist_category` (
  `ac_categoryId` int(11) NOT NULL,
  `ac_artistId` int(11) NOT NULL,
  `ac_dateCreated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_category`
--

INSERT INTO `artist_category` (`ac_categoryId`, `ac_artistId`, `ac_dateCreated`) VALUES
(1, 4, '2015-12-14 00:00:00'),
(1, 7, '2015-12-28 00:00:00'),
(1, 10, '2015-12-14 00:00:00'),
(1, 14, '0000-00-00 00:00:00'),
(3, 15, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `artist_subcategory`
--

CREATE TABLE `artist_subcategory` (
  `artist_subcatId` int(11) NOT NULL,
  `artist_subcatName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_subcategory`
--

INSERT INTO `artist_subcategory` (`artist_subcatId`, `artist_subcatName`) VALUES
(1, 'musician'),
(2, 'craftMan'),
(3, 'video Producer'),
(4, 'actor');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(45) DEFAULT NULL,
  `cat_DateCreated` datetime DEFAULT NULL,
  `cat_dateChanged` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryId`, `categoryName`, `cat_DateCreated`, `cat_dateChanged`) VALUES
(1, 'musician', '2015-12-08 00:00:00', NULL),
(2, 'fine Artist', '2015-12-14 00:00:00', NULL),
(3, 'video grapher', '2015-12-20 11:45:26', '2015-12-20 11:45:26');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contactId` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `active` int(11) DEFAULT NULL,
  `salary` float DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `eventId` int(11) NOT NULL,
  `eventTitle` varchar(255) DEFAULT NULL,
  `eventCategory` varchar(45) DEFAULT NULL,
  `eventLocation` varchar(255) DEFAULT NULL,
  `eventDescriptions` text,
  `eventDate` date DEFAULT NULL,
  `eventPostedBy` varchar(255) DEFAULT NULL,
  `viewCount` int(11) DEFAULT '0',
  `eventPhotoURL` varchar(255) DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eventId`, `eventTitle`, `eventCategory`, `eventLocation`, `eventDescriptions`, `eventDate`, `eventPostedBy`, `viewCount`, `eventPhotoURL`, `memberId`) VALUES
(1, 'British Dance Edition 2016', NULL, 'Wales Millennium Centre ', 'British Dance Edition is the biennial showcase and initiative of the National Dance Network hosted in different cities across the UK. It is renowned for presenting the best of British dance and is a market place for the buying and selling of dance product.\r\nBritish Dance Edition 2016 takes place in Wales Millennium Centre, set in the heart of Cardiff Bay and the thriving Capital of Wales. The four-day industry focused showcase for British Dance will also be hosted by Coreo Cymru, Chapter, National Dance Company of Wales and Riverfront Theatre, Newport who are looking forward to extending a warm Welsh welcome to UK and international promoters and presenters.\r\nThe 2016 showcase will present over 30 leading UK companies and independent artists and offers a dynamic, diverse and high quality platform that inspires exchanges, debates and new partnerships.\r\nRegistration opens from November 2015.', '2015-12-24', 'Calore Blake', 0, 'assets/images/events/sanaaeventd.jpg', 1),
(2, 'The Nyama Choma Festival – December edition', NULL, 'UDSM SPORTS GROUNDS', '<p>Welcome To Our Meat World.This time around Nyama Choma is on the 6th December, 2014.</p><p>\r\nIt is the one and only event that offers a unique experience every four months in Dar es Salaam. One of a kind BBQ extravaganza showcases different BBQ pitmasters all styling different bbq skills;attend and get served by the best bbq pitmasters from around the city of Dar es Salaam ,enjoy your drink at our beer garden and music from the finest djs in the entertainment industry.</p>\r\n\r\n<p>The Nyama Choma Festival – December edition is here, get your tickets for Tshs 5000 now or Tshs 7000 on 7th of December</p>', '2015-12-06', 'Peter Thomas', 0, 'assets/images/events/nyamachomaevent.jpg', 1),
(3, 'Swahili Fashion Week', NULL, 'SEA CLIFF HOTEL', '<p>The SIXTH Swahili Fashion Week is around the corner. East and Central Africa’s largest Fashion Platform.</p>\r\n\r\n<p>Swahili Fashion Week is now THE platform for fashion and accessory designers from Swahili speaking countries and the African continent to showcase their talent, market their creativity and network with clientele and the international fashion industry. This is all aimed at emphasizing to the region that fashion is an income generating creative industry, meanwhile promoting a “Made in Africa” concept.</p>\r\n\r\n<p>Swahili Fashion Week 2013 will feature more than 50 Designers from East and Central Africa and beyond with the Awards on the last day.\r\nSwahili Fashion Week – 5th to 8th Dec 2013</p>', '2015-12-06', 'John Doe', 0, 'assets/images/events/swahilifashion.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `jobId` int(11) NOT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `job_description` text,
  `job_key_skills` text,
  `job_contact_information` text,
  `job_coverphoto` varchar(255) DEFAULT NULL,
  `published_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`jobId`, `job_title`, `job_description`, `job_key_skills`, `job_contact_information`, `job_coverphoto`, `published_date`) VALUES
(1, 'sdfsffdsfds', '\n\nThe SIXTH Swahili Fashion Week is around the corner. East and Central Africa’s largest Fashion Platform.\n\nSwahili Fashion Week is now THE platform for fashion and accessory designers from Swahili speaking countries and the African continent to showcase their talent, market their creativity and network with clientele and the international fashion industry. This is all aimed at emphasizing to the region that fashion is an income generating creative industry, meanwhile promoting a “Made in Africa” concept.\n\nSwahili Fashion Week 2013 will feature more than 50 Designers from East and Central Africa and beyond with the Awards on the last day. Swahili Fashion Week – 5th to 8th Dec 2013\n', 'xcvbxcvgxhfjfgjfggjyyuiuiuku', 'gukghkjhjkhj', 'assets/images/jobs/jobcover.jpg', '2015-12-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `memberId` int(11) NOT NULL,
  `memberName` varchar(255) DEFAULT NULL,
  `memberEmail` varchar(255) DEFAULT NULL,
  `memberPassword` varchar(255) DEFAULT NULL,
  `memberPhoneNumber` varchar(255) DEFAULT NULL,
  `memberProfilePhoto` varchar(255) DEFAULT NULL,
  `memberIsApproved` int(11) DEFAULT NULL,
  `memberLocation` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=big5;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberId`, `memberName`, `memberEmail`, `memberPassword`, `memberPhoneNumber`, `memberProfilePhoto`, `memberIsApproved`, `memberLocation`) VALUES
(1, 'Peter Thomas', 'peter88tom@gmail.com', 'interface', '34635747567', NULL, 1, 'Kigamboni');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_name`) VALUES
(16, 'personnel'),
(18, 'humanresources'),
(19, 'usermanagement'),
(21, 'news'),
(22, 'organizations'),
(23, 'events'),
(24, 'artists');

-- --------------------------------------------------------

--
-- Table structure for table `module_permissions`
--

CREATE TABLE `module_permissions` (
  `permission_id` int(11) NOT NULL,
  `read` tinyint(1) DEFAULT '0',
  `write` tinyint(1) DEFAULT '0',
  `create` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  `module_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `newsId` int(11) NOT NULL,
  `newsTitle` varchar(255) DEFAULT NULL,
  `newsDescriptions` text,
  `newsPublishDate` date DEFAULT NULL,
  `publishedBy` varchar(45) DEFAULT NULL,
  `newsCoverPhoto` varchar(255) DEFAULT NULL,
  `personnel_id` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`newsId`, `newsTitle`, `newsDescriptions`, `newsPublishDate`, `publishedBy`, `newsCoverPhoto`, `personnel_id`, `published`) VALUES
(6, 'Xmas wishes from Jo', '<p>&nbsp;</p>\n<p class="p1"><span class="s1">Xmas wishes from Jo</span></p>', '2015-12-22', 'Sanaa Connect Tz', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450758191/nlri6rpwxkn3fk3xsaob.jpg', 1, 1),
(7, 'Xmas wishes from Jo', '<p>&nbsp;</p>\n<p class="p1"><span class="s1">Xmas wishes from Jo</span></p>', '2015-12-22', 'Sanaa Connect Tz', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450758244/hjtcdo4nqwvmtq0suzkj.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `organizationId` int(11) NOT NULL,
  `organizationName` varchar(45) DEFAULT NULL,
  `o_dateCreated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personnel`
--

CREATE TABLE `personnel` (
  `personnel_id` int(11) NOT NULL,
  `firstname` varchar(30) DEFAULT NULL,
  `middlename` varchar(30) DEFAULT NULL,
  `lastname` varchar(30) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `dob` varchar(12) DEFAULT NULL,
  `marital_status` varchar(15) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  `nok_phone` varchar(12) DEFAULT NULL,
  `phone` varchar(12) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `citizenship` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personnel`
--

INSERT INTO `personnel` (`personnel_id`, `firstname`, `middlename`, `lastname`, `gender`, `dob`, `marital_status`, `address`, `nok_phone`, `phone`, `region`, `citizenship`, `age`, `registration_date`) VALUES
(1, 'Sanaa', '', 'Connect', 'Male', '2015-12-21', 'Single', 'dar', '3333 333 333', '3444 444 443', 'Arusha', 'tanzanian', NULL, '2015-12-21 09:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `slideId` int(11) NOT NULL,
  `slideshowTitle` varchar(255) DEFAULT NULL,
  `slideshowImageURL` varchar(255) DEFAULT NULL,
  `slideshowDescriptions` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soundcloud_profile`
--

CREATE TABLE `soundcloud_profile` (
  `sc_profileId` varchar(100) NOT NULL,
  `sc_name` varchar(45) DEFAULT NULL,
  `sc_avatar` text,
  `sc_accessToken` text,
  `sc_username` varchar(45) DEFAULT NULL,
  `a_artistId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soundcloud_profile`
--

INSERT INTO `soundcloud_profile` (`sc_profileId`, `sc_name`, `sc_avatar`, `sc_accessToken`, `sc_username`, `a_artistId`) VALUES
('190243713', 'Peter Thomas', 'https://i1.sndcdn.com/avatars-000191082288-xmx26t-large.jpg', '1-167353-190243713-b2ec2b9c8486e', 'Peter Thomas', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sub_modules`
--

CREATE TABLE `sub_modules` (
  `sub_module_id` int(11) NOT NULL,
  `sub_module_name` varchar(45) DEFAULT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_modules`
--

INSERT INTO `sub_modules` (`sub_module_id`, `sub_module_name`, `module_id`) VALUES
(57, 'modules', 0),
(59, 'sub_modules', 0),
(60, 'view_personnel', 0),
(61, 'groups', 0),
(62, 'users', 0),
(63, 'employees', 0),
(64, 'departments', 0),
(65, 'modules', 19),
(66, 'sub_modules', 19),
(67, 'users', 19),
(68, 'groups', 19),
(69, 'employees', 18),
(70, 'view_personnel', 16),
(71, 'published', 21),
(72, 'create_article', 21);

-- --------------------------------------------------------

--
-- Table structure for table `sub_module_permissions`
--

CREATE TABLE `sub_module_permissions` (
  `read` tinyint(1) DEFAULT NULL,
  `create` tinyint(1) DEFAULT NULL,
  `write` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  `sub_permission_id` int(11) NOT NULL,
  `sub_module_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supporter`
--

CREATE TABLE `supporter` (
  `supporterId` int(11) NOT NULL,
  `supporterName` varchar(255) DEFAULT NULL,
  `supporterURL` varchar(255) DEFAULT NULL,
  `supporterLogo` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supporter`
--

INSERT INTO `supporter` (`supporterId`, `supporterName`, `supporterURL`, `supporterLogo`, `published`) VALUES
(1, 'European Union', 'http://europa.eu/index_en.htm', 'assets/images/supporters/1.png', 1),
(2, 'British Council Tanzania', 'https://www.britishcouncil.or.tz/', 'assets/images/supporters/2.png', 1),
(3, 'ALLIANCE FRANCAISE - ARUSHA', 'http://www.aftarusha.org/', 'assets/images/supporters/3.png', 1),
(4, 'Nafasi Art Space', 'http://nafasiartspace.org/s/', 'assets/images/supporters/4.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `user_agent` varchar(120) DEFAULT NULL,
  `user_data` text,
  `group_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `welcome`
--

CREATE TABLE `welcome` (
  `welcomeId` int(11) NOT NULL,
  `welcome_title` varchar(255) DEFAULT NULL,
  `welcome_descriptions` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `welcome`
--

INSERT INTO `welcome` (`welcomeId`, `welcome_title`, `welcome_descriptions`) VALUES
(1, 'Welcome to Sanaa Connect.', 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.');

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `workId` int(11) NOT NULL,
  `a_albumId` int(11) NOT NULL,
  `aa_artistId` int(11) NOT NULL,
  `w_dateCreated` date DEFAULT NULL,
  `w_location` text,
  `cdnId` text,
  `w_description` text,
  `w_viewCount` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`workId`, `a_albumId`, `aa_artistId`, `w_dateCreated`, `w_location`, `cdnId`, `w_description`, `w_viewCount`) VALUES
(2, 1, 10, '2015-12-15', 'http://soundcloud.com/peter-thomas-432181533/test-music', '237703300', NULL, 7),
(3, 1, 10, '2015-12-16', 'http://soundcloud.com/peter-thomas-432181533/fsdfhdfgh', '237722948', NULL, 0),
(4, 2, 10, '2015-12-17', 'http://soundcloud.com/peter-thomas-432181533/test-audio', '237943105', NULL, 1),
(5, 4, 11, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373626/jwcgaozxeimxnilsf6mn.jpg', 'jwcgaozxeimxnilsf6mn', 'Brother Jo', 0),
(6, 4, 11, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450373692/ky2fsb5nvtkwumloi6vo.jpg', 'ky2fsb5nvtkwumloi6vo', 'Brother Jo', 0),
(7, 5, 12, '2015-12-17', 'http://res.cloudinary.com/olcomlabs/image/upload/v1450380492/hmvog6t4ooc0pibeu7gx.jpg', 'hmvog6t4ooc0pibeu7gx', 'Lines and vines ', 0),
(11, 8, 15, '2015-12-20', 'https://www.youtube.com/watch?v=CwA1IpOiCHU', 'CwA1IpOiCHU', 'Minions', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`albumId`,`artist_artistId`),
  ADD KEY `fk_works_artist1_idx` (`artist_artistId`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`artistId`);

--
-- Indexes for table `artist_category`
--
ALTER TABLE `artist_category`
  ADD PRIMARY KEY (`ac_categoryId`,`ac_artistId`),
  ADD KEY `fk_category_has_artist_artist1_idx` (`ac_artistId`),
  ADD KEY `fk_category_has_artist_category_idx` (`ac_categoryId`);

--
-- Indexes for table `artist_subcategory`
--
ALTER TABLE `artist_subcategory`
  ADD PRIMARY KEY (`artist_subcatId`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contactId`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`,`department_id`,`job_id`),
  ADD KEY `fk_employees_personnel1_idx` (`employee_id`),
  ADD KEY `fk_employees_departments1_idx1` (`department_id`),
  ADD KEY `fk_employees_jobs1_idx1` (`job_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`eventId`),
  ADD KEY `fk_events_1_idx` (`memberId`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`jobId`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`memberId`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_permissions`
--
ALTER TABLE `module_permissions`
  ADD PRIMARY KEY (`permission_id`,`module_id`,`group_id`),
  ADD KEY `fk_module_permissions_modules1_idx` (`module_id`),
  ADD KEY `fk_module_permissions_groups1_idx` (`group_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`newsId`),
  ADD KEY `fk_news_personnel1_idx` (`personnel_id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`organizationId`);

--
-- Indexes for table `personnel`
--
ALTER TABLE `personnel`
  ADD PRIMARY KEY (`personnel_id`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`slideId`);

--
-- Indexes for table `soundcloud_profile`
--
ALTER TABLE `soundcloud_profile`
  ADD PRIMARY KEY (`sc_profileId`,`a_artistId`),
  ADD KEY `fk_soundcloud_profile_artist1_idx` (`a_artistId`);

--
-- Indexes for table `sub_modules`
--
ALTER TABLE `sub_modules`
  ADD PRIMARY KEY (`sub_module_id`,`module_id`),
  ADD KEY `fk_sub_modules_modules1_idx1` (`module_id`);

--
-- Indexes for table `sub_module_permissions`
--
ALTER TABLE `sub_module_permissions`
  ADD PRIMARY KEY (`sub_permission_id`,`sub_module_id`,`group_id`),
  ADD KEY `fk_sub_module_permissions_sub_modules1_idx1` (`sub_module_id`),
  ADD KEY `fk_sub_module_permissions_groups1_idx1` (`group_id`);

--
-- Indexes for table `supporter`
--
ALTER TABLE `supporter`
  ADD PRIMARY KEY (`supporterId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`group_id`,`employee_id`),
  ADD KEY `fk_users_groups1_idx1` (`group_id`),
  ADD KEY `fk_users_employees1_idx` (`employee_id`);

--
-- Indexes for table `welcome`
--
ALTER TABLE `welcome`
  ADD PRIMARY KEY (`welcomeId`);

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`workId`,`a_albumId`,`aa_artistId`),
  ADD KEY `fk_work_album1_idx` (`a_albumId`,`aa_artistId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `albumId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `artistId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `artist_subcategory`
--
ALTER TABLE `artist_subcategory`
  MODIFY `artist_subcatId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contactId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `eventId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `jobId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `memberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `module_permissions`
--
ALTER TABLE `module_permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `newsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `organizationId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `personnel`
--
ALTER TABLE `personnel`
  MODIFY `personnel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `slideId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sub_modules`
--
ALTER TABLE `sub_modules`
  MODIFY `sub_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `sub_module_permissions`
--
ALTER TABLE `sub_module_permissions`
  MODIFY `sub_permission_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supporter`
--
ALTER TABLE `supporter`
  MODIFY `supporterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `welcome`
--
ALTER TABLE `welcome`
  MODIFY `welcomeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `workId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `fk_works_artist1` FOREIGN KEY (`artist_artistId`) REFERENCES `artist` (`artistId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `artist_category`
--
ALTER TABLE `artist_category`
  ADD CONSTRAINT `fk_category_has_artist_artist1` FOREIGN KEY (`ac_artistId`) REFERENCES `artist` (`artistId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_category_has_artist_category` FOREIGN KEY (`ac_categoryId`) REFERENCES `category` (`categoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `fk_employees_departments1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employees_jobs1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`jobId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employees_personnel1` FOREIGN KEY (`employee_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `fk_events_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`memberId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `module_permissions`
--
ALTER TABLE `module_permissions`
  ADD CONSTRAINT `fk_module_permissions_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_module_permissions_modules1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `fk_news_personnel1` FOREIGN KEY (`personnel_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `soundcloud_profile`
--
ALTER TABLE `soundcloud_profile`
  ADD CONSTRAINT `fk_soundcloud_profile_artist1` FOREIGN KEY (`a_artistId`) REFERENCES `artist` (`artistId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_modules`
--
ALTER TABLE `sub_modules`
  ADD CONSTRAINT `fk_sub_modules_modules1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_module_permissions`
--
ALTER TABLE `sub_module_permissions`
  ADD CONSTRAINT `fk_sub_module_permissions_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sub_module_permissions_sub_modules1` FOREIGN KEY (`sub_module_id`) REFERENCES `sub_modules` (`sub_module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_employees1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `fk_work_album1` FOREIGN KEY (`a_albumId`,`aa_artistId`) REFERENCES `album` (`albumId`, `artist_artistId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
