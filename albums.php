<div class="row"> <!-- start row for use profile -->
                 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="cover-photo" style="background:url('<?php echo base_url();?>assets/images/artists/profile/fashion.jpg');
                            background-color: #435e9c;
                            background-repeat: no-repeat;
                            background-position: center;
                            background-size: cover;
                            color:white; 
                            height:315px; "> 
                                <img src="<?php 
                                if( isset($artist_profile['photoURL'] )AND $artist_profile[ 'photoURL'] != NULL){
                                  echo  $artist_profile['photoURL']; 
                                }
                                else{
                                    echo base_url().'assets/images/empty_profile.gif';                                  
                                }
                                ?>" class="profile-photo img-thumbnail show-in-modal"> 
                              <div class="cover-name"><?php echo $artist_profile['displayName']; ?></div> 
                                
                                
                        </div>

                </div>

             <div class="main-menu2">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                        <div class="panel-options">
                              <div class="navbar navbar-default navbar-cover"> 
                                   <div class="container-fluid"> 
                                         <div class="navbar-header">
                                               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#profile-opts-navbar" aria-expanded="true">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                     <span class="icon-bar"></span>
                                                </button> 
                                            </div>

                                             <div class="navbar-collapse collapse in" id="profile-opts-navbar" aria-expanded="true">
                                                    <ul class="nav navbar-nav navbar-right"> 
                                                          <li ><a href="<?php echo site_url('artists/myProfile')?>"><i class="fa fa-tasks"></i> About</a></li>
                                                          <li><a class="active" href="<?php echo site_url('artists/works')?>"><i class="fa fa-info-circle"></i>Works</a></li>
                                                          <li><a href="<?php echo site_url('artists/events')?>"><i class="fa fa-tags"></i> Events</a></li>
                                                          <li><a href="<?php echo site_url('artists/news')?>"><i class="fa fa-tags"></i> News</a></li>
                                                           <li><a href="<?php echo site_url('artist/connections')?>"><i class="fa fa-share-alt"></i> Connection</a></li>
                                                    </ul> 
                                             </div>
                                        </div>
                                  </div>
                           </div>
                      </div>

            </div>
              </div><!-- end row for user profile -->  
        </div>
<section id="blog" class="blog-area section">
        <div class="auto-container">
             <div class="sec-title wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInLeft;">
                <h2>My <span> Albums</span></h2>
            </div>
            <div class="row">
                    <div class="service-tabs style-two three-column">
                
                <!--Tab Buttons-->
                <ul class="tab-btns clearfix wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 0ms; animation-name: fadeInUp;">
                   <li class="tab-btn hvr-bounce-to-left"  >
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                        <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/add_album'); ?>'><strong>Add Album</strong></a>
                    
                    </li>
                    <!--Active Btn-->
                    <li class="tab-btn hvr-bounce-to-left" >
                        <!--<div class="icon"><span class="flaticon-list100"></span></div>-->
                       <a class = 'button' style = 'color : #444;' href = '<?php echo site_url('artists/albums/').'/'.$artist_profile['identifier']; ?>'><strong>Albums</strong></a> 
                    
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#renovation-tab">
                       <!-- <div class="icon"><span class="flaticon-hammer55"></span></div>-->
                        <strong>Most Viewed </strong>
               
                    </li>
                    
                    <li class="tab-btn hvr-bounce-to-left" data-id="#construction-tab">
                        <!--<div class="icon"><span class="flaticon-barrow"></span></div> -->
                        <strong>Search</strong>
                      
                    </li>
                    
                   
                </ul>
                
                <!--Tabs Content-->
                <div class="tab-content wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="renovation-tab" style="display: none;">
                        <!--Posts Container-->
                        <div class="posts-container clearfix">
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>FAMILY PHYSICIAN</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                        </div><!--Posts Container End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="construction-tab" style="display: none;">
                        <!--Posts Container-->
                        <div class="posts-container clearfix">
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>GASTROENTEROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                        </div><!--Posts Container End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix active-tab" id="isolation-tab" style="display: block;">
                        <!--Posts Container-->
                        <div class="posts-container clearfix">
                            
                            <?php 


                            if( isset($contents)){

                                foreach( $contents as $album){ 
                                ?>
                                <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo isset($album['albumCover']) ? $album['albumCover'] :base_url().'assets/images/resource/post-image-1.jpg';?>" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3><?php echo strtoupper( $album['albumName']); ?></h3>
                                            <div class="text"><?php echo $album['a_description']; ?></div>
                                            <div class="link"><a href="<?php echo site_url('artists/view_album/').'/'.$album['albumId'].'/'.$artist_profile['identifier']; ?>" class="read_more">View</a>
                                            <?php 
                                                $ci  = &get_instance();
                                                $authenticated = $ci->session->userdata('authenticated');
                                                if(isset($authenticated)){
                                            ?>
                                            | <a href="#" class="read_more">Edit</a> | <a href="#" class="read_more">Delete</a></div>
                                            <?php }else
                                                echo '</div>';
                                             ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            <?php
                                }
                            }
                            ?>
                        
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>INTERNIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                           
                           
                            
                        </div><!--Posts Container End-->
                        
                    </div><!--Tab End-->
                    
                    <!--Tab-->
                    <div class="tab clearfix" id="sanitary-tab" style="display: none;">
                        <!--Posts Container-->
                        <div class="posts-container clearfix">
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-1.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-2.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                            <!--Post-->
                            <article class="col-md-4 col-sm-6 col-xs-12 featured-box">
                                <div class="box-inner">
                        
                                    <figure class="image">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets/images/resource/post-image-3.jpg" alt="">
                                        <span class="curve"></span>
                                    </figure>
                                    <div class="content">
                                        <div class="inner-box">
                                            <h3>NEUROLOGIST</h3>
                                            <div class="text">There are many variations of passages of Lorem Ipsum available . . . . . .</div>
                                            <div class="link"><a href="#" class="read_more">Read More</a></div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </article><!--Post End-->
                            
                        </div><!--Posts Container End-->
                        
                    </div><!--Tab End-->
                    
                </div>
                
            </div>
            </div>
        </div>
</section>